AgorAsso
==========

A Symfony project created on February 2, 2018, 4:47 pm. It's license under a GNU AFFERO GENERAL PUBLIC LICENSE (Version 3). Repository is on https://gitlab.com/citoyens.grandchambery/agorasso. The first use was for https://chambecitoyenne.fr/, a citizen movement based in Chambéry for the #Municipales2020.

# Description
## French
AgorAsso a été conçu pour permettre un travail partagé dans une organisation horizontale, les premiers outils sont :
* Gestion d'événement
* Gestion de documents
* Gestion de groupe

Des fonctionnalités supplémentaires ont été ajoutées :
* Outil de vote
* Gestion de formulaire
* Outil de gestion d'inscription (candidatures externes)

## English
This project lets you manage events, documents and groups inside an horizontal organisation such as an association or a politics group.

Other features :
* vote tool
* subscription management (for extern candidates)
* form tool

# LICENSE
GNU AFFERO GENERAL PUBLIC LICENSE (Version 3) see LICENSE

# Installation 
See file INSTALL.md

