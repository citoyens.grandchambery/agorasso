Install process
===============

# Copy sources
git clone https://gitlab.com/soulet.informatique/agorasso.git
cd agorasso

# configuration
cp ./app/config/parameters.yml.dist ./app/config/parameters.yml
create database

# Initialize database and components
php bin/console do:sc:up --force
composer update

# Create and rights on specific folders
mkdir uploads
sudo chmod -R 777 uploads/
cd web
mkdir media
sudo chmod -R 777 media
cd ..
sudo chmod -R 777 var

# Install assets
php bin/console ckeditor:install
php bin/console assets:install --symlink
