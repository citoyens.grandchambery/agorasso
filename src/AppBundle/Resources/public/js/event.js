$(document).ready(function() {

    /**
     * Focntion qui dans la page événements permet d'afficher dynamiquement le résumé de l'événement.
     */
    $('#eventsList.timeline-panel').on('click', function(){
        eventUrl = $( this ).data("link");
        if($('#eventContainer').length > 0) {
            $('#eventContainer').load(eventUrl);
        } else {
            document.location = eventUrl;
        }
    });

    $('#events.citizenPanelClick').on('click', function(){
        eventUrl = $(this).data("link");
        if (eventUrl !== undefined){
           document.location = eventUrl;
        }

    });
});