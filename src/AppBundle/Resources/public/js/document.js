
/*** CONFIRM MODAL OVERRIDE ***/
//override the use of js alert on confirms
//requires bootstrap3-dialog from https://github.com/nakupanda/bootstrap3-dialog
$.rails.allowAction = function (link) {
    if (!link.is('[data-confirm]'))
        return true;

    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_DANGER,
        title: 'Confirmation',
        message: link.attr('data-confirm'),
        buttons: [{
            label: 'Confirmer',
            cssClass: 'btn-primary',
            action: function (dialogRef) {
                link.removeAttr('data-confirm');
                link.trigger('click.rails');
                dialogRef.close();
            }
        }, {
            label: 'Annuler',
            action: function (dialogRef) {
                dialogRef.close();
            }
        }]
    });
    return false; // always stops the action since code runs asynchronously
};
