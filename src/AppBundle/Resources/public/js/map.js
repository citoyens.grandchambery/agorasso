var coordinates = $('#mapid').data('coordinates');
var coord = undefined;
if(coordinates != undefined && coordinates != "") {
    coord = coordinates.split(',');
} else {
 coord = [45.6319,6.04];
}

var mapboxToken = $('#mapid').data('mapboxtoken');
var jsonFile =  $('#mapid').data('json');
var townDataRequest =  $('#mapid').data('towns');
var townData = undefined;
var zoom =  $('#mapid').data('zoom');

if(zoom == undefined) {
    zoom = 11;
}

var mymap = L.map('mapid').setView([coord[0], coord[1]], zoom);
// add back map
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 17,
    id: 'mapbox.streets',
    accessToken: mapboxToken
}).addTo(mymap);

const myHeaders = new Headers({
    "Content-Type": "application/json",
    Accept: "application/json"
});

var geojson;
var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

function getTown(insee_code)
{
    var result = townData.filter(town => town.insee_code == insee_code);

    if(result.length > 0) {
        return result[0];
    } else {
        return undefined;
    }
}

function getTownData(town)
{
    if(town == undefined) {
        return '';
    }
    var res = '';
    if(town.area != undefined) {
        res += 'Superficie :' + town.area + ' km<sup>2</sup><br>';
    }
    if(town.population != undefined) {
        res += 'Population :' + town.population + ' hab. (' + town.population_year + ')' + '<br>';
    }
    if(town.gentile != undefined) {
        res += 'Gentilé :' + town.gentile + '<br>';
    }
    res += 'Citoyen-nes inscrit-es : ' + town.users.length + '<br>';
    return res;
}

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    var town = undefined;
    if(props != undefined) {
        town = getTown(props.insee);
    }
    this._div.innerHTML = '<h4>Commune</h4>' + (props ?
        '<b>' + props.nom + '</b><br>'
        : 'Survoler une commune pour afficher ses informations') + getTownData(town);
};

info.addTo(mymap);

const color1 = '#df094f';
const color2 = '#f39f0b';
const color3 = '#048396';
const color4 = '#85b61e';

function getOneColorLegend(color)
{
    return '<span style="opacity:0.7;background-color: ' + color + ';color:'+color+'">A</span>';
}

function getColorLegend()
{
    var html = '';
    html += getOneColorLegend(color1);
    html += getOneColorLegend(color2);
    html += getOneColorLegend(color3);
    html += getOneColorLegend(color4);
    html += " Communes (couleurs tirées au sort)";
    return html;
}

function getColor(d) {
   var color = Math.floor(Math.random() * Math.floor(4));

   switch(color) {
       default:
       case 0: return color1;
       case 1: return color2;
       case 2: return color3;
       case 3: return color4;
   }
}

function style(feature) {
    return {
        fillColor: getColor(feature.properties.insee),
        weight: 2,
        opacity: 1,
        color: '#777',
        // dashArray: '3',
        fillOpacity: 0.3
    };
}

function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }

    // info
    info.update(layer.feature.properties);
}

function resetHighlight(e) {
    geojson.resetStyle(e.target);

    // info
    info.update();
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

var legendDiv = undefined;
function getLegendDiv()
{
    if(legendDiv == undefined) {
        legendDiv = L.DomUtil.create('div', 'info legend');
    }
    return legendDiv;
}

fetch(jsonFile, {
    headers: myHeaders,

})
    .then(response => {
    return response.json()
})
.then(data => {
    geojson = L.geoJSON(data, {
        style: style,
            onEachFeature: onEachFeature
    }
    ).addTo(mymap);
    var div = getLegendDiv();

    var tmp = div.innerHTML;
    div.innerHTML = "<h4>Carte de " + data.name +"</h4>" + getColorLegend() + tmp;
})
.catch(err => {
    // Do something for an error here
})

var legend = L.control({position: 'bottomleft'});

fetch(townDataRequest, {
    headers: myHeaders,

})
    .then(response => {
        return response.json()
    })
    .then(data => {
        townData = data;
        legend.addTo(mymap);
    })
    .catch(err => {
        // Do something for an error here
    })



legend.onAdd = function (map) {

    var div = getLegendDiv();

    var population = 0;
    var area = 0;
    var users = 0;
    townData.forEach(function(town) {
        if(town.population != undefined) {
            population += town.population;
        }
        if(town.users != undefined) {
            users += town.users.length;
        }
    });
    div.innerHTML += "<br>Population totale : " + population + ' hab.';
    div.innerHTML += "<br>Citoyen-nes inscrit-es sur ce site : " + users;

    return div;
};

/*
var myIcon = L.divIcon({className: 'my-div-icon', html: '250'});

L.marker([45.584723,5.904571], {icon: myIcon}).addTo(mymap);
*/