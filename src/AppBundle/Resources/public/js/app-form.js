// Une partie du contenu du fichier ajax.js a été intégrée à ce fichier, sous forme
// d'une fonction initializeAjaxForm(). Cela permet d'appeler
// les focntions initializeAllCollections() et initializeToggleFields() depuis le success de
// la requête ajax, qui est utilisé en cas d'erreur de formulaire et permet de récupérer et
// d'afficher le formulaire avec les erreurs.
function showModal(url)
{
    var formModal = $('#bootstrap-modal');
    var $overlay = $('#overlay').show();
    console.log(location);

    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(resp){
            formModal.find('.modal-content').html(resp);
            formModal.modal('show');
            //initialisation formulaires ajax
            initializeAjaxForm();
            initializeDatepicker();
        },
        complete: function() {
            $overlay.hide();
        }
    });
}
$(document).ready(function() {
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};

    // On fait apparaître le formulaire en AJAX dans la modal et on l'initialise
    $('body').on('click', '.modal-form', function (e) {
        var id = $(this).attr('id');
        var url = '';


        sessionStorage.setItem('elementId', $(this).attr('elementId')); // on mémorise l'élément qu'on va modifier pour le réafficher ensuite
        if(id== "userSearch") {
            // var email = $(this).parent().parent().find("user_search_email").attr('value');
            var email = document.getElementById("user_search_email").value;
            var groupId = $(this).attr('groupId');
            if(!email)
                return;
            url = Routing.generate('user_search', { group_id: groupId, email: email, step: 1 });
        } else {
            url = $(this).attr('actionUrl');
        }
        console.log(location);
        e.preventDefault();
        showModal(url);
    });

    // On supprime un fichier à partir d'un formulaire, et on cache ensuite les liens voir et supprimer
    $('body').on('click', '.delete-file', function (e) {
        if (confirm('Effacer le fichier ?')) {
            var $linksBlock = $(this).parent();
            var url = $(this).attr('actionUrl');
            var $overlay = $('#overlay').show();
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: url,
                cache: false,
                success: function(){
                    $linksBlock.remove();
                },
                complete: function() {
                   $overlay.hide();
                }
            });
        }
    });
    // On supprime le fichier sélectionné de l'input file quand on clique sur la croix
    $('body').on('click', '.unselect-file', function (e) {
        e.preventDefault();
        $(this).parent().parent().find('input').val(null);
    });

    function addItem($container, index) {
        var $prototype = $($container.attr('data-prototype').replace(/__name__/g, index));
        addDeleteLink($prototype);
        $container.append($prototype);
        index++;
        initializeDatepicker();
        initializeAutocomplete();
        initializeFieldsSelect2();
        //$('html, body').animate({scrollTop: $(document).scrollTop() + 80 });
        return index;
    }

    // La fonction qui ajoute un lien de suppression d'une catégorie !!! Ajouter la classe delete-line dans le div de première ligne
    function addDeleteLink($prototype) {
    // Création du lien
        $deleteLink = $('<div class=" col-md-2 col-sm-2 col-xs-4 text-right"><a href="#" class="btn btn-primary"><i class="fa fa-times"></i> Supprimer</a></div>');
        $prototype.children('#delete-line').append($deleteLink);
        $deleteLink.click(function(e) {
            $prototype.remove();
            $('#member_familySituation_childrenNb').val($('div#member_children').find('.sub-form').length);  // Spécial pour chilren: on actualise le nb d'enfants
            e.preventDefault(); // évite qu'un # apparaisse dans l'URL
            return false;
        });
    }
});

// Initialisation formulaire Ajax
function initializeAjaxForm(){
    $(':submit', '[data-ajax-form]').data('loading-text', 'Chargement...');
    $( "body" ).off( "submit", "[data-ajax-form]" ); // Pour éviter l'envoi de requêtes multiples si le bouton modal-form a été cliqué plusieurs fois (par ex. ouvrir le formulaire, le fermer avec annuler, le réouvrir)
    $('body').on('submit', '[data-ajax-form]', function(e) {
        e.preventDefault();
        $(':submit', this).button('loading'); // Changement texte bouton pendant chargement
        var $form = $(this);
        var formData = new FormData( this );

        if ($('#page-to-reload').length > 0) {
            formData.append('pageToReload', $('#page-to-reload').data('page-nb')); // Utilisé si on doit recharger en ajax un élément paginé après fermeture de la modal de formulaire
        };

        if (document.activeElement.getAttribute('type') == 'submit') { // Si on tape Enter quand on est dans un input, le form est submitted et l'élément actif est l'input => Pb.
            formData.append(document.activeElement.getAttribute('name'), ''); // On ajoute le nom du bouton cliqué pour les form avec plusieurs submits, pour pouvoir utiliser isCliked() dans un controller
        }

        $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) { // exécuté quand le formulaire revient avec uen erreur
                var target = $form.data('ajax-form-target');
                $(target).html(response);
                $(':submit', $form).button('reset');
                // initializeAllCollections();
                initializeFieldsSelect2();
                initializeToggleFields();
                initializeDatepicker();
                initializeAutocomplete();
                $('#bootstrap-modal').scrollTop(0); // pour voir les erreurs de formulaire sur un long formulaire
            },
            error: function() {
                $(':submit', $form).button('reset');
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_WARNING,
                    title: "Erreur",
                    message: "Un problème est survenu. Il est possible que vos données n'aient pas été enregistrées.",
                    buttons: [{
                        label: 'Fermer',
                        action: function(dialogRef){
                            dialogRef.close();
                        }
                    }]
                });
            }
        });
    });
}

function initializeAutocomplete() {
    var $allCityFields = $('[id ^=member_addresses_][id $=_town]');
    $allCityFields.off('focus');
    $allCityFields.off('keyup');
    for (i = 0; i < $allCityFields.length; i++) {

        // Créer une nouvelle variable à chaque tour de boucle qui contienne l'id du champ ne fonctionne pas en JS.
        // En passant la valeur comme variable à une fonction anonyme on force la création d'un nouveau scope
        (function(zipFieldId) {
            $('#member_addresses_' + i + '_town').focus(function(){
                if ($(this).val()==''){
                    searchTowns($(zipFieldId).val());// Si on utilise la variable 'i' dans cette closure ça marche pas, on utilise zipFieldId à la place
                }
            });

            $('#member_addresses_' + i + '_town').keyup(function(){
                if ($(this).val()==''){
                    searchTowns($(zipFieldId).val());
                }
            });
        })('#member_addresses_' + i + '_zipCode');

    }
}

// Initialise datepicker et datepicker
function initializeDatepicker() {
    
    $('input.month').datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        language: "fr",
        autoclose: true,
    });
    
    $('input.date').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        language: "fr", 
        todayBtn: "linked",
        autoclose: true,
    });

    $('input.datetime').datetimepicker({
        format: 'dd/mm/yyyy hh:ii',
        autoclose: true,
        todayBtn: true,
        language: 'fr',
        timezone: 'GMT' 
    });
}

// On scrolle vers l'erreur de formulaire après validation Symfony
function scrollToFormError() {
    if ($('body').find("span").hasClass('help-block')) {
         $('html, body').animate({scrollTop: $($(".help-block")[0]).offset().top - 100 }, 1);
    }
}

function initializeFieldsSelect2() {
    $('select[select2="true"]').select2({
        language: "fr",
        theme: "bootstrap",
    });
}

function initializeReloadFilteredList () {
    $('body').on('submit', '.ajax-filter', function(e) {
        e.preventDefault();
        var panelId = $(this).attr('panelToReload');
        reloadFilteredList(panelId);
    });
}
function initializeChangePagerfantaPage () {
    $('.ajax-pagerfanta').on('click', 'a', function(e) {
        e.preventDefault();
        changePagerfantaPage(e);
    });
}
function initializeReloadSortedTable () {
    $('body').on('click', '.ajax-sorting-column', function(e) {
        e.preventDefault();
        reloadSortedTable(e);
    });
}

function reloadFilteredList(panelId) {
        var $form = $('form[panelToReload="' + panelId + '"]');
        var $overlay = $('#overlay').show();
        var data = $form.serialize();
        
        $.ajax({
            type: "GET",
            url: $form.attr('action'),
            data: data,
            cache: false,
            success: function(resp){
                $('#'+panelId).html($(resp).find('#'+panelId));
            },
            complete: function() {
                initializeClicableRows();
                initializeChangePagerfantaPage();
                initializeReloadSortedTable();
                initializeFieldsSelect2();
                $overlay.hide();

            }
        });
}

function changePagerfantaPage(e) {
    var panelId = $(e.currentTarget).closest('.pagerfanta').attr('panelToReload');
    var $overlay = $('#overlay').show();
    var url = $(e.currentTarget).attr('href');

    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(resp){
            $('#'+panelId).html($(resp).find('#'+panelId));
        },
        complete: function() {
            initializeClicableRows();
            initializeChangePagerfantaPage();
            initializeReloadSortedTable();
            initializeFieldsSelect2();
            $overlay.hide();
        }
    });
}

function reloadSortedTable(e) {
    var panelId = $(e.currentTarget).closest('.table-sorted').attr('data-panel-to-reload');
    var $overlay = $('#overlay').show();
    var url = $(e.currentTarget).attr('data-url');

    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(resp){
            $('#'+panelId).html($(resp).find('#'+panelId));
        },
        complete: function() {
            initializeClicableRows();
            initializeChangePagerfantaPage();
            initializeFieldsSelect2();
            $overlay.hide();
        }
    });
}

function reloadElement(elementId, url) {
    var $overlay = $('#overlay').delay(100).show(0); // On delay sinon le overlay.hide de rails.js cache l'overlay (pour les confirmations de suppression)
    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        success: function(resp){
            $('#'+elementId).html($(resp).find('#'+elementId));
        },
        complete: function() {
            initializeChangePagerfantaPage();
            $overlay.hide();
        }
    });
}

function searchUsers(email) {
    if (!email ||  email == ''){
        return;
    }

    $.ajax({
        url: Routing.generate('members_search_query'),
        dataType: "html",
        data : { 'email': email },
        type: 'GET',
        success: function(data)
        {
            $('#member_list').empty();
            members = (jQuery.parseJSON(data));
            for (i = 0; i < members.length; i++) {
                $('#member_list').append("<option value='" + members[i].email + "'>" + members[i].firstName + " " + members[i].name + " " + members[i].email + "</option>");
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(textStatus, errorThrown);
        }
    });
}

function initializeEmailSearch()
{
    $('#user_search_email').keydown(function(event) {
        if([13, 17, 38, 39, 40].indexOf(event.which) !== -1)
            return;
        $email = event.target;

        searchUsers($($email).val());
    });
}

function visibleObject(isVisible, object) {
    if (isVisible)
        object.parent().show();
    else
        object.parent().hide();

}

function initClosedStatus() {
    var $status = $('#proposal_status');

    if ($status.val() == 'Ouvert') {
        $('#proposal_closedReason').parent().parent().hide();
    }
    else {
        $('#proposal_closedReason').parent().parent().show();
    }
}
