window.onscroll = function () {
    stickNavbar();
    // scrollTop();
};

// Get the header
var navbar = document.getElementById("stickyMenu");
var body = document.getElementById("body");
var btn = document.getElementById("backbutton");

// Get the offset position of the navbar
var sticky = navbar.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function stickNavbar() {

    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky");
        btn.classList.add('backshow');
        // btn.classList.add('backshow');
       //  menu.classList.add("sticky2");
        // body.classList.add("top-padded");

    } else {
        navbar.classList.remove("sticky");
        btn.classList.remove('backshow');
        // menu.classList.remove("sticky2");
        // body.classList.remove("top-padded");
    }
}

function scrollTop() {
    // if (window.scrollTopPos >= 300) {
    //     btn.classList.add('backshow');
    // } else {
    //     // btn.classList.remove('backshow');
    // }
}

$(document).ready(function () {
    $('#bootstrap-modal').on('click', '.closeModal', function (event) {
        event.preventDefault();
        $('#bootstrap-modal').modal('toggle');
    });


    $('[data-toggle="popover"]').popover({html: true, container: 'body'});

    $('body').on('click', function (e) {
        //did not click a popover toggle or popover
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-toggle="popover"]').popover('hide');
        }
    });

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    $('#backbutton').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });

});
