$(document).ready(function(){

    $('#appbundle_subscription_town').change(function(event) {
        $town = event.target;
        // ... retrieve the corresponding form.
        var $form = $(this).closest('form');

        // Simulate form data, but only include the selected town value.
        var data = {};

        data[$($town).attr('name')] = $($town).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type: $form.attr('method'),
            data : data,
            success: function(html) {
                // Replace current position field ...

                $('#appbundle_subscription_district').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('#appbundle_subscription_district')
                );
                $('#appbundle_subscription_city').replaceWith(
                    // ... with the returned one from the AJAX response.
                    $(html).find('#appbundle_subscription_city')
                );
                // Position field now displays the appropriate positions.
                toggletown();
            }
        });

    });

    toggletown();

    function toggletown() {
        $town = $("#appbundle_subscription_town").val();
        var length = $('#appbundle_subscription_district option').length;
        if (length > 1) {
            $('#myDistrict').show();
        } else {;
            $('#myDistrict').hide();
        }

        if ($('#appbundle_subscription_city').attr('required') === undefined) {
            $('#appbundle_subscription_city').parent().parent().hide();
        } else {
            $('#appbundle_subscription_city').parent().parent().show();
        };
    }
});