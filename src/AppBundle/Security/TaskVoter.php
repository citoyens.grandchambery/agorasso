<?php

namespace AppBundle\Security;

use AppBundle\Entity\Task\Task;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TaskVoter extends Voter
{
    const VIEW = 'VIEW_TASK';
    const UPDATE = 'UPDATE_TASK';
    const DELETE = 'DELETE_TASK';
    const STATUS = 'STATUS_TASK';
    const SUBSCRIBE = 'SUBSCRIBE_TASK';
    const UNSUBSCRIBE = 'UNSUBSCRIBE_TASK';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::UPDATE, self::DELETE, self::STATUS, self::SUBSCRIBE, self::UNSUBSCRIBE))) {
            return false;
        }

        if ($subject instanceof Task) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user, $token);
            case self::STATUS:
                return $this->canStatus($subject, $user, $token);
            case self::SUBSCRIBE:
                return $this->canSubscribe($subject, $user, $token);
            case self::UNSUBSCRIBE:
                return $this->canUnsubscribe($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::DELETE:
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canView(Task $task, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_FRIEND'))) {
            return true;
        }

        return false;
    }

    private function canUpdate(Task $task, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false; 
    }

    private function canStatus(Task $task, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_FRIEND'))) {
            return true;
        }

        return false;
    }

    private function isSubscribe(Task $task, User $user)
    {
        foreach($task->getReferedUsers() as $member) {
            if ($member->getId() == $user->getId()) {
                return true;
            }
        }
        return false;
    }

    private function canSubscribe(Task $task, User $user, $token)
    {
        if (!$this->isSubscribe($task, $user)) {
            return true;
        }

        return false;
    }

    private function canUnSubscribe(Task $task, User $user, $token)
    {
        if ($this->isSubscribe($task, $user)) {
            return true;
        }

        return false;
    }
    
    private function canDelete(Task $task, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN')) and
            $task->getCreatedBy() === $user) {
            return true;
        }
        return false; 
    }
}