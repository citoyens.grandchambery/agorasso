<?php

namespace AppBundle\Security;

use AppBundle\Entity\User\Operation;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class OperationVoter extends Voter
{
    const VIEW_OPERATION = 'VIEW_OPERATION';
    const EDIT_OPERATION = 'EDIT_OPERATION';
    const DELETE_OPERATION = 'DELETE_OPERATION';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW_OPERATION, self::EDIT_OPERATION, self::DELETE_OPERATION))) {
            return false;
        }

        if ($subject instanceof Operation) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW_OPERATION:
                return $this->canViewOperation($subject, $user, $token);
            case self::EDIT_OPERATION:
                return $this->canEditOperation($subject, $user, $token);
            case self::DELETE_OPERATION:
                return $this->canDeleteOperation($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canViewOperation(Operation $operation, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_CANDIDATE')))
            return true;

        return false;
    }

    private function canEditOperation(Operation $operation, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if($operation->getCreatedBy() == $user)
            return true;

        return false; 
    }
    
    private function canDeleteOperation(Operation $operation, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if($operation->getCreatedBy() == $user)
            return true;

        return false; 
    }
}