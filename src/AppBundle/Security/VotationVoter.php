<?php

namespace AppBundle\Security;

use AppBundle\Entity\Subscription\Votation;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class VotationVoter extends Voter
{
    const VOTE = 'CAN_VOTE';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VOTE))) {
            return false;
        }

        if ($subject instanceof Votation) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VOTE:
                return $this->canVote($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canVote(Votation $votation, User $user, $token)
    {
        if (!$this->decisionManager->decide($token, array('ROLE_FRIEND'))) {
            return false;
        }
        if($votation->getEndTime() < new \DateTime()) {
            return false;
        }
        if($user->hasVoted($votation))
            return false;

        return true;
    }
}