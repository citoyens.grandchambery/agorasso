<?php

namespace AppBundle\Security;

use AppBundle\Entity\Time\Xvent;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class XventVoter extends Voter
{
    const VIEW = 'VIEW_EVENT';
    const UPDATE = 'UPDATE_EVENT';
    const DELETE = 'DELETE_EVENT';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::UPDATE, self::DELETE))) {
            return false;
        }

        if ($subject instanceof Xvent) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::DELETE:
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canView(Xvent $event, User $user, $token)
    {
        /*if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_FRIEND'))) {
            return true;
        }
        if($event->getPublic())
            return true;*/

        return true;
    }

    private function canUpdate(Xvent $event, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false; 
    }
    
    private function canDelete(Xvent $event, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

//        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN')) and
//            $event->getCreatedBy() === $user) {
//            return true;
//        }
        return false; 
    }
}