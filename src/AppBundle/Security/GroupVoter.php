<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class GroupVoter extends Voter
{
    const SUBSCRIBE = 'SUBSCRIBE';
    const UNSUBSCRIBE = 'UNSUBSCRIBE';
    const REFERENT = 'REFERENT';
    const SEND_MAIL = 'SEND_MAIL';
       
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::SUBSCRIBE, self::UNSUBSCRIBE, self::REFERENT, self::SEND_MAIL))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Group) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN')) or
            $this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            if (in_array($attribute, array(self::SUBSCRIBE, self::UNSUBSCRIBE)))
                return false;
            return true;
        }

        $group = $subject;

        switch ($attribute) {
            case self::SUBSCRIBE:
                return $this->canSubscribe($group, $user, $token);
            case self::UNSUBSCRIBE:
                return $this->canUnsubscribe($group, $user, $token);
            case self::REFERENT:
                return $this->canReferent($group, $user, $token);
            case self::SEND_MAIL:
                return $this->canSendMail($group, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function isSubscribe(Group $group, User $user)
    {
        foreach($group->getMembers() as $member) {
            if ($member['user']->getId() == $user->getId() and $member['active'] == true) {
                return true;
            }
        }
        return false;
    }

    private function isReferent(Group $group, User $user)
    {
        foreach($group->getReferents() as $referent)
        {
            if($referent->getId() == $user->getId())
                return $referent;
        }
        return false;
    }

    private function canSubscribe(Group $group, User $user, $token)
    {
        if ($this->isSubscribe($group, $user)) {
            return false;
        }

        return true;
    }

    private function canUnsubscribe(Group $group, User $user, $token)
    {
        if (!$this->isSubscribe($group, $user)) {
            return false;
        }

        return true;
    }

    private function canReferent(Group $group, User $user, $token)
    {
        return $this->isReferent($group, $user);
    }

    private function canSendMail(Group $group, User $user, $token)
    {
        return $this->isReferent($group, $user);
    }
}