<?php

namespace AppBundle\Security;

use AppBundle\Entity\Project\Contribution;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ContributionVoter extends Voter
{
    const VIEW = 'VIEW_CONTRIBUTION';
    const UPDATE = 'UPDATE_CONTRIBUTION';
    const DELETE = 'DELETE_CONTRIBUTION';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::UPDATE, self::DELETE))) {
            return false;
        }

        if ($subject instanceof Contribution) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::DELETE:
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Contribution $contribution, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }
        if(!$contribution or !$user)
            return false;
        if($contribution->getCreatedBy()->getId() == $user->getId())
            return true;
        if(!$contribution->isCurrent())
            return true;

        return false;
    }

    private function canUpdate(Contribution $contribution, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_STORY'))) {
            return true;
        }
        if(!$contribution->isCurrent()) {
            return false;
        }
        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }
        if($contribution->getCreatedBy()->getId() == $user->getId())
            return true;

        return false; 
    }
    
    private function canDelete(Contribution $contribution, User $user, $token)
    {
        return false; 
    }
}