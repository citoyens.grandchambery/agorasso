<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    const EDIT_SELF = 'EDIT_USER_SELF';
    const EDIT_GROUP = 'EDIT_USER_GROUP';
    const ADD_FILE = 'ADD_FILE';
    const ADD_EVENT = 'ADD_EVENT';
    const ADD_INFO = 'ADD_INFO';
    const ADD_PUBLIC = 'ADD_PUBLIC';
    const ADD_TASK = 'ADD_TASK';
    const ADD_CANDIDATE = 'ADD_CANDIDATE';
    const EDIT_CONTRIBUTION = 'EDIT_CONTRIBUTION';
    const VIEW_CONTRIBUTION = 'VIEW_CONTRIBUTION';
       
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::EDIT_SELF, self::EDIT_GROUP, self::ADD_FILE, self::ADD_EVENT, self::ADD_INFO, self::ADD_TASK, self::ADD_PUBLIC,
            self::ADD_CANDIDATE))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        $member = $subject;

        switch ($attribute) {
            case self::EDIT_SELF:
                return $this->canEditSelf($member, $user, $token);
            case self::EDIT_GROUP:
                return $this->canEditGroup($member, $user, $token);
            case self::ADD_FILE:
                return $this->canAddFile($member, $user, $token);
            case self::ADD_EVENT:
                return $this->canAddEvent($member, $user, $token);
            case self::ADD_INFO:
                return $this->canAddInfo($member, $user, $token);
            case self::ADD_TASK:
                return $this->canAddTask($member, $user, $token);
            case self::ADD_CANDIDATE:
                return $this->canAddCandidate($member, $user, $token);
            case self::ADD_PUBLIC:
                return $this->canAddPublic($member, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canEdit(User $member, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        return false;
    }

    private function canEditSelf(User $member, User $user, $token)
    {
        if ($this->canEdit($member, $user, $token)) {
            return true;
        }

        return $member === $user;
    }

    private function canAddFile(User $member, User $user, $token)
    {

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false;
    }

    private function canAddEvent(User $member, User $user, $token)
    {

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false;
    }

    private function canAddInfo(User $member, User $user, $token)
    {

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false;
    }

    private function canAddTask(User $member, User $user, $token)
    {

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false;
    }

    private function canAddCandidate(User $member, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_CANDIDATE'))) {
            return true;
        }

        return false;
    }

    private function canAddPublic(User $member, User $user, $token)
    {

        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        return false;
    }
}