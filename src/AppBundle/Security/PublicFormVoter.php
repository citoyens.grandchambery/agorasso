<?php

namespace AppBundle\Security;

use AppBundle\Entity\Content\PublicForm;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PublicFormVoter extends Voter
{
    const FORM = 'CAN_FORM';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::FORM))) {
            return false;
        }

        if ($subject instanceof PublicForm) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::FORM:
                return $this->canForm($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canForm(PublicForm $publicForm, $user, $token)
    {
        $now = new \DateTime();
        if($now < $publicForm->getCreatedAt() or $now > $publicForm->getEndDate()) {
            return false;
        }

        return true;
    }
}