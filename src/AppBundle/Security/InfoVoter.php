<?php

namespace AppBundle\Security;

use AppBundle\Entity\Time\Info;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InfoVoter extends Voter
{
    const VIEW = 'VIEW_INFO';
    const UPDATE = 'UPDATE_INFO';
    const DELETE = 'DELETE_INFO';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::UPDATE, self::DELETE))) {
            return false;
        }

        if ($subject instanceof Info) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::DELETE:
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }
    
    private function canView(Info $info, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_FRIEND'))) {
            return true;
        }
        if($info->getPublic())
            return true;

        return false;
    }

    private function canUpdate(Info $info, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false; 
    }
    
    private function canDelete(Info $info, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN')) and
            $info->getCreatedBy() === $user) {
            return true;
        }
        return false; 
    }
}