<?php

namespace AppBundle\Security;

use AppBundle\Entity\Monetic\Payment;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PaymentVoter extends Voter
{
    const VIEW = 'VIEW_PAYMENT';
    const UPDATE = 'UPDATE_PAYMENT';
    const DELETE = 'DELETE_PAYMENT';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::UPDATE, self::DELETE))) {
            return false;
        }

        if ($subject instanceof Payment) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::DELETE:
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Payment $payment, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_MONETIC'))) {
            return true;
        }
        if(!$payment or !$user)
            return false;
        if($payment->getPayer()->getId() == $user->getId())
            return true;

        return false;
    }

    private function canUpdate(Payment $payment, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_MONETIC'))) {
            return true;
        }

        return false; 
    }
    
    private function canDelete(Payment $payment, User $user, $token)
    {
        return false; 
    }
}