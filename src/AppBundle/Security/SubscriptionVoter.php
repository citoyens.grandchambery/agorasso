<?php

namespace AppBundle\Security;

use AppBundle\Entity\User\Subscription;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SubscriptionVoter extends Voter
{
    const VIEW_CANDIDATE = 'VIEW_CANDIDATE';
    const EDIT_CANDIDATE = 'EDIT_CANDIDATE';
    const DELETE_CANDIDATE = 'DELETE_CANDIDATE';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW_CANDIDATE, self::EDIT_CANDIDATE, self::DELETE_CANDIDATE))) {
            return false;
        }

        if ($subject instanceof Subscription) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW_CANDIDATE:
                return $this->canViewCandidate($subject, $user, $token);
            case self::EDIT_CANDIDATE:
                return $this->canEditCandidate($subject, $user, $token);
            case self::DELETE_CANDIDATE:
                return $this->canDeleteCandidate($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canViewCandidate(Subscription $candidate, User $user, $token)
    {
        if(!$candidate->isCandidate()) {
            return false;
        }
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        if ($this->decisionManager->decide($token, array('ROLE_CANDIDATE')))
            return true;

        return false;
    }

    private function canEditCandidate(Subscription $candidate, User $user, $token)
    {
        if(!$candidate->isCandidate()) {
            return false;
        }
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if ($this->decisionManager->decide($token, array('ROLE_CANDIDATE'))) {
            return true;
        }

        return false; 
    }
    
    private function canDeleteCandidate(Subscription $candidate, User $user, $token)
    {
        if(!$candidate->isCandidate()) {
            return false;
        }
        if($candidate->getStatus() != Subscription::STATUS_WAITING)
            return false;
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }



//        if ($this->decisionManager->decide($token, array('ROLE_CANDIDATE'))) {
//            return true;
//        }

        return false; 
    }
}