<?php

namespace AppBundle\Security;

use AppBundle\Entity\Document\DocumentCategory;
use AppBundle\Entity\Document\UploadedFile;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UploadedFileVoter extends Voter
{
    const VIEW = 'VIEW_FILE';
    const UPDATE = 'UPDATE_FILE';
    const DELETE = 'DELETE_FILE';
    
    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }

    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, array(self::VIEW, self::UPDATE, self::DELETE))) {
            return false;
        }

        if ($subject instanceof UploadedFile) {
            return true;
        }

        return false;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // ROLE_SUPER_ADMIN can do anything! The power!
        if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
            return true;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $user, $token);
            case self::UPDATE:
                return $this->canUpdate($subject, $user, $token);
            case self::DELETE:
                return $this->canDelete($subject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(UploadedFile $file, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        if($file->getPublic())
            return true;

        $type = DocumentCategory::DOC_TYPE_DRAFT;
        if($file and $file->getCategory() and $file->getCategory()->getType())
            $type = $file->getCategory()->getType();

        switch($type) {
            case DocumentCategory::DOC_TYPE_DRAFT:
                if($this->decisionManager->decide($token, array('ROLE_FRIEND'))) {
                    $user = $token->getUser();
                    $group = $file->getGroup();
                    if ($group and $group->isAssemblee())
                        return true;
                    elseif ($user and $user->isMember($group)) {
                        return true;
                    }
                }
                break;
            case DocumentCategory::DOC_TYPE_CR:
                $group = $file->getGroup();
                $date = (new \DateTime)->setDate('2018', 6, 20);
                if($group and $group->isAssemblee() and $file->getCreatedAt() > $date)
                    return true;
                elseif($this->decisionManager->decide($token, array('ROLE_FRIEND')))
                    return true;
                break;
            case DocumentCategory::DOC_TYPE_CONTACT:
                if ($this->decisionManager->decide($token, array('ROLE_CONTACT'))) {
                    return true;
                }
                break;
            case DocumentCategory::DOC_TYPE_PUBLIC:
                return true;
        }

        return false;
    }

    private function canUpdate(UploadedFile $file, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }
        if($file->getPublic())
            return false;
        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            return true;
        }

        return false; 
    }
    
    private function canDelete(UploadedFile $file, User $user, $token)
    {
        if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
            return true;
        }

        if($file->getPublic())
            return false;

        if ($this->decisionManager->decide($token, array('ROLE_CITIZEN'))) {
            if($file->getCreatedBy() === $user) {
                return true;
            }
            if($user->isReferent($file->getGroup())) {
                return true;
            }
        }
        return false; 
    }
}