<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Mail\Mail as Mail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User\Subscription;
use AppBundle\Entity\Api\Citizen;
use AppBundle\Entity\Api\Procuration;

class MailerManager
{
    protected $mailer;
    protected $twig;
    protected $tokenStorage;
    protected $authorizationChecker;
    protected $session;
    protected $em;
    protected $senderEmail;
    protected $contactEmail;
    protected $container;
    protected $serverMode;
    protected $emailName;

    public function __construct(ContainerInterface $container, $senderEmail, $contactEmail, $serverMode, $emailName)
    {
        $this->container = $container;
        $this->senderEmail = $senderEmail;
        $this->contactEmail = $contactEmail;
        $this->serverMode = $serverMode;
        $this->emailName = $emailName;
        $this->mailer = $container->get('mailer');
        $this->twig = $container->get('twig');
        $this->tokenStorage = $container->get('security.token_storage');
        $this->authorizationChecker = $container->get('security.authorization_checker');
        $this->session = $container->get('session');
        $this->em = $container->get('doctrine')->getManager();
    }

    protected function newEmail($subject, $body)
    {
        $email = new Mail();
        $email->setSubject($subject);
        $email->setBody($body);
        $email->setFrom($this->senderEmail);
        return $email;
    }

    public function _sendSubscription($subscription, $subject, $body)
    {
        $email = $this->newEmail($subject, $body);
        $to = array();
        $to[] = $subscription->getEmail();
        $email->setTo($to);
        $this->em->persist($email);
        $this->sendMail($email, false);
    }

    public function sendSubscription($subscription)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:subscription.html.twig', array(
            'subscription' => $subscription,
        ));
        $subject = "Inscription pour nous rejoindre";
        return $this->_sendSubscription($subscription, $subject, $body);
    }

    public function _sendSponsor($subscription, $subject, $body)
    {
        $sponsor = $subscription->getSponsor();
        if (!$sponsor or !$sponsor->getEmail()) {
            return;
        }
        $email = $this->newEmail($subject, $body);
        $to = array();
        $to[] = $sponsor->getEmail();
        $email->setTo($to);
        $this->em->persist($email);
        $this->sendMail($email, false);
    }

    public function sendSponsor($candidate)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:sponsor.html.twig', array(
            'subscription' => $candidate,
        ));
        $subject = "Vous avez proposé une candidature";
        return $this->_sendSponsor($candidate, $subject, $body);
    }

    public function sendCandidate($subscription)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:candidate.html.twig', array(
            'subscription' => $subscription,
        ));
        if ($subscription->getGender() == Subscription::GENDER_MALE) {
            $subject = "Volontaire pour être candidat";
        } else {
            $subject = "Volontaire pour être candidate";
        }
        return $this->_sendSubscription($subscription, $subject, $body);
    }

    public function sendMail(Mail $email, $bagMessage = true)
    {
        $subject = null;
        $body = null;
        switch ($email->getType()) {
            case Mail::MAIL_TYPE_GROUP:
                $group = $email->getGroup();
                if ($group->isCopil()) {
                    $subject = sprintf("[%s] %s", $email->getGroup(), $email->getSubject());
                    $title = sprintf("%s : %s", $email->getGroup(), $email->getSubject());
                } elseif ($group->isAssemblee()) {
                    $subject = sprintf("[%s] %s", $this->emailName, $email->getSubject());
                    $title = sprintf("%s : %s", $this->emailName, $email->getSubject());
                } else {
                    $subject = sprintf("[Groupe %s] %s", $email->getGroup(), $email->getSubject());
                    $title = sprintf("Groupe %s : %s", $email->getGroup(), $email->getSubject());
                }
                $body = $this->twig->render('AppBundle:Mail/Templates:group.html.twig', array(
                    'group' => $email->getGroup(),
                    'email' => $email,
                    'title' => $title
                ));
                break;
            case Mail::MAIL_TYPE_ROOT:
                $subject = sprintf("[%s] %s", $this->emailName, $email->getSubject());
                break;
            case Mail::MAIL_TYPE_CONTACT:
                $subject = sprintf("[Contact %s] %s", $email->getGroup(), $email->getSubject());
                $title = sprintf("Contact %s : %s", $email->getGroup(), $email->getCreatedBy()->getEmail());
                $body = $this->twig->render('AppBundle:Mail/Templates:group.html.twig', array(
                    'group' => $email->getGroup(),
                    'email' => $email,
                    'title' => $title
                ));
                break;
            case Mail::MAIL_TYPE_GROUP_SUBSCRIPTION:
                $subject = sprintf("[Groupe %s] Inscription", $email->getGroup());
                $title = sprintf("Inscription au groupe %s : %s", $email->getGroup(), $email->getCreatedBy()->getEmail());
                $body = $this->twig->render('AppBundle:Mail/Templates:groupSubscription.html.twig', array(
                    'group' => $email->getGroup(),
                    'subscriber' => $email->getCreatedBy(),
                    'title' => $title
                ));
                break;
        }
        $this->send($email, $subject, $body, $bagMessage);
    }

    private function send(Mail $email, $subject = null, $body = null, $bagMessage = true)
    {
        try {
            $to = $email->getTo();
            $bcc = $email->getBcc();
            if ((!$to or $to == '' or 0 == count($to)) and (!$bcc or 0 == count($bcc))) {
                $to = array();
                $to[] = $this->contactEmail;
            }
            if ($this->serverMode == 'preprod') {
                $bcc = null;
                $to = array();
                $to[] = $this->contactEmail;
            }
            $message = \Swift_Message::newInstance()
                ->setSubject($subject ? $subject : $email->getSubject())
                ->setTo($to)
                ->setBcc($bcc)
                ->setFrom($this->senderEmail);

            if ($body)
                $message->setBody($body, 'text/html');
            else
                $message->setBody($email->getBody(), 'text/html');

            $this->mailer->send($message);
            $email->setSentAt(new \DateTime());
            $message = 'Mail envoyé';
            switch ($email->getType()) {
                case Mail::MAIL_TYPE_GROUP:
                    $message = sprintf("Un email a été envoyé aux membres du groupe %s.", $email->getGroup());
                    break;
                case Mail::MAIL_TYPE_CONTACT:
                case Mail::MAIL_TYPE_GROUP_SUBSCRIPTION:
                    $message = sprintf("Un email a été envoyé aux référents du groupe %s.", $email->getGroup());
                    break;
                case Mail::MAIL_TYPE_ROOT:
                    $message = sprintf("Un email vous a été envoyé : vérifiez que vous l'avez reçu.");
                    break;
            }
            if ($bagMessage)
                $this->session->getFlashBag()->add('success', $message);
        } catch (\Exception $e) {
            if ($bagMessage) {
                $this->session->getFlashBag()->add('danger', sprintf("L'email n'a pas pu être envoyé!"));
            }
            $email->setResult($e->getMessage());
        }
        $this->em->flush();
    }
    
    private function sendGenericCitizen($body, $subject, $emailAddress)
    {
        $email = $this->newEmail($subject, $body);
        $email->setFrom('no-reply@savoiecitoyenne.fr');
        $to = array();
        $to[] = $emailAddress;
        $to[] = 'savoie.citoyenne@gmail.com';
        $email->setTo($to);
        $this->em->persist($email);

        try {
            $message = \Swift_Message::newInstance()
                ->setSubject($email->getSubject())
                ->setTo($email->getTo())
                ->setFrom('no-reply@savoiecitoyenne.fr');

            $message->setBody($email->getBody(), 'text/html');
            $this->mailer->send($message);
            $email->setSentAt(new \DateTime());
        } catch (\Exception $e) {
            $email->setResult($e->getMessage());
        }
        $this->em->flush();
    }

    public function sendCitizen(Citizen $citizen)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:citizen.html.twig', array(
            'citizen' => $citizen,
        ));

        if ($citizen->getCandidate()) {
            $subject = "[Savoie Citoyenne] Confirmation de candidature aux départementales 2021 en Savoie";
        } elseif ($citizen->getSupport()) {
            $subject = "[Savoie Citoyenne] Confirmation de soutien à l'appel pour des départementales 2021 en Savoie";
        } else {
            $subject = "[Savoie Citoyenne] Confirmation d'inscription à la lettre d'info de Savoie Citoyenne";
        }
        $this->sendGenericCitizen($body, $subject, $citizen->getEmail());
    }

    public function sendProcuration(Procuration $procuration)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:procuration.html.twig', array(
            'procuration' => $procuration,
        ));

        $subject = "[Savoie Citoyenne] Confirmation de demande de procuration";
        $this->sendGenericCitizen($body, $subject, $procuration->getEmail());
    }

    private function sendGenericEnsemble($body, $subject, $emailAddress)
    {
        $email = $this->newEmail($subject, $body);
        $email->setFrom('no-reply@savoie-ensemble.fr');
        $to = array();
        $to[] = $emailAddress;
        $to[] = 'savoie.ensemble@gmail.com';
        $email->setTo($to);
        $this->em->persist($email);

        try {
            $message = \Swift_Message::newInstance()
                ->setSubject($email->getSubject())
                ->setTo($email->getTo())
                ->setFrom('no-reply@savoie-ensemble.fr');

            $message->setBody($email->getBody(), 'text/html');
            $this->mailer->send($message);
            $email->setSentAt(new \DateTime());
        } catch (\Exception $e) {
            $email->setResult($e->getMessage());
        }
        $this->em->flush();
    }


    public function sendEnsemble(Citizen $citizen)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:ensemble.html.twig', array(
            'citizen' => $citizen,
        ));

        if ($citizen->getCandidate()) {
            $subject = "[Savoie Ensemble] Confirmation de candidature aux départementales 2021 en Savoie";
        } elseif ($citizen->getSupport()) {
            $subject = "[Savoie Ensemble] Confirmation de soutien Ensemble pour une Savoie écologique, démocratique et solidaire";
        } else {
            $subject = "[Savoie Ensemble] Confirmation d'inscription à la lettre d'info de Savoie Ensemble";
        }
        $this->sendGenericEnsemble($body, $subject, $citizen->getEmail());

    }

    public function sendEnsembleProcuration(Procuration $procuration)
    {
        $body = $this->twig->render('AppBundle:Mail\Templates:ensembleProcuration.html.twig', array(
            'procuration' => $procuration,
        ));

        $subject = "[Savoie Ensemble] Confirmation de demande de procuration";
        $this->sendGenericEnsemble($body, $subject, $procuration->getEmail());
    }
}