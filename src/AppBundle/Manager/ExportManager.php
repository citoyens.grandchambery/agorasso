<?php

namespace AppBundle\Manager;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Task\Task;


class ExportManager
{
    protected $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function exportCsvTasks($tasks)
    {
        $titles = array(
            1 => "Groupe",
            2 => "Echéance",
            3 => "Statut",
            4 => "Tâche"
        );

        $rows[] = implode("\t", $titles);

        foreach ($tasks as $task) {

            $data = array (
                1 => sprintf("\"%s\"", $task->getGroup()->getName()),
                2 => sprintf("\"%s\"", $task->getDate() ? $task->getDate()->format('d/m/Y') : ""),
                3 => sprintf("\"%s\"", $task->getStatusLabel()),
                4 => sprintf("\"%s\"", $task->getTitle()),
            );

            $rows[] = implode("\t", $data);
        }

        $fileContent = implode("\n", $rows);
        $response = new Response($fileContent);
        $encoded_csv = mb_convert_encoding($response->getContent(), 'UTF-16LE', 'UTF-8');
        $response->headers->set('Content-type','application/vnd.ms-excel');
        $response->setContent(chr(255) . chr(254) . $encoded_csv);

        $this->session->getFlashBag()->add('success', sprintf("Les actions ont été exportées."));

        return $response;
    }

}