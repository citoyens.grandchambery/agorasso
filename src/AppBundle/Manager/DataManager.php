<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\User as User;
use AppBundle\Entity\Location\Town;
use AppBundle\Validator\Constraints\Subscription\SubscriptionTown;

class DataManager
{
    const TYPE_MAIL = 'Courriel';
    const TYPE_ADDRESS = 'Adresse';
    const TYPE_PHONE = 'Téléphone';

    const AUTH_NONE = 'Visible par personne';
    const AUTH_CITIZEN = 'Visible par les référent.e.s de groupe';
    const AUTH_FRIEND = 'Visible par les ami.e.s';

    public static $authChoices = array(
        self::AUTH_NONE => self::AUTH_NONE,
        self::AUTH_CITIZEN => self::AUTH_CITIZEN,
        self::AUTH_FRIEND => self::AUTH_FRIEND,
    );

    protected $tokenStorage;
    protected $authorizationChecker;
    protected $em;
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->tokenStorage = $container->get('security.token_storage');
        $this->em = $container->get('doctrine')->getManager();
        $this->authorizationChecker = $container->get('security.authorization_checker');
    }

    protected function isEmpty($member, $type)
    {
        
        switch($type) {
            case DataManager::TYPE_ADDRESS: $data = $member->getAddress(); break;
            case DataManager::TYPE_PHONE: $data = $member->getPhone(); break;
            case DataManager::TYPE_MAIL: $data = $member->getEmail(); break;
            default: $data = ''; break; 
        }
        return $data == '';
    }

    public function dataCanRead($member, $type)
    {
        if($this->authorizationChecker->isGranted('EDIT_USER_SELF', $member))
            return true;
        
        if($this->isEmpty($member, $type))
            return true;

        $visibility = $member->getDataVisibility($type);
        switch($visibility) {
            case self::AUTH_NONE: return false;
            case self::AUTH_FRIEND: return $this->authorizationChecker->isGranted('ROLE_FRIEND');
            case self::AUTH_CITIZEN: return $this->authorizationChecker->isGranted('ROLE_CITIZEN');
        }

        return false;
    }
    
    public function getEmail($member)
    {
        if($this->dataCanRead($member, self::TYPE_MAIL))
            return $member->getEmail();
        return 'Masqué';
    }

    public function getPhone($member)
    {
        if($this->dataCanRead($member, self::TYPE_PHONE))
            return $member->getPhone();
        return 'Masqué';
    }

    public function getAddress($member)
    {
        if($this->dataCanRead($member, self::TYPE_ADDRESS))
            return sprintf("%s", $member->getAddress());
        return 'Masquée';
    }

    public function getAssemblee()
    {
        return $this->em->getRepository('AppBundle:Group\Group')->queryGroupByWeight(0);
    }

    public function getCopil()
    {
        return $this->em->getRepository('AppBundle:Group\Group')->queryGroupByWeight(1);
    }

    public function getNextActionEvent($n)
    {
        return $this->em->getRepository('AppBundle:Time\Xvent')->queryNextActionEvent($n);
    }

    public function getRole(User $user)
    {
        if ($user->hasRole('ROLE_CITIZEN') or $user->hasRole('ROLE_ADMIN'))
            return 'Animateur.trice';
        elseif ($user->hasRole('ROLE_FRIEND'))
            return 'Ami.e';
        else
            return 'Curieux.se';

    }

    public function hasTowns()
    {
        $towns = $this->em->getRepository('AppBundle:Location\Town')->queryTowns()->getQuery()->getResult();
        return count($towns) > 0;
    }

    public function hasDistrict($town)
    {
        $districts = $this->em->getRepository('AppBundle:Location\District')->queryDistricts($town)->getQuery()->getResult();
        return count($districts) > 0;
    }

    public function isValidTown($member, &$message)
    {
        $constraint = new SubscriptionTown();

        if (!$this->hasTowns()){
            if(!$member->getCity()) {
                $message = $constraint->townMessage;
                return false;
            }
            return true;
        }

        $town = $member->getTown();
        if (!$town){
            $message = $constraint->townMessage;
            return false;
        }

        if($this->hasDistrict($town) and !$member->getDistrict()) {
            $message = $constraint->districtMessage;
            return false;
        }
        if($town->getOther() and !$member->getCity()) {
            $message = $constraint->otherMessage;
            return false;
        }
        return true;
    }
}
