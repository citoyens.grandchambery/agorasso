<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Site\Block;



class SiteManager
{
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    function getLogo()
    {
        $site = $this->em->getRepository('AppBundle:Site\Site')->findSite();
        if(!$site) {
            return null;
        }
        return $site->getLogo();
    }

    function getImage()
    {
        $site = $this->em->getRepository('AppBundle:Site\Site')->findSite();
        if(!$site) {
            return null;
        }
        return $site->getImage();
    }

    function getColumnImage()
    {
        $site = $this->em->getRepository('AppBundle:Site\Site')->findSite();
        if(!$site) {
            return null;
        }
        return $site->getColumnImage();
    }

    function getFacebook()
    {
        $site = $this->em->getRepository('AppBundle:Site\Site')->findSite();
        if(!$site) {
            return '';
        }
        return $site->getFacebook();
    }

    function getTwitter()
    {
        $site = $this->em->getRepository('AppBundle:Site\Site')->findSite();
        if(!$site) {
            return '';
        }
        return $site->getTwitter();
    }

    function getName()
    {
        $site = $this->em->getRepository('AppBundle:Site\Site')->findSite();
        if(!$site) {
            return '';
        }
        return $site->getName();
    }

    function getSite()
    {
        return $this->em->getRepository('AppBundle:Site\Site')->findSite();
    }

    function getCalendar()
    {
        $block = $this->em->getRepository('AppBundle:Site\Block')->findBlock(Block::TYPE_ALL_CALENDAR);
        if(!$block) {
            return null;
        }
        return $block->getReferedPage();
    }


}
