<?php

namespace AppBundle\Manager;

use AppBundle\Entity\AppParameter\ValueParameter;
use Doctrine\ORM\EntityManagerInterface;


/**
 * Description of ParameterManager
 *
 * @author Michael Souvy
 */
class ParameterManager
{  
    
    protected $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function getAllParameters()
    {
        return $this->manager->getRepository('AppBundle:AppParameter\ValueParameter')
            ->getAllParameters();
    }

    public function getParameter($key, $default = null)
    {
    	$result = $this->manager->getRepository('AppBundle:AppParameter\ValueParameter')
            ->getParameterValue($key);
    	if (null == $result and $default){
    	    return $default;
        }
        return $result;
    }

    public function updateParameter($key, $value)
    {
        $param = $this->manager->getRepository('AppBundle:AppParameter\ValueParameter')
            ->getParameter($key);
        if(!$param) {
            $param = new ValueParameter($key);
            $this->manager->persist($param);
        }
        if($param) {
            $param->setValue($value);
        }
    }
}
