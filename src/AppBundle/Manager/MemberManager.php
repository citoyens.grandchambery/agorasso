<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 05/11/18
 * Time: 12:31
 */

namespace AppBundle\Manager;

use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\User;
use AppBundle\Entity\User\Subscription;
use AppBundle\Entity\Location\Town;
use AppBundle\Entity\Monetic\Payment;
use AppBundle\Manager\ParameterManager;
use AppBundle\Entity\AppParameter\ValueParameter;
use Doctrine\ORM\EntityManagerInterface;


class MemberManager
{
    private $session;
    private $em;
    private $parameterManager;

    function __construct(Session $session, EntityManagerInterface $em, ParameterManager $parameterManager)
    {
        $this->session = $session;
        $this->em = $em;
        $this->parameterManager = $parameterManager;
    }

    // generic function curl
    private function CallAPI($method, $url, $data = false, $login = null, $password = null)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: text/xml; charset=utf-8',
                ));
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        if($login and $password) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $login . ":" . $password);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    //helloAsso module
    // From data in helloAsso payment we create a userf
    protected function readUserFromPayment($payment)
    {
        // create user
        $contact = new User(
            array('email' => $payment->payer_email,
                'firstName' => $payment->payer_first_name,
                'name' => $payment->payer_last_name,
                'role' => "ROLE_CONTACT"));
        $town = $this->em->getRepository('AppBundle:Location\Town')->findTown($payment->payer_city);
        if($town) {
            $contact->setTown($town);
            if($town->getOther()) {
                $contact->setCity($payment->payer_city);
            }
        } else {
            $contact->setCity($payment->payer_city);
        }
        $contact->setAddress($payment->payer_address);
        $contact->setPostalCode($payment->payer_zip_code);
        $contact->setBasicFields($contact->getEmail());

        return $contact;
    }
    
    public function createSubFromUser(User $user)
    {
        $sub = new Subscription();
        $sub->setName($user->getName());
        $sub->setFirstName($user->getFirstName());
        $sub->setAddress($user->getAddress());
        $sub->setPostalCode($user->getPostalCode());
        $sub->setCity($user->getCity());
        $sub->setTown($user->getTown());
        $sub->setDistrict($user->getDistrict());
        $sub->setPhone($user->getPhone());
        $sub->setEmail($user->getEmail());
        $sub->setGetNoticed(true);
        $sub->setUser($user);
        $sub->setRole($user->getRoles()[0]);
        return $sub;
    }

    // From data in helloAsso payment we create a user and the subscription
    protected function createUserFromPayment($payment)
    {
        // create user
        $usr = $this->readUserFromPayment($payment);
        $this->em->persist($usr);

        // create subscription
        $sub = $this->createSubFromUser($usr);
        $this->em->persist($sub);

        return $usr;
    }

    // From data in helloAsso payment we update empty user data
    protected function updateUserFromPayment(User $origin, $payment)
    {
        $usr = $this->readUserFromPayment($payment);
        if(!$origin->getTown() and $usr->getTown()) {
            $origin->setTown($usr->getTown());
        }
        if(!$origin->getCity() and $usr->getCity()) {
            $origin->setCity($usr->getCity());
        }
        if(!$origin->getAddress() and $usr->getAddress()) {
            $origin->setAddress($usr->getAddress());
        }
        if(!$origin->getPhone() and $usr->getPhone()) {
            $origin->setPhone($usr->getPhone());
        }
        if(!$origin->getPostalCode() and $usr->getPostalCode()) {
            $origin->setPostalCode($usr->getPostalCode());
        }
        return $origin;
    }

    // Date format : ISO 8601, example : "2018-08-14T09:13:00Z"
    protected function getPaymentDate($payment)
    {
        if(!$payment or ! $payment->date) {
            return null;
        }
        return \DateTime::createFromFormat('Y-m-d\TH:i:s+', $payment->date);
    }

    // From data in helloAsso payment we create a helloAsso payment
    protected function createPayment($payment, $usr, $action)
    {
        $p = $this->em->getRepository('AppBundle:Monetic\Payment')->findOneByReference($action->id);

        if(!$p) {
            $p = new Payment();
            $p->setReference($action->id);
            $p->setOrigin(Payment::ORIGIN_HELLO_ASSO);
            $p->setMode(Payment::MODE_CARD);
            $this->em->persist($p);
        }
        $p->setDate($this->getPaymentDate($payment));
        switch ($action->type) {
            case "SUBSCRIPTION":
                $p->setType(Payment::TYPE_SUBSCRIPTION);
                if (in_array('ROLE_CONTACT', $usr->getRoles())) {
                    $usr->setRoles(array('ROLE_FRIEND'));
                }
                break;
            case "DONATION":
                $p->setType(Payment::TYPE_DONATION);
                break;
            default:
                break;
        }
        $p->setAmount($action->amount);
        switch($action->status) {
            case "PROCESSED":
                $p->setStatus(Payment::STATUS_PROCESSED);
                break;
            default:
                $p->setStatus(Payment::STATUS_WAITING);
                break;
        }
        $p->setPayer($usr);
        $p->setReceipt($payment->url_receipt);
        return $p;
    }


    protected function updateHelloPayments($login, $password)
    {
        $lastDate = $this->parameterManager->getParameter(ValueParameter::HELLO_ASSO_LAST_DATE);
        $url = "https://api.helloasso.com/v3/payments.json";

        if($lastDate) {
            $url = $url."?".http_build_query(array("from" => $lastDate));
        }
        $response = $this->CallAPI("GET", $url, false, $login, $password);

        $payments = json_decode($response);

        foreach ($payments->resources as $payment) {
            $lastDate = $payment->date;
            $email = $payment->payer_email;
            $usr = $this->em->getRepository('AppBundle:User')
                ->findOneByEmail($email);
            if(!$usr) {
                $usr = $this->createUserFromPayment($payment);
            } else {
                $usr = $this->updateUserFromPayment($usr, $payment);
            }
            foreach($payment->actions as $action) {
                $this->createPayment($payment, $usr, $action);
            }
        }
        $this->parameterManager->updateParameter(ValueParameter::HELLO_ASSO_LAST_DATE, $lastDate);
        $this->em->flush();

        return $response;
    }

    public function updatePayments()
    {
        // HelloAsso payments
        $login = $this->parameterManager->getParameter(ValueParameter::HELLO_ASSO_LOGIN);
        $password = $this->parameterManager->getParameter(ValueParameter::HELLO_ASSO_PASSWORD);
        if($login and $password) {
            return $this->updateHelloPayments($login, $password);
        }
    }
}