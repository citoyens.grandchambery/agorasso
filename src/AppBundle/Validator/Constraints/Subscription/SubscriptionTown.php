<?php

namespace AppBundle\Validator\Constraints\Subscription;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class SubscriptionTown extends Constraint
{
    public $townMessage = "Vous devez spécifier une commune";
    public $districtMessage = "Vous devez spécifier un quartier";
    public $otherMessage = "Vous devez préciser la commune";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}