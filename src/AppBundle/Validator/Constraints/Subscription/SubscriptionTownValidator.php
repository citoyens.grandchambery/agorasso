<?php

namespace AppBundle\Validator\Constraints\Subscription;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SubscriptionTownValidator extends ConstraintValidator
{
    protected $dataManager;

    public function __construct($dataManager)
    {
        $this->dataManager = $dataManager;
    }
    
    public function validate($subscription, Constraint $constraint)
    {
        $message = '';
        if(!$this->dataManager->isValidTown($subscription, $message)) {
            $this->context->buildViolation($message)->addViolation();
        }
    }
}
