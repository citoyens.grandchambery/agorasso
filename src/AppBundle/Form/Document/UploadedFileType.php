<?php

namespace AppBundle\Form\Document;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class UploadedFileType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        
        $file = $builder->getData(); 
        
        $builder
            ->add('label', TextType::class, array(
                'label' => 'Libellé document',
            ))
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Document\DocumentCategory',
                'label' => 'Catégorie',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('file', FileType::class, array(
                'required'      => $file->getId()? false:true,
                'label' => $file->getid()?'Nouveau fichier':'Fichier',
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;

        // Si on doit afficher le champ "description" (comme les fichiers partnerAttachedFiles des utilisateurs partenaires)
        if ($builder->getData()->showDescriptionField()) {
            $builder
                ->add('description', TextareaType::class, array(
                    'label' => 'Description',
                ));
        }

        $type = $options['from_type'];

        if($type == 'event') {
            $builder->add('group', EntityType::class, array(
                'required' => true,
                'expanded' => false,
                'label' => 'Groupe',
                'class' => 'AppBundle:Group\Group',
                /*'choice_label' => function ($member) {
                    return $member->getChoiceLabel();
                },*/
                'attr' => array(
                    'style' => "width: 100%",
                    'select2' => "true"
                ),
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryAll();
                },
            ));
        }

        if($type != 'event') {
            $builder->add('event', EntityType::class, array(
                'required' => false,
                'expanded' => false,
                'label' => 'Événement',
                'class' => 'AppBundle:Time\Xvent',
                /*'choice_label' => function ($member) {
                    return $member->getChoiceLabel();
                },*/
                'attr' => array(
                    'style' => "width: 100%",
                    'select2' => "true"
                ),
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryAllEvents(false);
                },
            ));
        }
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Document\UploadedFile',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
            'from_type' => null,
        ));
    }

}
