<?php

namespace AppBundle\Form\Document;

use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('label', TextType::class, array(
                'label' => 'Libellé / Description',
            ))
            ->add('category', EntityType::class, array(
                'class' => 'AppBundle\Entity\Document\DocumentCategory',
                'label' => 'Catégorie',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('group', EntityType::class, array(
                'class' => 'AppBundle\Entity\Group\Group',
                'label' => 'Groupe',
                'required' => false,
                'query_builder' => function (GroupRepository $er) {
                    $qb = $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('search', SubmitType::class, array(
                'label' => 'Rechercher',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }


}
