<?php

namespace AppBundle\Form\Group;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class UserSearchType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',                  TextType::class, array(
                'required' => true,
                'label' => "Courriel ",
                'attr' => array(
                    'placeholder' => "Rechercher par prénom/nom/email",
                    'list' => 'member_list',
                    'autocomplete' => "off"
                )
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Go'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
            ],
        ));
    }

}