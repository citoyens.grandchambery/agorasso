<?php

namespace AppBundle\Form\Group;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Group\GroupSubscription;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


class GroupSubscribeType extends AbstractType {

    protected $tokenStorage;
    protected $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationChecker $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $self = false;
        $user = $this->tokenStorage->getToken()->getUser();
        $selfUser = $options['selfUser'];
        if($user->getId() == $selfUser->getId())
            $self = true;
        $groupSubscription = $builder->getData();
        if($groupSubscription->getActive()) {
            if($self)
                $label = sprintf("En décochant, je souhaite me désinscrire du groupe %s", $groupSubscription->getGroup());
            else
                $label = sprintf("Inscription au groupe %s", $groupSubscription->getGroup());
        }
        else {

            if($self)
                $label = sprintf("En cochant, je souhaite m'inscrire au groupe %s", $groupSubscription->getGroup());
            else
                $label = sprintf("Inscription au groupe %s", $groupSubscription->getGroup());
        }
        $builder
            ->add('active',
                checkboxType::class, array('label' => $label,
                    'required' => false,
                    'data' => $groupSubscription->getActive()))
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;
        if($options['group'] and ($user->isReferent($options['group']) or $this->authorizationChecker->isGranted('ROLE_ADMIN'))) {
            if($groupSubscription->getReferent()) {
                if($self)
                    $label = sprintf("En décochant, je souhaite ne plus être référent.e du groupe %s", $groupSubscription->getGroup());
                else
                    $label = sprintf("Référent.e du groupe %s", $groupSubscription->getGroup());
            }
            else {
                $label = sprintf("Référent.e du groupe %s", $groupSubscription->getGroup());
            }
            $builder->add('referent',
                checkboxType::class, array('label' => $label,
                    'required' => false,
                    'data' => $groupSubscription->getReferent()));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Group\GroupSubscription',
            'selfUser' => null,
            'group' => null,
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
            ],
        ));
    }

}
