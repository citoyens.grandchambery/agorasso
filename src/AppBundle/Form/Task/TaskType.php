<?php

namespace AppBundle\Form\Task;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Entity\Group\Group;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Task\Task;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class TaskType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {

        $builder
            ->add('title', TextType::class, array(
                'label' => 'Description de l\'action',
            ))
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'label' => 'Échéance',
                'required' => false
            ))
            ->add('group', EntityType::class, [
                'class' => 'AppBundle:Group\Group',
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryTaskGroups();
                },
                'label'     => 'Groupe référent',
                'required' => true,
            ])
            ->add('status', ChoiceType::class, array(
                'label' => "Statut",
                'required' => true,
                'choices' => Task::$statusesChoices,
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Task\Task',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
        ));
    }

}
