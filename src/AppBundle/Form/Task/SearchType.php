<?php

namespace AppBundle\Form\Task;

use AppBundle\AppBundle;
use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Task\Task as Task;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('label', TextType::class, array(
                'label' => 'Tâche',
            ))
            ->add('group', EntityType::class, array(
                'class' => 'AppBundle\Entity\Group\Group',
                'label' => 'Groupe',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryTaskGroups();
                },
            ))
            ->add('status', ChoiceType::class, array(
                'label' => "Statut",
                'required' => false,
                'multiple' => true,
                'expanded' => true,
                'choices' => Task::$statusesChoices,
                'data' => Task::$waitingStatus,
            ))
            ->add('compact',        ChoiceType::class, array(
                'required' => true,
                'label' => "Apparence",
                'choices' => array('Vue compacte' => true, 'Vue liste' => false),
                'expanded' => true,
                'multiple' => false,
                'label_attr' => array('class' => 'radio-inline'),
                'data' => true
            ))
            ->add('export_csv', SubmitType::class, array('label' => 'Export CSV'))
            ->add('search', SubmitType::class, array(
                'label' => 'Filtrer',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }


}
