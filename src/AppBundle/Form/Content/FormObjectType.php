<?php

namespace AppBundle\Form\Content;

use AppBundle\Object\VotationObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\Subscription\VotationSubType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class FormObjectType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('responses',                  CollectionType::class, array(
                'label' => false,
                'entry_type' => ResponseType::class,
                'label_attr' => array('class' => 'aligned'),
                'entry_options' => array('formType' => $options['formType'])
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Valider'
            ));
        ;
        
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Object\FormObject',
            'formType' => 'publicForm'
        ));
    }
}
