<?php

namespace AppBundle\Form\Content;

use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProposalSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Libellé proposition',
            ))
            ->add('group', EntityType::class, array(
                'class' => 'AppBundle\Entity\Group\Group',
                'label' => 'Groupe',
                'required' => false,
                'query_builder' => function (GroupRepository $er) {
                    $qb = $er->createQueryBuilder('g')
                        ->where('g.proposal = :proposal')
                        ->setParameter('proposal', true)
                        ->orderBy('g.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('search', SubmitType::class, array(
                'label' => 'Rechercher',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }


}
