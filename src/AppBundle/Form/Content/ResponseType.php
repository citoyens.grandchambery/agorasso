<?php

namespace AppBundle\Form\Content;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType; 
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use blackknight467\StarRatingBundle\Form\RatingType;


class ResponseType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $formType = $options['formType'] ? $options['formType'] : 'publicForm';
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($formType) {
            $form = $event->getForm();
            $response = $event->getData();

            $form->add('content',                       CKEditorType::class, array(
                    'config_name' => $formType == 'project' ? 'project_config' : 'simple_config',
                    'required' => false,
                    'label' => $formType == 'project' ? false : "Réponse :",
                    'label_attr' => $formType == 'project' ? array('class' => 'short-label') : array()

                )
            );
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content\Response',
            'formType' => 'publicForm'
//            'attr' => [
//                'data-ajax-form' => '',
//                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
//            ],
        ));
    }

}
