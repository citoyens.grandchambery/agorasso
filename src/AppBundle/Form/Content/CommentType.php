<?php

namespace AppBundle\Form\Content;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use AppBundle\Entity\Content\Comment;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CommentType extends AbstractType {

    protected $tokenStorage;
    protected $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationChecker $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $close = $options['close'];
        if(!$close) {
            $builder
                ->add('description', CKEditorType::class, array(
                        'config_name' => 'simple_config',
                        'required' => true,
                        'label' => "Commentaire"

                    )
                );
        }

        if($close and $this->authorizationChecker->isGranted('ROLE_CITIZEN')) {
            $builder->add('status', ChoiceType::class, array(
                'label' => "Statut",
                'required' => true,
                'choices' => Comment::$statusesChoices,
            ))
                ->add('closedReason',                       CKEditorType::class, array(
                        'config_name' => 'simple_config',
                        'required' => false,
                        'label' => "Raison de la clôture"

                    )
                );
        }
        $builder->add('submit', SubmitType::class, array(
            'label' => 'Enregistrer'
        ));
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content\Comment',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
            'close' => null
        ));
    }

}
