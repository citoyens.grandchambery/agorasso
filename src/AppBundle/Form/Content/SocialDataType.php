<?php

namespace AppBundle\Form\Content;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\Content\SocialData;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class SocialDataType extends AbstractType {

    protected $authorizationChecker;
    protected $dataManager;

    public function __construct(AuthorizationChecker $authorizationChecker, $dataManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->dataManager = $dataManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {

        $socialData = $builder->getData();
        $builder->add('lastName',
            TextType::class, array(
                'required' => true,
                'label' => 'Nom'))
            ->add('firstName',
                TextType::class, array(
                    'required' => true,
                    'label' => 'Prénom'));
        $builder
            ->add('gender', ChoiceType::class, array(
                'required' => true,
                'choices' => SocialData::$gendersChoices,
                'label' => "Genre",
            ))
            ->add('age',
                NumberType::class, array(
                    'required' => true,
                    'label' => 'Âge'))
            ->add('activity', ChoiceType::class, array(
                'required' => true,
                'choices' => SocialData::$activitiesChoices,
                'label' => "Activité",
            ));
        $builder->add('phone',
            TextType::class, array(
                'required' => false,
                'label' => 'Téléphone'))
                ->add('email',
            EmailType::class, array(
                'required' => false,
                'label' => 'E-mail'));
        $builder->add('address',
            TextType::class, array(
                'required' => false,
                'label' => 'Adresse'));
        $builder->add('postalCode',
            TextType::class, array('required' => false,'label' => 'Code postal'));
        $hasTown = $this->dataManager->hasTowns();
        if($hasTown) {
            $builder->add('town', EntityType::class, array(
                'class' => 'AppBundle\Entity\Location\Town',
                'label' => 'Commune',
                'placeholder' => 'Sélectionnez une commune',
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryTowns();
                },
            ));
        } else {
            $builder->add('city',
                TextType::class, array(
                    'required' => true,
                    'label' => 'Commune'));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content\SocialData',
//            'attr' => [
//                'data-ajax-form' => '',
//                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
//            ],
        ));
    }

}
