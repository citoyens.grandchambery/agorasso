<?php

namespace AppBundle\Form\Content;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use AppBundle\Entity\Content\Proposal;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProposalType extends AbstractType {

    protected $tokenStorage;
    protected $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationChecker $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder
            ->add('title',                       CKEditorType::class, array(
                    'config_name' => 'simple_config',
                    'required' => true,
                    'label' => "Proposition"

                )
            )
            ->add('referedGroups', EntityType::class, [
                'class' => 'AppBundle:Group\Group',
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryProposalGroups();
                },
                'label'     => 'Groupes référents',
                'expanded'  => true,
                'multiple'  => true,
                'required' => true,
            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;
        if($this->authorizationChecker->isGranted('ROLE_CITIZEN')) {
            $builder->add('status', ChoiceType::class, array(
                'label' => "Statut",
                'required' => true,
                'choices' => Proposal::$statusesChoices,
            ))
                ->add('closedReason',                       CKEditorType::class, array(
                        'config_name' => 'simple_config',
                        'required' => false,
                        'label' => "Raison de la clôture"

                    )
                );
        }
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content\Proposal',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
        ));
    }

}
