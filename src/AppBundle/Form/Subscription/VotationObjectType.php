<?php

namespace AppBundle\Form\Subscription;

use AppBundle\Object\VotationObject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\Subscription\VotationSubType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class VotationObjectType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('supervisedVotationSubs',                  CollectionType::class, array(
                'label' => false,
                'entry_type' => VotationSubType::class,
                'label_attr' => array('class' => 'aligned'),
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Voter'
            ));  
        ;
        
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Object\VotationObject',
        ));
    }
}
