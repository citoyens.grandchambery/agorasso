<?php

namespace AppBundle\Form\Subscription;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType; 
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use blackknight467\StarRatingBundle\Form\RatingType;


class VotationSubType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $votationSub = $event->getData();
//                $form->add('weight', TextType::class, array(
//                    'required' => false,
//                ));
            $form->add('weight', RatingType::class, [
                //...
                'stars' => $votationSub->getReferedVotation()->getMaxWeight(),
                'label' => 'Vote(s)',
                'required' => false,
                //...
            ]);

        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Subscription\VotationSub',
//            'attr' => [
//                'data-ajax-form' => '',
//                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
//            ],
        ));
    }

}
