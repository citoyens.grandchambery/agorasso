<?php

namespace AppBundle\Form\Subscription;

use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\User\Subscription;

class CandidateSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('label', TextType::class, array(
                'label' => 'Nom / prénom',
            ))
            ->add('type', ChoiceType::class, array(
                'required' => false,
                'choices' => Subscription::$typesChoices,
                'label' => "Type de candidature"
            ))
            ->add('status', ChoiceType::class, array(
                'required' => false,
                'choices' => Subscription::$statusesChoices,
                'label' => "Statut"
            ))
            ->add('sortOrder', ChoiceType::class, array(
                'required' => true,
                'choices' => array(
                    'Par inscription la plus récente' => 'dateDesc',
                    'Par inscription la plus ancienne' => 'dateAsc',
                    'Par ordre alphabétique nom/prénom' => 'nameAsc',),
                'label' => 'Triées par'
            ))
            ->add('search', SubmitType::class, array(
                'label' => 'Rechercher',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }


}
