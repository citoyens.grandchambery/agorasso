<?php

namespace AppBundle\Form\Monetic;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Monetic\Payment;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PaymentSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('dateBefore', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'label' => 'Date avant',
                'required' => false
            ))
            ->add('dateAfter',        DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'label' => 'Date après',
                'required' => false
            ))
            ->add('origin', ChoiceType::class, array(
                'required' => false,
                'choices' => Payment::$originChoices,
                'label' => "Origine"
            ))
            ->add('type', ChoiceType::class, array(
                'required' => false,
                'choices' => Payment::$typeChoices,
                'label' => "Type"
            ))
            ->add('search', SubmitType::class, array(
                'label' => 'Rechercher',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }


}
