<?php

namespace AppBundle\Form\Monetic;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use AppBundle\Entity\Monetic\Payment;

class PaymentType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        
        $payment = $builder->getData();
        
        $builder
            ->add('reference', TextType::class, array(
                'label' => 'Référence du paiement',
                'required' => true
            ))
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'required' => true,
                'label' => "Date",
            ));
            $builder->add('email',
                EmailType::class, array(
                    'required' => true,
                    'label' => 'E-mail'));
        $builder->add('amount', MoneyType::class, array(
            'required' => true,
            'currency' => 'EUR',
        ))
            ->add('mode', ChoiceType::class, array(
                'required' => true,
                'choices' => Payment::$modeChoices,
                'label' => "Mode de paiement"
            ))
            ->add('type', ChoiceType::class, array(
                'required' => true,
                'choices' => Payment::$typeChoices,
                'label' => "Type de paiement"
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Monetic\Payment',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
        ));
    }

}
