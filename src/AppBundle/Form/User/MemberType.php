<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use AppBundle\Manager\DataManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class MemberType extends AbstractType
{
    protected $authorizationChecker;
    protected $dataManager;

    public function __construct(AuthorizationChecker $authorizationChecker, $dataManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->dataManager = $dataManager;
    }

    protected function dataCanRead($user, $isEdit, $type)
    {
        if(!$isEdit)
            return true;
        return $this->dataManager->dataCanRead($user, $type);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $builder->getData();
        $type = $options['formType'];
        $isContact = $type[0] == 'contact';
        $isEdit = $options['step'] == 'edit';
        $builder->add('name',
            TextType::class, array(
                'required' => true,
                'label' => 'Nom'))
            ->add('firstName',
                TextType::class, array(
                    'required' => true,
                    'label' => 'Prénom'));
        if(!$isContact)
            $builder->add('userName',
                TextType::class, array(
                    'required' => false,
                    'label' => 'Nom d\'utilisateur.trice'));
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN') or
            ($isContact and !$isEdit)) {
                $builder->add('email',
                    EmailType::class, array(
                        'required' => true,
                        'label' => 'E-mail'));
        }
        if ($this->authorizationChecker->isGranted('ROLE_CITIZEN') and $isContact and !$isEdit) {
            $builder
                ->add('roles', ChoiceType::class, array(
                    'required' => true,
//                        'choices' => array('Curieu.x.se' => 'ROLE_CONTACT', 'Animateur.trice' => 'ROLE_CITIZEN'),
                    'choices' => $this->getRoles(),
                    'label' => "Rôle",
                    'multiple' => true,
                    'expanded' => false,
                ));
        }
        if($this->dataCanRead($user, $isEdit,DataManager::TYPE_PHONE))
            $builder->add('phone',
                    TextType::class, array(
                        'required' => false,
                        'label' => 'Téléphone'));
        if($this->dataCanRead($user, $isEdit,DataManager::TYPE_ADDRESS))
            $builder->add('address',
                TextType::class, array(
                    'required' => false,
                    'label' => 'Adresse'));
        $builder->add('postalCode',
            TextType::class, array('required' => false,'label' => 'Code postal'));

        $hasTown = $this->dataManager->hasTowns();
        if($hasTown) {
            $builder->add('town', EntityType::class, array(
                'class' => 'AppBundle\Entity\Location\Town',
                'label' => 'Commune',
                'placeholder' => 'Sélectionnez une commune',
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryTowns();
                },
            ));
            $formUpdate = function(FormInterface $form, $town = null) {
                if ($town and !$town->getOther()) {
                    $form->add('district', EntityType::class, array(
                        'required' => $this->dataManager->hasDistrict($town) ? true : false,
                        'class' => 'AppBundle\Entity\Location\District',
                        'label' => 'Quartier',
                        'placeholder' => 'Sélectionnez un quartier',
                        'query_builder' => function (EntityRepository $er) use ($town) {
                            return $er->queryDistricts($town);
                        }
                    ));
                } else {
                    $form->add('district', EntityType::class, array(
                        'required' => true,
                        'class' => 'AppBundle\Entity\Location\District',
                        'label' => 'Quartier',
                        'query_builder' => function (EntityRepository $er) use ($town) {
                            return $er->queryDistricts($town);
                        }
                    ));
                }
                if($town and $town->getOther()) {
                    $form->
                    add('city',
                        TextType::class, array('required' => true, 'label' => 'Précisions'));
                } else {
                    $form->
                    add('city',
                        TextType::class, array('required' => false, 'label' => 'Précisions'));
                }
            };

            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formUpdate) {
                $user = $event->getData();
                if($user)
                    $town = $user->getTown();

                $formUpdate($event->getForm(), $town);
            });
            $builder->get('town')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formUpdate) {
                $town = $event->getForm()->getData();

                $formUpdate($event->getForm()->getParent(), $town);
            });
        } else {
            $builder->
            add('city',
                TextType::class, array('required' => $hasTown ? false : true, 'label' => $hasTown ? 'Précision' : 'Commune'));
        }

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
//                'novalidate' => 'novalidate'
            ],
            'formType' => array('perso'),
            'step' => 'new'
        ));
    }

    protected function getRoles()
    {
        $roles = array();

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {

            foreach (User::$customRoleChoices as $param => $val) {
                if ($this->authorizationChecker->isGranted($param)) {
                    $roles[$val] = $param;
                }
            }
        } else
            $roles['Curieu.x.se'] = 'ROLE_CONTACT';
        return $roles;
    }
}
