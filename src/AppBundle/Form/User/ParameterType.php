<?php

namespace AppBundle\Form\User;

use AppBundle\Manager\DataManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ParameterType extends AbstractType
{
    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
                $builder
                    ->add('addressVisibility', ChoiceType::class, array(
                        'required' => true,
                        'choices' => DataManager::$authChoices,
                        'label' => "Adresse"
                    ))
                    ->add('emailVisibility', ChoiceType::class, array(
                        'required' => true,
                        'choices' => DataManager::$authChoices,
                        'label' => "Courriel"
                    ))
                    ->add('phoneVisibility', ChoiceType::class, array(
                        'required' => true,
                        'choices' => DataManager::$authChoices,
                        'label' => "Téléphone"
                    ))
                    ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
            ],
        ));
    }
}
