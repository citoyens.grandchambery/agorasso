<?php

namespace AppBundle\Form\User;

use AppBundle\Entity\User\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use AppBundle\Manager\DataManager;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class SubscriptionType extends AbstractType
{

    protected $dataManager;
    protected $server;

    public function __construct($dataManager, $server)
    {
        $this->dataManager = $dataManager;
        $this->server = $server;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type = $options['formType'];
        if($type == 'follow') {
            $builder->add('type', ChoiceType::class, array(
                'required' => true,
                'choices' => Subscription::$typesChoices,
                'label' => "Type de candidature",
            ));
        }
        $builder->add('name', TextType::class, array(
            'label' => 'Nom'
        ))
            ->add('firstName', TextType::class, array(
                'label' => 'Prénom'
            ));
        if($type == 'candidate' or $type == 'volunteer' or $type == 'follow') {
            $builder->add('gender', ChoiceType::class, array(
                'required' => true,
                'choices' => Subscription::$gendersChoices,
                'label' => "Civilité",
            ))
                ->add('birthDate', DateType::class, array(
//                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'years' => range(date('Y') - 17, date('Y') - 100),
//                    'attr' => array('class' => 'date', "onkeydown" => "return false"),
                    'label' => 'Date de naissance',
                    'required' => false
                ));
        }
        if($type == 'follow') {
            $builder->add('job', TextType::class, array(
                'label' => 'Profession',
                'required' => false
            ));
            $builder            ->add('commitment', checkboxType::class, array(
                'label' => 'Engagement syndical ou politique',
                'data' => false,
                'required' => false,
            ));
        }
        if($type == 'candidate' or $type == 'volunteer') {
            if($type != 'volunteer') {
                $builder->
                add('userAware', ChoiceType::class, array(
                    'required' => true,
                    'choices' => Subscription::$userAwareChoices,
                    'label' => "La personne concernée est-elle au courant de votre démarche ?",
                ));
            }
                $builder->add('motivation', CKEditorType::class, array(
                        'config_name' => 'simple_config',
                        'required' => true,
                        'label' => $type == 'volunteer' ? "Mes motivations pour être volontaire" : "Pourquoi je propose cette personne à la candidature"
                    )
                );

        }
            $builder->add('email', TextType::class, array(
                'label' => 'E-mail',
                'constraints' => [
                    new NotBlank(),
                    new Regex(array(
                        'pattern' => "/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
                        'message' => "Votre adresse email est invalide ou utilise des caractères interdits !"
                    ))
                ],
            ));
        if($type == 'sponsor' or $type == 'volunteer' or $type == 'follow') {
            $builder->add('phone', TextType::class, array(
                'label' => 'Téléphone',
                'required' => false
            ));
        }
        if($type == 'volunteer' or $type == 'follow') {
            $builder->add('address', TextType::class, array(
                'label' => 'Adresse',
                'required' => $type == 'follow' ? false : true
            ))
                ->add('postalCode', TextType::class, array(
                    'label' => 'Code postal',
                    'required' => $type == 'follow' ? false : true
                ));
        }
        if($type == 'sponsor') {
            $builder->add('submit', SubmitType::class, array(
                    'label' => 'Suivant'
                )
            );
        } elseif($type == 'candidate' or $type == 'volunteer') {
            $builder->
            add('getNoticed', checkboxType::class, array(
                'label' => 'Oui, je souhaite m\'enregistrer sur le site du Mouvement Citoyen',
                'data' => false,
                'required' => $type == 'contact'
            ))
                ->add('submit', SubmitType::class, array(
                        'label' => 'Enregistrer'
                    )
                );
            if ($this->server == 'preprod' or $this->server == 'prod') {
                $builder->add('recaptcha', EWZRecaptchaType::class, array(
                    'mapped' => false,
                    'constraints' => array(new RecaptchaTrue(["message" => "Veuillez remplir ce champ."])),
                ));
            }
        } elseif($type == 'follow') {
            $builder
                ->add('submit', SubmitType::class, array(
                        'label' => 'Enregistrer'
                    )
                );
        } else {
            $builder->
            add('getNoticed', checkboxType::class, array(
                'label' => 'Oui, je souhaite rester informé.e',
                'data' => false,
                'required' => $type == 'contact'
            ))
                ->add('submit', SubmitType::class, array(
                        'label' => 'Enregistrer'
                    )
                );
        }

        $hasTown = $this->dataManager->hasTowns();
        if($hasTown) {
            $builder->add('town', EntityType::class, array(
                'class' => 'AppBundle\Entity\Location\Town',
                'label' => 'Commune',
                'required' => true,
                'placeholder' => 'Sélectionnez une commune',
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryTowns();
                },
            ));
            $formUpdate = function(FormInterface $form, $town = null) {
                if ($town and !$town->getOther()) {
                    $form->add('district', EntityType::class, array(
                        'required' => $this->dataManager->hasDistrict($town) ? true : false,
                        'placeholder' => 'Sélectionnez un quartier',
                        'class' => 'AppBundle\Entity\Location\District',
                        'label' => 'Quartier',
                        'query_builder' => function (EntityRepository $er) use ($town) {
                            return $er->queryDistricts($town);
                        }
                    ));
                } else {
                    $form->add('district', EntityType::class, array(
                        'required' => false,
                        'class' => 'AppBundle\Entity\Location\District',
                        'label' => 'Quartier',
                        'query_builder' => function (EntityRepository $er) use ($town) {
                            return $er->queryDistricts($town);
                        }
                    ));
                }
                if($town and $town->getOther()) {
                    $form->
                    add('city',
                        TextType::class, array('required' => true, 'label' => 'Précisions'));
                } else {
                    $form->
                    add('city',
                        TextType::class, array('required' => false, 'label' => 'Précisions'));
                }
            };

            $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($formUpdate) {
                $user = $event->getData();
                if($user)
                    $town = $user->getTown();

                $formUpdate($event->getForm(), $town);
            });
            $builder->get('town')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($formUpdate) {
                $town = $event->getForm()->getData();

                $formUpdate($event->getForm()->getParent(), $town);
            });
        } else {
            $builder->
            add('city',
                TextType::class, array('required' => $hasTown ? false : true, 'label' => $hasTown ? 'Précision' : 'Commune'));
        }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User\Subscription',
            'validation_groups' => ['Default'],
            'formType' => 'contact',
//            'attr'=>array('novalidate'=>'novalidate')
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_subscription';
    }
}
