<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use AppBundle\Entity\User\Operation;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OperationType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder->add('operation', ChoiceType::class, array(
            'label' => "Type d'opération",
            'required' => true,
            'choices' => Operation::$operationsChoices,
        ));
            $builder
                ->add('content', CKEditorType::class, array(
                        'config_name' => 'simple_config',
                        'required' => true,
                        'label' => "Commentaire"

                    )
                );


        $builder->add('submit', SubmitType::class, array(
            'label' => 'Enregistrer'
        ));
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User\Operation',
            'close' => null
        ));
    }

}
