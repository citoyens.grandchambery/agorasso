<?php

namespace AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;

class ConfirmSubscriptionType extends AbstractType
{
    protected $server;

    public function __construct($server)
    {
        $this->server = $server;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('plainPassword', RepeatedType::class, array(
            'type' => PasswordType::class,
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'form.password'),
            'second_options' => array('label' => 'form.password_confirmation'),
            'invalid_message' => 'fos_user.password.mismatch',
        ))
        ->add('submit', SubmitType::class, array(
            'label' => 'Confirmer'
        ));
        if($this->server == 'preprod' or $this->server == 'prod') {
            $builder->add('recaptcha', EWZRecaptchaType::class, array(
                'mapped' => false,
                'constraints' => array(new RecaptchaTrue(["message" => "Veuillez remplir ce champ."])),
            ));
        }
    }

}
