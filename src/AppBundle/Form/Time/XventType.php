<?php

namespace AppBundle\Form\Time;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Group\Group;
use AppBundle\Repository\GroupRepository;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class XventType extends AbstractType {

    protected $tokenStorage;
    protected $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationChecker $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        
        $builder
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'required' => false,
                'label' => "Date",
            ))
            ->add('poll', TextType::class, array(
                'required'      => false,
                'label' => 'Sondage date',
            ))
            ->add('hour', TextType::class, array(
                'required'      => false,
                'label' => 'Heure',
            ))
            ->add('title', TextType::class, array(
                'required' => true,
                'label' => 'Titre',
            ))
            ->add('sub_title', TextType::class, array(
                'required' => true,
                'label' => 'Description courte',
            ))
            ->add('description',                       CKEditorType::class, array(
                'config_name' => 'simple_config',
                'required' => false,
                'label' => "Description"
            ))
            ->add('place', TextType::class, array(
                'required' => false,
                'label' => 'Lieu',
            ))
            ->add('referedGroups', EntityType::class, [
                'class' => 'AppBundle:Group\Group',
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryAll(1);
                },
                'label'     => 'Groupes référents',
                'expanded'  => true,
                'multiple'  => true,
                'required' => true,
            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;

        if($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $builder->add('public', CheckboxType::class, array(
                'required' => false,
                'label' => "Public"
            ));
        }
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Time\Xvent',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
        ));
    }

}
