<?php

namespace AppBundle\Form\Time;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class InfoType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        
        $file = $builder->getData(); 
        
        $builder
            ->add('title', TextType::class, array(
                'label' => 'Titre de l\'info',
            ))
            ->add('file', FileType::class, array(
                'required'      => false,
                'label' => $file->getid()?'Nouveau fichier':'Fichier',
            ))
            ->add('link', TextType::class, array(
                'required' => false,
                'label' => 'lien',
            ))
            ->add('description',                       CKEditorType::class, array(
                    'config_name' => 'simple_config',
                    'required' => true,
                    'label' => "Description"

                )
            )
            ->add('referedGroups', EntityType::class, [
                'class' => 'AppBundle:Group\Group',
                'query_builder' => function (EntityRepository $er) {
                    return $er->queryInfoGroups();
                },
                'label'     => 'Groupes référents',
                'expanded'  => true,
                'multiple'  => true,
                'required' => true,
            ])
            ->add('submit', SubmitType::class, array(
                'label' => 'Enregistrer'
            ))
        ;
    }
  
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Time\Info',
            'attr' => [
                'data-ajax-form' => '',
                'data-ajax-form-target' => '#bootstrap-modal .modal-content',              
            ],
        ));
    }

}
