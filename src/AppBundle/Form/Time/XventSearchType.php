<?php

namespace AppBundle\Form\Time;

use AppBundle\Repository\GroupRepository;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Time\Xvent;

class XventSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('label', TextType::class, array(
                'label' => 'Libellé / Description',
            ))
            ->add('dateOrder', ChoiceType::class, array(
                'required' => true,
                'choices' => array('Dates décroissantes' => 'DESC',
                    'Dates croissantes' => 'ASC'),
                'label' => "Classés par"
            ))
            ->add('dateAfter',        DateType::class, array(
//                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'years' => range(date('Y') + 1, date('Y') - 3),
//                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'label' => 'Date après',
                'required' => false
            ))
            ->add('dateBefore', DateType::class, array(
//                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'years' => range(date('Y') + 1, date('Y') - 3),
//                'attr' => array('class' => 'date', "onkeydown" => "return false"),
                'label' => 'Date avant',
                'required' => false
            ))
            ->add('timeline', ChoiceType::class, array(
                'required' => true,
                'choices' => Xvent::$timelineChoices,
                'label' => "Événements"
            ))
//            ->add('group', TextType::class)
            ->add('group', EntityType::class, array(
                'class' => 'AppBundle\Entity\Group\Group',
                'label' => 'Groupe',
                'required' => false,
                'query_builder' => function (GroupRepository $er) {
                    $qb = $er->createQueryBuilder('g')
                        ->orderBy('g.name', 'ASC');
                    return $qb;
                },
            ))
            ->add('search', SubmitType::class, array(
                'label' => 'Rechercher',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'attr'=>array('novalidate'=>'novalidate')
        ));
    }


}
