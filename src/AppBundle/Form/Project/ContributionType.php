<?php

namespace AppBundle\Form\Project;

use AppBundle\Entity\Project\Contribution;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\Content\SocialData;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ContributionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) 
    {
        $builder->add('title',
            TextType::class, array(
                'required' => false,
                'label' => 'Titre'));
        $builder
            ->add('type', ChoiceType::class, array(
                'required' => true,
                'choices' => Contribution::$personsChoices,
                'label' => "Type de contribution",
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Project\Contribution',
//            'attr' => [
//                'data-ajax-form' => '',
//                'data-ajax-form-target' => '#bootstrap-modal .modal-content',
//            ],
        ));
    }

}
