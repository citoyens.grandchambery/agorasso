<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 16/09/19
 * Time: 16:30
 */

namespace AppBundle\Object;

use AppBundle\Entity\Group\Group;


class EventSearch
{
    protected $label;
    protected $dateAfter;
    protected $dateBefore;
    protected $dateOrder;
    protected $timeline;
    protected $group;
    protected $change;
    
    function __construct()
    {
        $this->change = false;
    }

    public function setChange($change)
    {
        $this->change = $change;

        return $this;
    }

    public function getChange()
    {
        return $this->change;
    }

    protected function check($before, $after)
    {
        if($before and $after) {
            if($before != $after) {
                $this->setChange(true);
            }
        }
        elseif($before != $after) {
            $this->setChange(true);
        }
    }
    
    public function setLabel($label)
    {
        $this->check($this->label, $label);
        $this->label = $label;

        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setDateOrder($dateOrder)
    {
        $this->check($this->dateOrder, $dateOrder);
        $this->dateOrder = $dateOrder;

        return $this;
    }

    public function getDateOrder()
    {
        return $this->dateOrder;
    }

    public function setDateAfter($dateAfter)
    {
        $this->check($this->dateAfter, $dateAfter);
        $this->dateAfter = $dateAfter;

        return $this;
    }

    public function getDateAfter()
    {
        return $this->dateAfter;
    }

    public function setDateBefore($dateBefore)
    {
        $this->check($this->dateBefore, $dateBefore);
        $this->dateBefore = $dateBefore;

        return $this;
    }

    public function getDateBefore()
    {
        return $this->dateBefore;
    }

    public function setTimeline($timeline)
    {
        $this->check($this->timeline, $timeline);
        $this->timeline = $timeline;

        return $this;
    }

    public function getTimeline()
    {
        return $this->timeline;
    }

    public function setGroup(Group $group)
    {
        $this->check($this->group, $group);
        $this->group = $group;

        return $this;
    }

    public function getGroup()
    {
        return $this->group;
    }
}