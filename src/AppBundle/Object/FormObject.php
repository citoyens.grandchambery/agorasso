<?php

namespace AppBundle\Object;

use AppBundle\Entity\Content\Answer;
use AppBundle\Entity\Content\Response;
use AppBundle\Entity\Content\PublicForm;
use AppBundle\Entity\Project\Contribution;

class FormObject
{
    private $responses;

    public function __construct(PublicForm $form, Contribution $contribution = null)
    {
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($form->getAnswers() as $answer) {
            $response = null;
            if($contribution) {
                $response = $contribution->getResponse($answer);
            }
            if($response) {
                $this->addResponse($response);
            } else {
                $response = new Response();
                $response->setWeight($answer->getWeight());
                $response->setAnswer($answer);
                $this->addResponse($response);
            }
        }
    }

    /**
     * Add response
     *
     * @param \AppBundle\Entity\Content\Response $response
     *
     * @return FormObject
     */
    public function addResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses[] = $response;

        return $this;
    }

    /**
     * Remove response
     *
     * @param \AppBundle\Entity\Content\Response $response
     */
    public function removeResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses->removeElement($response);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }
}