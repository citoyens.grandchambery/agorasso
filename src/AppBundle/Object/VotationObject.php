<?php

namespace AppBundle\Object;

use AppBundle\Entity\Subscription\VotationSub;
use AppBundle\Entity\Subscription\Votation;
use AppBundle\Entity\User as User;

class VotationObject
{
    private $supervisedVotationSubs;

    public function __construct(Votation $votation)
    {
        $this->supervisedVotationSubs = new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($votation->getReferedProposals() as $proposal) {
            $sub = new VotationSub();
            $sub->setReferedProposal($proposal);
            $sub->setReferedVotation($votation);
            $sub->setWeight(0);
            $this->addSupervisedVotationSub($sub);
        }
    }

    /**
     * Add supervisedVotationSub
     *
     * @param \AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub
     *
     * @return Votation
     */
    public function addSupervisedVotationSub(\AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub)
    {
        $this->supervisedVotationSubs[] = $supervisedVotationSub;

        return $this;
    }

    /**
     * Remove supervisedVotationSub
     *
     * @param \AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub
     */
    public function removeSupervisedVotationSub(\AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub)
    {
        $this->supervisedVotationSubs->removeElement($supervisedVotationSub);
    }

    /**
     * Get supervisedVotationSubs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedVotationSubs()
    {
        return $this->supervisedVotationSubs;
    }

    public function getVotes()
    {
        $number = 0;
        foreach ($this->getSupervisedVotationSubs() as $sub) {
            $number += $sub->getWeight();
        }
        return $number;
    }
}