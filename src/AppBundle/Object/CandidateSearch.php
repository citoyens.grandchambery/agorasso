<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 16/09/19
 * Time: 16:30
 */

namespace AppBundle\Object;


class CandidateSearch
{
    protected $label;
    protected $type;
    protected $status;
    protected $sortOrder;
    protected $change;
    
    function __construct()
    {
        $this->change = false;
    }

    public function setChange($change)
    {
        $this->change = $change;

        return $this;
    }

    public function getChange()
    {
        return $this->change;
    }

    protected function check($before, $after)
    {
        if($before and $after) {
            if($before != $after) {
                $this->setChange(true);
            }
        }
        elseif($before != $after) {
            $this->setChange(true);
        }
    }
    
    public function setLabel($label)
    {
        $this->check($this->label, $label);
        $this->label = $label;

        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setType($type)
    {
        $this->check($this->type, $type);
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setStatus($status)
    {
        $this->check($this->status, $status);
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setSortOrder($sortOrder)
    {
        $this->check($this->sortOrder, $sortOrder);
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getSortOrder()
    {
        return $this->sortOrder;
    }
}