<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;


use AppBundle\Entity\Project\Project;
use AppBundle\Entity\Project\Section;
use AppBundle\Entity\Content\Proposal;

class ProjectImportCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this
            ->setDescription('Import d\'un projet.')
            ->setName('app:project:import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();

        $output->writeln(sprintf('<info>%s %s</info>', $date->format('Y-m-d H:i:s'), $this->getDescription()));
        $this->projectImport($output);
    }

    protected function lineType($line)
    {
        $text = trim($line, " \t");
        if($text[0] == "\n") {
            return 'empty';
        }
        return '';
    }

    protected function analyzeLine(&$text)
    {
        $text = trim($text, " \t\n");
        
        $results = explode(" ", $text);
        if(count($results) < 1) {
            return 'empty';
        }
        if(preg_match("/[0-9]+\.[0-9]+\./", $results[0])) {
            return 'subsection';
        }
        if(preg_match("/S[0-9]+\./", $results[0])) {
            $text = trim($text, "S");
            return 'section';
        }
        if(preg_match("/[0-9]+\./", $results[0])) {
            return 'proposition';
        }
        if(preg_match("/[0-9]+\/$/", $results[0])) {
            return 'explication';
        }
        if(preg_match("/soumises/", $text) and
            preg_match("/Propositions/", $text) and
            preg_match("/assemblée/", $text)) {
            return 'propositions';
        }
        return 'description';
    }

    protected function getExplicationText(&$text)
    {
        $results = explode(" ", $text);

        $text = substr($text, strlen($results[0]), strlen($text) - strlen($results[0]));
    }

    protected function getProposal(&$text)
    {
        $results = explode(".", $text);
        $proposal_number = $results[0];

        $this->getExplicationText($text);

        return $proposal_number;
    }

    protected function closeSubsection($em, &$subsection, $description)
    {
        if($subsection and $description) {
            $subsection->setDescription($description);
        }
        if($subsection) {
            $em->persist($subsection);
            $this->output->writeln(sprintf('persist subsection %s', $subsection->getTitle()));
        }
    }
    protected function projectImport(OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $nline = 0;
        $fileName = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import/project.txt';
        if (!file_exists($fileName)) {
            $this->output->writeln(sprintf('Pas de fichier « %s » trouvé.', $fileName));
            return;
        }

        $handle = fopen($fileName, "r");
        if ($handle) {
            $project = new Project();
            $project->setBeginDate(new \Datetime());
            $project->setEndDate(\DateTime::createFromFormat("Y-m-d", "2026-03-15"));
            $project->setTitle("Projet soumis à l'assemblée le 9 janvier 2020");
            $project->setShortDescription("Chambé citoyenne, pour une ville écologique et solidaire");
            $project->setDescription("Chambé citoyenne, pour une ville écologique et solidaire");
            $em->persist($project);
            $em->flush();
//            $section = new Section();
//            $section->setTitle('test');
//            $section->setWeight(0);
//            $section->setProject($project);
//            $em->persist($section);
//            $project->addSection($section);


            $section = null;
            $subsection = null;
            $description = null;
            $weightSubsection = 0;
            $proposal = null;
            $number = 1;
            $explication = array();
            $explication_number = 0;
            while (($line = fgets($handle)) !== false) {

                // process the line read.
                if($this->lineType($line) != 'empty') {
                    $text = $line;
                    $type = $this->analyzeLine($text);
                    $this->output->writeln(sprintf('Line %d (%s) : %s', $nline, $type, $text));
                    switch($type) {
                        case 'section':
                            $weight = 0;
//                            $this->closeSubsection($em, $subsection, $description);
                            if(!$subsection and $description and $section) {
                                $section->setDescription($description);
                                $description = null;
                            }
                            $subsection = null;
                            $description = null;
                            if($section) {
//                                $em->persist($section);
                                $weight = $section->getWeight() + 1;
                            }
                            $section = new Section();
                            $section->setTitle($text);
                            $section->setWeight($weight);
                            $section->setProject($project);
                            $em->persist($section);

                            $project->addSection($section);
                            $weightSubsection = 0;
                            break;
                        case 'subsection':
                            $explication_number = 0;
                            $explication = array();
                            if(!$subsection and $description) {
                                $section->setDescription($description);
                                $description = null;
                            }
                            if($subsection and $description) {
                                $subsection->setDescription($description);
                            }
                            if($subsection) {
//                                $em->persist($subsection);
                                $this->output->writeln(sprintf('persist subsection %s', $subsection->getTitle()));
                            }
                            $subsection = new Section();
                            $subsection->setTitle($text);
                            $subsection->setWeight($weightSubsection++);
                            $subsection->setSection($section);
                            $em->persist($subsection);
                            $section->addSubsection($subsection);
                            break;
                        case 'propositions':
                            if($explication_number > 0) {
                                $explication[$explication_number] = $description;
                                $description = null;
                                $this->output->writeln(sprintf('ADD explication %d', $explication_number));
                            }
                            if($subsection and $description) {
                                $subsection->setDescription($description);
                                $description = null;
                            }
                            if($subsection) {
//                                $em->persist($subsection);
                                $this->output->writeln(sprintf('persist subsection %s', $subsection->getTitle()));
                            }
                            break;
                        case 'proposition':
                            $proposal = new Proposal();
                            $proposal_number = $this->getProposal($text);
                            $proposal->setTitle($text);
                            $proposal->setDescription($explication[$proposal_number]);
                            $proposal->setSection($subsection);
                            $proposal->setNumber($number++);
                            $em->persist($proposal);
                            break;
                        case 'explication':
                            if($subsection and $description and $explication_number == 0) {
                                $subsection->setDescription($description);
                                $description = null;
                            }
                            $explication_number++;
                            if($explication_number > 1) {
                                $explication[$explication_number - 1] = $description;
                                $this->output->writeln(sprintf('ADD explication %d', $explication_number - 1));
                                $description = null;
                            }
                            if($text) {
                                if ($description) {
                                    $description = sprintf("%s<br><br><strong>%s</strong>", $description, $text);
                                } else {
                                    $this->getExplicationText($text);
                                    $description = sprintf("<strong>%s</strong>", $text);
                                }
                            }
                            break;
                        case 'description':
                            if($text) {
                                if ($description) {
                                    $description = sprintf("%s<br>%s", $description, $text);
                                } else {
                                    $description = $text;
                                }
                            }
                            break;
                        case 'empty':
                        default:
                            break;
                    }
                }
                $nline++;
            }
//            $this->closeSubsection($em, $subsection, $description);
            if(!$subsection and $description) {
                $section->setDescription($description);
                $description = null;
            }
            $em->flush();
            fclose($handle);
        } else {
            // error opening the file.
        }
    }
}