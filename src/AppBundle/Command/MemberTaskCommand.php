<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 05/06/18
 * Time: 14:37
 */

namespace AppBundle\Command;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Manager\MemberManager;
use Symfony\Component\Console\Input\InputArgument;

class MemberTaskCommand  extends ContainerAwareCommand
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var MemberManager
     */
    private $memberManager;


    protected function configure()
    {
        $this
            ->setDescription("Met à jour les adhésions des membres")
            ->setName('app:member:update')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $start = new \DateTime();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->memberManager = $this->getContainer()->get('app.manager.member');
        $output->writeln(sprintf('<info>%s</info>: début', $this->getDescription()));
        $this->updateMembers();
        $output->writeln(sprintf('<info>%s</info>: effectué en %s secondes', $this->getDescription(),  (new \DateTime())->getTimestamp() - $start->getTimestamp()));
    }

    public function updateMembers()
    {
        $response = $this->memberManager->updatePayments();
        if($response) {
            $payments = json_decode($response);

//            $this->output->writeln(sprintf('<info>%d paiements</info>', count($payments)));

            foreach ($payments->resources as $payment) {
                $this->output->writeln(sprintf('<info>id : %s, date : %s, email : %s</info>', $payment->id, $payment->date, $payment->payer_email));
            }
        } else {
        $this->output->writeln(sprintf('<info>Aucun paiement</info>'));
        }
    }
}