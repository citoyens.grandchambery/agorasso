<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;

use AppBundle\Entity\Location\Town;

class TownImportCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this
            ->setDescription('Import d\'un fichier csv auformat : name;inseeCode;gentile;area;population;populationYear')
            ->setName('app:town:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();
        $output->writeln(sprintf('<info>%s %s</info>', $date->format('Y-m-d H:i:s'), $this->getDescription()));
        $this->townImport($output);
    }

    protected function townImport(OutputInterface $output)
    {

        $fileName = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import/towns.csv';
        if (!file_exists($fileName)) {
            $this->output->writeln(sprintf('Pas de fichier « %s » trouvé.', $fileName));
            return;
        }

        $file = new \SplFileObject($fileName);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $reader = new CsvReader($file, ';');

        foreach ($reader as $row) {
            $name = $row[0];
            $output->writeln(sprintf('<info>%s </info>', $name));

            $town = $em->getRepository('AppBundle:User')
                ->findOneByName($name);
            if ($town == null) {

                $town = new Town();
                $town->setName($name);
                $em->persist($town);
            }
            if($row[1])
                $town->setInseeCode($row[1]);
            if($row[2])
                $town->setGentile($row[2]);
            if($row[3])
                $town->setArea($row[3]);
            if($row[4])
                $town->setPopulation($row[4]);
            if($row[5])
                $town->setPopulationYear($row[5]);

            $em->flush();
        }
    }
}