<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use AppBundle\Entity\Document\DocumentCategory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;
use AppBundle\Manager\DataManager;

use AppBundle\Entity\User;

class ConversionCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this
            ->setDescription('Conversion de la BDD.')
            ->setName('app:bdd:convert');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();
        $output->writeln(sprintf('<info>%s %s</info>', $date->format('Y-m-d H:i:s'), $this->getDescription()));
        $this->bddConvert($output);
    }

    protected function bddConvert(OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $groups =  $em->getRepository('AppBundle:Group\Group')
            ->findAll();

        foreach ($groups as $group) {
            $parent = $group->getParent();
            $group->setParent(null);
            $group->setParent($parent);
        }
        $em->flush();
    }
}