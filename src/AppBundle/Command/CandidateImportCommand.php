<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Component\Console\Input\InputArgument;

use AppBundle\Entity\User;
use AppBundle\Entity\User\Subscription;

class CandidateImportCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        // sample for random : php bin/console app:candidate:import random
        $this
            ->setDescription('Import des candidatures.')
            ->setName('app:candidate:import')
            ->addArgument('type', InputArgument::OPTIONAL, 'Type d\'import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();
        $type = $input->getArgument('type');
        $output->writeln(sprintf('<info>%s : %s %s</info>', $type, $date->format('Y-m-d H:i:s'), $this->getDescription()));

        if($type == 'random') {
            $this->randomImport($output);
        } else {
            $this->userImport($output);
        }
    }

    protected function userImport(OutputInterface $output)
    {

        $fileName = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import/liste.csv';
        if (!file_exists($fileName)) {
            $this->output->writeln(sprintf('Pas de fichier « %s » trouvé.', $fileName));
            return;
        }

        $file = new \SplFileObject($fileName);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $reader = new CsvReader($file, ',');

        foreach ($reader as $row) {
            $firstName = $row[0];
            $name = $row[1];
            $type = $row[2];
            $weight = $row[3];
            $email = $row[4];


            $usr = $em->getRepository('AppBundle:User')
                ->findOneBy(array('firstName' => $firstName, 'name' => $name));
            if($usr == null and $email != '') {
                $usr = $em->getRepository('AppBundle:User')
                    ->findOneBy(array('email' => $email));
            }

            if ($usr != null){
                $output->writeln(sprintf('<info>%s %s : %s</info>', $firstName, $name, $usr->getEmail()));
                $c = new Subscription($type == 'Volontaire' ? Subscription::TYPE_CANDIDATE : Subscription::TYPE_CANDIDATE_SPONSOR);
                $c->setFirstName($firstName);
                $c->setName($name);
                $c->setEmail($usr->getEmail());
                $c->setPhone($usr->getPhone());
                $c->setCity($usr->getCity());
                $c->setPostalCode($usr->getPostalCode());
                $c->setAddress($usr->getAddress());
                $c->setTown($usr->getTown());
                $c->setDistrict($usr->getDistrict());
                $c->setGetNoticed(false);
                if($type != 'Volontaire') {
                    $c->setWeight($weight);
                }
                $em->persist($c);
                $em->flush();
            } else {
                $output->writeln(sprintf('<info>Pas trouvé : %s %s</info>', $firstName, $name));
            }
        }
    }

    protected function randomImport(OutputInterface $output)
    {

        $fileName = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import/tirage_au_sort.csv';
        if (!file_exists($fileName)) {
            $this->output->writeln(sprintf('Pas de fichier « %s » trouvé.', $fileName));
            return;
        }

        $file = new \SplFileObject($fileName);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $reader = new CsvReader($file, ';');
        $town = $em->getRepository('AppBundle:Location\Town')
            ->findOneBy(array('name' => 'Chambéry'));

        foreach ($reader as $row) {
            $gender = $row[0] == 'M.' ? Subscription::GENDER_MALE : Subscription::GENDER_FEMALE;
            $name = $row[1];
            $firstName = $row[2];
            $address = $row[5];
            $district = $em->getRepository('AppBundle:Location\District')
                ->findOneBy(array('name' => $row[8]));

            if(!$district) {
                $output->writeln(sprintf('<error>%s - %s (%s)</error>', $firstName, $name, $district ? $district : 'error' . $row[8]));
            } else {
                $output->writeln(sprintf('<info>%s - %s (%s)</info>', $firstName, $name, $district ? $district : 'error' . $row[8]));
            }
                $c = new Subscription(Subscription::TYPE_RANDOM);
                $c->setGender($gender);
                $c->setFirstName($firstName);
                $c->setName($name);
                $c->setPostalCode('73000');
                $c->setAddress($address);
                $c->setEmail('Inconnu');
                $c->setTown($town);
                $c->setDistrict($district);
                $c->setBirthDate(\DateTime::createFromFormat("d/m/Y",$row[3]));
                $c->setGetNoticed(false);
                $em->persist($c);
                $em->flush();
        }
    }
}