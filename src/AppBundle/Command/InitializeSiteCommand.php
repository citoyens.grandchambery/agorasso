<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\Time\Xvent;
use AppBundle\Entity\Site\Page;

class InitializeSiteCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this
            ->setDescription('Création des éléments de base du site.')
            ->setName('app:site:initialize');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();
        $output->writeln(sprintf('<info>%s %s</info>', $date->format('Y-m-d H:i:s'), $this->getDescription()));
        $this->initializeEvents($output);
        $date = new \DateTime();
        $output->writeln(sprintf('<info>%s Traitement events terminé</info>', $date->format('Y-m-d H:i:s')));
        $this->initializePages();
        $date = new \DateTime();
        $output->writeln(sprintf('<info>%s Traitement pages terminé</info>', $date->format('Y-m-d H:i:s')));
    }

    protected function initializeEvents(OutputInterface $output)
    {
        $events = array(
            '0' => array(
                "title" => "L'impulsion",
                "date" => "2017-03-07",
                "img" => "-",
                "content" => "Un écrit pour dire qui nous sommes, un séminaire estival d'ici l'été, se laisser inspirer",
                "public" => true),
            '1' => array(
                "title" => "Naissance du groupe",
                "date" => "2017-07-08",
                "img" => "-",
                "content" => "Nous faisons de la politique \" mon rêve pour Chambéry \" a été très investi par le groupe, nous avons des idées et de l'envie",
                "public" => true),
            '2' => array(
                "title" => "La belle démocratie",
                "date" => "2017-09-30",
                "img" => "-",
                "content" => "Viser à être en responsabilité. Prendre la mairie avec les citoyens (projet, programme, liste), démarche participative / impliquante",
                "public" => true),
            '3' => array(
                "title" => "L'invitation",
                "date" => "2017-11-11",
                "img" => "-",
                "content" => "Validation des grandes lignes du texte d'invitation pour mise en mouvement élections de mars 2020, repère des grandes étapes",
                "public" => true),
            '4' => array(
                "title" => "L'élargissement",
                "date" => "2017-01-13",
                "img" => "-",
                "content" => "Décision de création d'une association pour pouvoir avancer les aspects logistiques",
                "public" => true),
            '5' => array(
                "title" => "Dynamiques citoyennes: assemblée générale constitutive",
                "date" => "2017-03-03",
                "img" => "-",
                "content" => "L'histoire officielle commence",
                "public" => true),
            '6' => array(
                "title" => "Première réunion publique",
                "date" => "2017-03-22",
                "img" => "-",
                "content" => "Des rencontres, des débats, des tables rondes",
                "public" => true),
        );

        $em = $this->getContainer()->get('doctrine')->getManager();
        foreach ($events as $id => $event) {
            $title = $event['title'];
            $output->writeln(sprintf('<info>%s </info>', $event['title']));

            $evt = $em->getRepository('AppBundle:Time\Xvent')
                ->findOneByTitle($title);
            if ($evt == null) {
                $e = new Xvent();
                $e->setTitle($title)
                    ->setSubTitle($title)
                    ->setDate(\DateTime::createFromFormat('Y-m-d', $event['date']))
                    ->setimage($event['img'])
                    ->setDescription($event['content'])
                    ->setPublic($event['public']);
                $em->persist($e);

            }
        }
        $em->flush();
    }

    protected function initializePages()
    {
        $definitions = array(
            '0' => array(
                "name" => "invitation",
                "color" => "pink",
                "altMenu" => "bla",
                "altMain" => "blabla"
            ),
            '1' => array(
                "name" => "histoire",
                "color" => "yellow",
                "altMenu" => "bla",
                "altMain" => "blabla"
            ),
            '2' => array(
                "name" => "contact",
                "color" => "blue",
                "altMenu" => "bla",
                "altMain" => "blabla"
            ),
            '3' => array(
                "name" => "diagnostic",
                "color" => "green",
                "altMenu" => "bla",
                "altMain" => "blabla"
            ),
        );

        $em = $this->getContainer()->get('doctrine')->getManager();
        foreach ($definitions as $order => $definition) {
            $def = $em->getRepository('AppBundle:Site\Page')
                ->findOneByName($definition['name']);
            if ($def == null) {
                $w = new Page();
                $w->setName($definition['name'])
                    ->setColor($definition['color'])
                    ->setAltMenu($definition['altMenu'])
                    ->setAltMain($definition['altMain']);
                $em->persist($w);

            }
        }
        $em->flush();
    }

}