<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;


use AppBundle\Entity\User;

class UserImportCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this
            ->setDescription('Création des éléments de base du site.')
            ->setName('app:user:import')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();

        $output->writeln(sprintf('<info>%s %s</info>', $date->format('Y-m-d H:i:s'), $this->getDescription()));
        $this->userImport($output);
    }

    protected function userImport(OutputInterface $output)
    {

        $fileName = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import/users.csv';
        if (!file_exists($fileName)) {
            $this->output->writeln(sprintf('Pas de fichier « %s » trouvé.', $fileName));
            return;
        }

        $file = new \SplFileObject($fileName);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $reader = new CsvReader($file, ',');

        foreach ($reader as $row) {
            $email = $row[0];
            $output->writeln(sprintf('<info>%s </info>', $email));

            $usr = $em->getRepository('AppBundle:User')
                ->findOneByEmail($email);
            if ($usr == null){

                $usr = new User(array(
                    "email"=>$email,
                    "firstName"=> $row[1],
                    "name"=> $row[2],
                    "role" => $row[3]));
                if($row[4])
                    $usr->setAddress($row[4]);
                if($row[5])
                    $usr->setCity($row[5]);
                if($row[6])
                    $usr->setPhone($row[6]);
                $em->persist($usr);
                $em->flush();
            } else {
                if(in_array('ROLE_CONTACT', $usr->getRoles()))
                    $usr->setRoles(array($row[3]));
                if($row[4])
                    $usr->setAddress($row[4]);
                if($row[5])
                    $usr->setCity($row[5]);
                if($row[6])
                    $usr->setPhone($row[6]);
                $em->persist($usr);
                $em->flush();
            }
        }
    }
}