<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 15/02/18
 * Time: 10:18
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ddeboer\DataImport\Reader\CsvReader;

use AppBundle\Entity\Group\GroupSubscription;
use AppBundle\Entity\Group;
use AppBundle\Entity\User;

class GroupImportCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this
            ->setDescription('Inscriptions des utilisateurs aux groupes.')
            ->setName('app:group:subscribe');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $date = new \DateTime();
        $output->writeln(sprintf('<info>%s %s</info>', $date->format('Y-m-d H:i:s'), $this->getDescription()));
        $this->groupSubscribe($output);
    }

    protected function groupSubscribe(OutputInterface $output)
    {

        $fileName = $this->getContainer()->get('kernel')->getRootDir() . '/Resources/import/groups.csv';
        if (!file_exists($fileName)) {
            $this->output->writeln(sprintf('Pas de fichier « %s » trouvé.', $fileName));
            return;
        }

        $file = new \SplFileObject($fileName);
        $em = $this->getContainer()->get('doctrine')->getManager();

        $reader = new CsvReader($file, ',');

        foreach ($reader as $row) {
            $email = $row[0];
            $name = $row[1];
            $user = $em->getRepository('AppBundle:User')
                ->findOneByEmail($email);
            $group = $em->getRepository('AppBundle:Group\Group')
                ->findOneBy(array('name' => $name));
            if ($user == null) {
                $output->writeln(sprintf('<error>Email inconnu: %s</error>', $email));
            } else if($group == null) {
                $output->writeln(sprintf('<error>Groupe inconnu: %s</error>', $name));
            } else {
                $referent = $row[2];
                $subscription = $em->getRepository('AppBundle:Group\GroupSubscription')->findGroupSubscription($group, $user);
                if(!$subscription) {
                    $subscription = new GroupSubscription();
                    $subscription->setSubscriber($user);
                    $subscription->setGroup($group);
                }
                $subscription->setActive(true);
                if($referent == '1' or $referent == 'true')
                    $subscription->setReferent(true);
                else
                    $subscription->setReferent(false);
                $em->persist($subscription);
                $output->writeln(sprintf('<info>%s au groupe %s %s</info>', $email, $name, $subscription->getReferent() ? '(référent)' : ''));
            }
        }
        $em->flush();
    }
}