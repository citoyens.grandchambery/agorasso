<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class RemovedListener
{
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof User) {
            return;
        }

        $user = $entity;
        if($user) {
            $entity->setUsernameCanonical($user->getId().'-'.$user->getUsernameCanonical());
            $entity->setUsername($user->getId().'-'.$user->getUsername());
            $entity->setEmail($user->getId().'-'.$user->getEmail());
            $entity->setEmailCanonical($user->getId().'-'.$user->getEmailCanonical());
            $em = $args->getEntityManager();
            $em->flush();
        }
    }
}