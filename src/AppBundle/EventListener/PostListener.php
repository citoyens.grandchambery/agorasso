<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Site\Post;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PostListener
{
    protected function setName(Post $post)
    {
        $name = $post->getTitle();
        $name = str_replace(array('\'', ' ', ',', '(', ')', '<', '>', '@', ':', ';', '\\', '”', '/', '[', ']', '?', '=', '*', '|', '?'), '_', $name);
        $post->setName($name);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Post) {
            return;
        }

        $this->setName($entity);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Post) {
            return;
        }

        $this->setName($entity);
    }
}