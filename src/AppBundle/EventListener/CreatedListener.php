<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Time\BaseTime;
use AppBundle\Entity\Subscription\BaseSubscription;
use AppBundle\Entity\Subscription\Vote;
use AppBundle\Entity\User\Operation;
use AppBundle\Entity\Content\Proposal;
use AppBundle\Entity\Content\Comment;
use AppBundle\Entity\Project\Contribution;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Block;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CreatedListener
{
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    protected function getUser()
    {
        if($this->tokenStorage->getToken())
            return $this->tokenStorage->getToken()->getUser();
        return null;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof BaseTime and
            !$entity instanceof BaseSubscription and
            !$entity instanceof Proposal and
            !$entity instanceof Contribution and
            !$entity instanceof Operation and
            !$entity instanceof Comment and
            !$entity instanceof Vote) {
            return;
        }

        $user = $this->getUser();
        if($user) {
            $entity->setCreatedBy($user);
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof BaseTime and
            !$entity instanceof Contribution and
            !$entity instanceof Proposal and
            !$entity instanceof Block and
            !$entity instanceof Page) {
            return;
        }

        if (!$entity instanceof Block and
            !$entity instanceof Page) {

            $user = $this->getUser();
            if ($user) {
                $entity->setUpdatedBy($user);
            }
        }
        $entity->setUpdatedAt(new \DateTime('now'));
    }
}