<?php
namespace AppBundle\EventListener;

use AppBundle\AppBundle;
use AppBundle\Entity\Group\GroupSubscription;
use AppBundle\Entity\Group\GroupSubscriptionHisto;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\User as User;

class GroupSubscriptionListener
{
    private $tokenStorage;

    public function __construct(TokenStorage $tokenStorage, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    private function getUser()
    {
        if($this->tokenStorage->getToken())
            return $this->tokenStorage->getToken()->getUser();
        return null;
    }

    private function postChange($entity)
    {
        if (!$entity instanceof GroupSubscription) {
            return;
        }

        // to complete
        $h = new GroupSubscriptionHisto($entity, $this->getUser());
        $this->em->persist($h);
        $user = $entity->getSubscriber();
        if($user->hasRole('ROLE_CITIZEN') and $user->getReason() != User::REASON_HISTO and !$user->isReferent() and !$user->hasRole('ROLE_WEB')) {
            $user->setRoles(array('ROLE_FRIEND'));
            $user->setReason('');
        } else if($user->hasRole('ROLE_FRIEND') and $entity->getReferent()) {
            $user->setRoles(array('ROLE_CITIZEN'));
            $user->setReason('User::REASON_REFERENT');
        }
        $this->em->flush();
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postChange($args->getEntity());
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->postChange($args->getEntity());
    }
}