<?php

namespace AppBundle\Controller\Time;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Time\Info;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Form\Time\InfoType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use AppBundle\Form\User\MemberType;

/**
 * Info controller.
 * @Route("/espace/infos")
 */
class InfoController extends AppController
{
    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("{id}/voir", name="info_view")
     * @ParamConverter("info", class="AppBundle:Time\Info", options={"mapping": {"id": "id"}})
     */
    public function viewAction(Request $request, Info $info)
    {
        return $this->render('AppBundle:Info:view.html.twig', array(
            'info' => $info
        ));
    }

    /**
     *  @Security("is_granted('UPDATE_INFO', info)")
     *  @Route("/{id}/modifier", name="info_edit")
     *  @ParamConverter("info", class="AppBundle:Time\Info", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Info $info)
    {
        $form = $this->createForm(InfoType::class, $info, array(
            'action' => $this->generateUrl('info_edit', array('id' => $info->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Info:edit.html.twig', array(
            'info' => $info,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{member_id}/ajouter/{group_id}", name="info_add",
     *     defaults={"group_id": null},)
     * @ParamConverter("member", class="AppBundle:User", options={"mapping": {"member_id": "id"}})
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     * @Security("is_granted('ADD_INFO', member)")
     */
    public function addInfoAction(Request $request, User $member, Group $group = null)
    {
        $info = new Info($member);

        if($group)
            $info->addReferedGroup($group);

        $form = $this->createForm(InfoType::class, $info, array(
            'action' => $this->generateUrl('info_add', array('member_id' => $member->getId(), 'group_id' => $group ? $group->getId() : null)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $info->setCreatedBy($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($info);
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Info:edit.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{id}/voir-fichier", name="file_info_view")
     * @Security("is_granted('ROLE_CONTACT')")
     */
    public function viewAttachedFile(Request $request, Info $info)
    {
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path = $helper->asset($info, 'file');
        $response = new Response(file_get_contents($info->getFile()));
        $response->headers->set('Content-Type', mime_content_type($path));
        $response->headers->set('Content-Disposition', 'inline;filename="'.$info->getOriginalFileName());
        return $response;
    }
}
