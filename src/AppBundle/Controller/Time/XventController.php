<?php
namespace AppBundle\Controller\Time;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Time\Xvent;
use AppBundle\Form\Time\XventType;
use AppBundle\Entity\User;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Form\Time\XventSearchType;
use AppBundle\Object\EventSearch;

/**
 * Xvent controller.
 * @Security("has_role('ROLE_CONTACT')")
 * @route("/espace/evenements")
 */

class XventController extends Controller
{
    protected function manageOwnObjects(EventSearch $filters)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($filters as $key => $value) {
            if (is_object($value) && get_class($value) != 'DateTime') {
                $filters->$key = $em->merge($value);
            }
        }

        return $filters;
    }

    /**
     * @Route("/", name="events")
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('eventPage', 1);
        $session = $this->get('session');
        $filters = new EventSearch(); // On regarde si on a un filtre en session avec lequel initialiser le form
        if ($session->has('eventFilters')) {
            $filters = $this->manageOwnObjects($session->get('eventFilters'));
        } else {
            $datetime = new \DateTime();
            $datetime->add(new \DateInterval('P1M'));
            $filters->setDateBefore($datetime);
        }
        $filters->setChange(false);

        $searchForm = $this->createForm(XventSearchType::class, $filters, array());
        $searchForm->handleRequest($request);
        if($filters->getChange()) {
            $page = 1;
        }
        $session->set('eventFilters', $filters);

        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:Time\Xvent')->findEvents($filters);
        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);
        return $this->render('AppBundle:Event:index.html.twig', array(
            'events' => $pagerfanta,
            'searchForm' => $searchForm->createView()
        ));

    }

    /**
     * @Route(path="/{id}", name="event_view")
     */
    public function viewAction(Request $request, Xvent $event)
    {
        if ($request->isXmlHttpRequest()){
            return $this->render('AppBundle:Event:_view.html.twig', array(
                'event'  => $event,
                'needLink' => true //permet d'ajouter le lien vers la page complète
            ));
        } else {
            return $this->render('AppBundle:Event:view.html.twig', array(
                'event'  => $event,

            ));
        }
    }

    /**
     *  @Security("is_granted('UPDATE_EVENT', event)")
     *  @Route("/{id}/modifier", name="event_edit")
     *  @ParamConverter("event", class="AppBundle:Time\Xvent", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Xvent $event)
    {
        $form = $this->createForm(XventType::class, $event, array(
            'action' => $this->generateUrl('event_edit', array('id' => $event->getId())),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if(!count($event->getReferedGroups()))
            $this->addFlash('danger', sprintf("Veuillez rattacher l'événement à un groupe"));

        elseif ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Event:edit.html.twig', array(
            'event' => $event,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{member_id}/ajouter/", name="event_add")
     * @ParamConverter("member", class="AppBundle:User", options={"mapping": {"member_id": "id"}})
     * })
     * @Security("is_granted('ADD_EVENT', member)")
     */
    public function addEventAction(Request $request, User $member)
    {
        $event = new Xvent();

        $form = $this->createForm(XventType::class, $event, array(
            'action' => $this->generateUrl('event_add', array('member_id' => $member->getId())),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            if (count($event->getReferedGroups()) == 0)
               $this->addFlash('danger', sprintf("Veuillez rattacher l'événement à un groupe"));
            else {
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();

                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
            }
        }
        return $this->render('AppBundle:Event:edit.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }
}