<?php

namespace AppBundle\Controller\Time;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Task\Task;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Form\Task\TaskType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use AppBundle\Form\User\MemberType;
use AppBundle\Form\Task\SearchType;

/**
 * Task controller.
 * @Route("/espace/actions")
 */
class TaskController extends AppController
{
    /**
     * @Route("/", name="task_list")
     * @Security("is_granted('ROLE_FRIEND')")
     */
    public function indexAction(Request $request)

    {
        $isCompact = false;
        $searchForm = $this->createForm(SearchType::class, null, array());
        $searchForm->handleRequest($request);
        $filters = $searchForm->getData();
        if (!$filters) {
            $filters = array('status' => Task::$waitingStatus, 'compact' => array('Vue compacte' => true));
        }

        if($filters['compact'])
            $isCompact = $filters['compact'];


        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:Task\Task')
            ->findTasks($filters, $isCompact);

        if ($searchForm->isSubmitted()) {
            if ($searchForm->getClickedButton() && 'export_csv' === $searchForm->getClickedButton()->getName()) {
                return $this->get('app.manager.export')
                    ->exportCsvTasks($qb->getQuery()->getResult());
            }
        }

        if ($isCompact) {
            $current = null;
            $groups = array();

            $tasks = $qb->getQuery()->getResult();

            foreach ($tasks as $task) {
                if($task->getGroup()->getId() != $current) {
                    $current = $task->getGroup()->getId();
                    $groups[] = $task->getGroup();
                }
            }

            return $this->render('AppBundle:Task:index.html.twig', array(
                'tasks' => $tasks,
                'groups' => $groups,
                'searchForm' => $searchForm->createView(),
                'isCompact' => $isCompact
            ));
        } else {
            $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
            $pagerfanta->setMaxPerPage(20);

            $curPage = $request->get('page');
            if (!$curPage)
                $curPage = 1;
            if ($curPage > $pagerfanta->getNbPages())
                $curPage = 1;
            $pagerfanta->setCurrentPage($curPage);
            return $this->render('AppBundle:Task:index.html.twig', array(
                'tasks' => $pagerfanta,
                'searchForm' => $searchForm->createView(),
                'isCompact' => $isCompact
            ));
        }


    }

    /**
     *  @Security("is_granted('VIEW_TASK', task)")
     *  @Route("{id}/voir/{context}", name="task_view", defaults={"context": "list"})
     * @ParamConverter("task", class="AppBundle:Task\Task", options={"mapping": {"id": "id"}})
     */
    public function viewAction(Request $request, Task $task, $context)
    {
        return $this->render('AppBundle:Task:view.html.twig', array(
            'task' => $task,
            'context' => $context
        ));
    }

    /**
     *  @Security("is_granted('UPDATE_TASK', task)")
     *  @Route("/{id}/modifier", name="task_edit")
     *  @ParamConverter("task", class="AppBundle:Task\Task", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Task $task)
    {
        $form = $this->createForm(TaskType::class, $task, array(
            'action' => $this->generateUrl('task_edit', array('id' => $task->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Task:edit.html.twig', array(
            'task' => $task,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('STATUS_TASK', task)")
     *  @Route("/{id}/etat/{status}/{context}/{object_id}", name="task_status")
     *  @ParamConverter("task", class="AppBundle:Task\Task", options={"mapping": {"id": "id"}})
     */
    public function statusAction(Request $request, Task $task, $status, $context = null, $object_id = null)
    {
        $task->setStatus($status);
        $em = $this->getDoctrine()->getManager();
        $em->flush();

        switch($context) {
            case 'list': return $this->redirectToRoute('task_list');
            case 'group': return $this->redirectToRoute('group_view', array('id' => $object_id));
            case 'user': return $this->redirectToRoute('user_view');
        }
        return $this->redirectToRoute('task_view', array('id' => $task->getId()));
    }

    /**
     * @Route(path="/{member_id}/ajouter/{group_id}", name="task_add",
     *     defaults={"group_id": null},)
     * @ParamConverter("member", class="AppBundle:User", options={"mapping": {"member_id": "id"}})
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     * @Security("is_granted('ADD_TASK', member)")
     */
    public function addTaskAction(Request $request, User $member, Group $group = null)
    {
        $task = new Task($member);

        if($group)
            $task->setGroup($group);

        $form = $this->createForm(TaskType::class, $task, array(
            'action' => $this->generateUrl('task_add', array('member_id' => $member->getId(), 'group_id' => $group ? $group->getId() : null)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $task->setCreatedBy($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Task:edit.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }
}
