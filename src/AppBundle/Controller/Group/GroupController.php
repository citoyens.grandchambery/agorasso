<?php



namespace AppBundle\Controller\Group;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Entity\Group\Group;
use AppBundle\Entity\Group\GroupSubscription;
use AppBundle\Entity\User as User;
use AppBundle\Entity\User\Subscription;
use AppBundle\Form\Group\GroupSubscribeType;
use AppBundle\Form\Group\UserSearchType;
use AppBundle\Form\Group\GroupType;
use AppBundle\Entity\Mail\Mail;
use AppBundle\Form\Mail\MailType;
use AppBundle\Form\User\MemberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Validation;

/**
 * Group controller.
 * @Security("has_role('ROLE_CONTACT')")
 * @route("/espace/groups")
 */

class GroupController extends Controller
{
    const STEP_CLOSE = 100;
    const STEP_SEARCH = 1;
    const STEP_NEW = 2;
    const STEP_SUBSCRIBING = 3;
    const STEP_EDIT = 4;
    /**
     * @Route("/", name="groups")
     */
    public function indexAction(Request $request)

    {
        $assemblee = $this->getDoctrine()->getManager()->getRepository('AppBundle:Group\Group')
            ->queryGroupByWeight(0);

        return $this->viewAction($request, $assemblee);
    }

    /**
     * @Route(path="/{id}", name="group_view")
     */
    public function viewAction(Request $request, Group $group)
    {
        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository('AppBundle:Group\Group')->queryMainGroups(1)->getQuery()->getResult();
        $subGroups = $em->getRepository('AppBundle:Group\Group')->queryAll(1, $group)->getQuery()->getResult();
//        if($group->getInfo())
//            $infos = $em->getRepository('AppBundle:Time\Info')->queryLastInfos(10)->getQuery()->getResult();
//        else
//            $infos = null;
        return $this->render('AppBundle:Group:view.html.twig', array(
            'group' => $group,
            'groups' => $groups,
            'subGroups' => $subGroups,
//            'infos' => $infos
        ));
    }

    protected function subscribing($active, Request $request, Group $group, User $user, $addReferent = true)
    {
        $em = $this->getDoctrine()->getManager();
        $subscription = $em->getRepository('AppBundle:Group\GroupSubscription')->findGroupSubscription($group, $user);
        if (!$subscription) {
            $subscription = new GroupSubscription();
            $subscription->setSubscriber($user);
            $subscription->setGroup($group);
        }
//        $subscription->setActive($active);
        $form = $this->createForm(GroupSubscribeType::class, $subscription, array(
            'action' => $this->generateUrl($active ? 'group_unsubscribe' : 'group_subscribe', array('group_id' => $group->getId(), 'user_id' => $user->getId())),
            'method' => 'POST',
            'selfUser' => $user,
            'group' => $addReferent ? $group : null
        ));

//        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid() and $form->isSubmitted()) {
            if (!$subscription->getActive())
                $subscription->setReferent(false);
            else {
                $mail = new Mail($user, $group, Mail::MAIL_TYPE_GROUP_SUBSCRIPTION);
                $em->persist($mail);
                $this->get('app.manager.mailer')->sendMail($mail);
                $this->get('session')->getFlashBag()->add('success', sprintf("Vous êtes inscrit au groupe %s", $group));
            }
            $em->persist($subscription);
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Group\GroupSubscription:edit.html.twig', array(
            'group' => $group,
            'subscription' => $subscription,
            'form' => $form->createView(),
            'user' => $user
        ));
    }

    /**
     * @Route(path="/{group_id}/subscribe/{user_id}", name="group_subscribe")
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"user_id": "id"}})
     * @Security("is_granted('REFERENT', group) or is_granted('EDIT_USER_SELF', user)")
     */
    public function susbcribeAction(Request $request, Group $group, User $user)
    {
        if($group->getNoSubscription()) {
            if(!$this->getUser()->isReferent($group) and !$this->getUser()->hasRole('ROLE_ADMIN')) {
                throw new HttpException(404, "Inscription impossible");
            }
        }
        return $this->subscribing(false, $request, $group, $user);
    }

    /**
     * @Security("is_granted('REFERENT', group) or is_granted('EDIT_USER_SELF', user)")
     * @Route(path="/{group_id}/unsubscribe/{user_id}", name="group_unsubscribe")
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"user_id": "id"}})
     */
    public function unsusbcribeAction(Request $request, Group $group, User $user)
    {
        return $this->subscribing(true, $request, $group, $user, false);
    }

    private function createSearchForm(Group $group = null)
    {
        $form = $this->createForm(UserSearchType::class, null, array(
            'action' => $this->generateUrl('user_search', array('group_id' => $group ? $group->getId() : null, 'step' => 1, 'email' => 'test@test.fr')),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * @Security("is_granted('REFERENT', group)")
     * @Route(path="/{group_id}/admin", name="group_admin")
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     */
    public function adminAction(Request $request, Group $group)
    {
        $form = $this->createSearchForm($group);
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:Mail\Mail')
            ->queryAll($group);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));
        return $this->render('AppBundle:Group\Admin:index.html.twig', array(
            'group' => $group,
            'search_form' => $form->createView(),
            'mails' => $pagerfanta,
        ));
    }

    protected function searchUser($email, &$user)
    {
        $em = $this->getDoctrine()->getManager();
        $validator = Validation::createValidator();
        $emailConstraint = new EmailConstraint();
        $emailConstraint->message = 'Cet email n\'est pas valide';
        $violations = $validator->validate($email, array(
            $emailConstraint
        ));

        if (0 !== count($violations)) {
//            $session = $this->get('session');
//
//            // there are errors, now you can show them
//            foreach ($violations as $violation) {
//                $session->getFlashBag()->add('danger', $violation->getMessage());
//            }
            return self::STEP_NEW;
        }
        $user = $em->getRepository('AppBundle:User')->findOneByEmail($email);

        if (!$user)
            $step = self::STEP_NEW;
        else
            $step = self::STEP_EDIT;
        return $step;
    }

    protected function newUser(Request $request, $group_id = null, $email, &$user, &$step)
    {
        $em = $this->getDoctrine()->getManager();
        $user = new User();
        if($email)
            $user->setEmail($email);
        $session = $this->get('session');

        // form creation
        $form = $this->createForm(MemberType::class, $user, array(
            'action' => $this->generateUrl('user_search', array('group_id' => $group_id ? $group_id : null, 'step' => self::STEP_NEW, "email" => $email)),
            'method' => 'POST',
            'formType' => array('contact')
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Créer, étape suivante'));
        $form->handleRequest($request);

        // data test
        if ($user->getEmail() and $em->getRepository('AppBundle:User')->findOneBy(array('email' => $user->getEmail()))) {
            $session->getFlashBag()->add('danger', sprintf("Cette adresse email existe déjà"));
        } elseif ($form->isSubmitted() and $form->isValid()) {
            $user->setBasicFields($user->getEmail());
            $sub = new Subscription();
            $sub->setName($user->getName());
            $sub->setFirstName($user->getFirstName());
            $sub->setAddress($user->getAddress());
            $sub->setPostalCode($user->getPostalCode());
            $sub->setCity($user->getCity());
            $sub->setPhone($user->getPhone());
            $sub->setEmail($user->getEmail());
            $sub->setGetNoticed(true);
            $sub->setUser($user);
            $sub->setRole($user->getRoles()[0]);
            $member = $this->getUser();
            if ($member)
                $sub->setCreatedBy(($member instanceof User) ? $member : null);
            $em->persist($sub);
//            $user->setSubscription($sub);
            $em->persist($user);
            $em->flush();

            $session->getFlashBag()->add('success', sprintf("%s ajouté aux curieux.ses", $user));

            if($group_id)
                $step = self::STEP_SUBSCRIBING;
            else
                $step = self::STEP_CLOSE;
        }
        return $form;
    }

    protected function editUser(Request $request, $group_id = null, $email, &$user, &$step)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');

        // form creation
        $form = $this->createForm(MemberType::class, $user, array(
            'action' => $this->generateUrl('user_search', array('group_id' => $group_id ? $group_id : null, 'step' => $step, "email" => $email)),
            'method' => 'POST',
            'formType' => array('contact'),
            'step' => 'edit'
        ));
        if($group_id or !$user->getConvention())
            $label = "Suivant";
        else
            $label = "Terminer";
        $form->add('submit', SubmitType::class, array('label' => $label));
        $form->handleRequest($request);

        // data test
        if ($form->isSubmitted() and $form->isValid()) {
            $em->flush();

            $session->getFlashBag()->add('success', sprintf("%s modifié.e", $user));

            if($group_id)
                $step = self::STEP_SUBSCRIBING;
            else
                $step = self::STEP_CLOSE;
        }
        return $form;
    }

    /**
     * @Security("is_granted('ROLE_CITIZEN')")
     * @Route(path="/search/{email}/{step}/{group_id}", defaults={"email" = null, "step"="1", "group_id" = null}, name="user_search", options = { "expose" = true })
     */
    public function searchAction(Request $request, $group_id, $email, $step)
    {
        $em = $this->getDoctrine()->getManager();
        $user = null;

        // check if we have to create contact
        if ($step == self::STEP_SEARCH) {
            $step = $this->searchUser($email, $user);
        }

        // contact creation
        if ($step == self::STEP_NEW) {
            $form = $this->newUser($request, $group_id, $email, $user, $step);

            // rendering form
            if ($step == self::STEP_NEW) {
                return $this->render('AppBundle:User:edit.html.twig', array(
                    'form' => $form->createView(),
                    'step' => $step,
                    'title' => 'Ajouter un membre',
                    'user' => $user
                ));
            }
        }

        // contact edition
        if ($step == self::STEP_EDIT) {
            if(!$user) {
                $this->searchUser($email, $user);
            }
            $form = $this->editUser($request, $group_id, $email, $user, $step);

//            dump($step);die;

            // rendering form
            if ($step == self::STEP_EDIT) {
                return $this->render('AppBundle:User:edit.html.twig', array(
                    'form' => $form->createView(),
                    'step' => $step,
                    'title' => 'Modifier un membre',
                    'user' => $user
                ));
            }
        }

        if($user and !$user->getConvention())
            return $this->redirectToRoute('convention_add', array('member_id' => $user->getId(), 'group_id' => $group_id));

        // contact already created, we can subscribe him
        if ($step == self::STEP_SUBSCRIBING && $group_id) {
            if(!$user)
                $this->searchUser($email, $user);
            $group = $em->getRepository('AppBundle:Group\Group')->find($group_id);
            return $this->subscribing(false, $request, $group, $user);
        }

        return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
    }

    /**
     * @Security("is_granted('REFERENT', group)")
     *  @Route("/{id}/modifier", name="group_edit")
     *  @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Group $group)
    {
        $form = $this->createForm(GroupType::class, $group, array(
            'action' => $this->generateUrl('group_edit', array('id' => $group->getId())),
            'method' => 'POST',
        ));

        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Group\Admin:edit.html.twig', array(
            'group' => $group,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("is_granted('REFERENT', group)")
     * @Route(path="/{group_id}/mail", name="group_mail")
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     */
    public function groupMailAction(Request $request, Group $group)
    {
        $em = $this->getDoctrine()->getManager();
        $mail = new Mail($this->getUser(), $group, Mail::MAIL_TYPE_GROUP);

        // form creation
        $form = $this->createForm(MailType::class, $mail, array(
            'action' => $this->generateUrl('group_mail', array('group_id' => $group->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Envoyer'));
        $form->handleRequest($request);

        // data test
        if ($form->isSubmitted() and $form->isValid()) {
            $em->persist($mail);
            $em->flush();

            $this->get('app.manager.mailer')->sendMail($mail);
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Mail:groupEdit.html.twig', array(
            'group' => $group,
            'mail' => $mail,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{group_id}/contact", name="group_contact")
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     */
    public function groupContactAction(Request $request, Group $group)
    {
        $em = $this->getDoctrine()->getManager();
        $mail = new Mail($this->getUser(), $group, Mail::MAIL_TYPE_CONTACT);

        // form creation
        $form = $this->createForm(MailType::class, $mail, array(
            'action' => $this->generateUrl('group_contact', array('group_id' => $group->getId())),
            'method' => 'POST'
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Envoyer'));
        $form->handleRequest($request);

        // data test
        if ($form->isSubmitted() and $form->isValid()) {
            $em->persist($mail);
            $em->flush();

            $this->get('app.manager.mailer')->sendMail($mail);
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Mail:groupEdit.html.twig', array(
            'group' => $group,
            'mail' => $mail,
            'form' => $form->createView(),
        ));
    }
}