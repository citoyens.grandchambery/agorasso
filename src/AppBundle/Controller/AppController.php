<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Site\Block;
use AppBundle\Entity\Site\PageTemplate;
use AppBundle\Entity\Project\Project;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * App controller.
 *
 */
class AppController extends Controller
{
    /**
     * Initialisation par référence des paramètres $sortingColumn et $sortingOrder, à partir de la requête ou à partir de la session
     * On récupère les données de la session uniquement si elles ont moins d'une heure
     * $filterName sert à rendre unique les noms de colonnes et d'ordre de tri au cas où il y en ait d'autres en session
     *
     * @param Request $request
     * @param $sortingColumn
     * @param $sortingOrder
     * @param $filterName
     */
    protected function getSortingParameters(Request $request, &$sortingColumn, &$sortingOrder)
    {
        $session = $this->get('session');
        $filterName = $request->attributes->get('_route').'_filter_';
        if ($request->query->has('sortingColumn')) {
            $sortingColumn = $request->query->get('sortingColumn');
            $sortingOrder = $request->query->get('sortingOrder');
            $session->set($filterName.'sortingColumn', $sortingColumn);
            $session->set($filterName.'sortingOrder', $sortingOrder);
            $session->set($filterName.'OrderTime', time());
        } elseif ($session->has($filterName.'sortingColumn') && (time() - $session->get($filterName.'OrderTime') < 3600)) {
            $sortingColumn = $session->get($filterName.'sortingColumn');
            $sortingOrder = $session->get($filterName.'sortingOrder');
        }
    }

    /**
     * Création d'un filtre de recherche à partir du formulaire renvoyé par la page ou à partir du filtre stocké en session
     * On récupère les données de la session uniquement si elles ont moins d'une heure
     * $filterName sert à rendre unique le nom du filtre stocké en session au cas où celle-ci en contienne d'autres
     *
     * @param Form $searchForm
     * @param $filterName
     * @return mixed|null
     */
    protected function createSearchFilter(Request $request, Form $searchForm)
    {
        $session = $this->get('session');
        $filterName = $request->attributes->get('_route').'_filter';

        if ($searchForm->getData()) {
            $searchFilter = $searchForm->getData();
            $session->set($filterName, $this->replaceObjectsWithClassAndId($searchFilter));
            $session->set($filterName.'_time', time());
        } elseif ($session->has($filterName) && (time() - $session->get($filterName.'_time') < 3600)) {
            $searchFilter = $this->replaceClassAndIdWithObjects($session->get($filterName));
            $searchForm->setData($searchFilter);
        } else {
            $searchFilter = null;
        }
        return $searchFilter;
    }

    /**
     * Transforme les entités ou les tableaux/collections d'entités en tableaux contenant l'Id et la classe de l'entité
     *
     * @param $array
     * @return array
     */
    protected function replaceObjectsWithClassAndId($array)
    {
        foreach ($array as $key => $value) {
            if ($value instanceof ArrayCollection) {
                $resultArray = array();
                foreach ($value as $subValue) {
                    if (is_object($subValue) && get_class($subValue) != 'DateTime') {
                        $resultArray[] = array(
                            'objectClass' => get_class($subValue),
                            'objectId' => $subValue->getId(),
                            'originalType' => 'ArrayCollection',
                        );
                    } else {
                        $resultArray[] = $subValue;
                    }
                }
                $array[$key] = $resultArray;
            } elseif (is_array($value)) {
                foreach ($value as $subKey => $subValue) {
                    if (is_object($subValue) && get_class($subValue) != 'DateTime') {
                        $value[$subKey] = array(
                            'objectClass' => get_class($subValue),
                            'objectId' => $subValue->getId(),
                            'originalType' => 'array',
                        );
                    }
                }
            } elseif (is_object($value) && get_class($value) != 'DateTime') {
                $array[$key] = array(
                    'objectClass' => get_class($value),
                    'objectId' => $value->getId(),
                );
            }
        }
        return $array;
    }

    /**
     * Transforme les tableaux contenant l'Id et la classe des entités en entités ou en tableaux/collections d'entités
     *
     * @param $array
     * @return array
     */
    protected function replaceClassAndIdWithObjects($array)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($array as $key => $value) {
            if (is_array($value) && isset($value['objectClass'])) {
                $array[$key] = $em->getRepository($value['objectClass'])->find($value['objectId']);
            } elseif ($value && is_array($value) && is_array($value[0]) && isset($value[0]['originalType'])) {
                if ($value[0]['originalType'] == 'ArrayCollection') {
                    $resultCollection = new ArrayCollection();
                    foreach ($value as $subValue) {
                        $resultCollection->add($em->getRepository($subValue['objectClass'])->find($subValue['objectId']));
                    }
                    $array[$key] = $resultCollection;
                } elseif ($value[0]['originalType'] == 'array') {
                    $resultArray = array();
                    foreach ($value as $subValue) {
                        $resultArray[] = $em->getRepository($subValue['objectClass'])->find($subValue['objectId']);
                    }
                    $array[$key] = $resultArray;
                }
            }
            // On renvoie null plutôt qu'un tableau vide pour ne pas renvoyer un tableau vide à la place d'une Arraycollection vide, ce qui fait planter les tests du repository
            if (is_array($array[$key]) && empty($array[$key])) {
                $array[$key] = null;
            }
        }
        return $array;
    }

    /**
     * @param $searchFilter
     * @return bool
     */
    protected function isFilled($searchFilter)
    {
        if ($searchFilter) {
            foreach ($searchFilter as $key => $filter) {
                if ($filter instanceof ArrayCollection && $filter->isEmpty()) {
                    unset($searchFilter[$key]);
                }
            }
        }
        return $searchFilter && array_filter($searchFilter);

    }

    /**
     * On rattache les objets pour lesquels le lien est perdu après stockage en session
     * (plus utilisée car on ne stocke plus d'entités mais des couples Id/classe en session)
     * 
     * @param $filter
     * @return array
     */
    protected function manageObjects($filter)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($filter as $key => $value) {
            if (is_object($value) && get_class($value) != 'DateTime') {
                $filter[$key] = $em->merge($value);
            }
        }

        return $filter;
    }

    protected function sendModalErrorResponse($message= "L'action demandée est impossible", $title = "Erreur", $type = null)
    {
        return $this->render('AppBundle:Ajax:modal.html.twig', array(
            'type' => $type,
            'title' => $title,
            'message' => $message
        ));
    }  
    
    protected function sendAjaxErrorResponse($message= "L'action demandée est impossible", $title = "Erreur")
    {
        return $this->render('AppBundle:Ajax:error.js.twig', array(
            'title' => $title,
            'message' => $message
        ));
    }  
    
    protected function saveCollections(array $collections)
    {
        foreach ($collections as $index => $collection) {
            $oldCollection = new ArrayCollection();
            foreach ($collection as $element) {
                $oldCollection->add($element);
            }
            $oldCollections[$index] = $oldCollection;
        }
        return $oldCollections;
    }

    protected function updateCollections($oldCollections, $newCollections, $em)
    {
        foreach ($oldCollections as $index => $oldCollection) {
            foreach ($oldCollection as $element) {
                if (false === $newCollections[$index]->contains($element)) {
                    $em->remove($element);
                }
            }
        }
    }

    private function getPostMaxResults($options)
    {
        if(!$options) {
            return 6;
        }
        return $options;
    }

    protected function renderAppPage($name = null, $fields = array(), $options = array(), Request $request = null)
    {
        /* retrieve page */
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AppBundle:Site\Page')->findOneBy(array('name' => $name));
        if ($page == null) {
            throw $this->createNotFoundException('Page inconnue');
        }
        if ($page->getRole()) {
            if (!$this->isGranted($page->getRole())) {
                throw $this->createNotFoundException('Vous n\'êtes pas autorisé-e à consulter cette page');
            }
        }

        /* retrieve blocks */
        $rightBlockName = null;
        if(array_key_exists('rightBlockName', $options)) {
            $rightBlockName = $options['rightBlockName'];
        }
        $left = $em->getRepository('AppBundle:Site\Block')->queryBlocks($page, PageTemplate::POSITION_LEFT)->getQuery()->getResult();
        $right = $em->getRepository('AppBundle:Site\Block')->queryBlocks($page, PageTemplate::POSITION_RIGHT, $rightBlockName)->getQuery()->getResult();
        $main = $em->getRepository('AppBundle:Site\Block')->queryBlocks($page, PageTemplate::POSITION_MAIN)->getQuery()->getResult();
        $events = null;
        $calendar = null;
        if ($page->hasBlockType(Block::TYPE_EVENTS)) {
            $events = $em->getRepository('AppBundle:Time\Xvent')->queryAllEvents(true, 'past')->getQuery()->getResult();
//            dump($events);die;
        }
        if ($page->hasBlockType(Block::TYPE_CALENDAR) || $page->hasBlockType(Block::TYPE_ALL_CALENDAR)) {
            $calendar = $em->getRepository('AppBundle:Time\Xvent')->queryAllEvents(true, 'future')->getQuery()->getResult();
        }
        $files = null;
        if ($page->hasBlockType(Block::TYPE_DOCS)) {
            $files = $em->getRepository('AppBundle:Document\UploadedFile')->findPublicFiles();
        }
        $posts = null;
        $more = null;
        if ($page->hasBlockType(Block::TYPE_POSTS)) {
            $pageOptions = null;
            if(array_key_exists('pageOptions', $options)) {
                $pageOptions = $options['pageOptions'];
            }
            $maxResults = $this->getPostMaxResults($pageOptions);
            $posts = $em->getRepository('AppBundle:Site\Post')->queryAll(true, $maxResults)->getQuery()->getResult();
            if (count($posts) == $maxResults) {
                $more = $maxResults + 9;
            } else {
                $more = null;
            }
        } elseif($page->hasBlockType(Block::TYPE_POSTS_LAST)) {
            $posts = $em->getRepository('AppBundle:Site\Post')->queryAll(true, 2)->getQuery()->getResult();
        } elseif($page->hasBlockType(Block::TYPE_POSTS_ALL)) {
            $npage = $request->get('postPage', 1);
            $qb = $em->getRepository('AppBundle:Site\Post')->queryAll(true);
            $posts = new Pagerfanta(new DoctrineORMAdapter($qb));
            $posts->setMaxPerPage(12);
            $posts->setCurrentPage($npage);
//            return $this->render('AppBundle:Event:index.html.twig', array(
//                'events' => $pagerfanta,
////                'searchForm' => $searchForm->createView()
//            ));
        }
        if ($page->hasBlockType(Block::TYPE_CANDIDATE_RESULT)) {
            $fields['results'] = $em->getRepository('AppBundle:User\Subscription')->resultCandidates(null/*$filters*/);
        }
        if ($page->hasBlockType(Block::TYPE_PROJECT)) {
            $fields['project'] = $em->getRepository('AppBundle:Project\Project')->queryProject();

        }
        if ($page->hasBlockType(Block::TYPE_PROPOSAL)) {
            $proposal = $fields['proposal'];
            $page->setTitle(sprintf("Proposition %d", $proposal->getNumber()));
            $page->setSubTitle($proposal->getTitle());
            $page->setDescription($proposal->getDescription());
            $keywords = '';
            $results = explode(" ", $proposal->getSection()->getSection());
            for ($i = 1; $i < count($results); $i++) {
                if($i > 1) {
                    $keywords = $keywords.",";
                }
                $keywords = $keywords.$results[$i];
            }
            $results = explode(" ", $proposal->getSection());
            for ($i = 1; $i < count($results); $i++) {
                $keywords = $keywords.",";
                $keywords = $keywords.$results[$i];
            }
            $page->setKeywords($keywords);
        }
        $fields['page'] = $page;
        $fields['events'] = $events;
        $fields['calendar'] = $calendar;
        $fields['files'] = $files;
        $fields['posts'] = $posts;
        $fields['more'] = $more;

        /* complete template and fields */
        switch ($page->getReferedPageTemplate()->getType()) {
            case PageTemplate::TYPE_COLUMN:
                $fields['left'] = $left;
                $fields['right'] = $right;
                $template = 'AppBundle:Site:two_columns.html.twig';
                break;
            case PageTemplate::TYPE_THREE_HEADER_COLUMN:
                $fields['left'] = $left;
                $fields['right'] = $right;
                $fields['center'] = $em->getRepository('AppBundle:Site\Block')->queryBlocks($page, PageTemplate::POSITION_CENTER)->getQuery()->getResult();
                $fields['main'] = $main;
                $template = 'AppBundle:Site:three_header_column.html.twig';
                break;
            default:
            case PageTemplate::TYPE_NO_COLUMN:
                $fields['main'] = $main;
                $template = 'AppBundle:Site:no_column.html.twig';
                break;
            case PageTemplate::TYPE_HEADER_COLUMN:
                $fields['left'] = $left;
                $fields['right'] = $right;
                $fields['main'] = $main;
                $template = 'AppBundle:Site:header_column.html.twig';
                break;
            case PageTemplate::TYPE_MAIN_AND_COLUMN:
                $fields['left'] = $left;
                $fields['right'] = $right;
                $fields['main'] = $main;
                $template = 'AppBundle:Site:main_and_column.html.twig';
                break;
        }
        /*$project = $em->getRepository('AppBundle:Project\Project')->findOneBy(array('id' => '8'));
        dump($project);die;
        $fields['project'] = $project;*/

//        dump($fields);die;
        return $this->render($template, $fields);
    }
}
