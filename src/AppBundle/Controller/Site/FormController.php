<?php

namespace AppBundle\Controller\Site;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Content\PublicForm;
use AppBundle\Form\Content\FormObjectType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use AppBundle\Object\FormObject;

/**
 * PublicForm controller.
 * @Route("/form")
 */
class FormController extends AppController
{

    /**
     *  @Route("/{id}/action", name="form_do")
     *  @ParamConverter("publicForm", class="AppBundle:Content\PublicForm", options={"mapping": {"id": "id"}})
     *  @Security("is_granted('CAN_FORM', publicForm)")
     */
    public function publicFormAction(Request $request, PublicForm $publicForm)
    {
        $formObject = new FormObject($publicForm);
        $form = $this->createForm(FormObjectType::class, $formObject, array(
            'action' => $this->generateUrl('form_do', array('id' => $publicForm->getId())),
            'method' => 'POST',
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid() ) {

                $em = $this->getDoctrine()->getManager();
                foreach ($formObject->getResponses() as $sub) {
                    $sub->setPublicForm($publicForm);
                    $em->persist($sub);
                    $publicForm->addResponse($sub);
                }
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', sprintf("Merci d'avoir participé! Vos réponses ont bien été prises en compte."));
                return $this->redirectToRoute('view_page', array('name'=>'actualites'));
        }

        return $this->render('AppBundle:Site\PublicForm:edit.html.twig', array(
            'publicForm' => $publicForm,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Route("/{id}/voir", name="form_view")
     *  @ParamConverter("publicForm", class="AppBundle:Content\PublicForm", options={"mapping": {"id": "id"}})
     *  @Security("is_granted('ROLE_CONTACT')")
     */
    public function publicFormView(Request $request, PublicForm $publicForm)
    {
        return $this->render('AppBundle:Content\PublicForm:view.html.twig', array(
            'publicForm' => $publicForm
        ));
    }
}
