<?php

namespace AppBundle\Controller\Site;

use AppBundle\Form\Document\SearchType;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Document\Image as Image;


/**
 * Document controller.
 * @Security("has_role('ROLE_CONTACT')")
 * @route("/espace/images")
 */
class ImageController extends Controller
{
    /**
     * @Route(path="/{id}", name="image_view")
     * @param Request $request
     * @param Image $file
     * @return Response
     */
    public function viewImage(Request $request, Image $file)
    {
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path = $helper->asset($file, 'file');
        $response = new Response(file_get_contents($file->getFile()));
        $response->headers->set('Content-Type', mime_content_type($path));
        $response->headers->set('Content-Disposition', 'inline;filename="' . $file->getOriginalFileName());
        return $response;
    }
}