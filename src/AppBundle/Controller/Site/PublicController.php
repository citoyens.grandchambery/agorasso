<?php


namespace AppBundle\Controller\Site;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User\Subscription;
use AppBundle\Form\User\SubscriptionType;
use AppBundle\Form\User\ConfirmSubscriptionType;
use AppBundle\Entity\Document\UploadedFile;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Site\Block;
use AppBundle\Entity\Site\PageTemplate;
use AppBundle\Controller\AppController;

/**
 * Public controller.
 * @route("/public")
 */
class PublicController extends AppController
{
    /**
     * @Route("/contact/{step}", defaults={"step"="1"}, name="public_contact")
     */
    public function contactAction(Request $request, $step)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $subscription = $session->get('contact');
        $contact = null;
        if ($subscription == null) {
            $subscription = new Subscription();
            $form = $this->createForm(SubscriptionType::class, $subscription, array(
                'method' => 'POST',
            ));
        } else {
            $subscription = $em->getRepository('AppBundle:User\Subscription')->findOneBy(array('id' => $subscription->getId()));
            if($step == 3) {
                return $this->renderAppPage('contact', array('form' => null, 'step' => 3, 'action' => 'contact'), array('rightBlockName' => 'success'), $request);
            }
            $step = 2;
            $contact = new User(
                array('email' => $subscription->getEmail(),
                    'firstName' => $subscription->getFirstName(),
                    'name' => $subscription->getName(),
                    'role' => $subscription->getRole()));
            $contact->setPlace($subscription->getTown(), $subscription->getDistrict(), $subscription->getCity());
            $form = $this->createForm(ConfirmSubscriptionType::class, $contact, array(
                'method' => 'POST',
            ));
        }

        $form->handleRequest($request);

        if ($subscription and $this->get('app.helper.base')->userEmailExist($subscription->getEmail())) {
            $session->getFlashBag()->add('danger', sprintf("Création impossible: cette adresse email est déjà dans les contacts: allez sur la page \"Espace citoyen.ne.s\" et 
     cliquez sur le bouton \"Mot de passe oublié/inconnu\" pour configurer votre mot de passe"));

            $step = 1;
        } elseif ($form->isSubmitted() and $form->isValid()) {
            if ($step == 2) {
                $em->persist($contact);
                $em->flush();
                $session->set("contact", null);

                $mailer = $this->get('app.manager.mailer');
                $mailer->sendSubscription($subscription);
                $this->get('session')->getFlashBag()->add('success', sprintf("Un courriel vous a été envoyé."));
                $this->get('session')->getFlashBag()->add('warning', sprintf("Si vous ne l'avez pas reçu, regardez dans votre dossier \"courriers indésirables\" (Spams)."));
                $this->get('session')->getFlashBag()->add('warning', sprintf("Indiquez à votre messagerie que ce n'est pas un courrier indésirable, sinon vous ne recevrez pas la lettre d'information."));
                return $this->renderAppPage('contact', array('form' => $form, 'step' => 3, 'action' => 'contact'), array('rightBlockName' => 'success'), $request);
            } else {
                $em->persist($subscription);
                $em->flush();
                $session = $this->get('session');
                $session->set("contact", $subscription);
                return $this->redirectToRoute('public_contact', array('step' => $step));
            }
        }

        return $this->renderAppPage('contact', array('form' => $form->createView(), 'step' => $step, 'action' => 'contact'), array('rightBlockName' => 'contact'), $request);
    }

    /**
     * @Route("/subscription/confirm/{id}/{cookie}", name="public_confirm_subscription")
     */
    public function subscriptionConfirmAction(Request $request, $id, $cookie)

    {
        $em = $this->getDoctrine()->getManager();

            $sub = $em->getRepository('AppBundle:User\Subscription')->findOneBy(array('id' => $id, 'cookie' => $cookie, 'emailConfirmed' => false));
            if ($sub) {
                $sub->setEmailConfirmed(true);
                $em->flush();
                $session = $this->get('session');
                $session->getFlashBag()->add('success', sprintf("Inscription à l\'espace citoyen.ne.s validée"));
                return $this->redirectToRoute("dashboard");
            }

        throw $this->createNotFoundException('Page inconnue');
    }

    /**
     * @Route("/action_cancel/{action}", name="cancel_action")
     */
    public function cancelContactAction(Request $request, $action)
    {
        $route = 'public_actualites';
        $session = $this->get('session');
        switch($action) {
            case 'contact':
                $session->set("contact", null);
                $route = 'public_contact';
                break;
            case 'candidate':
            case 'sponsor':
                $session->set("candidate", null);
                $session->set("sponsor", null);
                $route = 'candidate';
                break;
            case 'volunteer':
                $session->set("candidate", null);
                $route = 'volunteer';
                break;
        }

        return $this->redirectToRoute($route);
    }

    /**
     * @Route("/donées-personnelles", name="personal_data")
     */
    public function personalDataAction()
    {
        $em = $this->getDoctrine()->getManager();
        $right = $em->getRepository('AppBundle:Site\Block')->queryBlocks(null, null, 'personal_data')->getQuery()->getResult();
        return $this->render('AppBundle:Site:modal_two_columns.html.twig', array(
            'right' => $right,
            'title' => 'Que faisons nous de vos données personnelles ?'
        ));
    }


    /**
     * @Route(path="/document/{id}", name="public_file_view")
     * @ParamConverter("file", class="AppBundle:Document\UploadedFile", options={"mapping": {"id": "id"}})
     */
    public function viewAttachedFile(Request $request, UploadedFile $file)
    {
        if ($file->getPublic()) {
            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
            $path = $helper->asset($file, 'file');
            $response = new Response(file_get_contents($file->getFile()));
            $response->headers->set('Content-Type', mime_content_type($path));
            $response->headers->set('Content-Disposition', 'inline;filename="' . $file->getOriginalFileName());
            return $response;
        }
        throw new HttpException(404, "Document non visible");
    }

    /**
     * @Route("/{name}/{options}", name="view_page")
     */
    public function viewPageAction(Request $request, $name = null, $options = null)
    {
        if(!$name) {
            $name = 'actualites';
        }

        return $this->renderAppPage($name, null, array('pageOptions' => $options), $request);
    }

    /**
     * @Route("/{name}", name="redirect_view_page", requirements={"name" = ".*\/$"}))
     */
    public function redirectNameAction(Request $request, $name)
    {
        $pathInfo = $request->getPathInfo();
        $requestUri = $request->getRequestUri();

        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

        // 308 (Permanent Redirect) is similar to 301 (Moved Permanently) except
        // that it does not allow changing the request method (e.g. from POST to GET)
        return $this->redirect($url, 308);
    }
}