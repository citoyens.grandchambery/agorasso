<?php


namespace AppBundle\Controller\Site;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * Public controller.
 * @route("/post")
 */
class PostController extends Controller
{
    /**
     * @Route("/{name}", name="view_post"))
     */
    public function viewNameAction($name)
    {
        $em = $this->getDoctrine()->getManager();
        $page = $em->getRepository('AppBundle:Site\Page')->findOneBy(array('name' => 'actualites'));
        $post = $em->getRepository('AppBundle:Site\Post')->findOneBy(array('name' => $name));
        if(!$post) {
            throw $this->createNotFoundException('Page inconnue');
        }
        if($post->getPostPage()) {
            return $this->redirectToRoute('view_page', array('name' => $post->getPostPage()->getName()));
        }
        return $this->render(sprintf('AppBundle:Site:post.html.twig', $name), array(
            'page' => $page,
            'post' => $post,
        ));
    }

    /**
     * @Route("/{name}", name="redirect_view_post", requirements={"name" = ".*\/$"}))
     */
    public function redirectNameAction(Request $request, $name)
    {
        $pathInfo = $request->getPathInfo();
        $requestUri = $request->getRequestUri();

        $url = str_replace($pathInfo, rtrim($pathInfo, ' /'), $requestUri);

        // 308 (Permanent Redirect) is similar to 301 (Moved Permanently) except
        // that it does not allow changing the request method (e.g. from POST to GET)
        return $this->redirect($url, 308);
    }
}