<?php



namespace AppBundle\Controller\User;

use AppBundle\Controller\Group\GroupController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\User\Convention;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Entity\User;
use AppBundle\Form\User\ConventionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Group\Group as Group;

/**
 * convention controller.
 * @Security("has_role('ROLE_CITIZEN')")
 * @route("/espace/charte")
 */

class ConventionController extends Controller
{

    /**
     * @Route("/", name="conventions")
     * @Security("is_granted('ROLE_CITIZEN')")
     */
    public function indexAction(Request $request)

    {
        if(!$this->getUser()->isEnabled())
            return $this->redirectToRoute('fos_user_security_logout');
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:User\Convention')
            ->queryAll();

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render('AppBundle:User\Convention:index.html.twig', array(
            'conventions' => $pagerfanta,
        ));

    }

    /**
     * @Route(path="/{member_id}/ajouter/{group_id}", name="convention_add", defaults={"group_id" = null})
     * @ParamConverter("member", class="AppBundle:User", options={"mapping": {"member_id": "id"}})
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     * })
     * @Security("is_granted('ROLE_CITIZEN')")
     */
    public function addConventionAction(Request $request, User $member, Group $group = null)
    {
        $convention = new Convention($this->getUser());
        $convention->setUser($member);

        $form = $this->createForm(ConventionType::class, $convention, array(
            'action' => $this->generateUrl('convention_add', array('member_id' => $member->getId(), 'group_id' => $group ? $group->getId() : null)),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Valider'));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            if($convention->getValidated()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($convention);
                if (in_array('ROLE_CONTACT', $member->getRoles()))
                    $member->setRoles(array('ROLE_FRIEND'));
                $em->flush();
            }

            if($group) {
                return $this->redirectToRoute('user_search', array('group_id' => $group->getId(), 'step' => GroupController::STEP_SUBSCRIBING, 'email' => $member->getEmail()));
            }
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:User\Convention:edit.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }
}