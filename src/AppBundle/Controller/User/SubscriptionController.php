<?php



namespace AppBundle\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\User\Subscription;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Form\Group\UserSearchType;


/**
 * Subscription controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @route("/espace/inscriptions")
 */

class SubscriptionController extends Controller
{

    /**
     * @Route("/", name="subscriptions")
     * @Security("is_granted('ROLE_CITIZEN')")
     */
    public function indexAction(Request $request)

    {
        $form = $this->createSearchForm(null);
        $user = null;
        if(!$this->isGranted('ROLE_SUPER_ADMIN')) {
            $user = $this->getUser();
        }
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:User\Subscription')
            ->queryAll($user);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render('AppBundle:User\Subscription:index.html.twig', array(
            'subscriptions' => $pagerfanta,
            'search_form' => $form->createView(),
            'group' => null
        ));

    }

    private function createSearchForm(Group $group = null)
    {
        $form = $this->createForm(UserSearchType::class, null, array(
            'action' => $this->generateUrl('user_search', array('group_id' => $group ? $group->getId() : null, 'step' => 1, 'email' => 'test@test.fr')),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * @Route(path="/{id}", name="subscription_view")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function viewAction(Request $request, Subscription $event)
    {
        return $this->render('AppBundle:Event:view.html.twig', array(
            'event'  => $event
        ));
    }
}