<?php

namespace AppBundle\Controller\User;

/*use AppBundle\Form\User\CandidateSearchType;*/
use AppBundle\Object\CandidateSearch;
use Doctrine\Common\Collections\ArrayCollection;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\User\Subscription;
use AppBundle\Entity\User\Operation;
use AppBundle\Form\User\SubscriptionType;
use AppBundle\Form\User\OperationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Form\Subscription\CandidateSearchType;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\StreamedResponse;


/**
 * Document controller.
 * @Security("has_role('ROLE_CANDIDATE')")
 * @route("/espace/candidats")
 */
class CandidateController extends Controller
{

    protected function manageOwnObjects(CandidateSearch $filters)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($filters as $key => $value) {
            if (is_object($value) && get_class($value) != 'DateTime') {
                $filters->$key = $em->merge($value);
            }
        }

        return $filters;
    }
    /**
     * @Route("/", name="candidate_index")
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('candidatePage', 1);
        $session = $this->get('session');
        $filters = new CandidateSearch(); // On regarde si on a un filtre en session avec lequel initialiser le form
        if ($session->has('candidateFilters')) {
            $filters = $this->manageOwnObjects($session->get('candidateFilters'));
        }
        $filters->setChange(false);
        $searchForm = $this->createForm(CandidateSearchType::class, $filters, array());
        $searchForm->handleRequest($request);
        if($filters->getChange()) {
            $page = 1;
        }
        $session->set('candidateFilters', $filters);

        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:User\Subscription')->findCandidates($filters);
        $results = $em->getRepository('AppBundle:User\Subscription')->resultCandidates($filters);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);

        return $this->render('AppBundle:User\Candidate:index.html.twig', array(
            'candidates' => $pagerfanta,
            'results' => $results,
            'searchForm' => $searchForm->createView()
        ));

    }

    /**
     * @Security("is_granted('VIEW_CANDIDATE', candidate)")
     * @Route(path="/{id}/voir", name="candidate_view")
     * @param Request $request
     * @param Subscription $candidate
     */
    public function viewCandidate(Request $request, Subscription $candidate)
    {
        $candidates = new ArrayCollection();
        $candidates[] = $candidate;
        return $this->render('AppBundle:User\Candidate:view.html.twig', array(
            'candidate' => $candidate,
            'contact' => $this->get('app.helper.base')->userEmailExist($candidate->getEmail())
        ));
    }

    protected function createCandidateForm($candidate, $route)
    {
        return $this->createForm(SubscriptionType::class, $candidate, array(
            'method' => 'POST',
            'formType' => 'follow',
            'validation_groups' => array('Default'),
            'action' => $route,
                'attr' => [
                    'data-ajax-form' => '',
                    'data-ajax-form-target' => '#bootstrap-modal .modal-content',
                ]    )
        );
    }

    /**
     * @Security("is_granted('ROLE_CANDIDATE')")
     * @Route(path="/ajouter", name="candidate_add")
     * @param Request $request
     * @return Response
     */
    public function addCandidateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $candidate = new Subscription();
        $candidate->setOrigin(Subscription::ORIGIN_MEETING);
        $candidate->setType(null);

        $form = $this->createCandidateForm($candidate, $this->generateUrl('candidate_add'));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $candidateSearch = $this->get('app.helper.base')->candidateExist($candidate->getEmail(), $candidate->getType());
            if($candidateSearch) {
                if($candidate->getType() == Subscription::TYPE_CANDIDATE_SPONSOR) {
                    $candidate = $candidateSearch;
                    $candidate->incWeight();
                    $em->flush();
                } else {
                    $this->addFlash('danger', sprintf("Cette candidature existe déjà dans la base."));
                }
            } else {
                $candidate->setGetNoticed(false);
                $em->persist($candidate);
                $em->flush();

                $this->addFlash('success', sprintf("La candidature a été ajoutée."));
                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
            }
        }
        return $this->render('AppBundle:User\Candidate:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/export", name="candidate_export")
     * @Security("is_granted('ROLE_CANDIDATE')")
     */
    public function exportAction(Request $request)
    {
        $searchForm = $this->createForm(CandidateSearchType::class, null, array());
        $searchForm->handleRequest($request);
        $filters = $searchForm->getData();

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:User\Subscription');

        $response = new StreamedResponse();
        $response->setCallback(function () use ($repository, $filters) {
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, ['Inscription','Prénom', 'Nom','Genre','Date de naissance','Profession','Engagement','Email', 'Adresse','Code postal','Ville','Quartier',
                'Type candidature', 'Nombre', 'Origine', 'Statut','Motivation','Opérations'], ';');

            $results = $repository->findCandidates($filters)->getQuery()->getResult();

            foreach ($results as $c) {
                    fputcsv(
                        $handle, array($c->getContactDate()->format('d/m/Y'), $c->getFirstName(), $c->getName(), $c->getGender(),  $c->getBirthDate() ? $c->getBirthDate()->format('d/m/Y') : '',
                            $c->getJob(),
                            $c->getCommitment(), $c->getEmail(),
                            $c->getAddress() ? $c->getAddress() : '', $c->getPostalCode() ? $c->getPostalCode() : '', $c->getCity() ? $c->getCity() : '',
                            $c->getDistrict() ? $c->getDistrict() : '',
                            $c->getType(), $c->getWeight(), $c->getOrigin(), $c->getStatus(), strip_tags($c->getMotivation()),
                            $c->getOperationsText()

                        ),
                        ';'
                    );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export-candidates.csv"');

        return $response;
    }

    /**
     *  @Security("is_granted('EDIT_CANDIDATE', candidate)")
     *  @Route("/{id}/modifier", name="candidate_edit")
     *  @ParamConverter("candidate", class="AppBundle:User\Subscription", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Subscription $candidate)
    {
        $form = $this->createCandidateForm($candidate, $this->generateUrl('candidate_edit', array('id' => $candidate->getId())));

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', sprintf("La candidature a été modifiée."));
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:User\Candidate:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{id}/supprimer", name="candidate_delete")
     * @ParamConverter("candidate", class="AppBundle:User\Subscription", options={"mapping": {"id": "id"}})
     * @Security("is_granted('DELETE_CANDIDATE', candidate)")
     */
    public function deleteCandidateAction(Request $request, Subscription $candidate)
    {
        $owner = $this->getUser();
        /*if (!$request->isXmlHttpRequest()) {
            throw new HttpException(406, "Ajax nécessaire...");
        }*/

        $em = $this->getDoctrine()->getManager();
        $em->remove($candidate);
        $em->flush();
        $this->addFlash('success', sprintf("Le candidat a bien été supprimé !"));
        return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
    }

    protected function createOperationForm($operation, $route)
    {
        return $this->createForm(OperationType::class, $operation, array(
                'method' => 'POST',
                'action' => $route,
                'attr' => [
                    'data-ajax-form' => '',
                    'data-ajax-form-target' => '#bootstrap-modal .modal-content',
                ]    )
        );
    }

    protected function addCandidateFlash($candidate)
    {
        switch($candidate->getStatus()) {
            case Subscription::STATUS_CANCELLED:
                $this->addFlash('success', sprintf("La candidature est invalide."));
                break;
            case Subscription::STATUS_OK:
                $this->addFlash('success', sprintf("La candidature est validée."));
                break;
            case Subscription::STATUS_WAITING:
                $this->addFlash('success', sprintf("La candidature est en attente."));
                break;
        }
    }

    /**
     *  @Security("is_granted('EDIT_CANDIDATE', candidate)")
     *  @Route("/{id}/operation", name="operation_add")
     *  @ParamConverter("candidate", class="AppBundle:User\Subscription", options={"mapping": {"id": "id"}})
     */
    public function addOperation(Request $request, Subscription $candidate)
    {
        $operation = new Operation($candidate);
        $form = $this->createOperationForm($operation, $this->generateUrl('operation_add', array('id' => $candidate->getId())));

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($operation);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', sprintf("L'opération a été ajoutée."));
            if($candidate->updateStatus()) {
                $this->addCandidateFlash($candidate);
                $this->getDoctrine()->getManager()->flush();
            }

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:User\Operation:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('EDIT_OPERATION', operation)")
     *  @Route("/edit_operation/{id}", name="operation_edit")
     *  @ParamConverter("operation", class="AppBundle:User\Operation", options={"mapping": {"id": "id"}})
     */
    public function editOperation(Request $request, Operation $operation)
    {
        $form = $this->createOperationForm($operation, $this->generateUrl('operation_edit', array('id' => $operation->getId())));

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($operation);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', sprintf("L'opération a été ajoutée."));
            if($operation->getCandidate()->updateStatus()) {
                $this->addCandidateFlash($operation->getCandidate());
                $this->getDoctrine()->getManager()->flush();
            }

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:User\Operation:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/operation_delete/{id}", name="operation_delete")
     * @ParamConverter("operation", class="AppBundle:User\Operation", options={"mapping": {"id": "id"}})
     * @Security("is_granted('DELETE_OPERATION', operation)")
     */
    public function deleteOperation(Request $request, Operation $operation)
    {
        $op = $operation->getOperation();
        $candidate = $operation->getCandidate();

        $em = $this->getDoctrine()->getManager();
        $em->remove($operation);
        $em->flush();
        $this->addFlash('success', sprintf("L'opération a bien été supprimée !"));

        if($op == Operation::OPERATION_FOLLOW_OK or
            $op == Operation::OPERATION_OK or
            $op == Operation::OPERATION_CANCELLED) {

            if($candidate->updateStatus()) {
                $this->addCandidateFlash($candidate);
                $em->flush();
            }
        }
        return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
    }
}