<?php

namespace AppBundle\Controller\User;

use AppBundle\Controller\AppController;
use AppBundle\Entity\User\Subscription;
use AppBundle\Entity\User;
use AppBundle\Form\UnsubscribeType;
use AppBundle\Form\User\ParameterType;
use AppBundle\Validator\Constraints\Subscription\SubscriptionTown;
use FOS\UserBundle\Model\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use AppBundle\Form\User\MemberType;
use Symfony\Component\HttpFoundation\StreamedResponse;
use AppBundle\Manager\DataManager;
use Symfony\Component\Validator\Validation;
use AppBundle\Validator\Constraints\Subscription\SubscriptionTownValidator;

/**
 * User controller.
 * @Route("/espace/utilisateurs")
 */
class UserController extends AppController
{
    /**
     * @Security("is_granted('ROLE_CONTACT')")
     * @Route("/voir", name="user_view")
     */
    public function viewAction(Request $request)
    {
        if (!$this->getUser()->isEnabled()) {
            return $this->redirectToRoute('fos_user_security_logout');
        }
        return $this->render('AppBundle:User:view.html.twig', array());
    }

    protected function isValidTown(User $member)
    {
        $dataManager = $this->get('app.manager.data');
        $message = '';
        if(!$dataManager->isValidTown($member, $message)) {
            $this->get('session')->getFlashBag()->add('danger', $message);
            return false;
        }
        return true;
    }

    /**
     * @Security("is_granted('EDIT_USER_SELF', member)")
     * @Route("/{id}/modifier", name="user_edit")
     */
    public function editAction(Request $request, User $member)
    {
        $dataManager = $this->get('app.manager.data');
        $message = '';
        if(!$dataManager->isValidTown($this->getUser(), $message)) {
            $this->addFlash('danger', sprintf("Merci de renseigner les champs manquants : %s", $message));
        }

        $form = $this->createForm(MemberType::class, $member, array(
            'action' => $this->generateUrl('user_edit', array('id' => $member->getId())),
            'method' => 'POST',
            'formType' => array('perso')
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid() and $this->isValidTown($member)) {
            $this->get('session')->getFlashBag()->clear();
            $em = $this->getDoctrine()->getManager();
            if ($em->getRepository('AppBundle:User')->userNameExist($member)) {
                $this->get('session')->getFlashBag()->add('danger', sprintf("Ce nom d'utilisateur.trice existe déjà."));
            } else {
                $this->getDoctrine()->getManager()->flush();
                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
            }
        }

        return $this->render('AppBundle:User:edit.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Mes infos personnelles',
            'user' => $member
        ));
    }

    /**
     * @Security("is_granted('EDIT_USER_SELF', member)")
     * @Route("/{id}/parametres", name="parameter_edit")
     */
    public function parameterAction(Request $request, User $member)
    {
        $form = $this->createForm(ParameterType::class, $member, array(
            'action' => $this->generateUrl('parameter_edit', array('id' => $member->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:User:parameter.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Security("is_granted('EDIT_USER_SELF', member)")
     * @Route("/{id}/unsubscribe", name="user_unsubscribe")
     */
    public function unsubscribeAction(Request $request, User $member)
    {
        $form = $this->createForm(UnsubscribeType::class, $member, array(
            'action' => $this->generateUrl('user_unsubscribe', array('id' => $member->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Se désinscrire'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $member->setEnabled(false);
            $member->setEmail(sprintf("deleted_%s", $member->getEmail()));
            $member->setUserName(sprintf("deleted_%s", $member->getUserName()));
            $em->persist($member);
            $em->flush();
            $this->get('session')->set('user', null);
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:User:unsubscribe.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/ajouter/", name="user_create")
     * @Security("is_granted('ROLE_CITIZEN')")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = new User();

        $form = $this->createForm(MemberType::class, $user, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
            'formType' => array('contact')
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Créer'));

        $form->handleRequest($request);
        if ($user->getEmail() and $em->getRepository('AppBundle:User')->findOneBy(array('email' => $user->getEmail()))) {
            $session = $this->get('session');
            $session->getFlashBag()->add('danger', sprintf("Cet email existe déjà dans la base. Pour le modifier, utiliser l'outil de recherche "));
        } elseif ($form->isSubmitted() and $form->isValid()) {


            $user->setBasicFields($user->getEmail());
            $sub = new Subscription();
            $sub->setName($user->getName());
            $sub->setFirstName($user->getFirstName());
            $sub->setAddress($user->getAddress());
            $sub->setPostalCode($user->getPostalCode());
            $sub->setCity($user->getCity());
            $sub->setPhone($user->getPhone());
            $sub->setEmail($user->getEmail());
            $sub->setGetNoticed(true);
            $sub->setUser($user);
            $sub->setRole($user->getRoles()[0]);
            $member = $this->getUser();
            if ($member)
                $sub->setCreatedBy(($member instanceof User) ? $member : null);
            $em->persist($sub);
//            $user->setSubscription($sub);
            $em->persist($user);
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:User:edit.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Ajouter un membre',
            'user' => $user
        ));
    }

    /**
     * @Route(path="/export/{category}", name="user_list", defaults={"category" = null})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function exportAction(Request $request, $category)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:User');
        $response = new StreamedResponse();
        $response->setCallback(function () use ($repository, $category) {
            $handle = fopen('php://output', 'w+');

            if($category == 'citizen')
                fputcsv($handle, ['Email', 'Firstname', 'Lastname', 'Lang', 'Address','Postal code','City','District','Other','Reason'], ';');
            else
                fputcsv($handle, ['Email', 'Firstname', 'Lastname', 'Lang', 'Address','Postal code','City','District','Other', 'Role'], ';');

            $results = $repository->findActiveUsers($category);

            foreach ($results as $user) {
                $role = 'Curieux.se';
                if($user->hasRole('ROLE_ADMIN'))
                    $role = 'Administrateur';
                elseif($user->hasRole('ROLE_CITIZEN'))
                    $role = 'Animateur.trice';
                elseif($user->hasRole('ROLE_FRIEND'))
                    $role = 'Ami.e';
                if ($category and $category == 'citizen')
                    fputcsv(
                        $handle, array(
                        $this->get('app.manager.data')->getEmail($user), $user->getFirstName(), $user->getName(), 'fr', $user->getAddress(), $user->getPostalCode(), $user->getPlace(), $user->getDistrict() ? $user->getDistrict() : '', $user->getOther(), $user->getReason()),
                        ';'
                    );
                else
                fputcsv(
                    $handle, array(
                    $this->get('app.manager.data')->getEmail($user), $user->getFirstName(), $user->getName(), 'fr',  $user->getAddress(), $user->getPostalCode(), $user->getPlace(), $user->getDistrict() ? $user->getDistrict() : '', $user->getOther(), $role ),
                    ';'
                );
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="export-users.csv"');

        return $response;
    }

    /**
     * @Security("has_role('ROLE_CITIZEN')")
     * @Route(path="/liste", name="members_search_query", options={"expose"=true})
     */
    public function searchAction(Request $request)
    {
        $searchString = $request->get('email');
        if(strlen($searchString) < 1)
            return new Response("Invalid search query", 406);

        $entityManager = $this->getDoctrine()->getManager();
        if($this->isGranted('ROLE_ADMIN'))
            $query = $entityManager->createQuery("
            SELECT u FROM AppBundle:User u
            WHERE u.email LIKE :searchString OR u.firstName LIKE :searchString OR u.name LIKE :searchString 
            ORDER BY u.email ASC")
                ->setParameter('searchString', '%' . $searchString . '%');
        else if($this->isGranted('ROLE_CITIZEN')) {
            $visibility = DataManager::AUTH_NONE;
            $query = $entityManager->createQuery("
            SELECT u FROM AppBundle:User u
            WHERE (u.email LIKE :searchString AND u.emailVisibility <> :visibility)
            OR (u.name LIKE :searchString)
            OR (u.firstName LIKE :searchString)
            ORDER BY u.email ASC")
                ->setParameter('searchString', '%' . $searchString . '%')
                ->setParameter('visibility', $visibility);
        } else if($this->isGranted('ROLE_FRIEND')) {
            $visibility = DataManager::AUTH_FRIEND;
            $query = $entityManager->createQuery("
              SELECT u FROM AppBundle:User u
              WHERE (u.email LIKE :searchString AND u.emailVisibility = :visibility)
              OR (u.name LIKE :searchString)
              OR (u.firstName LIKE :searchString)
              ORDER BY u.email ASC")
                ->setParameter('searchString', '%' . $searchString . '%')
                ->setParameter('visibility', $visibility);
        } else
            return new Response('Forbidden', 400, array('Content-Type' => 'application/text'));

        $members = $query->getResult();
        return $this->sendJsonResponse($members);

    }

    private function sendJsonResponse($members)
    {
        $membersArray = array();
        foreach ($members as $member) {
            $membersArray[] = array('email' => $this->get('app.manager.data')->getEmail($member), 'firstName' => $member->getFirstName(), 'name' => $member->getName());
        }

        return new Response(json_encode($membersArray), 200, array('Content-Type' => 'application/json'));

    }
}
