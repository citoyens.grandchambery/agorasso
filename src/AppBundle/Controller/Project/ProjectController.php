<?php

namespace AppBundle\Controller\Project;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Content\SocialData;
use AppBundle\Entity\Project\Contribution;
use AppBundle\Entity\Project\Project;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Content\PublicForm;
use AppBundle\Form\Content\FormObjectType;
use AppBundle\Form\Content\SocialDataType;
use AppBundle\Form\Project\ContributionType;
use AppBundle\Object\FormObject;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Project controller.
 * @Route("/projet")
 */
class ProjectController extends AppController
{

    /**
     *  @Security("is_granted('ROLE_CITIZEN')")
     *  @Route("/{id}/comments", name="project_comments")
     *  @param Project $project
     */
    public function projectCommentsAction(Request $request, Project $project)
    {
        $em = $this->getDoctrine()->getManager();
        return $this->render('AppBundle:Project:comments.html.twig', array(
            'project' => $em->getRepository('AppBundle:Project\Project')->queryProject()
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/{id}/view", name="project_view")
     *  @param Project $project
     */
    public function projectViewAction(Request $request, Project $project)
    {
        return $this->render('AppBundle:Project:index.html.twig', array(
            'project' => $project
        ));
    }

    protected function contributionOpen(Request $request, Contribution $contribution)
    {
        $status = Contribution::STATUS_OPEN;

        $form = $this->createForm(ContributionType::class, $contribution, array(
            'action' => $this->generateUrl('contribution_edit', array('id' => $contribution->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Commencer'));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($contribution->getStatus() == Contribution::STATUS_OPEN) {
                $contribution->setStatus(Contribution::STATUS_QUESTIONS);
            }
            $em->flush();
            $this->addFlash('success', sprintf("Début de l'entretien"));
            return $this->redirectToRoute('contribution_edit', array('id'=>$contribution->getId()));
        }
        return $this->render('AppBundle:Project\Contribution:edit.html.twig', array(
            'contribution' => $contribution,
            'form' => $form->createView(),
            'status' => $status
        ));
    }

    protected function contributionValidation(Request $request, Contribution $contribution)
    {
        $status = Contribution::STATUS_VALIDATION;

        $form = $this->createForm(ContributionType::class, $contribution, array(
            'action' => $this->generateUrl('contribution_edit', array('id' => $contribution->getId())),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Valider'));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($contribution->getStatus() == Contribution::STATUS_VALIDATION) {
                $contribution->setStatus(Contribution::STATUS_CLASSIFICATION);
            }
            $em->flush();
            $this->addFlash('success', sprintf("Entretien validé"));
            return $this->redirectToRoute('contribution_edit', array('id'=>$contribution->getId()));
        }
        return $this->render('AppBundle:Project\Contribution:edit.html.twig', array(
            'contribution' => $contribution,
            'form' => $form->createView(),
            'status' => $status
        ));
    }

    protected function contributionAnalysis(Request $request, Contribution $contribution)
    {
        $status = Contribution::STATUS_ANALYSIS;
        return $this->render('AppBundle:Project\Contribution:edit.html.twig', array(
            'contribution' => $contribution,
            'status' => $status
        ));
    }

    protected function contributionClassification(Request $request, Contribution $contribution)
    {
        $status = Contribution::STATUS_CLASSIFICATION;
        return $this->render('AppBundle:Project\Contribution:edit.html.twig', array(
            'contribution' => $contribution,
            'status' => $status
        ));
    }
    
    protected function contributionSocialData(Request $request, Contribution $contribution)
    {
        $status = Contribution::STATUS_SOCIALDATA;
        $socialData = $contribution->getSocialData();
        if(!$socialData) {
            $socialData = new SocialData();
        }

        $form = $this->createForm(SocialDataType::class, $socialData, array(
            'action' => $this->generateUrl('contribution_edit', array('id' => $contribution->getId(), 'status' => $status)),
            'method' => 'POST',
        ));
        $form->add('submit', SubmitType::class, array('label' => 'Valider'));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($socialData);
                if(!$contribution->getSocialData()) {
                    $contribution->setSocialData($socialData);
                }
                if($contribution->getStatus() == Contribution::STATUS_SOCIALDATA) {
                    $contribution->setStatus(Contribution::STATUS_VALIDATION);
                }
                $em->flush();
            $this->addFlash('success', sprintf("Les données sociales ont été enregistrées."));
                return $this->redirectToRoute('contribution_edit', array('id'=>$contribution->getId()));
        }
        return $this->render('AppBundle:Project\Contribution:edit.html.twig', array(
            'socialData' => $socialData,
            'contribution' => $contribution,
            'form' => $form->createView(),
            'status' => $status
        ));
    }

    protected function contributionQuestions(Request $request, Contribution $contribution)
    {
        $status = Contribution::STATUS_QUESTIONS;
        $publicForm = $contribution->getPublicForm();
        $formObject = new FormObject($publicForm, $contribution);
        $form = $this->createForm(FormObjectType::class, $formObject, array(
            'action' => $this->generateUrl('contribution_edit', array('id' => $contribution->getId(), 'status' => $status)),
            'method' => 'POST',
            'formType' => 'project'
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid() ) {
            $em = $this->getDoctrine()->getManager();
            foreach ($formObject->getResponses() as $sub) {
                $response = $contribution->getResponse($sub->getAnswer());
                if($response == null) {
                    $sub->setPublicForm($publicForm);
                    $em->persist($sub);
                    $publicForm->addResponse($sub);
                    $sub->setContribution($contribution);
                }
            }
            if($contribution->getStatus() == Contribution::STATUS_QUESTIONS) {
                $contribution->setStatus(Contribution::STATUS_SOCIALDATA);
            }
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', sprintf("Les réponses de l'entretien ont été enregistrées."));
            return $this->redirectToRoute('contribution_edit', array('id'=>$contribution->getId()));
        }

        return $this->render('AppBundle:Project\Contribution:edit.html.twig', array(
            'contribution' => $contribution,
            'publicForm' => $publicForm,
            'form' => $form->createView(),
            'status' => $status
        ));
    }

    /**
     *  @Security("is_granted('UPDATE_CONTRIBUTION', contribution)")
     *  @Route("/contribution/{id}/edit/{status}", name="contribution_edit",defaults={"status" = null})
     *  @ParamConverter("contribution", class="AppBundle:Project\Contribution", options={"mapping": {"id": "id"}})
     */
    public function contributionEditAction(Request $request, Contribution $contribution, $status = null)
    {
        if(!$status) {
            $status = $contribution->getStatus();
        }
        switch($status) {
            case Contribution::STATUS_OPEN:
                return $this->contributionOpen($request, $contribution);
                break;
            case Contribution::STATUS_QUESTIONS:
                return $this->contributionQuestions($request, $contribution);
                break;
            case Contribution::STATUS_SOCIALDATA:
                return $this->contributionSocialData($request, $contribution);
                break;
            case Contribution::STATUS_VALIDATION:
                return $this->contributionValidation($request, $contribution);
                break;
            case Contribution::STATUS_CLASSIFICATION:
                return $this->contributionClassification($request, $contribution);
                break;
            case Contribution::STATUS_ANALYSIS:
                return $this->contributionAnalysis($request, $contribution);
                break;
        }
    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/{id}/contribution/add", name="contribution_add")
     *  @param Project $project
     */
    public function contributionAddAction(Request $request, Project $project)
    {
        $contribution = new Contribution();
        $contribution->addSupervisedProject($project);
        $contribution->setType(Contribution::TYPE_CITIZEN);
        $contribution->setProject($project);
        $contribution->setPublicForm($project->getCitizenForm());
        $em = $this->getDoctrine()->getManager();
        $em->persist($contribution);
        $em->flush();
        return $this->redirectToRoute('contribution_edit', array('id' => $contribution->getId()));
    }

    /**
     *  @Security("is_granted('VIEW_CONTRIBUTION', contribution)")
     *  @Route("/contribution/{id}/view", name="contribution_view")
     *  @ParamConverter("contribution", class="AppBundle:Project\Contribution",options={"mapping": {"id": "id"}})
     */
    public function contributionViewAction(Request $request, Contribution $contribution)
    {
        return $this->render('AppBundle:Project\Contribution:view.html.twig', array(
            'contribution' => $contribution,
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/contribution/index/{mine}", name="contribution_index", defaults={"mine" = false})
     */
    public function contributionIndexAction(Request $request, $mine = false)
    {
        $em = $this->getDoctrine()->getManager();
        $contributions = null;
        $project = $em->getRepository('AppBundle:Project\Project')->queryProject();
        if($mine) {
            $contributions = $this->getUser()->getAttachedContributions();
        } else {
            $contributions = $em->getRepository('AppBundle:Project\Contribution')->queryContributions()->getQuery()->getResult();
        }
        return $this->render('AppBundle:Project\Contribution:index.html.twig', array(
            'contributions' => $contributions,
            'project' => $project,
            'mine' => $mine
        ));
    }
}
