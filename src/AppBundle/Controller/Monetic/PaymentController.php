<?php

namespace AppBundle\Controller\Monetic;

use AppBundle\Form\Monetic\PaymentSearchType;
use Doctrine\Common\Collections\ArrayCollection;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Monetic\Payment;
use AppBundle\Form\Monetic\PaymentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Document controller.
 * @Security("has_role('ROLE_CONTACT')")
 * @route("/espace/paiements")
 */
class PaymentController extends Controller
{

    /**
     * @Route("/", name="payment_index")
     * @Security("has_role('ROLE_MONETIC')")
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('paymentPage', 1);
        $searchForm = $this->createForm(PaymentSearchType::class, null, array());
        $searchForm->handleRequest($request);
        $filters = $searchForm->getData();

        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:Monetic\Payment')->findPayments($filters);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);

        return $this->render('AppBundle:Monetic\Payment:index.html.twig', array(
            'payments' => $pagerfanta,
            'searchForm' => $searchForm->createView()
        ));

    }

    /**
     * @Route(path="/{id}/voir", name="payment_view")
     * @Security("is_granted('VIEW_PAYMENT', payment)")
     * @param Request $request
     * @param Payment $payment
     */
    public function viewPayment(Request $request, Payment $payment)
    {
        $payments = new ArrayCollection();
        $payments[] = $payment;
        return $this->render('AppBundle:Monetic\Payment:view.html.twig', array(
            'payment' => $payment,
        ));
    }

    protected function createPaymentForm($payment, $user, $route)
    {
        return $this->createForm(PaymentType::class, $payment, array(
            'method' => 'POST',
            'action' => $this->generateUrl($route, array("user" => $user))
        ));
    }

    /**
     * @Route(path="/ajouter/{user_id}", name="payment_add")
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"user_id": "id"}})
     * })
     * @Security("is_granted('ROLE_MONETIC')")
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function addPaymentAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();

        $payment = new Payment();
        $payment->setPayer($user);
        $payment->setOrigin(Payment::ORIGIN_MANUAL);
        $payment->setStatus(Payment::STATUS_PROCESSED);

        $form = $this->createPaymentForm($payment, $user, 'payment_add');
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $usr = $em->getRepository('AppBundle:User')
                ->findOneByEmail($payment->getEmail());
            if(!$usr) {
                $this->addFlash('danger', sprintf("Cet email n'existe pas dans la base."));
            } else {
                $em->persist($payment);
                $em->flush();

                $this->addFlash('success', sprintf("Le paiement a été ajouté."));
                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
            }
        }
        return $this->render('AppBundle:Monetic\Payment:edit.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('ROLE_MONETIC')")
     *  @Route("/{id}/modifier", name="payment_edit")
     *  @ParamConverter("payment", class="AppBundle:Monetic\Payment", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Payment $payment)
    {
        $form = $this->createPaymentForm($payment, $payment->getPayer(), 'payment_edit');

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', sprintf("Le paiement a été modifié."));
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Monetic\Payment:edit.html.twig', array(
            'user' => $payment->getPayer(),
            'form' => $form->createView(),
        ));
    }
}