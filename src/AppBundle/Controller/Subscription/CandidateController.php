<?php


namespace AppBundle\Controller\Subscription;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User\Subscription;
use AppBundle\Form\User\SubscriptionType;
use AppBundle\Form\User\ConfirmSubscriptionType;
use AppBundle\Entity\Document\UploadedFile;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Site\Block;
use AppBundle\Entity\Site\PageTemplate;
use AppBundle\Controller\AppController;

/**
 * Candidate controller.
 * @route("/candidat")
 */
class CandidateController extends AppController
{
    /**
     * @Route("/contact/{step}", defaults={"step"="1"}, name="candidate")
     */
    public function contactAction(Request $request, $step)

    {
        $em = $this->getDoctrine()->getManager();

        $session = $this->get('session');
        $sponsor = $session->get('sponsor');
        $candidate = null;
        if ($sponsor == null) {
            $sponsor = new Subscription();
            $form = $this->createForm(SubscriptionType::class, $sponsor, array(
                'method' => 'POST', 'formType' => 'sponsor'
            ));
            $step = 1;
        } else {
            if($step > 2) {
                $session->set("sponsor", null);
                $session->set("candidate", null);
                return $this->renderAppPage('candidate', array('form' => null, 'step' => 3, 'action' => 'candidate'), array('rightBlockName' => 'success'));
            }

            if($candidate == null) {
                $candidate = new Subscription(Subscription::TYPE_CANDIDATE_SPONSOR);
                $step = 2;
            }
            $form = $this->createForm(SubscriptionType::class, $candidate, array(
                    'method' => 'POST', 'formType' => 'candidate', 'validation_groups' => array('Default', 'other_candidates')
                ));
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            if (!$candidate) {
                $sponsor->setGetNoticed(false);
                $em->persist($sponsor);
                $em->flush();
                $session->set("sponsor", $sponsor);
                return $this->redirectToRoute('candidate', array('step' => 2));
            }
            if($candidate) {
                $sponsor = $em->getRepository('AppBundle:User\Subscription')
                    ->findOneById($sponsor->getId());
                if($candidate->getGetNoticed()) {
                    $sponsor->setGetNoticed(true);
                }
                $newContact = false;
                $contact = $this->get('app.helper.base')->userEmailExist($sponsor->getEmail());
                if(!$contact) {
                    $newContact = true;
                    $contact = new User(
                        array('email' => $sponsor->getEmail(),
                            'firstName' => $sponsor->getFirstName(),
                            'name' => $sponsor->getName(),
                            'role' => $sponsor->getRole(),
                            'phone' => $sponsor->getPhone()));
                    $contact->setPlace($sponsor->getTown(), $sponsor->getDistrict(), $sponsor->getCity());
                    $em->persist($contact);
                    $em->flush();
                }

                $candidateSearch = $this->get('app.helper.base')->candidateExist($candidate->getEmail(), Subscription::TYPE_CANDIDATE_SPONSOR);
                if(!$candidateSearch) {
                    $candidate->setSponsor($contact);
                    $em->persist($candidate);
                } else {
                    $candidate = $candidateSearch;
                    $candidate->incWeight();
                }

                $em->flush();
                $session->set("sponsor", null);
                $session->set("candidate", null);

                $mailer = $this->get('app.manager.mailer');
                if($newContact) {
                    $mailer->sendSubscription($sponsor);
                }
                $mailer->sendSponsor($candidate);
                $this->get('session')->getFlashBag()->add('success', sprintf("Un courriel vous a été envoyé."));
                $this->get('session')->getFlashBag()->add('warning', sprintf("Si vous ne l'avez pas reçu, regardez dans votre dossier \"courriers indésirables\" (Spams)."));
                $this->get('session')->getFlashBag()->add('warning', sprintf("Indiquez à votre messagerie que ce n'est pas un courrier indésirable, sinon vous ne recevrez pas la lettre d'information."));
                return $this->renderAppPage('candidate', array('form' => $form, 'step' => 3, 'action' => 'candidate'), array('rightBlockName' => 'success'));
            }
        }
        
        return $this->renderAppPage('candidate', array('form' => $form->createView(), 'step' => $step, 'action' => 'sponsor'), array('rightBlockName' => 'candidate'));
    }

    /**
     * @Route("/volontaire/{step}", defaults={"step"="1"}, name="volunteer")
     */
    public function candidateAction(Request $request, $step)

    {
        $em = $this->getDoctrine()->getManager();

        $session = $this->get('session');
        $candidate = $session->get('candidate');
        if ($candidate == null) {
            $candidate = new Subscription(Subscription::TYPE_CANDIDATE);
            $form = $this->createForm(SubscriptionType::class, $candidate, array(
                'method' => 'POST', 'formType' => 'volunteer', 'validation_groups' => ['Default', 'candidates']
            ));
            $step = 1;
        } else {
            if($step > 2) {
                $session->set("candidate", null);
                return $this->renderAppPage('volunteer', array('form' => null, 'step' => 3, 'action' => 'volunteer'), array('rightBlockName' => 'success'));
            }
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
                $newContact = false;
                $contact = $this->get('app.helper.base')->userEmailExist($candidate->getEmail());
                if(!$contact) {
                    $newContact = true;
                    $contact = new User(
                        array('email' => $candidate->getEmail(),
                            'firstName' => $candidate->getFirstName(),
                            'name' => $candidate->getName(),
                            'role' => $candidate->getRole(),
                            'phone' => $candidate->getPhone()));
                    $contact->setPlace($candidate->getTown(), $candidate->getDistrict(), $candidate->getCity());
                    $em->persist($contact);
                    $em->flush();
                }

                $candidateSearch = $this->get('app.helper.base')->candidateExist($candidate->getEmail(), Subscription::TYPE_CANDIDATE);

                if(!$candidateSearch) {
                    $candidate->setUser($contact);
                    $em->persist($candidate);
                }

                $em->flush();
                $session->set("candidate", null);

                $mailer = $this->get('app.manager.mailer');
                if($newContact) {
                    $mailer->sendSubscription($candidate);
                }
                $mailer->sendCandidate($candidate);
                $this->get('session')->getFlashBag()->add('success', sprintf("Un courriel vous a été envoyé."));
                $this->get('session')->getFlashBag()->add('warning', sprintf("Si vous ne l'avez pas reçu, regardez dans votre dossier \"courriers indésirables\" (Spams)."));
                $this->get('session')->getFlashBag()->add('warning', sprintf("Indiquez à votre messagerie que ce n'est pas un courrier indésirable, sinon vous ne recevrez pas la lettre d'information."));
                return $this->renderAppPage('volunteer', array('form' => $form, 'step' => 3, 'action' => 'volunteer'), array('rightBlockName' => 'success'));
        }

        return $this->renderAppPage('volunteer', array('form' => $form->createView(), 'step' => $step, 'action' => 'sponsor'), array('rightBlockName' => 'volunteer'));
    }
}