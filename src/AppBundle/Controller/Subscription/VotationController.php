<?php

namespace AppBundle\Controller\Subscription;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Subscription\Votation;
use AppBundle\Entity\Subscription\Vote;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Form\Subscription\VoteGenerationType;
use AppBundle\Form\Subscription\VotationObjectType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use AppBundle\Object\VotationObject;

/**
 * Votation controller.
 * @Route("/espace/votes")
 */
class VotationController extends AppController
{

    /**
     * @Route("/", name="votations")
     * @Security("is_granted('ROLE_CONTACT')")
     */
    public function indexAction(Request $request)

    {
        if(!$this->getUser()->isEnabled())
            return $this->redirectToRoute('fos_user_security_logout');
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:Subscription\Votation')->queryAll(true);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render('AppBundle:Subscription\Votation:index.html.twig', array(
            'votations' => $pagerfanta,
        ));

    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/{id}/voir", name="votation_view")
     * @ParamConverter("votation", class="AppBundle:Subscription\Votation", options={"mapping": {"id": "id"}})
     */
    public function viewAction(Request $request, Votation $votation)
    {
        return $this->render('AppBundle:Subscription\Votation:view.html.twig', array(
            'votation' => $votation
        ));
    }

    protected function votationObjectValid(VotationObject $votationObject, Votation $votation)
    {
        $votes = $votationObject->getVotes();

        return $votes == $votation->getWeight();
    }

    /**
     *  @Security("is_granted('CAN_VOTE', votation)")
     *  @Route("/{id}/action", name="votation_do")
     *  @ParamConverter("votation", class="AppBundle:Subscription\Votation", options={"mapping": {"id": "id"}})
     */
    public function voteAction(Request $request, Votation $votation)
    {
        $votationObject = new VotationObject($votation);
        $form = $this->createForm(VotationObjectType::class, $votationObject, array(
            'action' => $this->generateUrl('votation_do', array('id' => $votation->getId())),
            'method' => 'POST',
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid() ) {
            if($this->votationObjectValid($votationObject, $votation)) {
                if ($this->getUser()->hasVoted($votation)) {
                    $this->addFlash('danger', sprintf("Vous avez déjà voté."));
                    return $this->redirectToRoute('votation_view', array('id' => $votation->getId()));
                }
                $em = $this->getDoctrine()->getManager();
                foreach ($votationObject->getSupervisedVotationSubs() as $sub) {
                    $em->persist($sub);
                    $votation->addSupervisedVotationSub($sub);
                }
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', sprintf("Merci d'avoir participé! Votre vote a bien été pris en compte."));
                return $this->redirectToRoute('votation_view', array('id' => $votation->getId()));
            } else {
                $this->addFlash('danger', sprintf("Vous avez %d votes", $votation->getWeight()));
            }
        }

        return $this->render('AppBundle:Subscription\Votation:vote.html.twig', array(
            'votation' => $votation,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CITIZEN')")
     *  @Route("/code/index", name="vote_index")
     */
    public function voteIndex(Request $request)
    {

        $page = $request->get('votePage', 1);

        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:Subscription\Vote')->findVotes($this->isGranted('ROLE_ADMIN') ? null : $this->getUser());

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($page);

        return $this->render('AppBundle:Subscription\Vote:index.html.twig', array(
            'votes' => $pagerfanta,
        ));
    }

    protected function checkVote(Vote $vote)
    {
        $em = $this->getDoctrine()->getManager();
        if($em->getRepository('AppBundle:Subscription\Vote')->hasVote($vote->getCode())) {
            return false;
        }
        return true;
    }

    protected function generateCode(Vote $vote)
    {
        $seed = str_split('ABCDEFGHIJKLMNPQRSTUVWXYZ'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $rand = '';
        $letter = array_rand($seed, 1);
//        foreach (array_rand($seed, 1) as $k) $rand .= $seed[$k];
        $random = random_int(1, 999);
//        dump($seed[$letter]);die;
        $vote->setCode(sprintf("%s%03d", $seed[$letter], $random));
    }

    protected function newCode(Vote $vote)
    {
        $i = 0;
        do {
            $this->generateCode($vote);
            $i++;
        }
        while(!$this->checkVote($vote) and $i < 100);
        if($i >= 100) {
            return false;
        }
        return true;
    }

    /**
     *  @Security("is_granted('ROLE_CITIZEN')")
     *  @Route("/code/generation", name="vote_generation")
     */
    public function voteGeneration(Request $request)
    {
        $vote = new Vote();
        if($this->newCode($vote)) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vote);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', sprintf("Le code '%s' a bien été créé", $vote));
        } else {
            $this->addFlash('error', sprintf("impossible de créer un code"));
        }
        return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        $form = $this->createForm(VoteGenerationType::class, $vote, array(
            'action' => $this->generateUrl('vote_generation', array()),
            'method' => 'POST',
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid() and $this->checkVote($vote) ) {
//            dump($vote);die;
                $em = $this->getDoctrine()->getManager();
                $em->persist($vote);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', sprintf("Le code '%s' a bien été créé", $vote));
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }


        return $this->render('AppBundle:Subscription\Vote:edit.html.twig', array(
            'vote' => $vote,
            'form' => $form->createView(),
        ));
    }
}
