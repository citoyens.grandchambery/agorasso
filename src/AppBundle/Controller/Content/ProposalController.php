<?php

namespace AppBundle\Controller\Content;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Content\Proposal;
use AppBundle\Entity\Content\Comment as Comment;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Form\Content\ProposalType;
use AppBundle\Form\Content\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use AppBundle\Form\Content\ProposalSearchType;

/**
 * Proposal controller.
 * @Route("/espace/propositions")
 */
class ProposalController extends AppController
{

    /**
     * @Route("/", name="proposals")
     * @Security("is_granted('ROLE_CONTACT')")
     */
    public function indexAction(Request $request)

    {
        if(!$this->getUser()->isEnabled())
            return $this->redirectToRoute('fos_user_security_logout');
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');
        $page = $request->get('proposalsPage', 1);
        $searchForm = $this->createForm(ProposalSearchType::class, null, array());
        $searchForm->handleRequest($request);
        $filters = $searchForm->getData();
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:Content\Proposal')->findProposals($filters);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);
//        $votation = $this->getDoctrine()->getManager()->getRepository('AppBundle:Content\Proposal')->findVotation($filters);

        return $this->render('AppBundle:Content\Proposal:index.html.twig', array(
            'proposals' => $pagerfanta,
            'searchForm' => $searchForm->createView(),
//            'votation' => $votation
        ));

    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("{id}/voir", name="proposal_view")
     * @ParamConverter("proposal", class="AppBundle:Content\Proposal", options={"mapping": {"id": "id"}})
     */
    public function viewAction(Request $request, Proposal $proposal)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getFilters()->disable('softdeleteable');
        return $this->render('AppBundle:Content\Proposal:view.html.twig', array(
            'proposal' => $proposal
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CITIZEN')")
     *  @Route("/{id}/modifier", name="proposal_edit")
     *  @ParamConverter("proposal", class="AppBundle:Content\Proposal", options={"mapping": {"id": "id"}})
     */
    public function editAction(Request $request, Proposal $proposal)
    {
        $form = $this->createForm(ProposalType::class, $proposal, array(
            'action' => $this->generateUrl('proposal_edit', array('id' => $proposal->getId())),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Content\Proposal:edit.html.twig', array(
            'proposal' => $proposal,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{member_id}/ajouter/{group_id}", name="proposal_add",
     *     defaults={"group_id": null},)
     * @ParamConverter("member", class="AppBundle:User", options={"mapping": {"member_id": "id"}})
     * @ParamConverter("group", class="AppBundle:Group\Group", options={"mapping": {"group_id": "id"}})
     *  @Security("is_granted('ROLE_CONTACT')")
     */
    public function addProposalAction(Request $request, User $member, Group $group = null)
    {
        $proposal = new Proposal($member);

        if($group)
            $proposal->addReferedGroup($group);

        $form = $this->createForm(ProposalType::class, $proposal, array(
            'action' => $this->generateUrl('proposal_add', array('member_id' => $member->getId(), 'group_id' => $group ? $group->getId() : null)),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $proposal->setCreatedBy($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($proposal);
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Content\Proposal:edit.html.twig', array(
            'member' => $member,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/{id}/commenter", name="proposal_comment")
     *  @ParamConverter("proposal", class="AppBundle:Content\Proposal", options={"mapping": {"id": "id"}})
     */
    public function commentAction(Request $request, Proposal $proposal)
    {
        $comment = new Comment();
        $comment->setProposal($proposal);
        $form = $this->createForm(CommentType::class, $comment, array(
            'action' => $this->generateUrl('proposal_comment', array('id' => $proposal->getId())),
            'method' => 'POST',
        ));

//        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $proposal->incNComments();
            $em->persist($comment);
            $this->getDoctrine()->getManager()->flush();
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Content\Comment:edit.html.twig', array(
            'comment' => $comment,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/{id}/repondre/{comment_id}", name="proposal_answer")
     *  @ParamConverter("proposal", class="AppBundle:Content\Proposal", options={"mapping": {"id": "id"}})
     *  @ParamConverter("comment", class="AppBundle:Content\Comment", options={"mapping": {"comment_id": "id"}})
     */
    public function answerAction(Request $request, Proposal $proposal, Comment $comment)
    {
        $answer = new Comment();
        $answer->setComment($comment);
        $form = $this->createForm(CommentType::class, $answer, array(
            'action' => $this->generateUrl('proposal_answer', array('id' => $proposal->getId(), 'comment_id' => $comment->getId())),
            'method' => 'POST',
        ));

//        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $proposal->incNComments();
            $em->persist($answer);
            $this->getDoctrine()->getManager()->flush();
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Content\Comment:edit.html.twig', array(
            'comment' => $answer,
            'form' => $form->createView(),
        ));
    }

    /**
     *  @Security("is_granted('ROLE_CITIZEN')")
     *  @Route("/{id}/fermer/{comment_id}", name="proposal_close")
     *  @ParamConverter("proposal", class="AppBundle:Content\Proposal", options={"mapping": {"id": "id"}})
     *  @ParamConverter("comment", class="AppBundle:Content\Comment", options={"mapping": {"comment_id": "id"}})
     */
    public function closeCommentAction(Request $request, Proposal $proposal, Comment $comment)
    {
        $form = $this->createForm(CommentType::class, $comment, array(
            'action' => $this->generateUrl('proposal_close', array('id' => $proposal->getId(), 'comment_id' => $comment->getId())),
            'method' => 'POST',
            'close' => "1",

        ));

//        $form->add('submit', SubmitType::class, array('label' => 'Enregistrer'));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }

        return $this->render('AppBundle:Content\Comment:edit.html.twig', array(
            'comment' => $comment,
            'form' => $form->createView(),
        ));
    }
}
