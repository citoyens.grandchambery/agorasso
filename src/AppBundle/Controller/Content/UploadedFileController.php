<?php

namespace AppBundle\Controller\Content;

use AppBundle\Form\Document\SearchType;
use Doctrine\Common\Collections\ArrayCollection;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Document\UploadedFile;
use AppBundle\Form\Document\UploadedFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Group\Group;
use AppBundle\Entity\User;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * Document controller.
 * @Security("has_role('ROLE_CONTACT')")
 * @route("/espace/documents")
 */
class UploadedFileController extends Controller
{

    /**
     * @Route("/", name="documents")
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('newsPage', 1);
        $searchForm = $this->createForm(SearchType::class, null, array());
        $searchForm->handleRequest($request);
        $filters = $searchForm->getData();

        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository('AppBundle:Document\UploadedFile')->findDocuments($filters);

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($page);

        return $this->render('AppBundle:Documents:index.html.twig', array(
            'files' => $pagerfanta,
            'searchForm' => $searchForm->createView()
        ));

    }

    /**
     * @Route(path="/{id}", name="file_display")
     * @Security("is_granted('VIEW_FILE', file)")
     * @param Request $request
     * @param UploadedFile $file
     * @return Response
     */
    public function displayAttachedFile(Request $request, UploadedFile $file)
    {
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        $path = $helper->asset($file, 'file');
        $response = new Response(file_get_contents($file->getFile()));
        $response->headers->set('Content-Type', mime_content_type($path));
        $response->headers->set('Content-Disposition', 'inline;filename="' . $file->getOriginalFileName());
        return $response;
    }

    /**
     * @Route(path="/{id}/voir", name="file_view")
     * @Security("is_granted('VIEW_FILE', file)")
     * @param Request $request
     * @param UploadedFile $file
     */
    public function viewAttachedFile(Request $request, UploadedFile $file)
    {
        $files = new ArrayCollection();
        $files[] = $file;
        return $this->render('AppBundle:Documents:view.html.twig', array(
            'file' => $file,
            'files' => $files,
        ));
    }

    protected function attachFile(UploadedFile $file, $owner)
    {
        $owner->addAttachedFile($file);
    }

    protected function getAddFileRoute(Group $group, $event, $member, $from_type)
    {
        return $this->generateUrl('file_add', array('element' => $from_type == 'event' ? $event->getId() : $group->getId(), 'member_id' => $member->getId(), 'from_type' => $from_type));
    }


    protected function createAddForm($file, Group $group, $event, $member, $from_type)
    {
        return $this->createForm(UploadedFileType::class, $file, array(
            'method' => 'POST',
            'validation_groups' => array('fileCreation', 'Default'),
            'action' => $this->getAddFileRoute($group, $event, $member, $from_type),
            'from_type' => $from_type
        ));
    }

    /**
     * @Route(path="/{element}/ajouter/{member_id}/{from_type}", name="file_add")
     * @ParamConverter("member", class="AppBundle:User", options={"mapping": {"member_id": "id"}})
     * })
     * @Security("is_granted('ADD_FILE', member)")
     * @param Request $request
     * @param $element
     * @param User $member
     * @param $from_type
     * @return Response
     */
    public function addFileAction(Request $request, $element, User $member, $from_type = 'group')
    {
        $event = null;
        $em = $this->getDoctrine()->getManager();
        if ($from_type == 'group') {
            $group = $em->getRepository('AppBundle:Group\Group')->find($element);
        } elseif ($from_type == 'event') {
            $event = $em->getRepository('AppBundle:Time\Xvent')->find($element);
            if ($event)
                $group = $event->getReferedGroups()->first();
            else
                throw new HttpException(404, "Création impossible");
        } else
            throw new HttpException(404, "Création impossible");

        $file = new UploadedFile($group, $event, $member);

        $form = $this->createAddForm($file, $group, $event, $member, $from_type);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $this->attachFile($file, $member);
            $file->setCreatedBy($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $this->addFlash('success', sprintf("Votre fichier a été ajouté."));
            return $this->reloadFilesResponse($member, $group, $event, $from_type);
        }
        return $this->generateFormView($member, $file, $form, $this->getAddingFileTitle($member, $group, $event));
    }

    protected function reloadFilesResponse($owner, $group, $event, $from_type = 'group')
    {
        return $this->render('AppBundle:Files:reloadAttachedFiles.js.twig', array(
            'attachedFilesTemplate' => $this->getListFilesTemplate($group, $event),
            'attachedFilesTarget' => $this->getListFilesTarget($group, $event, $from_type),
            'element' => $owner,
            'group' => $group,
            'event' => $event,
            'from_type' => $from_type
        ));
    }

    protected function generateFormView($owner, $file, $form, $title)
    {
        return $this->render('AppBundle:Files:form.html.twig', array(
            'file' => $file,
            'element' => $owner,
            'form' => $form->createView(),
            'title' => $title,
        ));
    }

    protected function getAddingFileTitle($owner, $group, $event)
    {
        if ($group)
            return sprintf("Ajout d'un fichier au groupe: %s", $group);
        if ($event)
            return sprintf("Ajout d'un fichier à l'événement: %s", $event);

        return sprintf("Ajout d'un fichier");
    }

    protected function getUpdatingFileTitle(UploadedFile $file)
    {
        if ($file->getGroup())
            return sprintf("Modification d'un fichier du groupe: %s", $file->getGroup());
        if ($file->getEvent())
            return sprintf("Modification d'un fichier de l'événement: %s", $file->getEvent());
        return sprintf("Modification d'un fichier");
    }

    protected function getListFilesTemplate($group, $event)
    {
        return 'AppBundle:Documents:list.html.twig';
    }

    protected function getListFilesTarget($group = null, $event, $from_type = 'group')
    {
        if ($group and $from_type == 'group') {
            return sprintf('"#attachedFiles.%s"', $group->getName());
        } elseif ($event and $from_type == 'event') {
            return sprintf('"#attachedFiles.%s"', $event->getName());
        } else {
            return '"#attachedFiles.noGroup"';
        }
    }

    protected function getUpdateFileRoute(UploadedFile $file, $from_type)
    {
        return $this->generateUrl('file_update', array('file_id' => $file->getId(), 'from_type' => $from_type));
    }

    protected function createUpdateForm($file, $from_type)
    {
        return $this->createForm(UploadedFileType::class, $file, array(
            'method' => 'POST',
            'action' => $this->getUpdateFileRoute($file, $from_type),
        ));
    }

    /**
     * @Route(path="/{file_id}/modifier/{from_type}", name="file_update")
     * @ParamConverter("file", class="AppBundle:Document\UploadedFile", options={"mapping": {"file_id": "id"}})
     * @Security("is_granted('UPDATE_FILE', file)")
     */
    public function updateFileAction(Request $request, UploadedFile $file, $from_type = 'group')
    {
        $owner = $this->getUser();
        $form = $this->createUpdateForm($file, $from_type);
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $file->setUpdatedBy($this->getUser())
                ->setUpdatedAt(new \Datetime());
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->addFlash('success', sprintf("Votre fichier a bien été modifié."));
            return $this->reloadFilesResponse($owner, $file->getGroup(), $file->getEvent(), $from_type);
        }
        return $this->generateFormView($owner, $file, $form, $this->getUpdatingFileTitle($file));
    }

    /**
     * @Route(path="/{file_id}/supprimer/{from_type}", name="file_delete")
     * @ParamConverter("file", class="AppBundle:Document\UploadedFile", options={"mapping": {"file_id": "id"}})
     * @Security("is_granted('DELETE_FILE', file)")
     */
    public function deleteFileAction(Request $request, UploadedFile $file, $from_type = 'group')
    {
        $owner = $this->getUser();
        if (!$request->isXmlHttpRequest()) {
            throw new HttpException(406, "Ajax nécessaire...");
        }

//        $owner->removeAttachedFile($file);
        $group = $file->getGroup();
        $event = $file->getEvent();
        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();
        $this->addFlash('success', sprintf("Votre fichier a bien été supprimé !"));
        return $this->reloadFilesResponse($owner, $group, $event, $from_type);
    }

}