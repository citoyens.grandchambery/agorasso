<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AppController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
//        if($this->isGranted('ROLE_CONTACT'))
//            return $this->redirectToRoute("dashboard");
//        else
            return $this->renderAppPage('accueil', null, array('pageOptions' => null, $request));
        // replace this example code with whatever you need
//        return $this->render('AppBundle::index.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
//        ]);
    }

    /**
     * @Route("/leprojet", name="projet")
     */
    public function projectAction(Request $request)
    {
        return $this->renderAppPage('leprojet', null, array('pageOptions' => null, $request));
    }

    /**
     * @Route("/laliste", name="liste")
     */
    public function listAction(Request $request)
    {
        return $this->renderAppPage('laliste', null, array('pageOptions' => null), $request);
    }

    /**
     * @Route("/leprojet/proposition/{id}", name="proposition")
     */
    public function propositionAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('AppBundle:Project\Project')->queryProject();
        $proposal = $em->getRepository('AppBundle:Content\Proposal')->findProposal($project, $id);
        $fields = array();
        $fields['proposal'] = $proposal;
        return $this->renderAppPage('proposition', $fields, array('pageOptions' => null), $request);
    }


    /**
     * Génère le sitemap du site.
     *
     * @Route("/sitemap.{_format}", name="front_sitemap", Requirements={"_format" = "xml"})
     */
    public function siteMapAction()
    {
        return $this->render(
            'AppBundle:Front:sitemap.xml.twig',
            ['urls' => $this->get('app.helper.menu')->getSitemap()]
        );
    }
}
