<?php

namespace AppBundle\Controller\Location;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Entity\Location\Town;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;


/**
 * Town controller.
 * @Route("/data")
 */
class TownController extends AppController
{
    private function getNoTown()
    {
        $em = $this->getDoctrine()->getManager();
        $town = new Town();
        $town->setOther(true);
        $town->setUsers($em->getRepository('AppBundle:User')->findNoTownUsers());
        return $town;
    }
    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/town/json", name="town_json")
     */
    public function townJsonAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $towns = $em->getRepository('AppBundle:Location\Town')->queryTowns()->getQuery()->getResult();

        $towns[] = $this->getNoTown();

        try {
            $data = $this->get('jms_serializer')->serialize($towns, 'json', SerializationContext::create());
        } catch(\Exception $e) {
        }

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     *  @Security("is_granted('ROLE_CONTACT')")
     *  @Route("/site/{id}/json", name="site_json")
     *  @param Site $site
     */
    public function siteJsonAction(Request $request, Site $site)
    {
        $response = new Response(file_get_contents($site->getFile()));
        $response->headers->set('Content-Type', 'applicatoin/json');
        $response->headers->set('Content-Disposition', 'inline;filename="' . $site->getOriginalFileName());
        return $response;
    }
}
