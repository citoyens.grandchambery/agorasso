<?php



namespace AppBundle\Controller;

use AppBundle\Entity\Document\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\Group\Group;
use AppBundle\Entity\Site\Block;
use AppBundle\Entity\Site\PageTemplate;


/**
 * Dashboard controller.
 * @Security("has_role('ROLE_CONTACT')")
 * @route("/espace")
 */

class DashboardController extends AppController
{

    protected function getDatas($elements)
    {
        $em = $this->getDoctrine()->getManager();
        $datas = array();
        $infos = $em->getRepository('AppBundle:Time\Info')->queryLastInfos($elements, true)->getQuery()->getResult();
        foreach ($infos as $info) {
            $datas[] = array('id' => $info->getId(), 'type' => 'info', 'title' => $info->getTitle(), 'createdAt' => $info->getCreatedAt());
        }
        $files = $em->getRepository('AppBundle:Document\UploadedFile')->queryLastFiles($elements + 10, true)->getQuery()->getResult();
        foreach ($files as $file) {
            $datas[] = array('id' => $file->getId(), 'type' => 'file', 'title' => $file->getLabel(), 'createdAt' => $file->getCreatedAt());
        }
        $events = $em->getRepository('AppBundle:Time\Xvent')->queryLastEvents(null, true)->getQuery()->getResult();

        foreach ($events as $event) {
            $datas[] = array('id' => $event->getId(), 'type' => 'event', 'title' => $event->getTitle(),
                'createdAt' => $event->getCreatedAt(), 'date' => $event->getDate());
        }
        $times = $em->getRepository('AppBundle:Time\BaseTime')->queryBaseTimes($elements, true)->getQuery()->getResult();
        foreach ($times as $time) {
            $datas[] = array('id' => $time->getId(), 'type' => $time->getDiscr(), 'title' => $time->getTitle(), 'createdAt' => $time->getCreatedAt(),
                'date' => $time->getDate(), 'name' => $time->getName());
        }
        usort($datas, function($a, $b) {
            return ($a['createdAt'] > $b['createdAt']) ? -1 : 1;
        });
        return $datas;
    }
    /**
     * @Route(path="/accueil/{elements}", name="dashboard", defaults={"elements"="10"})
     */
    public function indexAction($elements)
    {
        if(!$this->getUser()->isEnabled())
            return $this->redirectToRoute('fos_user_security_logout');
        $dataManager = $this->get('app.manager.data');

        $actionUrl = null;
        $message = '';
        if(!$dataManager->isValidTown($this->getUser(), $message)) {
            $actionUrl = $this->generateUrl('user_edit', array('id' => $this->getUser()->getId()));
        }
//        $this->addFlash('warning', sprintf("Attention, l'espace citoyen.ne.s est en développement... Si vous avez des remarques partagez-les dans la boîte à idées"));

        $em = $this->getDoctrine()->getManager();
        $masterGroup = $em->getRepository('AppBundle:Group\Group')->queryGroupByWeight(1);
        $assemblee = $em->getRepository('AppBundle:Group\Group')->queryGroupByWeight(0);

        $groups = $em->getRepository('AppBundle:Group\Group')->queryAll(1, $masterGroup)->getQuery()->getResult();
        $events = $em->getRepository('AppBundle:Time\Xvent')->queryDashboardEvents()->getQuery()->getResult();

        $em->getFilters()->disable('softdeleteable');
        $proposals = $em->getRepository('AppBundle:Content\Proposal')->queryProposals()->getQuery()->getResult();
        $votations = $em->getRepository('AppBundle:Subscription\Votation')->queryAll(true)->getQuery()->getResult();
        $project = $em->getRepository('AppBundle:Project\Project')->queryProject();
        $page = $em->getRepository('AppBundle:Site\Page')->findOneBy(array('dashboard' => true));
        if($page) {
            $main = $em->getRepository('AppBundle:Site\Block')->queryBlocks($page, PageTemplate::POSITION_MAIN)->getQuery()->getResult();
        } else {
            $main = null;
        }
        return $this->render('AppBundle:User/Dashboard:index.html.twig', array(
            'assemblee' => $assemblee,
            'masterGroup' => $masterGroup,
            'groups' => $groups,
            'events' => $events,
            'datas' => $this->getDatas($elements, $votations, $events),
            'proposals' => $proposals,
            'votations' => $votations,
            'elements' => $elements,
            'actionUrl' => $actionUrl,
            'project' => $project,
            'page' => $page,
            'main' => $main
        ));

    }

    /**
     * @Route(path="/carte", name="map")
     */
    public function mapAction()
    {
        return $this->render('AppBundle:Map:index.html.twig');
    }
}