<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Api\Citizen;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Form\Time\InfoType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use AppBundle\Form\User\MemberType;
use JMS\Serializer\SerializationContext;

/**
 * Info controller.
 * @Route("/api/signup")
 */
class SignupController extends AppController
{
    /**
     * @Route(path="/members/count", name="allmembers", methods={"GET"})
     */
    public function allmembers(Request $request)
    {
        $category = $request->query->get('category');
        $em = $this->getDoctrine()->getManager();
        $allMembers = $em->getRepository('AppBundle:Api\Citizen')->queryAllMembers($category)->getQuery()->getSingleScalarResult();


        return $this->response($allMembers);
    }

    /**
     * @Route(path="/members", name="members", methods={"GET"})
     */
    public function members(Request $request)
    {
        $category = $request->query->get('category');
        $em = $this->getDoctrine()->getManager();
        $towns = $em->getRepository('AppBundle:Api\Citizen')->queryMembers($category)->getQuery()->getResult();

        try {
            $data = $this->get('jms_serializer')->serialize($towns, 'json', SerializationContext::create());
        } catch(\Exception $e) {
        }

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    private function response($text, $code = 200) {
        $result = sprintf('{"result":"%s"}', $text);
        $response = new Response($result);
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode($code);
        return $response;
    }

    private function responseCitizen(Citizen $citizen)
    {
        return $this->response($citizen->getStatus());
    }

    /**
     * @Route(path="/confirm", name="signup-confirm", methods={"GET"})
     */
    public function confirm(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cookie = $request->query->get('cookie');
        $confirmCitizen = $em->getRepository('AppBundle:Api\Citizen')
            ->findOneBy(array('cookie' => $cookie));
        if(!$confirmCitizen) {
            return $this->response("Inconnu", 404);
        }
        if($confirmCitizen->getActive()) {
            return $this->responseCitizen($confirmCitizen);
        }
        $criteries = [];
        $criteries['email'] = $confirmCitizen->getEmail();
        $criteries['active'] = true;
        if($confirmCitizen->getEnsemble()) {
            $criteries['ensemble'] = true;
        } elseif($confirmCitizen->getDepartment()) {
            $criteries['department'] = true;
        } else {
            $criteries['senat'] = true;
        }
        $activeCitizen = $em->getRepository('AppBundle:Api\Citizen')
            ->findOneBy($criteries);
        if(!$activeCitizen) {
            $confirmCitizen->setActive(true);
            $em->flush();
            return $this->responseCitizen($confirmCitizen);
        } else {
            if ($activeCitizen->getCandidate()) {
                return $this->responseCitizen($activeCitizen);
            }

            if ($activeCitizen->getPub()) {
                if($confirmCitizen->getCandidate()) {
                    $activeCitizen->setCandidate(true);
                    $em->flush();
                }
                return $this->responseCitizen($activeCitizen);
            }

            if ($activeCitizen->getSupport()) {
                if($confirmCitizen->getCandidate() || 
                    $confirmCitizen->getPub()) {
                    $activeCitizen->setCandidate($confirmCitizen->getCandidate());
                    $activeCitizen->setPub($confirmCitizen->getPub());
                    $em->flush();
                }
                return $this->responseCitizen($activeCitizen);
            }
            if($confirmCitizen->getSupport() || $confirmCitizen->getCandidate() || $confirmCitizen->getPub()) {
                $confirmCitizen->setActive(true);
                $activeCitizen->setActive(false);
                $em->flush();
                return $this->responseCitizen($confirmCitizen);
            }
            return $this->responseCitizen($activeCitizen);
        }
        return $this->response("ok");
    }


        // generic function curl
    private function CallAPI($method, $data, $url, $user)
    {
        $encode = false;
        $login=null;
        $password=null;
        $curl = curl_init();

//        die("callapi");
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic ' . $user
                ));
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $encode ? json_encode($data) : $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        if($login and $password) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $login . ":" . $password);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    /**
     * @Route(path="/", name="signup", methods={"POST", "GET"})
     */
    public function signup(Request $request)
    {
        $content = null;
        $category = $request->query->get('category');
        if($request->isMethod('GET')) {
            $email = $request->query->get('email');
        } else {
            $content = json_decode($request->getContent(), true);
            $email = $content->email;
        }
        if(!$email) {
            return $this->response("Error", 403);
        }

        $em = $this->getDoctrine()->getManager();

        $existCitizen = $em->getRepository('AppBundle:Api\Citizen')
            ->findOneBy(array('email' => $email));

        $citizen = new Citizen();
        $citizen->setEmail($email);
        if($category == 'ensemble') {
            $citizen->setEnsemble(true);
        } elseif($category == 'department') {
            $citizen->setDepartment(true);
        } else {
            $citizen->setSenat(true);
        }
        if($request->isMethod('GET')) {
            $citizen->setCity($request->query->get('city'));
            $citizen->setFirstName($request->query->get('firstname'));
            $citizen->setLastName($request->query->get('lastname'));
            $citizen->setSupport($request->query->get('support'));
            $citizen->setPub($request->query->get('pub'));
            $citizen->setCandidate($request->query->get('candidate'));
            $citizen->setInfo($request->query->get('info'));
//            $citizen->setGender($request->query->get('gender'));
            $citizen->setFollower($request->query->get('follower'));
        } else {
            $citizen->setCity($content->city);
            $citizen->setFirstName($content->firstName);
            $citizen->setLastName($content->lastName);
            $citizen->setSupport($content->support);
            $citizen->setPub($content->pub);
            $citizen->setCandidate($content->candidate);
            $citizen->setInfo($content->info);
//            $citizen->setGender($content->gender);
            $citizen->setFollower($content->follower);
        }
        $em->persist($citizen);

        $em->flush();
        $mailer = $this->get('app.manager.mailer');
        if($citizen->getEnsemble()) {
            $mailer->sendEnsemble($citizen);
            if(!$existCitizen) {
                $url = 'https://us17.api.mailchimp.com/3.0/lists/1bb24fc427/members';
                $user = 'e91e2dc795cc3d21bcb8cc8a5fe6ce67-us1';
                $data = '{"email_address":"' . $email . '","status":"subscribed"}';
                $this->CallAPI("POST", $data, $url, $user);
            }
        } else {
            $mailer->sendCitizen($citizen);

            if(!$existCitizen) {
                $url = 'https://us17.api.mailchimp.com/3.0/lists/527b6eeb77/members';
                $user='19bd9262c2748ca1caa4dedcf3cf76ed-us17';
                $data =  '{"email_address":"'.$email.'","status":"subscribed"}';
                $this->CallAPI("POST", $data, $url, $user);
            }
        }

        return $this->responseCitizen($citizen);
    }
}
