<?php

namespace AppBundle\Controller\Api;

use AppBundle\Controller\AppController;
use AppBundle\Entity\Api\Procuration;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;
use AppBundle\Form\Time\InfoType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\ArrayAdapter;
use AppBundle\Form\User\MemberType;
use JMS\Serializer\SerializationContext;

/**
 * Info controller.
 * @Route("/api/procuration")
 */
class ProcurationController extends AppController
{
    private function response($text, $code = 200) {
        $result = sprintf('{"result":"%s"}', $text);
        $response = new Response($result);
        $response->headers->set('Content-Type', 'application/json');
        $response->setStatusCode($code);
        return $response;
    }

    private function responseProcuration(Procuration $procuration)
    {
        return $this->response($procuration->getStatus());
    }

    /**
     * @Route(path="/confirm", name="procuration-confirm", methods={"GET"})
     */
    public function confirm(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cookie = $request->query->get('cookie');
        $confirmProcuration = $em->getRepository('AppBundle:Api\Procuration')
            ->findOneBy(array('cookie' => $cookie));
        if(!$confirmProcuration) {
            return $this->response("Inconnu", 404);
        }
        if($confirmProcuration->getActive()) {
            return $this->responseProcuration($confirmProcuration);
        }
        $criteries = [];
        $criteries['email'] = $confirmProcuration->getEmail();
        $criteries['active'] = true;
        if($confirmProcuration->getEnsemble()) {
            $criteries['ensemble'] = true;
        } elseif($confirmProcuration->getDepartment()) {
            $criteries['department'] = true;
        } else {
            $criteries['senat'] = true;
        }
        $activeProcuration = $em->getRepository('AppBundle:Api\Procuration')
            ->findOneBy($criteries);
        if(!$activeProcuration) {
            $confirmProcuration->setActive(true);
            $em->flush();
            return $this->responseProcuration($confirmProcuration);
        } else {
            if ($activeProcuration->getCandidate()) {
                return $this->responseProcuration($activeProcuration);
            }

            if ($activeProcuration->getPub()) {
                if($confirmProcuration->getCandidate()) {
                    $activeProcuration->setCandidate(true);
                    $em->flush();
                }
                return $this->responseProcuration($activeProcuration);
            }

            if ($activeProcuration->getSupport()) {
                if($confirmProcuration->getCandidate() || 
                    $confirmProcuration->getPub()) {
                    $activeProcuration->setCandidate($confirmProcuration->getCandidate());
                    $activeProcuration->setPub($confirmProcuration->getPub());
                    $em->flush();
                }
                return $this->responseProcuration($activeProcuration);
            }
            if($confirmProcuration->getSupport() || $confirmProcuration->getCandidate() || $confirmProcuration->getPub()) {
                $confirmProcuration->setActive(true);
                $activeProcuration->setActive(false);
                $em->flush();
                return $this->responseProcuration($confirmProcuration);
            }
            return $this->responseProcuration($activeProcuration);
        }
        return $this->response("ok");
    }

    // generic function curl
    private function CallAPI($method, $data, $url, $user)
    {
        $encode = false;
        $login=null;
        $password=null;
        $curl = curl_init();

//        die("callapi");
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json; charset=utf-8',
                    'Authorization: Basic ' . $user
                ));
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $encode ? json_encode($data) : $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        if($login and $password) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $login . ":" . $password);
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    /**
     * @Route(path="/", name="procuration", methods={"POST", "GET"})
     */
    public function procuration(Request $request)
    {
        $content = null;
        $category = $request->query->get('category');
        if($request->isMethod('GET')) {
            $email = $request->query->get('email');
        } else {
            $content = json_decode($request->getContent(), true);
            $email = $content->email;
        }
        if(!$email) {
            return $this->response("Error", 403);
        }

        $em = $this->getDoctrine()->getManager();

        $existProcuration = $em->getRepository('AppBundle:Api\Procuration')
            ->findOneBy(array('email' => $email));

        $procuration = new Procuration();
        $procuration->setEmail($email);
        if($category == 'ensemble') {
            $procuration->setEnsemble(true);
        } elseif($category == 'department') {
            $procuration->setDepartment(true);
        } else {
            $procuration->setSenat(true);
        }
        if($request->isMethod('GET')) {
            $procuration->setPhone($request->query->get('phone'));
            $procuration->setCity($request->query->get('city'));
            $procuration->setFirstName($request->query->get('firstname'));
            $procuration->setLastName($request->query->get('lastname'));
            $procuration->setSupport($request->query->get('support'));
            $procuration->setInfo($request->query->get('info'));
        } else {
            $procuration->setPhone($content->phone);
            $procuration->setCity($content->city);
            $procuration->setFirstName($content->firstName);
            $procuration->setLastName($content->lastName);
            $procuration->setSupport($content->support);
            $procuration->setInfo($content->info);
        }
        $em->persist($procuration);

        $em->flush();
        $mailer = $this->get('app.manager.mailer');
        if($procuration->getEnsemble()) {
            $mailer->sendEnsembleProcuration($procuration);
            if($procuration->getInfo() && !$existProcuration) {
                $url = 'https://us17.api.mailchimp.com/3.0/lists/1bb24fc427/members';
                $user = 'e91e2dc795cc3d21bcb8cc8a5fe6ce67-us1';
                $data = '{"email_address":"' . $email . '","status":"subscribed"}';
                $this->CallAPI("POST", $data, $url, $user);
            }
        } else {
            $mailer->sendProcuration($procuration);

            if(!$existProcuration && $procuration->getInfo()) {
                $url = 'https://us17.api.mailchimp.com/3.0/lists/527b6eeb77/members';
                $user='19bd9262c2748ca1caa4dedcf3cf76ed-us17';
                $data =  '{"email_address":"'.$email.'","status":"subscribed"}';
                $this->CallAPI("POST", $data, $url, $user);
            }
        }

        return $this->responseProcuration($procuration);
    }
}
