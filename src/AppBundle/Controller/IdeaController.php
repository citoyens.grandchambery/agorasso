<?php



namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Idea;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Entity\User;
use AppBundle\Form\IdeaType;

/**
 * Subscription controller.
 * @Security("has_role('ROLE_ADMIN')")
 * @route("/espace/idees")
 */

class IdeaController extends Controller
{

    /**
     * @Route("/", name="ideas")
     * @Security("is_granted('ROLE_CONTACT')")
     */
    public function indexAction(Request $request)

    {
        if(!$this->getUser()->isEnabled())
            return $this->redirectToRoute('fos_user_security_logout');
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:Idea')
            ->queryAll();

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render('AppBundle:Idea:index.html.twig', array(
            'ideas' => $pagerfanta,
        ));

    }

    /**
     * @Route(path="/{id}", name="idea_view")
     * @Security("is_granted('ROLE_CONTACT')")
     */
    public function viewAction(Request $request, Idea $idea)
    {
        return $this->render('AppBundle:Idea:view.html.twig', array(
            'idea'  => $idea
        ));
    }


    /**
     * @Route(path="/ajouter/", name="idea_add")
     * @Security("is_granted('ROLE_CONTACT')")
     */
    public function addIdeaAction(Request $request)
    {
        $idea = new Idea($this->getUser());
        $form = $this->createForm(IdeaType::class, $idea, array(
            'action' => $this->generateUrl('idea_add'),
            'method' => 'POST',
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($idea);
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Idea:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route(path="/{id}/modifier/", name="idea_edit")
     * @Security("is_granted('EDIT_USER_SELF', idea.getCreatedBy())")
     */
    public function editIdeaAction(Request $request, Idea $idea)
    {
         $form = $this->createForm(IdeaType::class, $idea, array(
             'action' => $this->generateUrl('idea_edit', array('id' => $idea->getId())),
            'method' => 'POST',
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->render('AppBundle:Ajax:closeModalAndReload.js.twig');
        }
        return $this->render('AppBundle:Idea:edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}