<?php



namespace AppBundle\Controller\Mail;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Mail\Mail;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use AppBundle\Entity\Group\Group;

/**
 * Subscription controller.
 * @route("/espace/mails")
 */

class MailController extends Controller
{

    /**
     * @Route("/", name="mails")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function indexAction(Request $request)

    {
        if(!$this->getUser()->isEnabled())
            return $this->redirectToRoute('fos_user_security_logout');
        $qb = $this->getDoctrine()->getManager()->getRepository('AppBundle:Mail\Mail')
            ->queryAll();

        $pagerfanta = new Pagerfanta(new DoctrineORMAdapter($qb));
        $pagerfanta->setMaxPerPage(20);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render('AppBundle:Mail:index.html.twig', array(
            'mails' => $pagerfanta,
        ));

    }

    /**
     * @Route(path="/{id}", name="mail_view")
     * @Security("is_granted('ROLE_CITIZEN')")
     */
    public function viewAction(Request $request, Mail $mail)
    {
        if(!$this->isGranted('ROLE_ADMIN')) {
            if(!$mail->getGroup() or !$this->getUser()->isReferent($mail->getGroup()))
                return new Response('Forbidden', 400, array('Content-Type' => 'application/text'));
        }
        return $this->render('AppBundle:Mail:view.html.twig', array(
            'mail'  => $mail
        ));
    }
}