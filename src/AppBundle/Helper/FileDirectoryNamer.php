<?php

namespace AppBundle\Helper;

use Doctrine\Common\Util\ClassUtils;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;
use AppBundle\Entity\Document\UploadedFile;
use AppBundle\Entity\Time\Info;
use AppBundle\Entity\Document\Image as Image;
use AppBundle\Entity\Document\Candidate as Candidate;
use AppBundle\Entity\Site\Site;

/**
 * Description of FileDirectoryNamer
 *
 * @author Michael Souvy
 */
class FileDirectoryNamer implements DirectoryNamerInterface
{  
    public function directoryName($file, PropertyMapping $mapping)
    {
        if($file instanceof Info)
            return sprintf("infos");
        if($file instanceof Image)
            return sprintf("images");
        if($file instanceof Candidate)
            return sprintf("images");
        if($file instanceof Site)
            return sprintf("sites");
        if($file->getGroup()) {
            $group = $file->getGroup();
            return sprintf("groupes/%s", $group->getName());
        }
        return sprintf("general");
    }

}
