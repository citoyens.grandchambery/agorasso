<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 26/01/18
 * Time: 15:47
 */

namespace AppBundle\Helper;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Post;
/**
 * Description of MenuHelper
 *
 * @author Stéphane Soulet
 */
class MenuHelper
{
    protected $tokenStorage;
    protected $translator;
    protected $requestStack;
    protected $authorizationChecker;
    protected $em;
    private $router;

    public function __construct(
        TokenStorage $tokenStorage,
        $translator,
        RequestStack $requestStack,
        AuthorizationCheckerInterface $authorizationChecker,
        EntityManagerInterface $em, RouterInterface $router)
    {
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
        $this->requestStack = $requestStack;
        $this->authorizationChecker = $authorizationChecker;
        $this->em = $em;
        $this->router = $router;
    }

    protected function makeMenu($label, $icon, $path, $role = null, $pathList = array(), $subMenu = null, $fa = true, $href = "#")
    {
        $active = false;
        if($role) {
            if(!$this->authorizationChecker->isGranted($role))
                return null;
        }
        $routeName = '';
        $request = $this->requestStack->getCurrentRequest();
        if($request)
            $routeName = $request->get('_route');
        $active = $path == $routeName;

        if($subMenu) {
            $user = $this->tokenStorage->getToken()->getUser();

            foreach ($subMenu as $key => $sub) {
                if ($sub["path"] and $sub["path"] == $routeName) {
                    if ($sub["pathList"]) {
                        foreach ($sub["pathList"] as $param => $val) {
                            if ($val == $request->get($param))
                                $active = true;
                            else {
                                $active = false;
                                break;
                            }
                        }
                    } else {
                        if ($request->get('mines')) {
                            $active = false;
                        }
                        else
                            $active = true;
                    }
                }
            }
        }

        return array('label' => $label,
            'icon' => $fa ? sprintf("fa fa-%s fa-fw", $icon) : sprintf("%s", $icon),
            'path' => $path,
            'pathList' => $pathList,
            'href' => $href,
            'subMenu' => $subMenu,
            'active' => $active,
            'hr' => false
        );
    }

    protected function addMenu(&$root, $label, $icon, $path, $role = null, $pathList = array(), $subMenu = null, $fa = true, $href = "#")
    {
        $tmp = $this->makeMenu($label, $icon, $path, $role, $pathList, $subMenu, $fa, $href);

        if($tmp)
            $root[] = $tmp;
        return $tmp;
    }

    protected function getParametersSubMenu()
    {
        $subMenu = array();

//        $this->addMenu($subMenu, "Admin", "users", null,null,null,null,true,"admin");
        $this->addMenu($subMenu, "Admin", "gear", "easyadmin","ROLE_CITIZEN");
        return $subMenu;
    }

    public function getMainMenu()
    {
        $rootMenu = array();
        // on ajoute le menu public si pas de connexion, sinon on arrive sur le site public en se déconnectant
        if(!$this->authorizationChecker->isGranted("ROLE_CONTACT"))
            $this->addMenu($rootMenu, "Site public", "angle-double-left", "view_page");
        $this->addMenu($rootMenu, "Accueil", "home", "dashboard","ROLE_CONTACT");
        $this->addMenu($rootMenu, "Mes infos", "user", "user_view","ROLE_CONTACT");
        $this->addMenu($rootMenu, "Les événements", "glyphicon glyphicon-calendar", "events","ROLE_CONTACT", array(), null, false);
        $this->addMenu($rootMenu, "Les groupes", "gears", "groups","ROLE_CONTACT");
//        $this->addMenu($rootMenu, "J'agis", "star", "task_list","ROLE_FRIEND");
        $this->addMenu($rootMenu, "Les membres", "users", "subscriptions","ROLE_CITIZEN");
//        $this->addMenu($rootMenu, "Les propositions", "file-o", "proposals","ROLE_CONTACT");
        $this->addMenu($rootMenu, "Les documents", "files-o", "documents","ROLE_CONTACT");
//        $this->addMenu($rootMenu, "Boîte à idées", "lightbulb-o ", "ideas","ROLE_CONTACT");
//        $this->addMenu($rootMenu, "Le diagnostic", "map", "diagnostic","ROLE_CITIZEN");
        $this->addMenu($rootMenu, "Paramétrage", "gear", "easyadmin", "ROLE_WEB");

        return $rootMenu;
    }
    public function getMenu($name)
    {
        return $this->em->getRepository('AppBundle:Site\Menu')->findOneBy(array('name' => $name));
    }

    protected function getPageUrl($page)
    {
        if($page->getName() == 'leprojet')
            return $this->router->generate('projet', [], true);
        elseif($page->getName() == 'laliste')
            return $this->router->generate('liste', [], true);

        return $this->router->generate('view_page', ['name' => $page->getName()], true);
    }

    protected function addPage(&$urls, Page $page, $priority)
    {
        if($page->getName() == 'leprojet')
            $priority = '1.0';
        elseif($page->getName() == 'laliste')
            $priority = '1.0';
        $now = new \DateTime();
        if($page) {
            $date = $page->getLastUpdatedAt();
            $urls[] = [
                'loc' => $this->getPageUrl($page),
                'lastmod' => $date ? $date : $now,
                'priority' => $priority
            ];
        }
    }

    protected function addPost(&$urls, Post $post, $priority)
    {
        $now = new \DateTime();
        if($post) {
            $date = $post->getUpdatedAt();
            $urls[] = [
                'loc' => $this->router->generate('view_post', ['name' => $post->getName()], true),
                'lastmod' => $date ? $date : $now,
                'priority' => $priority
            ];
        }
    }

    public function getSitemap()
    {
        $menu = $this->getMenu('public');
        $urls = [];
        $first = 0;


        foreach($menu->getSupervisedMenuItems() as $item)
        {
            $page = $item->getPage();
            $this->addPage($urls, $page, $first > 0 ? '0.80' : '1.0');

                foreach($item->getSubMenuItems() as $sub) {
                    $this->addPage($urls, $sub->getPage(), '0.64');
                }
            $first++;
        }

        $posts = $this->em->getRepository('AppBundle:Site\Post')->queryAll(true, 3)->getQuery()->getResult();
        foreach($posts as $post) {
            $this->addPost($urls, $post, '1.0');
        }
     /*   {% for item in menu.supervisedMenuItems %}

        {% if item.subMenuItems | length > 0 %}
        <li class="{{ item.color ~ 'Li' }} dropdown {{ item.isPage(page|default(null)) ? 'active' }}">
                        <a class="dropdown-toggle white" data-toggle="dropdown"
                           href="{{ item.page ? path('view_page', {'name': item.page.name}) }}" title="{{ item.page ? item.page.subtitle : item.altTitle }}"><i
                                    class="{{ item.mainIcon ? item.icon }}"></i> {{ item.page ? item.page.title : item.altTitle }}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            {% for sub in item.subMenuItems %}
                                <li><a href="{{ path('view_page', {'name': sub.page.name}) }}" title="{{ sub.page.title }}"
                                       class="white"><i class="{{ sub.mainIcon ? sub.icon }}"></i> {{ sub.page.title }}</a></li>
                            {% endfor %}
                        </ul>
                    </li>
                {% else %}
                    <li class="{{ item.color ~ 'Li' }} {% if page is defined %}{{ item.isPage(page) ? 'active'}}{% endif %}">
                        <a class="white" href="{{ path('view_page', {'name': item.page.name}) }}" title="{{ item.page.subtitle }}"><i
                                    class="{{ item.mainIcon ? item.icon }}"></i> {{ item.page.title }}</a>
                    </li>
                {% endif %}
                {% endfor %}

                <a class="white" href="{{ path('dashboard') }}" title="Espace du collectif"><i
                                class="fa fa-users"></i> Espace citoyen.ne.s</a>*/

        return $urls;
    }
}
