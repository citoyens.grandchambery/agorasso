<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 26/01/18
 * Time: 15:47
 */

namespace AppBundle\Helper;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Doctrine\ORM\EntityManagerInterface;
/**
 * Description of MenuHelper
 *
 * @author Stéphane Soulet
 */
class BaseHelper
{
    protected $tokenStorage;
    protected $authorizationChecker;
    protected $em;

    public function __construct(
        TokenStorage $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->em = $em;
    }

    public function userEmailExist($email)
    {
        return $this->em->getRepository('AppBundle:User')->findOneBy(array('email' => $email));
    }

    public function candidateExist($email, $type)
    {
        return $this->em->getRepository('AppBundle:User\Subscription')->findOneBy(array('email' => $email, 'type' => $type));
    }
}
