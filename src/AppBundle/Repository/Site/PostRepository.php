<?php

namespace AppBundle\Repository\Site;

/**
 * PostRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PostRepository extends \Doctrine\ORM\EntityRepository
{
    public function queryPosts($publish = false, $group = null, $maxResults = null)
    {
        $qb = $this->createQueryBuilder('x');
        $qb->orderBy('x.date', 'DESC')
        ->addOrderBy('x.startTime', 'DESC');

        $qb
            ->where('x.publish = :publish')
            ->setParameter("publish", $publish);

        if($group) {
            $qb ->andWhere(":wgroup IN x.referedGroups")
                ->setParameter("wgroup", $group);
        }

        if($maxResults) {
            $qb->setMaxResults($maxResults);
        }

        return $qb;
    }

    public function queryAll($publish = true, $maxResults = null)
    {
        return $this->queryPosts($publish, null, $maxResults);
    }
}