<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    public function userNameExist(User $member)
    {
        $res = $this->createQueryBuilder('u')
            ->andWhere('u.username = :userName')
            ->setParameter("userName", $member->getUserName())
            ->andWhere('u.id <> :userId')
            ->setParameter("userId", $member->getId())
            ->getQuery()
            ->getResult();
        return !empty($res);
    }

    private function findActiveUsers_($role = null, $noTown)
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.email IS NOT NULL')
            ->andWhere('u.email <> \'\' ')
            ->andWhere('u.enabled = :enabled')
            ->setParameter(":enabled", true);
        if($noTown) {
            $qb->andWhere('u.town IS NULL');
        }
        switch($role) {
            default:
            case 'ROLE_CONTACT':break;
            case 'ROLE_FRIEND':
                $qb->andWhere("u.roles LIKE '%ROLE_FRIEND%' OR u.roles LIKE '%ROLE_CITIZEN%' OR u.roles LIKE '%ROLE_ADMIN%'");
                break;
            case 'ROLE_CITIZEN':
                $qb->andWhere("u.roles LIKE '%ROLE_CITIZEN%' OR u.roles LIKE '%ROLE_ADMIN%'");
                break;
        }
        $res = $qb->getQuery()
                ->getResult();
        return $res;
    }

    public function findActiveUsers($role = null)
    {
        return $this->findActiveUsers_($role, false);
    }

    public function findNoTownUsers()
    {
        return $this->findActiveUsers_(null, true);
    }
}
