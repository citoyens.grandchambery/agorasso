<?php

namespace AppBundle\Repository\AppParameter;

use Doctrine\ORM\EntityRepository;

/**
 * ValueParameterRepository
 *
 */
class ValueParameterRepository extends EntityRepository
{
    public function getGroups()
    {
        $results = $this->createQueryBuilder('p')
            ->select('DISTINCT p.parameterGroup')
            ->orderBy('p.parameterGroup')
            ->getQuery()
            ->execute()
        ;

        return array_map(function($group) {
            return $group['parameterGroup'];
        }, $results);
    }

    public function getAllParameters()
    {
        return  $this->createQueryBuilder('p')
            ->where('p.type is not null and p.type != \'\'')
            ->orderBy('p.parameterGroup')
            ->getQuery()
            ->getResult()
        ;

    }


    public function getParameterValue($aParameterName)
    {
        $qb = $this->createQueryBuilder('pv')
            ->select('pv.value')
            ->where('pv.key = :key')
            ->setParameter('key', $aParameterName)
        ;

        try {
            $result = $qb->getQuery()->getSingleScalarResult();
            return $result;
        } catch (\Doctrine\Orm\NoResultException $e) {
            return null;
        }
    }

    public function getParameter($aParameterName)
    {
        $qb = $this->createQueryBuilder('pv')
            ->where('pv.key = :key')
            ->setParameter('key', $aParameterName)
        ;

        try {
            $result = $qb->getQuery()->getResult();
            if(count($result) > 0) {
                return $result['0'];
            }
            return null;
        } catch (\Doctrine\Orm\NoResultException $e) {
            return null;
        }
    }
}
