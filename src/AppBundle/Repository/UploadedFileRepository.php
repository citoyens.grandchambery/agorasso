<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * UploadedFileRepository
 *
 */
class UploadedFileRepository extends EntityRepository
{
    public function findAllDeletedFiles(\Datetime $aDate)
    {   
        return $this->createQueryBuilder('f')
            ->where('date(f.deletedAt) <= :aDate')
            ->setParameter(':aDate', $aDate)
            ->getQuery()
            ->getResult();   
    }

    public function findGroupFile($group_id, $file_id)
    {
        return $this->createQueryBuilder('f')
            ->innerJoin('f.group', 'g')
            ->andWhere('g.id = :group_id')
            ->andWhere('f.id = :file_id')
            ->setParameter(':group_id', $group_id)
            ->setParameter(':file_id', $file_id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findDocuments($filters)
    {
        $qb =  $this->createQueryBuilder('f')
            ->andWhere('f.group IS NOT NULL OR f.event IS NOT NULL')
            ->orderBy('f.createdAt', 'DESC');

        if (isset($filters['label'])){
            $qb->andWhere('f.label like :label or f.description like :label')
                ->setParameter('label', '%'.$filters['label'].'%');
        }

        if (isset($filters['group'])){
            $qb->andWhere('f.group = :group')
                ->setParameter('group', $filters['group']);
        }

        if (isset($filters['category'])){
            $qb->andWhere('f.category = :category')
                ->setParameter('category', $filters['category']);
        }


        return $qb;
    }

    public function findPublicFiles()
    {
        return $this->createQueryBuilder('f')
            ->where('f.public = :public')
            ->setParameter(':public', true)
            ->andWhere('f.notInList != :notInList')
            ->setParameter(':notInList', true)
            ->orderBy('f.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function queryLastFiles($limit = 5, $recent = false)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->orderBy('f.createdAt', 'DESC')
            ->setMaxResults($limit);
        if($recent) {
            $date = new \DateTime();
            $date->sub(new \DateInterval("P3M")); // check info sooner than 3 month
            $qb->where('f.createdAt > :date')
                ->setParameter('date', $date);
        }

        return $qb;
    }
 
}
