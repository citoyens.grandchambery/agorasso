<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 07/02/18
 * Time: 16:40
 */

namespace AppBundle\Entity;


use AppBundle\Entity\Group\Group;
use FOS\UserBundle\Model\User as FosUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Group\GroupSubscription;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Manager\DataManager;
use AppBundle\Entity\Subscription\Votation;
use AppBundle\Entity\User\Convention;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Validator\Constraints\Subscription as SubscriptionAssert;
use AppBundle\Entity\Location\Town;
use AppBundle\Entity\Location\District;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @Serializer\ExclusionPolicy("ALL")
 */

class User extends FOSUser
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_CITIZEN = 'ROLE_CITIZEN';
    const ROLE_FRIEND = 'ROLE_FRIEND';
    const ROLE_CONTACT = 'ROLE_CONTACT';

    // referent reason
    const REASON_HISTO = 'Historique';
    const REASON_REFERENT = 'Référent';

    public static $customRoleChoices = array(
        self::ROLE_CONTACT => 'Curieu.x.se',
        self::ROLE_FRIEND => 'Ami.e',
        self::ROLE_CITIZEN => 'Animateur.trice',
        self::ROLE_ADMIN => 'Admin',
    );

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable = true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="addressVisibility", type="string", length=255, nullable=true)
     */
    protected $addressVisibility;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=255, nullable = true)
     * @Assert\Regex(
     *     pattern     = "/^[0-9]/",
     *     message = "Cette valeur n'est pas valide.",
     * )
     */
    protected $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneVisibility", type="string", length=255, nullable=true)
     */
    protected $phoneVisibility;

    /**
     * @var string
     *
     * @ORM\Column(name="emailVisibility", type="string", length=255, nullable=true)
     */
    protected $emailVisibility;

    /**
     * @var bool
     *
     * @ORM\Column(name="getInvolved", type="boolean", options={"default" = false}, nullable=true)
     */
    private $getInvolved;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Subscription", mappedBy="user")
     * @ORM\OrderBy({"contactDate" = "DESC"})
     */
    private $subscriptions;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User\Convention", mappedBy="user")
     */
    private $convention;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document\UploadedFile", mappedBy="createdBy", cascade={"persist", "remove"})
     * @ORM\OrderBy({"label" = "ASC"})
     */
    private $attachedFiles;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Subscription", mappedBy="createdBy")
     * @ORM\OrderBy({"contactDate" = "DESC"})
     */
    private $subscribedUsers;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group\GroupSubscription", cascade={"remove"}, mappedBy="subscriber")
     * @ORM\OrderBy({"group" = "ASC"})
     */
    private $groupSubscriptions;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Subscription", mappedBy="sponsor")
     * @ORM\OrderBy({"contactDate" = "DESC"})
     */
    private $sponsors;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Task\Task", mappedBy="referedUsers")
     * @ORM\OrderBy({"date" = "ASC", "status" = "ASC"})
     */
    private $supervisedTasks;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable = true)
     */
    protected $reason;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $deletedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Town", inversedBy="users")
     * @ORM\JoinColumn(name="town_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $town;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\District", inversedBy="users")
     * @ORM\JoinColumn(name="district_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $district;

    /**
     * @Assert\Length(
     *      min = 8,
     *      minMessage = "Le mot de passe doit contenir au moins 8 caractères",
     * )
     */
    protected $plainPassword;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Monetic\Payment", mappedBy="payer")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $payments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project\Contribution", mappedBy="createdBy")
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $attachedContributions;

    public function __construct($datas = null)
    {
        parent::__construct();
        if ($datas) {
            $email = $datas['email'];
            $this->setBasicFields($email);
            $this->setFirstName($datas['firstName']);
            $this->setName($datas['name']);
            $this->setRoles(array($datas['role']));
            if(array_key_exists('phone', $datas)) {
                $this->setPhone($datas['phone']);
            }
        } else
            $this->setRoles(array('ROLE_CONTACT'));
        $random = random_int(1, 8);
        $this->setPlainPassword(sprintf("%08d", $random));
        $this->setEnabled(true);
        $this->attachedFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscribedUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->groupSubscriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscriptions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sponsors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->payments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->attachedContributions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setAddressVisibility(DataManager::AUTH_NONE);
        $this->setPhoneVisibility(DataManager::AUTH_NONE);
        $this->setEmailVisibility(DataManager::AUTH_CITIZEN);
        $this->supervisedTasks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return sprintf("%s %s", $this->getFirstName(), $this->getName());
    }

    /**
     * Set name
     *
     * @param string $email
     *
     * @return User
     */
    public function setBasicFields($email)
    {
        $this->setEmail($email);
        $this->setUsername($email);
        $this->setUsernameCanonical($email);

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return User
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }
    
    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return User
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phoneVisibility
     *
     * @param string $phoneVisibility
     *
     * @return User
     */
    public function setPhoneVisibility($phoneVisibility)
    {
        $this->phoneVisibility = $phoneVisibility;

        return $this;
    }

    /**
     * Get phoneVisibility
     *
     * @return string
     */
    public function getPhoneVisibility()
    {
        return $this->phoneVisibility;
    }

    /**
     * Set emailVisibility
     *
     * @param string $emailVisibility
     *
     * @return User
     */
    public function setEmailVisibility($emailVisibility)
    {
        $this->emailVisibility = $emailVisibility;

        return $this;
    }

    /**
     * Get emailVisibility
     *
     * @return string
     */
    public function getEmailVisibility()
    {
        return $this->emailVisibility;
    }

    /**
     * Set addressVisibility
     *
     * @param string $addressVisibility
     *
     * @return User
     */
    public function setAddressVisibility($addressVisibility)
    {
        $this->addressVisibility = $addressVisibility;

        return $this;
    }

    /**
     * Get addressVisibility
     *
     * @return string
     */
    public function getAddressVisibility()
    {
        return $this->addressVisibility;
    }
    
    /**
     * Set getInvolved
     *
     * @param boolean $getInvolved
     *
     * @return User
     */
    public function setGetInvolved($getInvolved)
    {
        $this->getInvolved = $getInvolved;

        return $this;
    }

    /**
     * Get getInvolved
     *
     * @return boolean
     */
    public function getGetInvolved()
    {
        return $this->getInvolved;
    }

    /**
     * Add subscription
     *
     * @param \AppBundle\Entity\User\Subscription $subscription
     *
     * @return User
     */
    public function addSubscription(\AppBundle\Entity\User\Subscription $subscription)
    {
        $this->subscriptions[] = $subscription;
        return $this;
    }

    /**
     * Remove subscription
     *
     * @param \AppBundle\Entity\User\Subscription $subscription
     */
    public function removeSubscription(\AppBundle\Entity\User\Subscription $subscription)
    {
        $this->subscriptions->removeElement($subscription);
    }

    /**
     * Get subscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        $result = new ArrayCollection();
        foreach ($this->subscriptions as $subscription) {
            $result[] = $subscription;
        }
        return $result;
    }

    /**
     * Add sponsor
     *
     * @param \AppBundle\Entity\User\Subscription $sponsor
     *
     * @return User
     */
    public function addSponsor(\AppBundle\Entity\User\Subscription $sponsor)
    {
        $this->sponsors[] = $sponsor;
        return $this;
    }

    /**
     * Remove sponsor
     *
     * @param \AppBundle\Entity\User\Subscription $sponsor
     */
    public function removeSponsor(\AppBundle\Entity\User\Subscription $sponsor)
    {
        $this->sponsors->removeElement($sponsor);
    }

    /**
     * Get sponsors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSponsors()
    {
        $result = new ArrayCollection();
        foreach ($this->sponsors as $sponsor) {
            $result[] = $sponsor;
        }
        return $result;
    }


    /**
     * Add payment
     *
     * @param \AppBundle\Entity\Monetic\Payment $payment
     *
     * @return User
     */
    public function addPayment(\AppBundle\Entity\Monetic\Payment $payment)
    {
        $this->payments[] = $payment;
        return $this;
    }

    /**
     * Remove payment
     *
     * @param \AppBundle\Entity\Monetic\Payment $payment
     */
    public function removePayment(\AppBundle\Entity\Monetic\Payment $payment)
    {
        $this->payments->removeElement($payment);
    }

    /**
     * Get payments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayments()
    {
        $result = new ArrayCollection();
        foreach ($this->payments as $payment) {
            $result[] = $payment;
        }
        return $result;
    }

    /**
     * Set convention
     *
     * @param \AppBundle\Entity\User\Convention $convention
     *
     * @return User
     */
    public function setConvention(\AppBundle\Entity\User\Convention $convention = null)
    {
        $this->convention = $convention;

        return $this;
    }

    /**
     * Get convention
     *
     * @return \AppBundle\Entity\User\Convention
     */
    public function getConvention()
    {
        return $this->convention;
    }

    /**
     * Add attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     *
     * @return User
     */
    public function addAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles[] = $attachedFile;
        return $this;
    }

    /**
     * Remove attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     */
    public function removeAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles->removeElement($attachedFile);
    }

    /**
     * Get attachedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedFiles()
    {
        $result = new ArrayCollection();
        foreach ($this->attachedFiles as $file) {
                $result[] = $file;
        }
        return $result;
    }

    /**
     * Add subscribedUser
     *
     * @param \AppBundle\Entity\User\Subscription $subscribedUser
     *
     * @return User
     */
    public function addSubscribedUser(\AppBundle\Entity\User\Subscription $subscribedUser)
    {
        $this->subscribedUsers[] = $subscribedUser;
        return $this;
    }

    /**
     * Remove subscribedUser
     *
     * @param \AppBundle\Entity\User\Subscription $subscribedUser
     */
    public function removeSubscribedUser(\AppBundle\Entity\User\Subscription $subscribedUser)
    {
        $this->subscribedUsers->removeElement($subscribedUser);
    }

    /**
     * Get subscribedUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscribedUsers()
    {
        $result = new ArrayCollection();
        foreach ($this->subscribedUsers as $subscription) {
            $result[] = $subscription;
        }
        return $result;
    }
    
    /**
     * Add groupSubscription
     *
     * @param \AppBundle\Entity\Group\GroupSubscription $subscription
     *
     * @return User
     */
    public function addGroupSubscription(\AppBundle\Entity\Group\GroupSubscription $subscription)
    {
        $this->groupSubscriptions[] = $subscription;
        return $this;
    }

    /**
     * Remove group
     *
     * @param \AppBundle\Entity\Group\GroupSubscription $group
     */
    public function removeGroupSubscription(\AppBundle\Entity\Group\GroupSubscription $subscription)
    {
        $this->groupSubscriptions->removeElement($subscription);
    }

    /**
     * Get groupSubscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupSubscriptions()
    {
        $groupSubscriptions = $this->groupSubscriptions->toArray();
        return $groupSubscriptions;
    }

    /**
     * Get workingGroups
     */
    public function getWorkingGroups()
    {
        $workingGroups = new ArrayCollection();
        $isReferent = $this->isReferent();
        foreach ($this->getGroupSubscriptions() as $subscriber)
        {
            if($subscriber->getActive()) {
                $group = $subscriber->getGroup();
                if(!(($isReferent and $group->isCopil()) or ($this->isActionUser($group) and $group->getEnableActionUser())))
                    $workingGroups->add(array('group' => $subscriber->getGroup(), 'referent' => $subscriber->getReferent()));
            }
        }

        return $workingGroups;
    }

    /**
     * is Referent
     */
    public function isReferent(Group $group = null)
    {
        foreach ($this->getGroupSubscriptions() as $subscriber)
        {
            if($group) {
                if($subscriber->getGroup()->getId() != $group->getId())
                    continue;
            }
            if($subscriber->getActive() and $subscriber->getReferent())
                return true;
        }

        return false;
    }

    /**
     * is Action User
     */
    public function isActionUser(Group $group)
    {
        if(!$group)
            return false;
        if(!$group->getEnableActionUser())
            return false;
        if($group->getActionUser() and $group->getActionUser()->getId() == $this->getId())
            return true;
        foreach ($group->getSubGroups() as $g)
        {
            if($g->getEnableActionUser() and $g->getActionUser() and $g->getActionUser()->getId() == $this->getId())
                return true;
        }

        return false;
    }

    /**
     * is subscribed
     */
    public function isSubscribed(Group $group = null)
    {
        if(!$group)
            return false;
        foreach ($this->getGroupSubscriptions() as $subscriber)
        {
            if($group) {
                if($subscriber->getGroup()->getId() != $group->getId())
                    continue;
            }
            if($subscriber->getActive())
                return true;
        }

        return false;
    }

    /**
     * is member
     */
    public function isMember(Group $group = null)
    {
        if(!$group)
            return false;
        if($group->isAssemblee())
            return true;
        if($group->isCopil() and $this->isReferent())
            return true;
        if($group->getEnableActionUser() and $this->isActionUser($group))
            return true;
        return $this->isSubscribed($group);
    }

    /**
     * is validated
     */
    public function isValidated()
    {
        if(in_array('ROLE_CONTACT', $this->getRoles()))
            return false;

        return true;
    }

    public function getRole()
    {
        if(in_array('ROLE_ADMIN', $this->getRoles()))
            return 'Administrateur.trice';
        if(in_array('ROLE_CITIZEN', $this->getRoles()))
            return 'Animateur.trice';
        if(in_array('ROLE_FRIEND', $this->getRoles()))
            return 'Ami.e';
        return 'Curieux.se';
    }

    /**
     * Get data visibility
     */
    public function getDataVisibility($type)
    {
        switch($type) {
            case DataManager::TYPE_ADDRESS: return $this->getAddressVisibility();
            case DataManager::TYPE_PHONE: return $this->getPhoneVisibility();
            case DataManager::TYPE_MAIL: return $this->getEmailVisibility();
        }
        return null;
    }

    /**
     * Add supervisedTask
     *
     * @param \AppBundle\Entity\Task\Task $supervisedTask
     *
     * @return Group
     */
    public function addSupervisedTask(\AppBundle\Entity\Task\Task $supervisedTask)
    {
        $this->supervisedTasks[] = $supervisedTask;

        return $this;
    }

    /**
     * Remove supervisedTask
     *
     * @param \AppBundle\Entity\Task\Task $supervisedTask
     */
    public function removeSupervisedTask(\AppBundle\Entity\Task\Task $supervisedTask)
    {
        $this->supervisedTasks->removeElement($supervisedTask);
    }

    /**
     * Get supervisedTasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedTasks()
    {
        return $this->supervisedTasks;
    }

    // check if user has participated to the vote
    public function hasVoted(Votation $votation)
    {
        foreach ($votation->getSupervisedVotationSubs() as $vote)
        {
            if($vote->getCreatedBy()->getId() == $this->getId())
                return true;
        }

        return false;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set town
     *
     * @param \AppBundle\Entity\Location\Town $town
     *
     * @return User
     */
    public function setTown(\AppBundle\Entity\Location\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \AppBundle\Entity\Location\Town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set district
     *
     * @param \AppBundle\Entity\Location\District $district
     *
     * @return User
     */
    public function setDistrict(\AppBundle\Entity\Location\District $district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \AppBundle\Entity\Location\District
     */
    public function getDistrict()
    {
        return $this->district;
    }

    public function getPlace() {
        $town = $this->getTown();
        if(!$town) {
            return $this->getCity();
        }
        return $town->getName();
    }

    public function getPublicPlace() {
        $town = $this->getTown();
        if(!$town or $town->getOther()) {
            return $this->getCity();
        }
        if($town->hasDistrict() and $this->getDistrict()) {
            return sprintf("%s - %s", $town, $this->getDistrict());
        }
        return $town->getName();
    }

    public function setPlace(Town $town = null, District $district = null, $city = null) {
        $this->town = $town;
        $this->district = $district;
        $this->setCity($city);
        return $this;
    }

    public function getOther() {
        $town = $this->getTown();
        if(!$town) {
            return "";
        }
        if($town->getOther()) {
            return $this->getCity();
        }
        return "";
    }

    /**
     * Add attachedContribution
     *
     * @param \AppBundle\Entity\Project\Contribution $attachedContribution
     *
     * @return User
     */
    public function addAttachedContribution(\AppBundle\Entity\Project\Contribution $attachedContribution)
    {
        $this->attachedContributions[] = $attachedContribution;
        return $this;
    }

    /**
     * Remove attachedContribution
     *
     * @param \AppBundle\Entity\Project\Contribution $attachedContribution
     */
    public function removeAttachedContribution(\AppBundle\Entity\Project\Contribution $attachedContribution)
    {
        $this->attachedContributions->removeElement($attachedContribution);
    }

    /**
     * Get attachedContributions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedContributions()
    {
        $result = new ArrayCollection();
        foreach ($this->attachedContributions as $contribution) {
            $result[] = $contribution;
        }
        return $result;
    }

    public function getCurrentContributions()
    {
        $result = new ArrayCollection();
        foreach ($this->attachedContributions as $contribution) {
            if($contribution->isCurrent()) {
                $result[] = $contribution;
            }
        }
        return $result;
    }
}
