<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 29/05/18
 * Time: 10:22
 */

namespace AppBundle\Entity\User;


use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="convention")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConventionRepository")
 */
class Convention
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean", options={"default" = false})
     */
    private $validated;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="convention")
     * @Assert\Valid
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $createdBy;

    public function __construct(User $createdBy)
    {
        $this->createdAt = new \DateTime();
        $this->createdBy = $createdBy;
    }


    public function __toString()
    {
        return sprintf("Charte enregistrée le %s", $this->getCreatedAt()->format('d/m/Y'));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Convention
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set user
     *
     * @param User $user
     *
     * @return Convention
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get createdBy
     *
     * @return CreatedBy
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Convention
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }
    
}
