<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 14/02/18
 * Time: 14:44
 */

namespace AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints\Subscription as SubscriptionAssert;
use AppBundle\Entity\User;
use AppBundle\Entity\User\Operation;

/**
 * @ORM\Table(name="subscription")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "subscription" = "Subscription",
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\SubscriptionRepository")
 * @SubscriptionAssert\SubscriptionTown
 */
class Subscription
{
    const GENDER_FEMALE = 'Femme';
    const GENDER_MALE = 'Homme';

    const TYPE_CONTACT = 'Contact';
    const TYPE_CANDIDATE_SPONSOR = 'Candidat suggéré';
    const TYPE_CANDIDATE = 'Candidat volontaire';
    const TYPE_RANDOM = 'Candidat tiré au sort';
    
    const USER_AWARE_YES = 'Oui';
    const USER_AWARE_NO = 'Non';
    
    const STATUS_WAITING = 'En attente';
    const STATUS_CANCELLED = 'Refusé';
    const STATUS_OK = 'Validé';

    const ORIGIN_INTERNET = 'Internet';
    const ORIGIN_MEETING = 'Manuelle';
    const ORIGIN_RANDOM = 'Tirage au sort';

    public static $genders = array(
        self::GENDER_FEMALE,
        self::GENDER_MALE
    );
    public static $gendersChoices = array(
        self::GENDER_FEMALE => self::GENDER_FEMALE,
        self::GENDER_MALE => self::GENDER_MALE
    );

    public static $userAwareChoices = array(
        self::USER_AWARE_YES => self::USER_AWARE_YES,
        self::USER_AWARE_NO => self::USER_AWARE_NO
    );

    public static $statuses = array(
        self::STATUS_WAITING,
        self::STATUS_CANCELLED,
        self::STATUS_OK
    );
    public static $statusesChoices = array(
        self::STATUS_WAITING => self::STATUS_WAITING,
        self::STATUS_CANCELLED => self::STATUS_CANCELLED,
        self::STATUS_OK => self::STATUS_OK
    );

    public static $typesChoices = array(
        self::TYPE_CANDIDATE_SPONSOR => self::TYPE_CANDIDATE_SPONSOR,
        self::TYPE_CANDIDATE => self::TYPE_CANDIDATE,
        self::TYPE_RANDOM => self::TYPE_RANDOM
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="contactDate", type="datetime", nullable=true)
     */
    protected $contactDate;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=170, nullable=true)
     * @Assert\NotBlank(groups={"candidates"},message="Ce champ est obligatoire !")
     */
    private $gender;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="birthDate", type="datetime", nullable=true)
     */
    protected $birthDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank(groups={"candidates"},message="Ce champ est obligatoire !")
     *
     * @var string
     */
    private $motivation;

    /**
     * @var bool
     *
     * @ORM\Column(name="userAware", type="boolean", nullable=true)
     * @Assert\NotBlank(groups={"other_candidates"},message="Ce champ est obligatoire !")
     */
    protected $userAware;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email(
     *     message = "Cette adresse mail n'est pas valide."
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern     = "/^[0-9]/",
     *     message = "Cette valeur n'est pas valide.",
     * )
     */
    protected $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var bool
     *
     * @ORM\Column(name="getNoticed", type="boolean", options={"default" = true})
     */
    protected $getNoticed;

    /**
     * @var bool
     *
     * @ORM\Column(name="emailConfirmed", type="boolean", options={"default" = false}, nullable=true)
     */
    protected $emailConfirmed;

    /**
     * @var string
     *
     * @ORM\Column(name="cookie", type="string", length=255, nullable=true)
     */
    protected $cookie;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="origin", type="string", length=255, nullable=true)
     */
    protected $origin;

    /**
     * @var string
     *
     * @ORM\Column(name="job", type="string", length=255, nullable=true)
     */
    protected $job;

    /**
     * @var bool
     *
     * @ORM\Column(name="commitment", type="boolean", options={"default" = false}, nullable=true)
     */
    protected $commitment;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    protected $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="contacted", type="boolean", options={"default" = false}, nullable=true)
     */
    protected $contacted;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"}, inversedBy="subscriptions")
     * @Assert\Valid
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"}, inversedBy="sponsors")
     * @Assert\Valid
     */
    protected $sponsor;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="subscribedUsers")
     */
    protected $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    protected $role;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Town")
     */
    protected $town;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\District")
     * @ORM\JoinColumn(name="district_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $district;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Operation", mappedBy="candidate", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $operations;

    /**
     * Constructor
     */
    public function __construct($type = null)
    {
        $this->contactDate = new \DateTime();
        $this->role = 'ROLE_CONTACT';
        $this->setCookie(uniqid());
        $this->setEmailConfirmed(false);
        $this->setType($type ? $type : self::TYPE_CONTACT);
        $this->setOrigin(self::ORIGIN_INTERNET);
        $this->operations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        $date = $this->getContactDate();
        return sprintf("%s %s %s", $date ? $date->format('d/m/Y') : '?', $this->getFirstName(), $this->getName());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactDate
     *
     * @param \DateTime $contactDate
     *
     * @return Subscription
     */
    public function setContactDate($contactDate)
    {
        $this->contactDate = $contactDate;

        return $this;
    }

    /**
     * Get contactDate
     *
     * @return \DateTime
     */
    public function getContactDate()
    {
        return $this->contactDate;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Subscription
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Subscription
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Subscription
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Subscription
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set motivation
     *
     * @param string $motivation
     *
     * @return Subscription
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;

        return $this;
    }

    /**
     * Get motivation
     *
     * @return string
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * Set userAware
     *
     * @param boolean $userAware
     *
     * @return Subscription
     */
    public function setUserAware($userAware)
    {
        $this->userAware = $userAware;

        return $this;
    }

    /**
     * Get userAware
     *
     * @return boolean
     */
    public function getUserAware()
    {
        return $this->userAware;
    }


    /**
     * Set email
     *
     * @param string $email
     *
     * @return Subscription
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Subscription
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Subscription
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Subscription
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Subscription
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set getNoticed
     *
     * @param boolean $getNoticed
     *
     * @return Subscription
     */
    public function setGetNoticed($getNoticed)
    {
        $this->getNoticed = $getNoticed;

        return $this;
    }

    /**
     * Get getNoticed
     *
     * @return boolean
     */
    public function getGetNoticed()
    {
        return $this->getNoticed;
    }

    /**
     * Set emailConfirmed
     *
     * @param boolean $emailConfirmed
     *
     * @return Subscription
     */
    public function setEmailConfirmed($emailConfirmed)
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    /**
     * Get emailConfirmed
     *
     * @return boolean
     */
    public function getEmailConfirmed()
    {
        return $this->emailConfirmed;
    }

    /**
     * Set cookie
     *
     * @param string $cookie
     *
     * @return Subscription
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;

        return $this;
    }

    /**
     * Get cookie
     *
     * @return string
     */
    public function getCookie()
    {
        return $this->cookie;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Subscription
     */
    public function setType($type)
    {
        $this->type = $type;
        
        if($type) {
            switch ($type) {
                case self::TYPE_CANDIDATE_SPONSOR:
                case self::TYPE_CANDIDATE:
                case self::TYPE_RANDOM:
                    if(!$this->getStatus()) {
                        $this->setStatus(self::STATUS_WAITING);
                    }
                    break;
            }
        }

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return Subscription
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set job
     *
     * @param string $job
     *
     * @return Subscription
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set commitment
     *
     * @param boolean $commitment
     *
     * @return Subscription
     */
    public function setCommitment($commitment)
    {
        $this->commitment = $commitment;

        return $this;
    }

    /**
     * Get commitment
     *
     * @return boolean
     */
    public function getCommitment()
    {
        return $this->commitment;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Subscription
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Increment weight
     *
     * @return Subscription
     */
    public function incWeight()
    {
        $this->weight += 1;

        return $this;
    }
    
    /**
     * Set status
     *
     * @param string $status
     *
     * @return Subscription
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Update status, return true if change
     *
     * @return boolean
     */
    public function updateStatus()
    {
        $status = $this->status;
        if($this->isOperation(Operation::OPERATION_CANCELLED)) {
            $this->setStatus(Subscription::STATUS_CANCELLED);
        } else if($this->isOperation(Operation::OPERATION_OK) and
                $this->isOperation(Operation::OPERATION_FOLLOW_OK)) {
            $this->setStatus(Subscription::STATUS_OK);
        } else {
            $this->setStatus(Subscription::STATUS_WAITING);
        }

        return $this->status != $status;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set contacted
     *
     * @param boolean $contacted
     *
     * @return Subscription
     */
    public function setContacted($contacted)
    {
        $this->contacted = $contacted;

        return $this;
    }

    /**
     * Get contacted
     *
     * @return boolean
     */
    public function getContacted()
    {
        return $this->contacted;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Subscription
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set sponsor
     *
     * @param \AppBundle\Entity\User $sponsor
     *
     * @return Subscription
     */
    public function setSponsor(\AppBundle\Entity\User $sponsor = null)
    {
        $this->sponsor = $sponsor;

        return $this;
    }

    /**
     * Get sponsor
     *
     * @return \AppBundle\Entity\User
     */
    public function getSponsor()
    {
        return $this->sponsor;
    }


    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Subscription
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Subscription
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get role label
     *
     * @return string
     */
    public function getRoleLabel()
    {
        foreach (User::$customRoleChoices as $param => $val) {
            if($this->getRole() == $param) {
                return $val;
            }
        }
        return 'Aucun';
    }

    public function needUser()
    {
        return $this->getGetNoticed() and !$this->getUser();
    }

    /**
     * Set town
     *
     * @param \AppBundle\Entity\Location\Town $town
     *
     * @return Subscription
     */
    public function setTown(\AppBundle\Entity\Location\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \AppBundle\Entity\Location\Town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set district
     *
     * @param \AppBundle\Entity\Location\District $district
     *
     * @return Subscription
     */
    public function setDistrict(\AppBundle\Entity\Location\District $district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \AppBundle\Entity\Location\District
     */
    public function getDistrict()
    {
        return $this->district;
    }

    public function isCandidate()
    {
        if(!$this->getType()) {
            return false;
        }
        switch($this->getType()) {
            case self::TYPE_CANDIDATE:
            case self::TYPE_CANDIDATE_SPONSOR:
            case self::TYPE_RANDOM:
                return true;
        }
        return false;
    }

    /**
     * Add operation
     *
     * @param \AppBundle\Entity\User\Operation $operation
     *
     * @return Subscription
     */
    public function addOperation(\AppBundle\Entity\USer\Operation $operation)
    {
        $this->operations[] = $operation;
        $operation->setCandidate($this);

        return $this;
    }

    /**
     * Remove operation
     *
     * @param \AppBundle\Entity\User\Operation $operation
     */
    public function removeOperation(\AppBundle\Entity\User\Operation $operation)
    {
        $this->operations->removeElement($operation);
        $operation->setCandidate(null);
    }

    /**
     * Get operations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    public function getOperationsText()
    {
        $result = '';
        foreach ($this->operations as $op) {
            $result = $result.sprintf('%s - %s\n', $op->getOperation(), strip_tags($op->getContent()));
        }
        return $result;
    }

    public function isOperation($operation)
    {
        foreach ($this->operations as $op) {
            if($op->getOperation() == $operation) {
                return true;
            }
        }
        return false;
    }

}
