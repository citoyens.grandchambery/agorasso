<?php
namespace AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Project\Contribution;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="operation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\OperationRepository")
 */
class Operation
{
    const OPERATION_FOLLOW_OK = 'Validation comité de suivi';
    const OPERATION_MEET = 'Rencontre';
    const OPERATION_NOTE = 'Information';
    const OPERATION_OK = 'Candidature validée';
    const OPERATION_CANCELLED = 'Refus de candidature';
    
    
    public static $operationsChoices = array(
        self::OPERATION_NOTE => self::OPERATION_NOTE,
        self::OPERATION_FOLLOW_OK => self::OPERATION_FOLLOW_OK,
        self::OPERATION_MEET => self::OPERATION_MEET,
        self::OPERATION_OK => self::OPERATION_OK,
        self::OPERATION_CANCELLED => self::OPERATION_CANCELLED,
    );

   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text")
     *
     * @var string
     * @Assert\NotBlank(message="L'opération doit être motivée")
     */
    private $content;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="operation", type="string", length=170)
     */
    private $operation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Subscription", inversedBy="operations")
     */
    private $candidate;
    
    public function __construct(Subscription $candidate)
    {
        $this->createdAt = new \Datetime();
        $this->setCandidate($candidate);
    }

    
    public function __toString()
    {
        return sprintf("%s (%s)", $this->getOperation(), $this->getCreatedAt()->format('d/m/Y'));
    }
    
    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt; 
        } else {
            return $this->createdAt; 
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Operation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Operation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Operation
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Operation
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Operation
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set operation
     *
     * @param string $operation
     *
     * @return Operation
     */
    public function setOperation($operation)
    {
        $this->operation = $operation;

        return $this;
    }

    /**
     * Get operation
     *
     * @return string
     */
    public function getOperation()
    {
        return $this->operation;
    }

    /**
     * Set candidate
     *
     * @param \AppBundle\Entity\User\Subscription $candidate
     *
     * @return Operation
     */
    public function setCandidate(\AppBundle\Entity\User\Subscription $candidate = null)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate
     *
     * @return \AppBundle\Entity\User\Subscription
     */
    public function getCandidate()
    {
        return $this->candidate;
    }
}
