<?php
namespace AppBundle\Entity\Document;

use AppBundle\Entity\User;
use AppBundle\Entity\Time\Xvent as Xvent;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Group\Group;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="fichiers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UploadedFileRepository")
 * @Vich\Uploadable
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UploadedFile
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="file", fileNameProperty="fileName")
     * @Assert\File(
     *     mimeTypes={
     *          "image/jpeg", 
     *          "image/pjpeg", 
     *          "image/png", 
     *          "image/x-png", 
     *          "application/pdf", 
     *          "application/vnd.ms-excel",
     *          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
     *          "application/msword",
     *          "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
     *          "application/vnd.oasis.opendocument.spreadsheet",
     *          "application/vnd.oasis.opendocument.text",
     *          "application/vnd.ms-powerpoint",
     *          "application/vnd.openxmlformats-officedocument.presentationml.presentation",
     *          "application/vnd.ms-office",
     *          "text/rtf",
     *          "text/plain", 
     *          "application/zip"
     *     },
     *     maxSize="500M",
     *     mimeTypesMessage = "Mauvais type de fichier (types acceptés: jpeg, png, pdf, xls, doc, docx, odt, ppt, pptx, rtf, txt, zip)"
     * )
     *
     * @var File
     * @Assert\NotBlank(message="Veuillez sélectionner un fichier", groups={"fileCreation"})
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $fileName;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\DocumentCategory")
     * @Assert\NotBlank(message="Le type de fichier est obligatoire")
     */
    private $category;
    
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     * @Assert\NotBlank(message="Le libellé du fichier est obligatoire")
     */
    private $label;

    /**
     * @ORM\Column(name="sub_title", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $subTitle;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="attachedFiles")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * Ce champ est modifié à chaque fois pour lancer l'event listener de VichUploader
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $internalUpdatedAt;
    
     /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;
    
     /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="public", type="boolean", nullable=true)
     */
    private $public;

    /**
     * @var bool
     *
     * @ORM\Column(name="notInList", type="boolean", nullable=true)
     */
    private $notInList;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Group\Group", inversedBy="attachedFiles")
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Time\Xvent", inversedBy="attachedFiles")
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Time\BaseTime", inversedBy="attachedFiles")
     */
    private $time;

    public function __construct(Group $group = null, Xvent $event = null, User $member = null)
    {
        $this->group = $group;
        $this->event = $event;
        $this->createdBy = $member;
        $this->createdAt = new \Datetime();
        $this->public = false;
        $this->notInList = false;
    }

    
    public function __toString()
    {
        return $this->label; 
    }

    // Indique si le champ "catégorie" est de type string ou de type entité (comme pour les activité)
    public function hasEntityCategory()
    {
        return false;
    }

    // Indique si on doit afficher le champ description du fichier (comme pour les fichiers partnerAttachedFiles des utilisateurs partenaires)
    public function showDescriptionField()
    {
        return true;
    }
    
    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt; 
        } else {
            return $this->createdAt; 
        }
    }
    
    public function getExtension()
    {
        $path_parts = pathinfo($this->fileName);
        return $path_parts['extension'];
        
    }
    
    public function getOriginalFileName()
    {
        if (strpos($this->fileName, '_')!==false){
            return substr($this->getFileName(), strpos($this->fileName, '_')+1);
        } else {
            return $this->fileName;
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
        
        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->internalUpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return UploadedFile
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return UploadedFile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return UploadedFile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return UploadedFile
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return UploadedFile
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return UploadedFile
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return UploadedFile
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return UploadedFile
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set internalUpdatedAt
     *
     * @param \DateTime $internalUpdatedAt
     *
     * @return UploadedFile
     */
    public function setInternalUpdatedAt($internalUpdatedAt)
    {
        $this->internalUpdatedAt = $internalUpdatedAt;

        return $this;
    }

    /**
     * Get internalUpdatedAt
     *
     * @return \DateTime
     */
    public function getInternalUpdatedAt()
    {
        return $this->internalUpdatedAt;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return UploadedFile
     */
    public function setGroup(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set event
     *
     * @param \AppBundle\Entity\Time\Xvent $event
     *
     * @return UploadedFile
     */
    public function setEvent(Xvent $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \AppBundle\Entity\Time\Xvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set time
     *
     * @param \AppBundle\Entity\Time\BaseTime $time
     *
     * @return UploadedFile
     */
    public function setTime(\AppBundle\Entity\Time\BaseTime $time = null)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \AppBundle\Entity\Time\BaseTime
     */
    public function getTime()
    {
        return $this->time;
    }
    
    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return UploadedFile
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set notInList
     *
     * @param boolean $notInList
     *
     * @return UploadedFile
     */
    public function setNotInList($notInList)
    {
        $this->notInList = $notInList;

        return $this;
    }

    /**
     * Get notInList
     *
     * @return boolean
     */
    public function getNotInList()
    {
        return $this->notInList;
    }

    /**
     * Set subTitle
     *
     * @param string $subTitle
     *
     * @return UploadedFile
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }
}
