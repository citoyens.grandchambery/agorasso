<?php
namespace AppBundle\Entity\Document;

use AppBundle\Entity\User;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="candidate")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\CandidateRepository")
 * @Vich\Uploadable
 */
class Candidate
{
    const GENDER_FEMALE = 'Femme';
    const GENDER_MALE = 'Homme';

    public static $genders = array(
        self::GENDER_FEMALE,
        self::GENDER_MALE
    );
    public static $gendersChoices = array(
        self::GENDER_FEMALE => self::GENDER_FEMALE,
        self::GENDER_MALE => self::GENDER_MALE
    );

    const TYPE_CANDIDATE_SPONSOR = 'Proposé';
    const TYPE_CANDIDATE = 'Volontaire';
    const TYPE_CANDIDATE_RANDOM = 'Tiré au sort';

    public static $typesChoices = array(
        self::TYPE_CANDIDATE_SPONSOR => self::TYPE_CANDIDATE_SPONSOR,
        self::TYPE_CANDIDATE => self::TYPE_CANDIDATE,
        self::TYPE_CANDIDATE_RANDOM => self::TYPE_CANDIDATE_RANDOM
    );

   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="images", fileNameProperty="fileName")
     * @Assert\File(
     *     mimeTypes={
     *          "image/jpeg", 
     *          "image/pjpeg", 
     *          "image/png", 
     *          "image/x-png"
     *     },
     *     maxSize="500M",
     *     mimeTypesMessage = "Mauvais type de fichier (types acceptés: jpeg, png)"
     * )
     *
     * @var File
     * @Assert\NotBlank(message="Veuillez sélectionner un fichier", groups={"fileCreation"})
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $lastName;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=170, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $profession;

    /**
     * @ORM\Column(name="content", type="text")
     *
     * @var string
     */
    private $description;
    
    /**
     * Ce champ est modifié à chaque fois pour lancer l'event listener de VichUploader
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $internalUpdatedAt;
    
    public function __construct(User $member = null)
    {
    }

    
    public function __toString()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }
    
    public function getExtension()
    {
        $path_parts = pathinfo($this->fileName);
        return $path_parts['extension'];
        
    }
    
    public function getOriginalFileName()
    {
        if (strpos($this->fileName, '_')!==false){
            return substr($this->getFileName(), strpos($this->fileName, '_')+1);
        } else {
            return $this->fileName;
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
        
        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->internalUpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Candidate
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Candidate
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return Candidate
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Candidate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Candidate
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set internalUpdatedAt
     *
     * @param \DateTime $internalUpdatedAt
     *
     * @return Candidate
     */
    public function setInternalUpdatedAt($internalUpdatedAt)
    {
        $this->internalUpdatedAt = $internalUpdatedAt;

        return $this;
    }

    /**
     * Get internalUpdatedAt
     *
     * @return \DateTime
     */
    public function getInternalUpdatedAt()
    {
        return $this->internalUpdatedAt;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Candidate
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }
}
