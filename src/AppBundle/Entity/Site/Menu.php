<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="menu")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\MenuRepository")
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     */
    private $name;
    
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\MenuItem", mappedBy="referedMenu")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $supervisedMenuItems;
    

    public function __construct()
    {
        $this->supervisedMenuItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->name;

    }
    
    /**
     * Add supervisedMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $supervisedMenuItem
     *
     * @return Menu
     */
    public function addSupervisedMenuItem(\AppBundle\Entity\Site\MenuItem $supervisedMenuItem)
    {
        $this->supervisedMenuItems[] = $supervisedMenuItem;

        return $this;
    }

    /**
     * Remove supervisedMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $supervisedMenuItem
     */
    public function removeSupervisedMenuItem(\AppBundle\Entity\Site\MenuItem $supervisedMenuItem)
    {
        $this->supervisedMenuItems->removeElement($supervisedMenuItem);
    }

    /**
     * Get supervisedMenuItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedMenuItems()
    {
        return $this->supervisedMenuItems;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
