<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Document\Image as Image;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="block")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\BlockRepository")
 */
class Block
{
    const TYPE_TEXT = 'Texte';
    const TYPE_IMAGE = 'Image';
    const TYPE_EVENTS = 'Événements';
    const TYPE_DOCS = 'Documents';
    const TYPE_CONTACT = 'Contact';
    const TYPE_CANDIDATE_SPONSOR = 'Candidat suggéré';
    const TYPE_CANDIDATE = 'Candidat volontaire';
    const TYPE_POSTS = 'Articles';
    const TYPE_POSTS_ALL = 'Tous les articles';
    const TYPE_POSTS_LAST = 'Derniers articles';
    const TYPE_JOINUS = 'Nous-rejoindre';
    const TYPE_CALENDAR = 'Calendrier';
    const TYPE_ALL_CALENDAR = 'Tout le calendrier';
    const TYPE_CANDIDATE_RESULT = 'Bilan candidatures';
    const TYPE_PROJECT = 'Projet';
    const TYPE_PROJECT_MENU = 'Menu projet';
    const TYPE_PROPOSAL = 'Proposition';
    const TYPE_CANDIDATES = 'Candidats';

    public static $types = array(
        self::TYPE_TEXT,
        self::TYPE_IMAGE,
        self::TYPE_EVENTS,
        self::TYPE_DOCS,
        self::TYPE_CONTACT,
        self::TYPE_CANDIDATE_SPONSOR,
        self::TYPE_CANDIDATE,
        self::TYPE_POSTS,
        self::TYPE_POSTS_ALL,
        self::TYPE_POSTS_LAST,
        self::TYPE_JOINUS,
        self::TYPE_CALENDAR,
        self::TYPE_ALL_CALENDAR,
        self::TYPE_CANDIDATE_RESULT,
        self::TYPE_PROJECT,
        self::TYPE_PROJECT_MENU,
        self::TYPE_PROPOSAL,
        self::TYPE_CANDIDATES
    );
    public static $typesChoices = array(
        self::TYPE_TEXT => self::TYPE_TEXT,
        self::TYPE_IMAGE => self::TYPE_IMAGE,
        self::TYPE_EVENTS => self::TYPE_EVENTS,
        self::TYPE_DOCS => self::TYPE_DOCS,
        self::TYPE_CONTACT => self::TYPE_CONTACT,
        self::TYPE_CANDIDATE_SPONSOR => self::TYPE_CANDIDATE_SPONSOR,
        self::TYPE_CANDIDATE => self::TYPE_CANDIDATE,
        self::TYPE_POSTS => self::TYPE_POSTS,
        self::TYPE_POSTS_ALL => self::TYPE_POSTS_ALL,
        self::TYPE_POSTS_LAST => self::TYPE_POSTS_LAST,
        self::TYPE_JOINUS => self::TYPE_JOINUS,
        self::TYPE_CALENDAR => self::TYPE_CALENDAR,
        self::TYPE_ALL_CALENDAR => self::TYPE_ALL_CALENDAR,
        self::TYPE_CANDIDATE_RESULT => self::TYPE_CANDIDATE_RESULT,
        self::TYPE_PROJECT => self::TYPE_PROJECT,
        self::TYPE_PROJECT_MENU => self::TYPE_PROJECT_MENU,
        self::TYPE_PROPOSAL => self::TYPE_PROPOSAL,
        self::TYPE_CANDIDATES => self::TYPE_CANDIDATES
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page", inversedBy="supervisedBlocks")
     * )
     */
    private $referedPage;

    /**
     * @var bool
     *
     * @ORM\Column(name="hideOnMobile", type="boolean", options={"default" = false},nullable=true)
     */
    private $hideOnMobile;

    /**
     * @var bool
     *
     * @ORM\Column(name="showOnlyMobile", type="boolean", options={"default" = false},nullable=true)
     */
    private $showOnlyMobile;

    /**
     * @var bool
     *
     * @ORM\Column(name="preserveWidth", type="boolean", options={"default" = false},nullable=true)
     */
    private $preserveWidth;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->updatedAt = new \Datetime();
    }

    public function __toString()
    {
        return sprintf('%s (%d): %s', $this->type, $this->weight, $this->title);

    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Block
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Block
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Block
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Block
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Block
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }
    
    /**
     * Set image
     *
     * @param Image $image
     *
     * @return Block
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Block
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return BaseTime
     */
    public function setcontent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     */
    public function getcontent()
    {
        return $this->content;
    }

    /**
     * Set referedPage
     *
     * @param \AppBundle\Entity\Site\Page $page
     *
     * @return Block
     */
    public function setReferedPage(\AppBundle\Entity\Site\Page $page = null)
    {
        $this->referedPage = $page;

        return $this;
    }

    /**
     * Get referedPage
     *
     * @return \AppBundle\Entity\Site\Page
     */
    public function getReferedPage()
    {
        return $this->referedPage;
    }

    /**
     * Set hideOnMobile
     *
     * @param boolean $hideOnMobile
     *
     * @return Block
     */
    public function setHideOnMobile($hideOnMobile)
    {
        $this->hideOnMobile = $hideOnMobile;

        return $this;
    }

    /**
     * Get hideOnMobile
     *
     * @return boolean
     */
    public function getHideOnMobile()
    {
        return $this->hideOnMobile;
    }

    /**
     * Set preserveWidth
     *
     * @param boolean $preserveWidth
     *
     * @return Block
     */
    public function setPreserveWidth($preserveWidth)
    {
        $this->preserveWidth = $preserveWidth;

        return $this;
    }

    /**
     * Get preserveWidth
     *
     * @return boolean
     */
    public function getPreserveWidth()
    {
        return $this->preserveWidth;
    }

    /**
     * Set showOnlyMobile
     *
     * @param boolean $showOnlyMobile
     *
     * @return Block
     */
    public function setShowOnlyMobile($showOnlyMobile)
    {
        $this->showOnlyMobile = $showOnlyMobile;

        return $this;
    }

    /**
     * Get showOnlyMobile
     *
     * @return boolean
     */
    public function getShowOnlyMobile()
    {
        return $this->showOnlyMobile;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Block
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        if($this->referedPage) {
            $this->referedPage->setUpdatedAt($updatedAt);
        }

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * has form
     *
     * @return boolean
     */
    public function hasForm()
    {
        return $this->getType() == self::TYPE_CANDIDATE or
            $this->getType() == self::TYPE_CANDIDATE_SPONSOR or
            $this->getType() == self::TYPE_CONTACT or
            $this->getType() == self::TYPE_PROJECT;// using anchor cannot set same object id
    }
}
