<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Site;

use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Document\Image as Image;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Table(name="site")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\SiteRepository")
 * @Vich\Uploadable
 */
class Site
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="instagram", type="string", length=255)
     */
    private $instagram;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $icon;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $logo;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="file", fileNameProperty="fileName", nullable=true)
     * @Assert\File(
     *     mimeTypes={
     *          "text/plain",
     *          "application/vnd.geo+json",
     *     },
     *     maxSize="20M"
     * )
     *
     * @var File
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $fileName;

    /**
     * Ce champ est modifié à chaque fois pour lancer l'event listener de VichUploader
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $internalUpdatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="coordinates", type="string", length=255, nullable=true)
     */
    private $coordinates;

    /**
     * @var string
     *
     * @ORM\Column(name="mapboxToken", type="string", length=255, nullable=true)
     */
    private $mapboxToken;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $columnImage;
    
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Site
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return Site
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     *
     * @return Site
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set instagram
     *
     * @param string $instagram
     *
     * @return Site
     */
    public function setInstagram($instagram)
    {
        $this->instagram = $instagram;

        return $this;
    }

    /**
     * Get instagram
     *
     * @return string
     */
    public function getInstagram()
    {
        return $this->instagram;
    }
    
    /**
     * Set coordinates
     *
     * @param string $coordinates
     *
     * @return Site
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Get coordinates
     *
     * @return string
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Set mapboxToken
     *
     * @param string $mapboxToken
     *
     * @return Site
     */
    public function setMapboxToken($mapboxToken)
    {
        $this->mapboxToken = $mapboxToken;

        return $this;
    }

    /**
     * Get mapboxToken
     *
     * @return string
     */
    public function getMapboxToken()
    {
        return $this->mapboxToken;
    }

    public function isMap()
    {
        return $this->coordinates != '' and $this->mapboxToken != '';
    }

    
    /**
     * Set image
     *
     * @param Image $image
     *
     * @return Site
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Site
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set logo
     *
     * @param Image $logo
     *
     * @return Site
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return Site
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set icon
     *
     * @param Image $icon
     *
     * @return Site
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return Site
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set columnImage
     *
     * @param Image $columnImage
     *
     * @return Site
     */
    public function setColumnImage($columnImage)
    {
        $this->columnImage = $columnImage;

        return $this;
    }

    /**
     * Get columnImage
     *
     * @return Site
     */
    public function getColumnImage()
    {
        return $this->columnImage;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'Info' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\Info $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->internalUpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    public function getExtension()
    {
        $path_parts = pathinfo($this->fileName);
        return $path_parts['extension'];

    }

    public function getOriginalFileName()
    {
        if (strpos($this->fileName, '_')!==false){
            return substr($this->getFileName(), strpos($this->fileName, '_')+1);
        } else {
            return $this->fileName;
        }
    }
    /**
     * Set internalUpdatedAt
     *
     * @param \DateTime $internalUpdatedAt
     *
     * @return Site
     */
    public function setInternalUpdatedAt($internalUpdatedAt)
    {
        $this->internalUpdatedAt = $internalUpdatedAt;

        return $this;
    }

    /**
     * Get internalUpdatedAt
     *
     * @return \DateTime
     */
    public function getInternalUpdatedAt()
    {
        return $this->internalUpdatedAt;
    }
}
