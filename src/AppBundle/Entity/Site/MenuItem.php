<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="menuItem")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\MenuItemRepository")
 */
class MenuItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="altTitle", type="string", length=255, nullable=true)
     */
    private $altTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;

    /**
     * @var bool
     *
     * @ORM\Column(name="mainIcon", type="boolean", options={"default" = false},nullable=true)
     */
    private $mainIcon;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page", inversedBy="supervisedMenuItems")
     */
    private $page;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Menu", inversedBy="supervisedMenuItems")
     */
    private $referedMenu;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\MenuItem", mappedBy="referedMenuItem")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $subMenuItems;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\MenuItem", inversedBy="subMenuItems")
     */
    private $referedMenuItem;

    public function __construct()
    {
        $this->subMenuItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return sprintf("%02i-%s", $this->weight, $this->page ? $this->page : $this->altTitle);

    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return MenuItem
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return menuItem
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set altTitle
     *
     * @param string $altTitle
     *
     * @return menuItem
     */
    public function setAltTitle($altTitle)
    {
        $this->altTitle = $altTitle;

        return $this;
    }

    /**
     * Get altTitle
     *
     * @return string
     */
    public function getAltTitle()
    {
        return $this->altTitle;
    }

    /**
     * Set page
     *
     * @param \AppBundle\Entity\Site\Page $page
     *
     * @return MenuItem
     */
    public function setPage(\AppBundle\Entity\Site\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \AppBundle\Entity\Site\Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set referedMenu
     *
     * @param \AppBundle\Entity\Site\Menu $menu
     *
     * @return MenuItem
     */
    public function setReferedMenu(\AppBundle\Entity\Site\Menu $menu = null)
    {
        $this->referedMenu = $menu;

        return $this;
    }

    /**
     * Get referedMenu
     *
     * @return \AppBundle\Entity\Site\Menu
     */
    public function getReferedMenu()
    {
        return $this->referedMenu;
    }


    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return MenuItem
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set mainIcon
     *
     * @param boolean $mainIcon
     *
     * @return MenuItem
     */
    public function setMainIcon($mainIcon)
    {
        $this->mainIcon = $mainIcon;

        return $this;
    }

    /**
     * Get mainIcon
     *
     * @return boolean
     */
    public function getMainIcon()
    {
        return $this->mainIcon;
    }

    /**
     * Add subMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $subMenuItem
     *
     * @return MenuItem
     */
    public function addSubMenuItem(\AppBundle\Entity\Site\MenuItem $subMenuItem)
    {
        $this->subMenuItems[] = $subMenuItem;

        return $this;
    }

    /**
     * Remove subMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $subMenuItem
     */
    public function removeSubMenuItem(\AppBundle\Entity\Site\MenuItem $subMenuItem)
    {
        $this->subMenuItems->removeElement($subMenuItem);
    }

    /**
     * Get subMenuItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubMenuItems()
    {
        return $this->subMenuItems;
    }

    /**
     * Set referedMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $menuItem
     *
     * @return MenuItem
     */
    public function setReferedMenuItem(\AppBundle\Entity\Site\MenuItem $menuItem = null)
    {
        $this->referedMenuItem = $menuItem;

        return $this;
    }

    /**
     * Get referedMenuItem
     *
     * @return \AppBundle\Entity\Site\MenuItem
     */
    public function getReferedMenuItem()
    {
        return $this->referedMenuItem;
    }


    /**
     * isPage
     *
     * @return boolean
     */
    public function isPage(Page $page = null)
    {
        if(!$page) {
            return false;
        }
        if($this->page) {
            if($this->page->getName() == $page->getName()) {
                return true;
            }
        }
        foreach ($this->subMenuItems as $item) {
            if($item->isPage($page)) {
                return true;
            }
        }
        return false;
    }
}
