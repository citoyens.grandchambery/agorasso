<?php

namespace AppBundle\Entity\Site;

use AppBundle\Entity\Time\BaseTime;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Document\Image as Image;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Note
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\PostRepository")
 */
class Post extends BaseTime
{
    protected $discr = 'post';

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255,nullable=true)
     */
    private $video;

    /**
     * @var bool
     *
     * @ORM\Column(name="publish", type="boolean", options={"default" = false})
     */
    private $publish;

    /**
     * @var bool
     *
     * @ORM\Column(name="hideImage", type="boolean", options={"default" = false},nullable=true)
     */
    private $hideImage;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255)
     */
    private $keywords;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page", inversedBy="supervisedPosts")
     * )
     */
    private $postPage;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255,nullable=true)
     */
    private $description;

    /**
     * Set image
     *
     * @param Image $image
     *
     * @return Post
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Post
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Post
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     *
     * @return BaseTime
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set hideImage
     *
     * @param boolean $hideImage
     *
     * @return BaseTime
     */
    public function setHideImage($hideImage)
    {
        $this->hideImage = $hideImage;

        return $this;
    }

    /**
     * Get hideImage
     *
     * @return boolean
     */
    public function getHideImage()
    {
        return $this->hideImage;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Post
     */
    public function setName($name)
    {

        $this->name = $this->clean($name);

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->name;
        if(!$name) {
            $name = $this->getTitle();
            $name = $this->clean($name);
        }

        return $name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Post
     */
    public function setDescription($description)
    {
        return $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        $description = $this->description;
        if(!$description) {
            return $this->getResume();
        }

        return $description;
    }
    
    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Post
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set postPage
     *
     * @param \AppBundle\Entity\Site\Page $page
     *
     * @return Post
     */
    public function setPostPage(\AppBundle\Entity\Site\Page $page = null)
    {
        $this->postPage = $page;

        return $this;
    }

    /**
     * Get postPage
     *
     * @return \AppBundle\Entity\Site\Page
     */
    public function getPostPage()
    {
        return $this->postPage;
    }
}