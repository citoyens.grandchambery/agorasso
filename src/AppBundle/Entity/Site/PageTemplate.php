<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pageTemplate")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\PageTemplateRepository")
 */
class PageTemplate
{
    const TYPE_COLUMN = 'Column';
    const TYPE_HEADER_COLUMN = 'Header column';
    const TYPE_MAIN_AND_COLUMN = 'Main and column';
    const TYPE_THREE_HEADER_COLUMN = 'Three header column';
    const TYPE_NO_COLUMN = 'No column';

    public static $types = array(
        self::TYPE_COLUMN,
        self::TYPE_HEADER_COLUMN,
        self::TYPE_MAIN_AND_COLUMN,
        self::TYPE_THREE_HEADER_COLUMN,
        self::TYPE_NO_COLUMN,
    );
    public static $typesChoices = array(
        self::TYPE_COLUMN => self::TYPE_COLUMN,
        self::TYPE_HEADER_COLUMN => self::TYPE_HEADER_COLUMN,
        self::TYPE_MAIN_AND_COLUMN => self::TYPE_MAIN_AND_COLUMN,
        self::TYPE_THREE_HEADER_COLUMN => self::TYPE_THREE_HEADER_COLUMN,
        self::TYPE_NO_COLUMN => self::TYPE_NO_COLUMN,
    );
    
    const POSITION_LEFT = "Left";
    const POSITION_CENTER = "Center";
    const POSITION_RIGHT = "Right";
    const POSITION_MAIN = "Main";

    public static $columnChoices = array(
        self::POSITION_LEFT => self::POSITION_LEFT,
        self::POSITION_RIGHT => self::POSITION_RIGHT,
        self::POSITION_CENTER => self::POSITION_CENTER,
    );

    public static $headerColumnChoices = array(
        self::POSITION_LEFT => self::POSITION_LEFT,
        self::POSITION_RIGHT => self::POSITION_RIGHT,
        self::POSITION_CENTER => self::POSITION_CENTER,
        self::POSITION_MAIN => self::POSITION_MAIN,
    );
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
    

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Page", mappedBy="referedPageTemplate")
     * )
     */
    private $supervisedPages;

    public function __construct()
    {
        $this->supervisedPages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->type;

    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PageTemplate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add supervisedPage
     *
     * @param \AppBundle\Entity\Site\Page $supervisedPage
     *
     * @return PageTemplate
     */
    public function addSupervisedPage(\AppBundle\Entity\Site\Page $supervisedPage)
    {
        $this->supervisedPages[] = $supervisedPage;

        return $this;
    }

    /**
     * Remove supervisedPage
     *
     * @param \AppBundle\Entity\Site\Page $supervisedPage
     */
    public function removeSupervisedPage(\AppBundle\Entity\Site\Page $supervisedPage)
    {
        $this->supervisedPages->removeElement($supervisedPage);
    }

    /**
     * Get supervisedPages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedPages()
    {
        return $this->supervisedPages;
    }

    public function getPositions()
    {
        if($this->type == self::TYPE_COLUMN) {
            return self::$columnChoices;
        }
        return self::$headerColumnChoices;
    }
}
