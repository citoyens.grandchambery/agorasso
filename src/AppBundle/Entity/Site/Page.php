<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Document\Image as Image;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Site\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subTitle", type="string", length=255)
     */
    private $subTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=255)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_menu", type="string", length=255)
     */
    private $altMenu;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_main", type="string", length=255)
     */
    private $altMain;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Document\Image")
     * @Assert\Valid
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255, nullable=true)
     */
    private $role;

    /**
     * @var bool
     *
     * @ORM\Column(name="dashboard", type="boolean", options={"default" = false})
     */
    private $dashboard;

    /**
     * @var bool
     *
     * @ORM\Column(name="home", type="boolean", options={"default" = false})
     */
    private $home;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\PageTemplate", inversedBy="supervisedPages")
     * )
     */
    private $referedPageTemplate;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Block", mappedBy="referedPage")
     * )
     */
    private $supervisedBlocks;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Post", mappedBy="postPage")
     * )
     */
    private $supervisedPosts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\MenuItem", mappedBy="page")
     */
    private $supervisedMenuItems;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group\Group", inversedBy="pages")
     */
    private $group;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->supervisedBlocks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedPosts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedMenuItems = new \Doctrine\Common\Collections\ArrayCollection();
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Page
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Page
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subTitle
     *
     * @param string $subTitle
     *
     * @return Page
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set keywords
     *
     * @param string $keywords
     *
     * @return Page
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Page
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set dashboard
     *
     * @param boolean $dashboard
     *
     * @return Page
     */
    public function setDashboard($dashboard)
    {
        $this->dashboard = $dashboard;

        return $this;
    }

    /**
     * Get dashboard
     *
     * @return boolean
     */
    public function getDashboard()
    {
        return $this->dashboard;
    }

    /**
     * Set home
     *
     * @param boolean $home
     *
     * @return Page
     */
    public function setHome($home)
    {
        $this->home = $home;

        return $this;
    }

    /**
     * Get home
     *
     * @return boolean
     */
    public function getHome()
    {
        return $this->home;
    }


    public function __toString()
    {
        return $this->name;

    }

    /**
     * Set altMenu
     *
     * @param string $altMenu
     *
     * @return Page
     */
    public function setAltMenu($altMenu)
    {
        $this->altMenu = $altMenu;

        return $this;
    }

    /**
     * Get altMenu
     *
     * @return string
     */
    public function getAltMenu()
    {
        return $this->altMenu;
    }

    /**
     * Set altMain
     *
     * @param string $altMain
     *
     * @return Page
     */
    public function setAltMain($altMain)
    {
        $this->altMain = $altMain;

        return $this;
    }

    /**
     * Get altMain
     *
     * @return string
     */
    public function getAltMain()
    {
        return $this->altMain;
    }

    /**
     * Set image
     *
     * @param Image $image
     *
     * @return Page
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return Page
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set referedPageTemplate
     *
     * @param \AppBundle\Entity\Site\PageTemplate $pageTemplate
     *
     * @return PageTemplate
     */
    public function setReferedPageTemplate(\AppBundle\Entity\Site\PageTemplate $pageTemplate = null)
    {
        $this->referedPageTemplate = $pageTemplate;

        return $this;
    }

    /**
     * Get referedPageTemplate
     *
     * @return \AppBundle\Entity\Site\PageTemplate
     */
    public function getReferedPageTemplate()
    {
        return $this->referedPageTemplate;
    }

    /**
     * Add supervisedBlock
     *
     * @param \AppBundle\Entity\Site\Block $supervisedBlock
     *
     * @return Page
     */
    public function addSupervisedBlock(\AppBundle\Entity\Site\Block $supervisedBlock)
    {
        $this->supervisedBlocks[] = $supervisedBlock;

        return $this;
    }

    /**
     * Remove supervisedBlock
     *
     * @param \AppBundle\Entity\Site\Block $supervisedBlock
     */
    public function removeSupervisedBlock(\AppBundle\Entity\Site\Block $supervisedBlock)
    {
        $this->supervisedBlocks->removeElement($supervisedBlock);
    }

    /**
     * Get supervisedBlocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedBlocks()
    {
        return $this->supervisedBlocks;
    }

    public function hasBlockType($type)
    {
        foreach ($this->getSupervisedBlocks() as $block) {
            if($block->getType() == $type) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add supervisedPost
     *
     * @param \AppBundle\Entity\Site\Post $supervisedPost
     *
     * @return Page
     */
    public function addSupervisedPost(\AppBundle\Entity\Site\Post $supervisedPost)
    {
        $this->supervisedPosts[] = $supervisedPost;

        return $this;
    }

    /**
     * Remove supervisedPost
     *
     * @param \AppBundle\Entity\Site\Post $supervisedPost
     */
    public function removeSupervisedPost(\AppBundle\Entity\Site\Post $supervisedPost)
    {
        $this->supervisedPosts->removeElement($supervisedPost);
    }

    /**
     * Get supervisedPosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedPosts()
    {
        return $this->supervisedPosts;
    }

    /**
     * Add supervisedMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $supervisedMenuItem
     *
     * @return page
     */
    public function addSupervisedMenuItem(\AppBundle\Entity\Site\MenuItem $supervisedMenuItem)
    {
        $this->supervisedMenuItems[] = $supervisedMenuItem;

        return $this;
    }

    /**
     * Remove supervisedMenuItem
     *
     * @param \AppBundle\Entity\Site\MenuItem $supervisedMenuItem
     */
    public function removeSupervisedMenuItem(\AppBundle\Entity\Site\MenuItem $supervisedMenuItem)
    {
        $this->supervisedMenuItems->removeElement($supervisedMenuItem);
    }

    /**
     * Get supervisedMenuItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedMenuItems()
    {
        return $this->supervisedMenuItems;
    }

    public function getParentPage()
    {
        foreach  ($this->getSupervisedMenuItems() as $menuItem) {
            if($menuItem->getReferedMenuItem()) {
                return $menuItem->getReferedMenuItem()->getPage();
            }
        }
        return null;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return Page
     */
    public function setGroup(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getGroup()
    {
        return $this->group;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Page
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getLastUpdatedAt()
    {
        $date = $this->updatedAt;

        foreach($this->getSupervisedBlocks() as $block) {
            $altDate = $block->getUpdatedAt();
            if($altDate) {
                if($date) {
                    if($altDate > $date) {
                        $date = $altDate;
                    }
                } else {
                    $date = $altDate;
                }
            }
        }

        foreach($this->getSupervisedPosts() as $post) {
            $altDate = $post->getUpdatedAt();
            if($altDate) {
                if($date) {
                    if($altDate > $date) {
                        $date = $altDate;
                    }
                } else {
                    $date = $altDate;
                }
            }
        }

        return $date;
    }
}
