<?php
namespace AppBundle\Entity\Api;

use AppBundle\Entity\User;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Accessor;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="citizen")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Api\CitizenRepository")
 * @Vich\Uploadable
 * @ExclusionPolicy("all")
 */
class Citizen
{
    const GENDER_FEMALE = 'Femme';
    const GENDER_MALE = 'Homme';

    public static $genders = array(
        self::GENDER_FEMALE,
        self::GENDER_MALE
    );
    public static $gendersChoices = array(
        self::GENDER_FEMALE => self::GENDER_FEMALE,
        self::GENDER_MALE => self::GENDER_MALE
    );
    
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="images", fileNameProperty="fileName")
     * @Assert\File(
     *     mimeTypes={
     *          "image/jpeg", 
     *          "image/pjpeg", 
     *          "image/png", 
     *          "image/x-png"
     *     },
     *     maxSize="500M",
     *     mimeTypesMessage = "Mauvais type de fichier (types acceptés: jpeg, png)"
     * )
     *
     * @var File
     * @Assert\NotBlank(message="Veuillez sélectionner un fichier", groups={"fileCreation"})
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Expose
     * @Accessor(getter="getExposedFirstName",setter="setFirstName")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Expose
     * @Accessor(getter="getExposedLastName",setter="setLastName")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     * @Expose
     * @Accessor(getter="getExposedCity",setter="setCity")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=170, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Accessor(getter="getExposedFollower",setter="setFollower")
     * @Expose
     */
    private $follower;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $cookie;
    
    /**
     * Ce champ est modifié à chaque fois pour lancer l'event listener de VichUploader
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $internalUpdatedAt;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="support", type="boolean", nullable=true)
     */
    private $support;

    /**
     * @var bool
     *
     * @ORM\Column(name="pub", type="boolean", nullable=true)
     */
    private $pub;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="info", type="boolean", nullable=true)
     */
    private $info;

    /**
     * @var bool
     *
     * @ORM\Column(name="candidate", type="boolean", nullable=true)
     * @Expose
     */
    private $candidate;

    /**
     * @var bool
     *
     * @ORM\Column(name="senat", type="boolean", nullable=true)
     */
    private $senat;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="department", type="boolean", nullable=true)
     */
    private $department;

    /**
     * @var bool
     *
     * @ORM\Column(name="ensemble", type="boolean", nullable=true)
     */
    private $ensemble;
    
    public function __construct(User $member = null)
    {
        $this->createdAt = new \Datetime();
        $this->active = false;
        $this->info = false;
        $this->candidate = false;
        $this->pub = false;
        $this->support = false;
        $this->setCookie(uniqid());
    }

    
    public function __toString()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }
    
    public function getExtension()
    {
        $path_parts = pathinfo($this->fileName);
        return $path_parts['extension'];
        
    }
    
    public function getOriginalFileName()
    {
        if (strpos($this->fileName, '_')!==false){
            return substr($this->getFileName(), strpos($this->fileName, '_')+1);
        } else {
            return $this->fileName;
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
        
        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->internalUpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Citizen
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    private function mb_ucfirst($string, $encoding)
    {
        $strlen = mb_strlen($string, $encoding);
        if($strlen < 1) {
            return "";
        }

        $firstChar = mb_substr($string, 0, 1, $encoding);
        if($strlen > 1) {
            $then = mb_substr($string, 1, $strlen - 1, $encoding);
        } else {
            $then = "";
        }
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    private function normalize($res)
    {
        return $this->mb_ucfirst(strtolower(trim($res)), "utf8");
    }

    /**
     * Get exposedFirstName
     *
     * @return string
     */
    public function getExposedFirstName()
    {
        $result = "";
        if($this->getPub()) {
            $result = $this->normalize($this->getFirstName());
        } else {
            $result = "***";
        }
        return $result;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Citizen
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Citizen
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Get exposedLastName
     *
     * @return string
     */
    public function getExposedLastName()
    {
        $result = "";
        if($this->getPub()) {
            $result = $this->normalize($this->getLastName());
        } else {
            $result = "***";
        }
        return $result;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Citizen
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get exposedCity
     *
     * @return string
     */
    public function getExposedCity()
    {
        return $this->normalize($this->getCity());
    }

    /**
     * Set follower
     *
     * @param string $follower
     *
     * @return Citizen
     */
    public function setFollower($follower)
    {
        $this->follower = $follower;

        return $this;
    }

    /**
     * Get follower
     *
     * @return string
     */
    public function getFollower()
    {
        return $this->follower;
    }

    /**
     * Get exposedFollower
     *
     * @return string
     */
    public function getExposedFollower()
    {
        return $this->normalize($this->getFollower());
    }

    /**
     * Set cookie
     *
     * @param string $cookie
     *
     * @return Citizen
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;

        return $this;
    }

    /**
     * Get cookie
     *
     * @return string
     */
    public function getCookie()
    {
        return $this->cookie;
    }
    
    /**
     * Set internalUpdatedAt
     *
     * @param \DateTime $internalUpdatedAt
     *
     * @return Citizen
     */
    public function setInternalUpdatedAt($internalUpdatedAt)
    {
        $this->internalUpdatedAt = $internalUpdatedAt;

        return $this;
    }

    /**
     * Get internalUpdatedAt
     *
     * @return \DateTime
     */
    public function getInternalUpdatedAt()
    {
        return $this->internalUpdatedAt;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Citizen
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Citizen
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Citizen
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set support
     *
     * @param boolean $support
     *
     * @return Citizen
     */
    public function setSupport($support)
    {
        $this->support = $support;

        return $this;
    }

    /**
     * Get support
     *
     * @return boolean
     */
    public function getSupport()
    {
        return $this->support;
    }


    /**
     * Set pub
     *
     * @param boolean $pub
     *
     * @return Citizen
     */
    public function setPub($pub)
    {
        $this->pub = $pub;

        return $this;
    }

    /**
     * Get pub
     *
     * @return boolean
     */
    public function getPub()
    {
        return $this->pub;
    }

    /**
     * Set info
     *
     * @param boolean $info
     *
     * @return Citizen
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return boolean
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set senat
     *
     * @param boolean $senat
     *
     * @return Citizen
     */
    public function setSenat($senat)
    {
        $this->senat = $senat;

        return $this;
    }

    /**
     * Get senat
     *
     * @return boolean
     */
    public function getSenat()
    {
        return $this->senat;
    }

    /**
     * Set department
     *
     * @param boolean $department
     *
     * @return Citizen
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return boolean
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set ensemble
     *
     * @param boolean $ensemble
     *
     * @return Citizen
     */
    public function setEnsemble($ensemble)
    {
        $this->ensemble = $ensemble;

        return $this;
    }

    /**
     * Get ensemble
     *
     * @return boolean
     */
    public function getEnsemble()
    {
        return $this->ensemble;
    }

    /**
     * Set candidate
     *
     * @param boolean $candidate
     *
     * @return Citizen
     */
    public function setCandidate($candidate)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate
     *
     * @return boolean
     */
    public function getCandidate()
    {
        return $this->candidate;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        if($this->getCandidate())
            return 'candidate';
        if($this->getPub()) {
            return 'public';
        }
        if($this->getSupport()) {
            return 'support';
        }
        return 'info';
    }
}
