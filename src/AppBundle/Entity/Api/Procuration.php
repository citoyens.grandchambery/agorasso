<?php
namespace AppBundle\Entity\Api;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Accessor;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="procuration")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Api\ProcurationRepository")
 * @ExclusionPolicy("all")
 */
class Procuration
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Expose
     * @Accessor(getter="getExposedFirstName",setter="setFirstName")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Expose
     * @Accessor(getter="getExposedLastName",setter="setLastName")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     * @Expose
     * @Accessor(getter="getExposedCity",setter="setCity")
     */
    private $city;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Accessor(getter="getExposedFollower",setter="setFollower")
     * @Expose
     */
    private $follower;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $cookie;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="support", type="boolean", nullable=true)
     */
    private $support;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="info", type="boolean", nullable=true)
     */
    private $info;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="department", type="boolean", nullable=true)
     */
    private $department;

    /**
     * @var bool
     *
     * @ORM\Column(name="ensemble", type="boolean", nullable=true)
     */
    private $ensemble;
    
    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->active = false;
        $this->info = false;
        $this->support = false;
        $this->setCookie(uniqid());
    }

    
    public function __toString()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }
    
    public function getId() {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Procuration
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    private function mb_ucfirst($string, $encoding)
    {
        $strlen = mb_strlen($string, $encoding);
        if($strlen < 1) {
            return "";
        }

        $firstChar = mb_substr($string, 0, 1, $encoding);
        if($strlen > 1) {
            $then = mb_substr($string, 1, $strlen - 1, $encoding);
        } else {
            $then = "";
        }
        return mb_strtoupper($firstChar, $encoding) . $then;
    }

    private function normalize($res)
    {
        return $this->mb_ucfirst(strtolower(trim($res)), "utf8");
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Procuration
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Procuration
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Procuration
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Procuration
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Get exposedCity
     *
     * @return string
     */
    public function getExposedCity()
    {
        return $this->normalize($this->getCity());
    }

    /**
     * Set follower
     *
     * @param string $follower
     *
     * @return Procuration
     */
    public function setFollower($follower)
    {
        $this->follower = $follower;

        return $this;
    }

    /**
     * Get follower
     *
     * @return string
     */
    public function getFollower()
    {
        return $this->follower;
    }

    /**
     * Set cookie
     *
     * @param string $cookie
     *
     * @return Procuration
     */
    public function setCookie($cookie)
    {
        $this->cookie = $cookie;

        return $this;
    }

    /**
     * Get cookie
     *
     * @return string
     */
    public function getCookie()
    {
        return $this->cookie;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Procuration
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Procuration
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set support
     *
     * @param boolean $support
     *
     * @return Procuration
     */
    public function setSupport($support)
    {
        $this->support = $support;

        return $this;
    }

    /**
     * Get support
     *
     * @return boolean
     */
    public function getSupport()
    {
        return $this->support;
    }

    /**
     * Set info
     *
     * @param boolean $info
     *
     * @return Procuration
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return boolean
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set department
     *
     * @param boolean $department
     *
     * @return Procuration
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return boolean
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set ensemble
     *
     * @param boolean $ensemble
     *
     * @return Procuration
     */
    public function setEnsemble($ensemble)
    {
        $this->ensemble = $ensemble;

        return $this;
    }

    /**
     * Get ensemble
     *
     * @return boolean
     */
    public function getEnsemble()
    {
        return $this->ensemble;
    }
    
    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        if($this->getSupport()) {
            return 'support';
        }
        return 'info';
    }
}
