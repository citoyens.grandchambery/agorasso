<?php
namespace AppBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="publicForm")
 * @ORM\Entity()
 */
class PublicForm
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @ORM\Column(type="text")
     *
     * @var string
     * @Assert\NotBlank(message="Commentaire")
     */
    private $description;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \Date
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Answer", mappedBy="publicForm")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Response", mappedBy="publicForm")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $responses;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Project\Project", mappedBy="citizenForm")
     */
    private $citizenProject;

    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->endDate = new \Datetime();
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        return sprintf("%s", $this->getTitle());
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return PublicForm
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return PublicForm
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $date
     *
     * @return PublicForm
     */
    public function setEndDate($date)
    {
        $this->endDate = $date;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return PublicForm
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\Content\Answer $answer
     *
     * @return PublicForm
     */
    public function addAnswer(\AppBundle\Entity\Content\Answer $answer)
    {
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\Content\Answer $answer
     */
    public function removeAnswer(\AppBundle\Entity\Content\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add response
     *
     * @param \AppBundle\Entity\Content\Response $response
     *
     * @return PublicForm
     */
    public function addResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses[] = $response;

        return $this;
    }

    /**
     * Remove response
     *
     * @param \AppBundle\Entity\Content\Response $response
     */
    public function removeResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses->removeElement($response);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Set citizenProject
     *
     * @param \AppBundle\Entity\Project\Project $citizenProject
     *
     * @return PublicForm
     */
    public function setCitizenProject(\AppBundle\Entity\Project\Project $citizenProject = null)
    {
        $this->citizenProject = $citizenProject;

        return $this;
    }

    /**
     * Get citizenProject
     *
     * @return \AppBundle\Entity\Project\Project
     */
    public function getCitizenProject()
    {
        return $this->citizenProject;
    }
}
