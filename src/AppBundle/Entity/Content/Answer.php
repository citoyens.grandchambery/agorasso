<?php
namespace AppBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="answer")
 * @ORM\Entity()
 */
class Answer
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text")
     *
     * @var string
     * @Assert\NotBlank(message="Question")
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $weight;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\PublicForm", inversedBy="answers")
     */
    private $publicForm;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Response", mappedBy="answer")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $responses;

    public function __construct()
    {
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        return sprintf("%s", $this->getContent());
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Answer
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Answer
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set publicForm
     *
     * @param \AppBundle\Entity\Content\PublicForm $publicForm
     *
     * @return Answer
     */
    public function setPublicForm(\AppBundle\Entity\Content\PublicForm $publicForm = null)
    {
        $this->publicForm = $publicForm;

        return $this;
    }

    /**
     * Get publicForm
     *
     * @return \AppBundle\Entity\Content\PublicForm
     */
    public function getPublicForm()
    {
        return $this->publicForm;
    }

    /**
     * Add response
     *
     * @param \AppBundle\Entity\Content\Response $response
     *
     * @return Answer
     */
    public function addResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses[] = $response;

        return $this;
    }

    /**
     * Remove response
     *
     * @param \AppBundle\Entity\Content\Response $response
     */
    public function removeResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses->removeElement($response);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }

}
