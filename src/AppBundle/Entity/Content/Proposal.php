<?php
namespace AppBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Subscription\Votation;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="proposal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Content\ProposalRepository")
 */
class Proposal
{
    const STATUS_OPEN = 'Ouvert';
    const STATUS_CLOSED = 'Fermé';

    public static $statuses = array(
        self::STATUS_OPEN,
        self::STATUS_CLOSED
    );
    public static $statusesChoices = array(
        self::STATUS_OPEN => self::STATUS_OPEN,
        self::STATUS_CLOSED => self::STATUS_CLOSED
    );

   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text")
     *
     * @var string
     * @Assert\NotBlank(message="Proposition")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group\Group", inversedBy="supervisedProposals")
     * )
     */
    private $referedGroups;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Subscription\Votation", mappedBy="referedProposals")
     * )
     */
    private $supervisedVotations;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Subscription\VotationSub", mappedBy="referedProposal")
     * )
     */
    private $supervisedVotationSubs;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=170)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $closedReason;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Comment", mappedBy="proposal", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $NComments;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Section", inversedBy="proposals")
     */
    private $section;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $number;

    /**
     * @var bool
     *
     * @ORM\Column(name="detailed", type="boolean", options={"default" = false}, nullable=true)
     */
    private $detailed;


    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->referedGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedVotations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedVotationSubs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setStatus(self::STATUS_OPEN);
        $this->setNComments(0);
        $this->detailed = false;
    }

    
    public function __toString()
    {
        if(strlen($this->getTitle() < 100))
            return $this->getTitle();
        else
            return sprintf('Proposition n°%d', $this->getId());
    }
    
    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt; 
        } else {
            return $this->createdAt; 
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Proposal
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Proposal
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Proposal
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Proposal
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Proposal
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Proposal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Proposal
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set closedReason
     *
     * @param string $closedReason
     *
     * @return Proposal
     */
    public function setClosedReason($closedReason)
    {
        $this->closedReason = $closedReason;

        return $this;
    }

    /**
     * Get closedReason
     *
     * @return string
     */
    public function getClosedReason()
    {
        return $this->closedReason;
    }
    /**
     * Add referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     *
     * @return Proposal
     */
    public function addReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups[] = $referedGroup;

        return $this;
    }

    /**
     * Remove referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     */
    public function removeReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups->removeElement($referedGroup);
    }

    /**
     * Get referedGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedGroups()
    {
        return $this->referedGroups;
    }

    /**
     * Add supervisedVotation
     *
     * @param \AppBundle\Entity\Subscription\Votation $supervisedVotation
     *
     * @return Votation
     */
    public function addSupervisedVotation(\AppBundle\Entity\Subscription\Votation $supervisedVotation)
    {
        $this->supervisedVotations[] = $supervisedVotation;

        return $this;
    }

    /**
     * Remove supervisedVotation
     *
     * @param \AppBundle\Entity\Subscription\Votation $supervisedVotation
     */
    public function removeSupervisedVotation(\AppBundle\Entity\Subscription\Votation $supervisedVotation)
    {
        $this->supervisedVotations->removeElement($supervisedVotation);
    }

    /**
     * Get supervisedVotations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedVotations()
    {
        return $this->supervisedVotations;
    }

    /**
     * Get votation
     *
     * @return Votation
     */
    public function getVotation()
    {
        return $this->supervisedVotations->first();
    }

    /**
     * Add supervisedVotationSub
     *
     * @param \AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub
     *
     * @return Proposal
     */
    public function addSupervisedVotationSub(\AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub)
    {
        $this->supervisedVotationSubs[] = $supervisedVotationSub;

        return $this;
    }

    /**
     * Remove supervisedVotationSub
     *
     * @param \AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub
     */
    public function removeSupervisedVotationSub(\AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub)
    {
        $this->supervisedVotationSubs->removeElement($supervisedVotationSub);
    }

    /**
     * Get supervisedVotationSubs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedVotationSubs()
    {
        return $this->supervisedVotationSubs;
    }

    public function getWeight()
    {
        $result = 0;
        foreach ($this->supervisedVotationSubs as $sub) {
            $result += $sub->getWeight();
        }
        return $result;
    }
    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     *
     * @return Proposal
     */
    public function addComment(\AppBundle\Entity\Content\Comment $comment)
    {
        $this->comments[] = $comment;
        $comment->setProposal($this);

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Content\Comment $comment)
    {
        $this->comments->removeElement($comment);
        $comment->setProposal(null);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set NComments
     *
     * @param integer $NComments
     *
     * @return Proposal
     */
    public function setNComments($NComments)
    {
        $this->NComments = $NComments;

        return $this;
    }

    /**
     * Get NComments
     *
     * @return integer
     */
    public function getNComments()
    {
        return $this->NComments;
    }

    /**
     * Increment NComments
     *
     * @param integer $NComments
     *
     * @return Proposal
     */
    public function incNComments()
    {
        $this->NComments = $this->NComments + 1;

        return $this;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\Project\Section $section
     *
     * @return Proposal
     */
    public function setSection(\AppBundle\Entity\Project\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\Project\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Proposal
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set detailed
     *
     * @param boolean $detailed
     *
     * @return Proposal
     */
    public function setDetailed($detailed)
    {
        $this->detailed = $detailed;

        return $this;
    }

    /**
     * Get detailed
     *
     * @return boolean
     */
    public function getDetailed()
    {
        return $this->detailed;
    }
}
