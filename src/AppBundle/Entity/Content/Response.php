<?php
namespace AppBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="response")
 * @ORM\Entity()
 */
class Response
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $weight;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\PublicForm", inversedBy="responses")
     */
    private $publicForm;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\Answer", inversedBy="responses")
     */
    private $answer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Contribution", inversedBy="responses")
     */
    private $contribution;

    public function __construct()
    {
    }

    
    public function __toString()
    {
        return sprintf("%s", $this->getContent());
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Response
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Response
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set publicForm
     *
     * @param \AppBundle\Entity\Content\PublicForm $publicForm
     *
     * @return Response
     */
    public function setPublicForm(\AppBundle\Entity\Content\PublicForm $publicForm = null)
    {
        $this->publicForm = $publicForm;

        return $this;
    }

    /**
     * Get publicForm
     *
     * @return \AppBundle\Entity\Content\PublicForm
     */
    public function getPublicForm()
    {
        return $this->publicForm;
    }

    /**
     * Set answer
     *
     * @param \AppBundle\Entity\Content\Answer $answer
     *
     * @return Response
     */
    public function setAnswer(\AppBundle\Entity\Content\Answer $answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return \AppBundle\Entity\Content\Answer
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set contribution
     *
     * @param \AppBundle\Entity\Project\Contribution $contribution
     *
     * @return Response
     */
    public function setContribution(\AppBundle\Entity\Project\Contribution $contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution
     *
     * @return \AppBundle\Entity\Project\Contribution
     */
    public function getContribution()
    {
        return $this->contribution;
    }

}
