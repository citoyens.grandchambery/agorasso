<?php
namespace AppBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Location\Town;
use AppBundle\Entity\Project\Contribution;
use JMS\Serializer\Annotation as Serializer;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="socialData")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Content\SocialDataRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class SocialData
{
    const GENDER_FEMALE = 'Femme';
    const GENDER_MALE = 'Homme';

    public static $genders = array(
        self::GENDER_FEMALE,
        self::GENDER_MALE
    );
    public static $gendersChoices = array(
        self::GENDER_FEMALE => self::GENDER_FEMALE,
        self::GENDER_MALE => self::GENDER_MALE
    );

    const ACTIVITY_STUDENT = 'Étudiant-e';
    const ACTIVITY_RETIRED = 'Retraité-e';
    const ACTIVITY_SALARIED = 'Salarié-e';
    const ACTIVITY_UNEMPLOYED = 'Sans-emploi';
    const ACTIVITY_OTHER = 'Autre';

    public static $activities = array(
        self::ACTIVITY_STUDENT,
        self::ACTIVITY_RETIRED,
        self::ACTIVITY_SALARIED,
        self::ACTIVITY_UNEMPLOYED,
        self::ACTIVITY_OTHER
    );

    public static $activitiesChoices = array(
        self::ACTIVITY_STUDENT => self::ACTIVITY_STUDENT,
        self::ACTIVITY_RETIRED => self::ACTIVITY_RETIRED,
        self::ACTIVITY_SALARIED => self::ACTIVITY_SALARIED,
        self::ACTIVITY_UNEMPLOYED => self::ACTIVITY_UNEMPLOYED,
        self::ACTIVITY_OTHER => self::ACTIVITY_OTHER,
    );

   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=170)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="string", length=170)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    private $activity;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable = true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=255, nullable = true)
     * @Assert\Regex(
     *     pattern     = "/^[0-9]/",
     *     message = "Cette valeur n'est pas valide.",
     * )
     */
    protected $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    protected $city;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Town", inversedBy="socialDatas")
     * @ORM\JoinColumn(name="town_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email(
     *     message = "Cette adresse mail n'est pas valide."
     * )
     */
    protected $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer")
     */
    private $age;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Project\Contribution", mappedBy="socialData")
     */
    private $contribution;


    public function __construct()
    {
    }


    public function __toString()
    {
        return sprintf("%s - %s", $this->firstName, $this->lastName);
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return SocialData
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return SocialData
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SocialData
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return SocialData
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return SocialData
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return SocialData
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return SocialData
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return SocialData
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return SocialData
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return SocialData
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set town
     *
     * @param \AppBundle\Entity\Location\Town $town
     *
     * @return User
     */
    public function setTown(\AppBundle\Entity\Location\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \AppBundle\Entity\Location\Town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set contribution
     *
     * @param \AppBundle\Entity\Project\Contribution $contribution
     *
     * @return SocialData
     */
    public function setContribution(\AppBundle\Entity\Project\Contribution $contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution
     *
     * @return \AppBundle\Entity\Project\Contribution
     */
    public function getContribution()
    {
        return $this->contribution;
    }

}
