<?php
namespace AppBundle\Entity\Content;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Project\Contribution;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="comment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Content\CommentRepository")
 */
class Comment
{
    const STATUS_OPEN = 'Ouvert';
    const STATUS_CLOSED = 'Fermé';

    public static $statuses = array(
        self::STATUS_OPEN,
        self::STATUS_CLOSED
    );
    public static $statusesChoices = array(
        self::STATUS_OPEN => self::STATUS_OPEN,
        self::STATUS_CLOSED => self::STATUS_CLOSED
    );

   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text")
     *
     * @var string
     * @Assert\NotBlank(message="Commentaire")
     */
    private $description;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=170)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $closedReason;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\Proposal", inversedBy="comments")
     */
    private $proposal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Contribution", inversedBy="comments")
     */
    private $contribution;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Section", inversedBy="comments")
     */
    private $section;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\Comment", inversedBy="answers")
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Comment", mappedBy="comment", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $answers;


    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->setStatus(self::STATUS_OPEN);
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        return sprintf("%s", $this->getCreatedAt()->format('d/m/Y'));
    }
    
    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt; 
        } else {
            return $this->createdAt; 
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Comment
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Comment
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Comment
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Comment
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Comment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set closedReason
     *
     * @param string $closedReason
     *
     * @return Comment
     */
    public function setClosedReason($closedReason)
    {
        $this->closedReason = $closedReason;

        return $this;
    }

    /**
     * Get closedReason
     *
     * @return string
     */
    public function getClosedReason()
    {
        return $this->closedReason;
    }

    /**
     * Set proposal
     *
     * @param \AppBundle\Entity\Content\Proposal $proposal
     *
     * @return Comment
     */
    public function setProposal(\AppBundle\Entity\Content\Proposal $proposal = null)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return \AppBundle\Entity\Content\Proposal
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set contribution
     *
     * @param \AppBundle\Entity\Project\Contribution $contribution
     *
     * @return Comment
     */
    public function setContribution(\AppBundle\Entity\Project\Contribution $contribution = null)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution
     *
     * @return \AppBundle\Entity\Project\Contribution
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\Project\Section $section
     *
     * @return Comment
     */
    public function setSection(\AppBundle\Entity\Project\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\Project\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     *
     * @return Comment
     */
    public function setComment(\AppBundle\Entity\Content\Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \AppBundle\Entity\Content\Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add answer
     *
     * @param \AppBundle\Entity\Content\Comment $answer
     *
     * @return Comment
     */
    public function addAnswer(\AppBundle\Entity\Content\Comment $answer)
    {
        $this->answers[] = $answer;
        $answer->setComment($this);

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \AppBundle\Entity\Content\Comment $answer
     */
    public function removeAnswer(\AppBundle\Entity\Content\Comment $answer)
    {
        $this->answers->removeElement($answer);
        $answer->setComment(null);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function getLevel()
    {
        if($this->getProposal()) {
            return 0;
        } else {
            return 1 + $this->getComment()->getLevel();
        }
    }
}
