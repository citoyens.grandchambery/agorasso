<?php
namespace AppBundle\Entity\Time;

use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User as User;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="infos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Time\InfoRepository")
 * @Vich\Uploadable
 */
class Info
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="file", fileNameProperty="fileName", nullable=true)
     * @Assert\File(
     *     mimeTypes={
     *          "image/jpeg",
     *          "image/pjpeg", 
     *          "image/png", 
     *          "image/x-png", 
     *          "application/pdf", 
     *          "application/vnd.ms-excel",
     *          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
     *          "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
     *          "application/vnd.oasis.opendocument.spreadsheet",
     *          "application/vnd.oasis.opendocument.text",
     *          "application/msword",
     *          "application/vnd.ms-powerpoint",
     *          "application/vnd.openxmlformats-officedocument.presentationml.presentation",
     *          "application/vnd.ms-office",
     *          "text/rtf",
     *          "text/plain", 
     *          "application/zip"
     *     },
     *     maxSize="20M"
     * )
     *
     * @var File
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $fileName;
    
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     * @Assert\NotBlank(message="Titre de l'info")
     */
    private $title;

    /**
     * @ORM\Column(name="sub_title", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $subTitle;

    /**
     * @ORM\Column(name="content", type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Url
     * @var string
     */
    private $link;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group\Group", inversedBy="supervisedInfos")
     * )
     */
    private $referedGroups;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="attachedFiles")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * Ce champ est modifié à chaque fois pour lancer l'event listener de VichUploader
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $internalUpdatedAt;
    
     /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $deletedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="public", type="boolean", nullable=true)
     */
    private $public;

    public function __construct(User $member = null)
    {
        $this->createdBy = $member;
        $this->createdAt = new \Datetime();
        $this->public = false;
        $this->referedGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        return $this->title;
    }
    
    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt; 
        } else {
            return $this->createdAt; 
        }
    }
    
    public function getExtension()
    {
        $path_parts = pathinfo($this->fileName);
        return $path_parts['extension'];
        
    }
    
    public function getOriginalFileName()
    {
        if (strpos($this->fileName, '_')!==false){
            return substr($this->getFileName(), strpos($this->fileName, '_')+1);
        } else {
            return $this->fileName;
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'Info' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\Info $file
     */
    public function setFile(File $file = null)
    {
        $this->file = $file;
        
        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->internalUpdatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Info
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Info
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Info
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Info
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Info
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Info
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set internalUpdatedAt
     *
     * @param \DateTime $internalUpdatedAt
     *
     * @return Info
     */
    public function setInternalUpdatedAt($internalUpdatedAt)
    {
        $this->internalUpdatedAt = $internalUpdatedAt;

        return $this;
    }

    /**
     * Get internalUpdatedAt
     *
     * @return \DateTime
     */
    public function getInternalUpdatedAt()
    {
        return $this->internalUpdatedAt;
    }
    
    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return Info
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set subTitle
     *
     * @param string $subTitle
     *
     * @return Info
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Info
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Info
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
    /**
     * Add referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     *
     * @return Info
     */
    public function addReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups[] = $referedGroup;

        return $this;
    }

    /**
     * Remove referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     */
    public function removeReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups->removeElement($referedGroup);
    }

    /**
     * Get referedGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedGroups()
    {
        return $this->referedGroups;
    }

}
