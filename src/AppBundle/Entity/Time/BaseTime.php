<?php

namespace AppBundle\Entity\Time;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User as User;

/**
 * @ORM\Table(name="baseTime")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"base" = "BaseTime",
 *     "post" = "AppBundle\Entity\Site\Post",
 *     "votation" = "AppBundle\Entity\Subscription\Votation"})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Time\BaseTimeRepository")
 */
class BaseTime
{
    protected $discr = 'time';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="startTime", type="string", length=5, nullable=true)
     */
    private $startTime;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group\Group", inversedBy="supervisedTimes")
     * )
     */
    private $referedGroups;

    /**
     * Used to store attachedFiles
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document\UploadedFile", mappedBy="time", cascade={"persist", "remove"})
     * @ORM\OrderBy({"label" = "ASC"})
     */
    private $attachedFiles;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="attachedFiles")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->referedGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->attachedFiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        $date = $this->getDate();
        return sprintf("%s - %s", $date ? $date->format('d/m/Y') : '?', $this->getTitle());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return BaseTime
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return BaseTime
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Set content
     *
     * @param string $content
     *
     * @return BaseTime
     */
    public function setcontent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getcontent()
    {
        return $this->content;
    }

    /**
     * Add referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     *
     * @return BaseTime
     */
    public function addReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups[] = $referedGroup;

        return $this;
    }

    /**
     * Remove referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     */
    public function removeReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups->removeElement($referedGroup);
    }

    /**
     * Get referedGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedGroups()
    {
        return $this->referedGroups;
    }

    protected function clean($string) {
        // replace spaces and separators with hyphens
        $string = str_replace(array('_','~','\'', ' ', ',', '<', '>', '@', ':', ';', '\\', '/', '=', '*', '|'), '-', $string);
        $string = str_replace(array('é', 'è', 'ê'), 'e', $string);
        $string = str_replace(array('à'), 'a', $string);
        $string = str_replace(array('ù'), 'u', $string);

        // remove other special character
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->title;
        $name = str_replace(array('\'', ' ', ',', '(', ')', '<', '>', '@', ':', ';', '\\', '”', '/', '[', ']', '?', '=', '*', '|', '?'), '_', $name);

        return $name;
    }

    /**
     * Add attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     *
     * @return BaseTime
     */
    public function addAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles[] = $attachedFile;
        return $this;
    }

    /**
     * Remove attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     */
    public function removeAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles->removeElement($attachedFile);
    }

    /**
     * Get attachedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedFiles()
    {
        $attachedFiles = $this->attachedFiles->toArray();;
        usort($attachedFiles, function($a, $b) {
            return ($a->getLastUpdate() > $b->getLastUpdate()) ? -1 : 1;
        });
        return $attachedFiles;
    }
    
    /**
     * Set startTime
     *
     * @param string $startTime
     *
     * @return BaseTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return string
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BaseTime
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return BaseTime
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return BaseTime
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return BaseTime
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Is past
     *
     * @return boolean
     */
    public function isPast()
    {

        if($this->getDate()) {
            $now = new \DateTime();
            $now->setTime(0, 0, 0);
            return $this->getDate() < $now;
        }

        return false;
    }

    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt;
        } else {
            return $this->createdAt;
        }
    }

    public function getHtmlContent()
    {
        $content = $this->getcontent();
        $pos = strpos($content, '[...]');
        if($pos) {
            return substr($content, 0, $pos).substr($content, $pos+5, strlen($content));
        } else {
            return $content;
        }
    }

    public function getResume()
    {
        $pos = strpos($this->getcontent(), '[...]');
        if($pos) {
            return substr($this->getcontent(), 0, $pos + 5);
        } else {
            return sprintf("%s [...]", substr($this->getcontent(), 0, 255));
        }
    }

    /**
     * @return string
     */
    public function getDiscr()
    {
        return $this->discr;
    }
}
