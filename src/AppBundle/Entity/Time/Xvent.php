<?php

namespace AppBundle\Entity\Time;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Time\XventRepository")
 */
class Xvent
{    
    const TIMELINE_ALL = 'all';
    const TIMELINE_PAST = 'past';
    const TIMELINE_FUTURE = 'future';

    public static $timelines = array(
        self::TIMELINE_ALL,
        self::TIMELINE_PAST,
        self::TIMELINE_FUTURE,
    );
    public static $timelineChoices = array(
        'Tous' => self::TIMELINE_ALL,
        'Passés' => self::TIMELINE_PAST,
        'Futures' => self::TIMELINE_FUTURE
    );
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="hour", type="string", length=5, nullable=true)
     */
    private $hour;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="sub_title", type="string", length=255)
     */
    private $subTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var bool
     *
     * @ORM\Column(name="public", type="boolean", nullable=true)
     */
    private $public;

    /**
     * @var bool
     *
     * @ORM\Column(name="actionDate", type="boolean", nullable=true)
     */
    private $actionDate;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group\Group", inversedBy="supervisedEvents")
     * )
     */
    private $referedGroups;
    
    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=255, nullable=true)
     */
    private $place;

    /**
     * @var string
     *
     * @ORM\Column(name="poll", type="string", length=255, nullable=true)
     */
    private $poll;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->referedGroups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->attachedFiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        $date = $this->getDate();
        return sprintf("%s - %s", $date ? $date->format('d/m/Y') : '?', $this->getTitle());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Xvent
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Xvent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set subTitle
     *
     * @param string $subTitle
     *
     * @return Xvent
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;

        return $this;
    }

    /**
     * Get subTitle
     *
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Xvent
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Xvent
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }



    /**
     * Set public
     *
     * @param boolean $public
     *
     * @return Xvent
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set actionDate
     *
     * @param boolean $actionDate
     *
     * @return Xvent
     */
    public function setActionDate($actionDate)
    {
        $this->actionDate = $actionDate;

        return $this;
    }

    /**
     * Get actionDate
     *
     * @return boolean
     */
    public function getActionDate()
    {
        return $this->actionDate;
    }

    /**
     * Used to store attachedFiles
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document\UploadedFile", mappedBy="event", cascade={"persist", "remove"})
     * @ORM\OrderBy({"label" = "ASC"})
     */
    private $attachedFiles;



    /**
     * Add referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     *
     * @return Xvent
     */
    public function addReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups[] = $referedGroup;

        return $this;
    }

    /**
     * Remove referedGroup
     *
     * @param \AppBundle\Entity\Group\Group $referedGroup
     */
    public function removeReferedGroup(\AppBundle\Entity\Group\Group $referedGroup)
    {
        $this->referedGroups->removeElement($referedGroup);
    }

    /**
     * Get referedGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedGroups()
    {
        return $this->referedGroups;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->title;
        $name = str_replace(array('\'', ' ', ',', '(', ')', '<', '>', '@', ':', ';', '\\', '”', '/', '[', ']', '?', '=', '*', '|', '?'), '_', $name);
        return $name;
    }

    /**
     * Add attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     *
     * @return Xvent
     */
    public function addAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles[] = $attachedFile;
        return $this;
    }

    /**
     * Remove attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     */
    public function removeAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles->removeElement($attachedFile);
    }

    /**
     * Get attachedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedFiles()
    {
        $attachedFiles = $this->attachedFiles->toArray();;
        usort($attachedFiles, function($a, $b) {
            return ($a->getLastUpdate() > $b->getLastUpdate()) ? -1 : 1;
        });
        return $attachedFiles;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Xvent
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set poll
     *
     * @param string $poll
     *
     * @return Xvent
     */
    public function setPoll($poll)
    {
        $this->poll = $poll;

        return $this;
    }

    /**
     * Get poll
     *
     * @return string
     */
    public function getPoll()
    {
        return $this->poll;
    }

    /**
     * Set hour
     *
     * @param string $hour
     *
     * @return Xvent
     */
    public function setHour($hour)
    {
        $this->hour = $hour;

        return $this;
    }

    /**
     * Get hour
     *
     * @return string
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * Is past
     *
     * @return boolean
     */
    public function isPast()
    {

        if($this->getDate()) {
            $now = new \DateTime();
            $now->setTime(0, 0, 0);
            return $this->getDate() < $now;
        }

        return false;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Xvent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
