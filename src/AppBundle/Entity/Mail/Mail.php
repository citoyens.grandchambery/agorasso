<?php

namespace AppBundle\Entity\Mail;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User as User;
use AppBundle\Entity\Group\Group as Group;


/**
 * Mail
 *
 * @ORM\Table(name="mails")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "mail" = "Mail"
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MailRepository")
 */
class Mail
{
    const MAIL_TYPE_ROOT = 'Interne';
    const MAIL_TYPE_GROUP = 'Groupe';
    const MAIL_TYPE_CONTACT = 'Contact';
    const MAIL_TYPE_GROUP_SUBSCRIPTION = 'Inscription groupe';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="mail_to", type="array", nullable=true)
     * @Assert\All({
     *     @Assert\Email(message="Veuillez corriger l'adresse", strict=true),
     * })
     */
    private $to;

    /**
     * @var array
     *
     * @ORM\Column(name="cc", type="array", nullable=true)
     * @Assert\All({
     *     @Assert\Email(message="Veuillez corriger l'adresse", strict=true),
     * })
     */
    private $cc;

    /**
     * @var array
     *
     * @ORM\Column(name="bcc", type="array", nullable=true)
     * @Assert\All({
     *     @Assert\Email(message="Veuillez corriger l'adresse", strict=true),
     * })
     */
    private $bcc;

    /**
     * @var string
     *
     * @ORM\Column(name="mail_from", type="string", nullable=true)
     */
    private $from;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", nullable=true)
     * @Assert\NotBlank(message="Veuillez saisir le sujet de l'email !")
     */
    private $subject;

    /**
     * @var text
     *
     * @ORM\Column(name="body", type="text", nullable=true)
     * @Assert\NotBlank(message="Veuillez saisir le corps de l'email !")
     */
    private $body;

    /**
     * @var text
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sentAt", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", nullable=true)
     */
    private $result;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Group\Group", inversedBy="attachedFiles")
     */
    private $group;

    public function __construct(User $user = null, Group $group = null, $type = MAIL::MAIL_TYPE_ROOT)
    {
        $this->setType($type);
        $this->setCreatedBy($user);
        $this->setGroup($group);

        $this->createdAt = new \DateTime();
    }
    
    public function __toString()
    {
        return 'Email du ' . $this->date->format('d/m/Y') . ' Objet: ' . $this->subject;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set to
     *
     * @param array $to
     *
     * @return Mail
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * Get to
     *
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Set cc
     *
     * @param array $cc
     *
     * @return Mail
     */
    public function setCc($cc)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * Get cc
     *
     * @return array
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set bcc
     *
     * @param array $bcc
     *
     * @return Mail
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * Get bcc
     *
     * @return array
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set from
     *
     * @param string $from
     *
     * @return Mail
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get from
     *
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Mail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Mail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Mail
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Mail
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return Mail
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Mail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Mail
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return Mail
     */
    public function setGroup(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->group = $group;

        switch($this->getType()) {
            case Mail::MAIL_TYPE_GROUP:
                if($group) {
                    $users = array();
                    foreach ($group->getMembers() as $member) {
                        if ($member['active'] == true) {
                            $email = $member['user']->getEmail();
                            if($email)
                                $users[] = $email;
                        }
                    }
                    $this->setBcc($users);
                } else {
                    $this->setBcc(array());
                }
                break;
            default:
            case Mail::MAIL_TYPE_GROUP_SUBSCRIPTION:
            case Mail::MAIL_TYPE_CONTACT:
                if($group) {
                    $users = array();
                    foreach ($group->getReferents() as $member) {
                            $email = $member->getEmail();
                            if($email)
                                $users[] = $email;
                    }
                    $this->setTo($users);
                }
                break;
        }

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Mail
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        switch($this->getType()) {
            case Mail::MAIL_TYPE_GROUP:
            case Mail::MAIL_TYPE_CONTACT:
            case Mail::MAIL_TYPE_GROUP_SUBSCRIPTION:
                $this->setFrom($createdBy->getEmail());
                break;
        }

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
