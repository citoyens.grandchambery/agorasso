<?php

namespace AppBundle\Entity\Task;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User;
use AppBundle\Entity\Group\Group;

/**
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 */
class Task
{
    const STATUS_TO_BE_DONE = '0';
    const STATUS_DOING = '1';
    const STATUS_DONE = '2';

    const LABEL_STATUS_TO_BE_DONE = 'À faire';
    const LABEL_STATUS_DOING = 'En cours';
    const LABEL_STATUS_DONE = 'Fini';

    public static $statuses = array(
        self::STATUS_TO_BE_DONE,
        self::STATUS_DOING,
        self::STATUS_DONE,
    );
    public static $statusesChoices = array(
        self::LABEL_STATUS_TO_BE_DONE => self::STATUS_TO_BE_DONE,
        self::LABEL_STATUS_DOING => self::STATUS_DOING,
        self::LABEL_STATUS_DONE => self::STATUS_DONE,
    );

    public static $waitingStatus = [self::STATUS_TO_BE_DONE, self::STATUS_DOING];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=170)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=170)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Group\Group", inversedBy="tasks")
     */
    private $group;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="supervisedTasks")
     * )
     */
    private $referedUsers;


    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="attachedFiles")
     */
    protected $createdBy;

    /**
     * Constructor
     */
    public function __construct(User $member = null)
    {
        $this->createdBy = $member;
        $this->createdAt = new \Datetime();
        $this->setStatus(self::STATUS_TO_BE_DONE);
        $this->referedUsers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Task
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Task
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Task
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatusLabel()
    {
        switch($this->status) {
            case self::STATUS_TO_BE_DONE: return self::LABEL_STATUS_TO_BE_DONE;
            case self::STATUS_DOING: return self::LABEL_STATUS_DOING;
            case self::STATUS_DONE: return self::LABEL_STATUS_DONE;
        }
        return 'Inconnu';
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return Task
     */
    public function setGroup(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Add referedUser
     *
     * @param \AppBundle\Entity\User $referedUser
     *
     * @return Task
     */
    public function addReferedUser(\AppBundle\Entity\User $referedUser)
    {
        $this->referedUsers[] = $referedUser;

        return $this;
    }

    /**
     * Remove referedUser
     *
     * @param \AppBundle\Entity\User $referedUser
     */
    public function removeReferedUser(\AppBundle\Entity\User $referedUser)
    {
        $this->referedUsers->removeElement($referedUser);
    }

    /**
     * Get referedUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedUsers()
    {
        return $this->referedUsers;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Task
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Task
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
