<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="town")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Location\TownRepository")
 */
class Town
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="inseeCode", type="string", length=5, nullable=true)
     */
    private $inseeCode;

    /**
     * @var string
     *
     * @ORM\Column(name="gentile", type="string", length=255, nullable=true)
     */
    private $gentile;

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="decimal", precision=9, scale=2, nullable=true)
     */
    private $area;

    /**
     * @var integer
     *
     * @ORM\Column(name="population", type="integer", nullable=true)
     */
    private $population;

    /**
     * @var integer
     *
     * @ORM\Column(name="populationYear", type="integer", nullable=true)
     */
    private $populationYear;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Location\District", mappedBy="town")
     * )
     */
    private $districts;

    /**
     * @var bool
     *
     * @ORM\Column(name="other", type="boolean", options={"default" = false},nullable=true)
     */
    private $other;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="town")
     * )
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\SocialData", mappedBy="town")
     * )
     */
    private $socialDatas;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group\Group", mappedBy="referedTowns")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $supervisedGroups;
    

    public function __construct()
    {
        $this->districts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->socialDatas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Town
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set inseeCode
     *
     * @param string $inseeCode
     *
     * @return Town
     */
    public function setInseeCode($inseeCode)
    {
        $this->inseeCode = $inseeCode;

        return $this;
    }

    /**
     * Get inseeCode
     *
     * @return string
     */
    public function getInseeCode()
    {
        return $this->inseeCode;
    }

    /**
     * Set gentile
     *
     * @param string $gentile
     *
     * @return Town
     */
    public function setGentile($gentile)
    {
        $this->gentile = $gentile;

        return $this;
    }

    /**
     * Get gentile
     *
     * @return string
     */
    public function getGentile()
    {
        return $this->gentile;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return Town
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set population
     *
     * @param string $population
     *
     * @return Town
     */
    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }

    /**
     * Get population
     *
     * @return string
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Set populationYear
     *
     * @param string $populationYear
     *
     * @return Town
     */
    public function setPopulationYear($populationYear)
    {
        $this->populationYear = $populationYear;

        return $this;
    }

    /**
     * Get populationYear
     *
     * @return string
     */
    public function getPopulationYear()
    {
        return $this->populationYear;
    }

    /**
     * Add districts
     *
     * @param \AppBundle\Entity\Location\District $district
     *
     * @return Town
     */
    public function addDistricts(\AppBundle\Entity\Location\District $district)
    {
        $this->districts[] = $district;

        return $this;
    }

    /**
     * Remove district
     *
     * @param \AppBundle\Entity\Location\District $district
     */
    public function removeDistricts(\AppBundle\Entity\Location\District $district)
    {
        $this->districts->removeElement($district);
    }

    /**
     * Get districts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDistricts()
    {
        return $this->districts;
    }

    public function hasDistrict()
    {
        return count($this->getDistricts()) > 0;
    }

    /**
     * Set other
     *
     * @param boolean $other
     *
     * @return Town
     */
    public function setOther($other)
    {
        $this->other = $other;

        return $this;
    }

    /**
     * Get other
     *
     * @return boolean
     */
    public function getOther()
    {
        return $this->other;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Town
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        $result = new ArrayCollection();
        foreach ($this->users as $user) {
            $result[] = $user;
        }
        return $result;
    }

    public function setUsers($users)
    {
        $this->users = new ArrayCollection($users);
    }

    /**
     * Add socialData
     *
     * @param \AppBundle\Entity\Content\SocialData $socialData
     *
     * @return Town
     */
    public function addSocialData(\AppBundle\Entity\Content\SocialData $socialData)
    {
        $this->socialDatas[] = $socialData;
        return $this;
    }

    /**
     * Remove socialData
     *
     * @param \AppBundle\Entity\Content\SocialData $socialData
     */
    public function removeSocialData(\AppBundle\Entity\Content\SocialData $socialData)
    {
        $this->socialDatas->removeElement($socialData);
    }

    /**
     * Get socialDatas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSocialDatas()
    {
        $result = new ArrayCollection();
        foreach ($this->socialDatas as $socialData) {
            $result[] = $socialData;
        }
        return $result;
    }

    /**
     * Add supervisedGroup
     *
     * @param \AppBundle\Entity\Group\Group $supervisedGroup
     *
     * @return Town
     */
    public function addSupervisedGroup(\AppBundle\Entity\Group\Group $supervisedGroup)
    {
        $this->supervisedGroups[] = $supervisedGroup;

        return $this;
    }

    /**
     * Remove supervisedGroup
     *
     * @param \AppBundle\Entity\Group\Group $supervisedGroup
     */
    public function removeSupervisedGroup(\AppBundle\Entity\Group\Group $supervisedGroup)
    {
        $this->supervisedGroups->removeElement($supervisedGroup);
    }

    /**
     * Get supervisedGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedGroups()
    {
        return $this->supervisedGroups;
    }

}
