<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:52
 */

namespace AppBundle\Entity\Location;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="district")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Location\DistrictRepository")
 */
class District
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Location\Town", inversedBy="districts")
     * @ORM\JoinColumn(name="town_id", referencedColumnName="id", onDelete="SET NULL")
     * )
     */
    private $town;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="district")
     * )
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Group\Group", mappedBy="referedDistricts")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $supervisedGroups;
    

    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedGroups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return District
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    
    /**
     * Set town
     *
     * @param \AppBundle\Entity\Location\Town $town
     *
     * @return District
     */
    public function setTown(\AppBundle\Entity\Location\Town $town = null)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return \AppBundle\Entity\Location\Town
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return District
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;
        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        $result = new ArrayCollection();
        foreach ($this->users as $user) {
            $result[] = $user;
        }
        return $result;
    }

    /**
     * Add supervisedGroup
     *
     * @param \AppBundle\Entity\Group\Group $supervisedGroup
     *
     * @return District
     */
    public function addSupervisedGroup(\AppBundle\Entity\Group\Group $supervisedGroup)
    {
        $this->supervisedGroups[] = $supervisedGroup;

        return $this;
    }

    /**
     * Remove supervisedGroup
     *
     * @param \AppBundle\Entity\Group\Group $supervisedGroup
     */
    public function removeSupervisedGroup(\AppBundle\Entity\Group\Group $supervisedGroup)
    {
        $this->supervisedGroups->removeElement($supervisedGroup);
    }

    /**
     * Get supervisedGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedGroups()
    {
        return $this->supervisedGroups;
    }
}
