<?php

namespace AppBundle\Entity\Monetic;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User as User;

/**
 * @ORM\Table(name="payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Monetic\PaymentRepository")
 */
class Payment
{
    const STATUS_WAITING = 'En cours';
    const STATUS_PROCESSED = 'Réalisé';
    const STATUS_ERROR = 'Échec';
    
    public static $statuses = array(
        self::STATUS_WAITING,
        self::STATUS_PROCESSED,
        self::STATUS_ERROR,
    );
    public static $statusChoices = array(
        self::STATUS_WAITING => self::STATUS_WAITING,
        self::STATUS_PROCESSED => self::STATUS_PROCESSED
    );

    const ORIGIN_HELLO_ASSO = 'HelloAsso';
    const ORIGIN_MANUAL = 'Manuel';
    const ORIGIN_CCI = 'CCI';

    public static $originChoices = array(
        self::ORIGIN_HELLO_ASSO => self::ORIGIN_HELLO_ASSO,
        self::ORIGIN_MANUAL => self::ORIGIN_MANUAL
    );

    const TYPE_SUBSCRIPTION = 'Adhésion';
    const TYPE_DONATION = 'Don';

    public static $types = array(
        self::TYPE_SUBSCRIPTION,
        self::TYPE_DONATION
    );
    public static $typeChoices = array(
        self::TYPE_SUBSCRIPTION => self::TYPE_SUBSCRIPTION,
        self::TYPE_DONATION => self::TYPE_DONATION
    );

    const MODE_CARD = 'Carte bancaire';
    const MODE_CHECK = 'Chèque';
    const MODE_CASH = 'Espèce';

    public static $modes = array(
        self::MODE_CARD,
        self::MODE_CHECK,
        self::MODE_CASH
    );
    public static $modeChoices = array(
        self::MODE_CARD => self::MODE_CARD,
        self::MODE_CHECK => self::MODE_CHECK,
        self::MODE_CASH => self::MODE_CASH
    );
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", nullable=true)
     */
    private $reference;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @var \DateTime
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @Assert\Email(
     *     message = "Cette adresse mail n'est pas valide."
     * )
     */
    protected $email;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="payments")
     */
    protected $payer;

    /**
     * @var string
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @ORM\Column(name="amount", type="decimal", precision=9, scale=2)
     */
    private $amount;

    /**
     * @var string
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @ORM\Column(name="origin", type="string", length=255)
     */
    private $origin;

    /**
     * @var string
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @ORM\Column(name="mode", type="string", length=255)
     */
    private $mode;

    /**
     * @var string
     * @Assert\NotBlank(message="Ce champ est obligatoire !")
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="receipt", type="string", length=255, nullable=true)
     */
    private $receipt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
    }

    public function __toString()
    {
        $date = $this->getDate();
        return sprintf("%s - %s - %.f", $date ? $date->format('d/m/Y H:i:s') : '?', $this->getType(), $this->getAmount());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Payment
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set receipt
     *
     * @param string $receipt
     *
     * @return Payment
     */
    public function setReceipt($receipt)
    {
        $this->receipt = $receipt;

        return $this;
    }

    /**
     * Get receipt
     *
     * @return string
     */
    public function getReceipt()
    {
        return $this->receipt;
    }

    /**
     * Set origin
     *
     * @param string $origin
     *
     * @return Payment
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }
    
    /**
     * Set status
     *
     * @param string $status
     *
     * @return Payment
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return Payment
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Payment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Payment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set payer
     *
     * @param \AppBundle\Entity\User $payer
     *
     * @return Payment
     */
    public function setPayer(\AppBundle\Entity\User $payer = null)
    {
        $this->payer = $payer;
        if($payer) {
            $this->setEmail($this->payer->getEmail());
        }

        return $this;
    }

    /**
     * Get payer
     *
     * @return \AppBundle\Entity\User
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Subscription
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

}
