<?php

namespace AppBundle\Entity\AppParameter;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Parameter
 *
 * @ORM\Table("valueParameter")
 * @UniqueEntity(
 *     fields={"key"},
 *     message="Ce paramètre a déjà été utilisé."
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AppParameter\ValueParameterRepository")
 */
class ValueParameter
{
    const HELLO_ASSO_LAST_DATE = 'helloAssoLastDate';
    const HELLO_ASSO_LOGIN = 'helloAssoLogin';
    const HELLO_ASSO_PASSWORD = 'helloAssoPassword';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cle", type="string", length=255, nullable=false)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="groupe", type="string", length=255, nullable=true)
     */
    private $parameterGroup;

    /**
     * @ORM\Column(name="options", type="array", nullable=true)
     */
    private $options;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    public function __construct($key = null)
    {
        $this->key = $key;
        $this->options = [];
    }

    public function __toString()
    {
        return $this->label;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     * @return ValueParameter
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string 
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set label
     *
     * @param string $key
     * @return ValueParameter
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return ValueParameter
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set parameterGroup
     *
     * @param string $parameterGroup
     * @return ValueParameter
     */
    public function setParameterGroup($parameterGroup)
    {
        $this->parameterGroup = $parameterGroup;

        return $this;
    }

    /**
     * Get parameterGroup
     *
     * @return string 
     */
    public function getParameterGroup()
    {
        return $this->parameterGroup;
    }

    public function setOptions(array $options = array())
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getOption($key)
    {
        if (!isset($this->options[$key])) {
            return null;
        }

        return $this->options[$key];
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ValueParameter
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
