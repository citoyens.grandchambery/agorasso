<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 29/05/18
 * Time: 10:22
 */

namespace AppBundle\Entity\Group;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="group_subscription")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupSubscriptionRepository")
 */
class GroupSubscription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="referent", type="boolean", options={"default" = true})
     */
    private $referent;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", options={"default" = true})
     */
    private $active;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="groupSubscriptions")
     */
    protected $subscriber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group\Group", inversedBy="subscribers")
     */
    protected $group;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->referent = false;
    }


    public function __toString()
    {
        return sprintf("%s - %s", $this->group, $this->getCreatedAt()->format('d/m/Y'));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return GroupSubscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set referent
     *
     * @param boolean $referent
     *
     * @return GroupSubscription
     */
    public function setReferent($referent)
    {
        $this->referent = $referent;

        return $this;
    }

    /**
     * Get referent
     *
     * @return boolean
     */
    public function getReferent()
    {
        return $this->referent;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return GroupSubscription
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set subscriber
     *
     * @param \AppBundle\Entity\User $subscriber
     *
     * @return GroupSubscription
     */
    public function setSubscriber(\AppBundle\Entity\User $subscriber = null)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \AppBundle\Entity\User
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return GroupSubscription
     */
    public function setGroup(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getGroup()
    {
        return $this->group;
    }
}
