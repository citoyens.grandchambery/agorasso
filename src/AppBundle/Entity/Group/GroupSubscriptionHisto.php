<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 29/05/18
 * Time: 10:22
 */

namespace AppBundle\Entity\Group;


use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User as AppUser;

/**
 * @ORM\Table(name="group_subscription_histo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupSubscriptionHistoRepository")
 */
class GroupSubscriptionHisto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Datetime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="referent", type="boolean", options={"default" = true})
     */
    private $referent;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", options={"default" = true})
     */
    private $active;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"remove"})
     */
    protected $subscriber;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group\Group")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="citizen_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $citizen;

    public function __construct(GroupSubscription $sub, AppUser $citizen = null)
    {
        $this->createdAt = new \DateTime();
        $this->group = $sub->getGroup();
        $this->subscriber = $sub->getSubscriber();
        $this->referent = $sub->getReferent();
        $this->active = $sub->getActive();
        $this->citizen = $citizen;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return GroupSubscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set referent
     *
     * @param boolean $referent
     *
     * @return GroupSubscription
     */
    public function setReferent($referent)
    {
        $this->referent = $referent;

        return $this;
    }

    /**
     * Get referent
     *
     * @return boolean
     */
    public function getReferent()
    {
        return $this->referent;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return GroupSubscription
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set subscriber
     *
     * @param \AppBundle\Entity\User $subscriber
     *
     * @return GroupSubscription
     */
    public function setSubscriber(\AppBundle\Entity\User $subscriber = null)
    {
        $this->subscriber = $subscriber;

        return $this;
    }

    /**
     * Get subscriber
     *
     * @return \AppBundle\Entity\User
     */
    public function getSubscriber()
    {
        return $this->subscriber;
    }

    /**
     * Set citizen
     *
     * @param \AppBundle\Entity\User $citizen
     *
     * @return GroupSubscription
     */
    public function setCitizen(\AppBundle\Entity\User $citizen = null)
    {
        $this->citizen = $citizen;

        return $this;
    }

    /**
     * Get citizen
     *
     * @return \AppBundle\Entity\User
     */
    public function getCitizen()
    {
        return $this->citizen;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return GroupSubscription
     */
    public function setGroup(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getGroup()
    {
        return $this->group;
    }
}
