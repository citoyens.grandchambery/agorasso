<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 11/02/18
 * Time: 21:54
 */

namespace AppBundle\Entity\Group;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\User;
use AppBundle\Entity\Time\Info;
use AppBundle\Entity\Time\Xvent as Xvent;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="wgroup")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GroupRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Group
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="definition", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="mandat", type="text", nullable=true)
     */
    private $mandat;

    /**
     * @var string
     *
     * @ORM\Column(name="roadmap", type="text", nullable=true)
     */
    private $roadmap;

    /**
     * @var bool
     *
     * @ORM\Column(name="Actif", type="boolean", nullable=true)
     */
    private $active;

    /**
     * Used to store attachedFiles
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Document\UploadedFile", mappedBy="group", cascade={"persist", "remove"})
     * @ORM\OrderBy({"label" = "ASC"})
     */
    private $attachedFiles;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group\GroupSubscription", mappedBy="group", cascade={"persist", "remove"})
     * @ORM\OrderBy({"group" = "ASC"})
     */
    private $subscribers;

    /**
     * @ORM\ManyToMany(targetEntity="\AppBundle\Entity\Time\Xvent", mappedBy="referedGroups")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $supervisedEvents;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Time\BaseTime", mappedBy="referedGroups")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $supervisedTimes;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Group\Group", inversedBy="subGroups1")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Group\Group", inversedBy="subGroups2")
     */
    private $parent2;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group\Group", mappedBy="parent")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    private $subGroups1;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Group\Group", mappedBy="parent2")
     */
    private $subGroups2;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Task\Task", mappedBy="group")
     */
    private $tasks;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="info", type="boolean", nullable=true)
     */
    private $info;

    /**
     * @var bool
     *
     * @ORM\Column(name="task", type="boolean", nullable=true)
     */
    private $task;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="proposal", type="boolean", nullable=true)
     */
    private $proposal;

    /**
     * @var bool
     *
     * @ORM\Column(name="enableActionUser", type="boolean", nullable=true)
     */
    private $enableActionUser;

    /**
     * @var bool
     *
     * @ORM\Column(name="monetic", type="boolean", nullable=true)
     */
    private $monetic;

    /**
     * @var bool
     *
     * @ORM\Column(name="candidate", type="boolean", nullable=true)
     */
    private $candidate;

    /**
     * @var bool
     *
     * @ORM\Column(name="noSubscription", type="boolean", nullable=true)
     */
    private $noSubscription;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Time\Info", mappedBy="referedGroups")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $supervisedInfos;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Content\Proposal", mappedBy="referedGroups")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $supervisedProposals;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
     */
    private $actionUser;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Location\Town", inversedBy="supervisedGroups")
     * @ORM\OrderBy({"name" = "ASC"})
     * )
     */
    private $referedTowns;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Location\District", inversedBy="supervisedGroups")
     * @ORM\OrderBy({"name" = "ASC"})
     * )
     */
    private $referedDistricts;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Page", mappedBy="group")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    private $pages;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->active = true;
        $this->attachedFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedEvents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedInfos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedTimes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedProposals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subGroups1 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subGroups2 = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subscribers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tasks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referedTowns = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referedDistricts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Group
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        $name = $this->name;
        if(!$name)
            $name = $this->title;
        $name = str_replace(array('\'', ' ', ',', '(', ')', '<', '>', '@', ':', ';', '\\', '”', '/', '[', ']', '?', '=', '*', '|', '?'), '_', $name);
        return $name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Group
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mandat
     *
     * @param string $mandat
     *
     * @return Group
     */
    public function setMandat($mandat)
    {
        $this->mandat = $mandat;

        return $this;
    }

    /**
     * Get mandat
     *
     * @return string
     */
    public function getMandat()
    {
        return $this->mandat;
    }

    /**
     * Set roadmap
     *
     * @param string $roadmap
     *
     * @return Group
     */
    public function setRoadmap($roadmap)
    {
        $this->roadmap = $roadmap;

        return $this;
    }

    /**
     * Get roadmap
     *
     * @return string
     */
    public function getRoadmap()
    {
        return $this->roadmap;
    }
    
    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Group
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set info
     *
     * @param boolean $info
     *
     * @return Group
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return boolean
     */
    public function getInfo()
    {
        return $this->info;
    }
    
    /**
     * Set proposal
     *
     * @param boolean $proposal
     *
     * @return Group
         */
    public function setProposal($proposal)
    {
        $this->proposal = $proposal;

        return $this;
    }

    /**
     * Get proposal
     *
     * @return boolean
     */
    public function getProposal()
    {
        return $this->proposal;
    }

    /**
     * Set monetic
     *
     * @param boolean $monetic
     *
     * @return Group
     */
    public function setMonetic($monetic)
    {
        $this->monetic = $monetic;

        return $this;
    }

    /**
     * Get monetic
     *
     * @return boolean
     */
    public function getMonetic()
    {
        return $this->monetic;
    }

    /**
     * Set candidate
     *
     * @param boolean $candidate
     *
     * @return Group
     */
    public function setCandidate($candidate)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate
     *
     * @return boolean
     */
    public function getCandidate()
    {
        return $this->candidate;
    }

    /**
     * Set noSubscription
     *
     * @param boolean $noSubscription
     *
     * @return Group
     */
    public function setNoSubscription($noSubscription)
    {
        $this->noSubscription = $noSubscription;

        return $this;
    }

    /**
     * Get noSubscription
     *
     * @return boolean
     */
    public function getNoSubscription()
    {
        return $this->noSubscription;
    }

    /**
     * Set task
     *
     * @param boolean $task
     *
     * @return Group
     */
    public function setTask($task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return boolean
     */
    public function getTask()
    {
        return $this->task;
    }
    /**
     * Add attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     *
     * @return Group
     */
    public function addAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles[] = $attachedFile;
        return $this;
    }

    /**
     * Remove attachedFile
     *
     * @param \AppBundle\Entity\Document\UploadedFile $attachedFile
     */
    public function removeAttachedFile(\AppBundle\Entity\Document\UploadedFile $attachedFile)
    {
        $this->attachedFiles->removeElement($attachedFile);
    }

    /**
     * Get attachedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttachedFiles()
    {
        $attachedFiles = $this->attachedFiles->toArray();;
        usort($attachedFiles, function($a, $b) {
            return ($a->getLastUpdate() > $b->getLastUpdate()) ? -1 : 1;
        });
        return $attachedFiles;
    }

    /**
     * Add subscriber
     *
     * @param \AppBundle\Entity\Group\GroupSubscription $subscriber
     *
     * @return Group
     */
    public function addSubscriber(\AppBundle\Entity\Group\GroupSubscription $subscriber)
    {
        $this->subscribers[] = $subscriber;
        return $this;
    }

    /**
     * Remove subscriber
     *
     * @param \AppBundle\Entity\Group\GroupSubscription $subscriber
     */
    public function removeSubscriber(\AppBundle\Entity\Group\GroupSubscription $subscriber)
    {
        $this->subscribers->removeElement($subscriber);
    }

    /**
     * Get subscribers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscribers()
    {
        $subscribers = $this->subscribers->toArray();;
        return $subscribers;
    }

    protected function addMemberGroup($members, $referedGroup, $user)
    {
        foreach($members as $member) {
            if($member['user'] == $user) {
                return $members;
            }
        }
        $members->add(array('user' => $user, 'referent' => $user->isReferent($referedGroup), 'active' => true));
        return $members;
    }

    protected function addSubReferents($members, $referedGroup)
    {
        foreach ($this->getSubGroups() as $group) {
            foreach ($group->getReferents() as $user) {
                $members = $this->addMemberGroup($members, $referedGroup, $user);
            }
            $members = $group->addSubReferents($members, $referedGroup);
        }

        return $members;
    }

    protected function addActionUsers($members, $referedGroup)
    {
        if($this->getEnableActionUser() and $this->getActionUser())
            $members = $this->addMemberGroup($members, $referedGroup, $this->getActionUser());
        foreach ($this->getSubGroups() as $group) {
            $members = $group->addActionUsers($members, $referedGroup);
        }

        return $members;
    }

    protected function addTownContacts($members, $town)
    {
        foreach ($town->getUsers() as $user) {
            $members = $this->addMemberGroup($members, $this, $user);
        }
    }

    protected function addDistrictContacts($members, $district)
    {
        foreach ($district->getUsers() as $user) {
            $members = $this->addMemberGroup($members, $this, $user);
        }
    }
    
    /**
     * Get subReferents
     */
    public function getSubReferents()
    {
        $members = new ArrayCollection();


        return $this->addSubReferents($members, $this);
    }

    /**
     * Get members
     */
    public function getMembers()
    {
        $members = new ArrayCollection();
        foreach ($this->getSubscribers() as $subscriber) {

            // group referents will be added in addSubReferent
            if($subscriber->getGroup()->isCopil() and $subscriber->getSubscriber()->isReferent())
                continue;
            if($subscriber->getGroup()->getEnableActionUser() and $subscriber->getSubscriber()->isActionUser($subscriber->getGroup()))
                continue;
            if ($subscriber->getActive())
                $members = $this->addMemberGroup($members, $this, $subscriber->getSubscriber());
        }

        // Copil has a special subscription: are always subscribed referent of groups
        if ($this->isCopil()) {
            $members = $this->addSubReferents($members, $this);
        }

        if ($this->getEnableActionUser()) {
            $members = $this->addActionUsers($members, $this);
        }

        foreach ($this->getReferedTowns() as $town) {
            $this->addTownContacts($members, $town);
        }

        foreach ($this->getReferedDistricts() as $district) {
            $this->addDistrictContacts($members, $district);
        }

        return $members;
    }

    /**
     * Get referents
     */
    public function getReferents()
    {
        $referents = new ArrayCollection();
        foreach ($this->getSubscribers() as $subscriber)
        {
            if($subscriber->getActive() and $subscriber->getReferent())
                $referents->add($subscriber->getSubscriber());
        }

        return $referents;
    }
    
    /**
     * Add subGroup1
     *
     * @param \AppBundle\Entity\Group\Group $subGroup
     *
     * @return Group
     */
    public function addSubGroup1(\AppBundle\Entity\Group\Group $subGroup)
    {
        $this->subGroups1[] = $subGroup;
        return $this;
    }

    /**
     * Remove subGroup1
     *
     * @param \AppBundle\Entity\Group\Group $subGroup
     */
    public function removeSubGroup1(\AppBundle\Entity\Group\Group $subGroup)
    {
        $this->subGroups1->removeElement($subGroup);
    }

    /**
     * Get subGroups1
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups1()
    {
        return $this->subGroups1;
    }

    /**
     * Add subGroup2
     *
     * @param \AppBundle\Entity\Group\Group $subGroup
     *
     * @return Group
     */
    public function addSubGroup2(\AppBundle\Entity\Group\Group $subGroup)
    {
        $this->subGroups2[] = $subGroup;
        return $this;
    }

    /**
     * Remove subGroup2
     *
     * @param \AppBundle\Entity\Group\Group $subGroup
     */
    public function removeSubGroup2(\AppBundle\Entity\Group\Group $subGroup)
    {
        $this->subGroups2->removeElement($subGroup);
    }

    /**
     * Get subGroups2
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups2()
    {
        return $this->subGroups2;
    }


    /**
     * Get subGroups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubGroups()
    {
        if($this->subGroups1)
            $subGroups = $this->subGroups1->toArray();
        else {
            $subGroups = array();
        }

        if($this->subGroups2) {
            foreach ($this->subGroups2->toArray() as $group) {
                $subGroups[] = $group;
            }
        }

        return $subGroups;
    }
        /**
     * Add task
     *
     * @param \AppBundle\Entity\Task\Task $task
     *
     * @return Task
     */
    public function addTask(\AppBundle\Entity\Task\Task $task)
    {
        $this->tasks[] = $task;
        return $this;
    }

    /**
     * Remove task
     *
     * @param \AppBundle\Entity\Task\Task $task
     */
    public function removeTask(\AppBundle\Entity\Task\Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTasks()
    {
        $tasks = $this->tasks->toArray();;
        return $tasks;
    }


    /**
     * Add supervisedEvent
     *
     * @param \AppBundle\Entity\Time\Xvent $supervisedEvent
     *
     * @return Group
     */
    public function addSupervisedEvent(Xvent $supervisedEvent)
    {
        $this->supervisedEvents[] = $supervisedEvent;

        return $this;
    }

    /**
     * Remove supervisedEvent
     *
     * @param \AppBundle\Entity\Time\Xvent $supervisedEvent
     */
    public function removeSupervisedEvent(Xvent $supervisedEvent)
    {
        $this->supervisedEvents->removeElement($supervisedEvent);
    }

    /**
     * Get supervisedEvents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedEvents()
    {
        return $this->supervisedEvents;
    }

    /**
     * Add supervisedTime
     *
     * @param \AppBundle\Entity\Time\BaseTime $supervisedTime
     *
     * @return Group
     */
    public function addSupervisedTime(\AppBundle\Entity\Time\BaseTime $supervisedTime)
    {
        $this->supervisedTimes[] = $supervisedTime;

        return $this;
    }

    /**
     * Remove supervisedTime
     *
     * @param \AppBundle\Entity\Time\BaseTime $supervisedTime
     */
    public function removeSupervisedTime(\AppBundle\Entity\Time\BaseTime $supervisedTime)
    {
        $this->supervisedTimes->removeElement($supervisedTime);
    }

    /**
     * Get supervisedTimes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedTimes()
    {
        return $this->supervisedTimes;
    }

    /**
     * Add supervisedInfo
     *
     * @param \AppBundle\Entity\Time\Info $supervisedInfo
     *
     * @return Group
     */
    public function addSupervisedInfo(\AppBundle\Entity\Time\Info $supervisedInfo)
    {
        $this->supervisedInfos[] = $supervisedInfo;

        return $this;
    }

    /**
     * Remove supervisedInfo
     *
     * @param \AppBundle\Entity\Time\Info $supervisedInfo
     */
    public function removeSupervisedInfo(\AppBundle\Entity\Time\Info $supervisedInfo)
    {
        $this->supervisedInfos->removeElement($supervisedInfo);
    }

    /**
     * Get supervisedInfos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedInfos()
    {
        return $this->supervisedInfos;
    }

    /**
     * Add supervisedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $supervisedProposal
     *
     * @return Group
     */
    public function addSupervisedProposal(\AppBundle\Entity\Content\Proposal $supervisedProposal)
    {
        $this->supervisedProposals[] = $supervisedProposal;

        return $this;
    }

    /**
     * Remove supervisedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $supervisedProposal
     */
    public function removeSupervisedProposal(\AppBundle\Entity\Content\Proposal $supervisedProposal)
    {
        $this->supervisedProposals->removeElement($supervisedProposal);
    }

    /**
     * Get supervisedProposals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedProposals()
    {
        return $this->supervisedProposals;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Group
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color ? : 'yellow';
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Group
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }


    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return Group
     */
    public function setParent(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->parent = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set actionUser
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Group
     */
    public function setActionUser(\AppBundle\Entity\User $user = null)
    {
        $this->actionUser = $user;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\User
     */
    public function getActionUser()
    {
        return $this->actionUser;
    }

    /**
     * Set group
     *
     * @param \AppBundle\Entity\Group\Group $group
     *
     * @return Group
     */
    public function setParent2(\AppBundle\Entity\Group\Group $group = null)
    {
        $this->parent2 = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \AppBundle\Entity\Group\Group
     */
    public function getParent2()
    {
        return $this->parent2;
    }
    /**
     * is assemblee
     *
     * @return boolean
     */
    public function isAssemblee()
    {
        return $this->weight == 0;
    }

    /**
     * is copil
     *
     * @return boolean
     */
    public function isCopil()
    {
        return $this->weight == 1;
    }

    /**
     * Set enableActionUser
     *
     * @param boolean $enableActionUser
     *
     * @return Group
     */
    public function setEnableActionUser($enableActionUser)
    {
        $this->enableActionUser = $enableActionUser;

        return $this;
    }

    /**
     * Get enableActionUser
     *
     * @return boolean
     */
    public function getEnableActionUser()
    {
        return $this->enableActionUser;
    }

    public function isParent()
    {
        return count($this->getSubGroups()) > 0;
    }

    /**
     * Add referedTown
     *
     * @param \AppBundle\Entity\Location\Town $referedTown
     *
     * @return Group
     */
    public function addReferedTown(\AppBundle\Entity\Location\Town $referedTown)
    {
        $this->referedTowns[] = $referedTown;

        return $this;
    }

    /**
     * Remove referedTown
     *
     * @param \AppBundle\Entity\Location\Town $referedTown
     */
    public function removeReferedTown(\AppBundle\Entity\Location\Town $referedTown)
    {
        $this->referedTowns->removeElement($referedTown);
    }

    /**
     * Get referedTowns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedTowns()
    {
        return $this->referedTowns;
    }

    /**
     * Add referedDistrict
     *
     * @param \AppBundle\Entity\Location\District $referedDistrict
     *
     * @return Group
     */
    public function addReferedDistrict(\AppBundle\Entity\Location\District $referedDistrict)
    {
        $this->referedDistricts[] = $referedDistrict;

        return $this;
    }

    /**
     * Remove referedDistrict
     *
     * @param \AppBundle\Entity\Location\District $referedDistrict
     */
    public function removeReferedDistrict(\AppBundle\Entity\Location\District $referedDistrict)
    {
        $this->referedDistricts->removeElement($referedDistrict);
    }

    /**
     * Get referedDistricts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedDistricts()
    {
        return $this->referedDistricts;
    }

    /**
     * Add page
     *
     * @param \AppBundle\Entity\Site\Page $page
     *
     * @return Group
     */
    public function addPage(\AppBundle\Entity\Site\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page
     *
     * @param \AppBundle\Entity\Site\Page $page
     */
    public function removePage(\AppBundle\Entity\Site\Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

}
