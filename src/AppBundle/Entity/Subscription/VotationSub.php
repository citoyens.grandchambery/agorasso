<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 14/02/18
 * Time: 14:44
 */

namespace AppBundle\Entity\Subscription;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Subscription\VotationSubRepository")
 */
class VotationSub  extends \AppBundle\Entity\Subscription\BaseSubscription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Subscription\Votation", inversedBy="supervisedVotationSubs")
     * )
     */
    private $referedVotation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\Proposal", inversedBy="supervisedVotationSubs")
     * )
     */
    private $referedProposal;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return VotationSub
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set referedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $proposal
     *
     * @return VotationSub
     */
    public function setReferedProposal(\AppBundle\Entity\Content\Proposal $proposal = null)
    {
        $this->referedProposal = $proposal;

        return $this;
    }

    /**
     * Get referedProposal
     *
     * @return \AppBundle\Entity\Content\Proposal
     */
    public function getReferedProposal()
    {
        return $this->referedProposal;
    }

    /**
     * Set referedVotation
     *
     * @param \AppBundle\Entity\Subscription\Votation $votation
     *
     * @return VotationSub
     */
    public function setReferedVotation(\AppBundle\Entity\Subscription\Votation $votation = null)
    {
        $this->referedVotation = $votation;

        return $this;
    }

    /**
     * Get referedVotation
     *
     * @return \AppBundle\Entity\Subscription\Votation
     */
    public function getReferedVotation()
    {
        return $this->referedVotation;
    }
}
