<?php

namespace AppBundle\Entity\Subscription;

use AppBundle\Entity\Time\BaseTime;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Document\Image as Image;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Note
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Subscription\VotationRepository")
 */
class Votation extends BaseTime
{
    const STATUS_WAITING = 'À venir';
    const STATUS_OPEN = 'Ouvert';
    const STATUS_CLOSED = 'Fermé';

    protected $discr = 'votation';

    public static $statuses = array(
        self::STATUS_WAITING,
        self::STATUS_OPEN,
        self::STATUS_CLOSED
    );
    public static $statusesChoices = array(
        self::STATUS_WAITING => self::STATUS_WAITING,
        self::STATUS_OPEN => self::STATUS_OPEN,
        self::STATUS_CLOSED => self::STATUS_CLOSED
    );

    
    /**
     * @var \Date
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $beginTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $endTime;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $weight;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $maxWeight;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Content\Proposal", inversedBy="supervisedVotations")
     * )
     */
    private $referedProposals;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Subscription\VotationSub", mappedBy="referedVotation")
     * )
     */
    private $supervisedVotationSubs;

    /**
     * @var string
     *
     * @ORM\Column(name="help", type="text", nullable=true)
     */
    private $help;

    /**
     * @var bool
     *
     * @ORM\Column(name="publish", type="boolean", options={"default" = false})
     */
    private $publish;

    public function __construct()
    {
        parent::__construct();
        $this->referedProposals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->supervisedVotationSubs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set endDate
     *
     * @param \DateTime $date
     *
     * @return Votation
     */
    public function setEndDate($date)
    {
        $this->endDate = $date;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Votation
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set maxWeight
     *
     * @param integer $maxWeight
     *
     * @return Votation
     */
    public function setMaxWeight($maxWeight)
    {
        $this->maxWeight = $maxWeight;

        return $this;
    }

    /**
     * Get maxWeight
     *
     * @return integer
     */
    public function getMaxWeight()
    {
        return $this->maxWeight;
    }

    /**
     * Add referedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $referedProposal
     *
     * @return Votation
     */
    public function addReferedProposal(\AppBundle\Entity\Content\Proposal $referedProposal)
    {
        $this->referedProposals[] = $referedProposal;

        return $this;
    }

    /**
     * Remove referedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $referedProposal
     */
    public function removeReferedProposal(\AppBundle\Entity\Content\Proposal $referedProposal)
    {
        $this->referedProposals->removeElement($referedProposal);
    }

    /**
     * Get referedProposals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedProposals()
    {
        return $this->referedProposals;
    }

    /**
     * Add supervisedVotationSub
     *
     * @param \AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub
     *
     * @return Votation
     */
    public function addSupervisedVotationSub(\AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub)
    {
        $this->supervisedVotationSubs[] = $supervisedVotationSub;

        return $this;
    }

    /**
     * Remove supervisedVotationSub
     *
     * @param \AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub
     */
    public function removeSupervisedVotationSub(\AppBundle\Entity\Subscription\VotationSub $supervisedVotationSub)
    {
        $this->supervisedVotationSubs->removeElement($supervisedVotationSub);
    }

    /**
     * Get supervisedVotationSubs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedVotationSubs()
    {
        return $this->supervisedVotationSubs;
    }

    public function getStatus()
    {
        $now = new \DateTime();
//        $now->setTime(0, 0, 0);
        if($this->getBeginTime() > $now)
            return self::STATUS_WAITING;
        if($this->getEndTime() < $now)
            return self::STATUS_CLOSED;
        return self::STATUS_OPEN;
    }

    /**
     * Set help
     *
     * @param string $help
     *
     * @return BaseTime
     */
    public function sethelp($help)
    {
        $this->help = $help;

        return $this;
    }

    /**
     * Get help
     *
     * @return string
     */
    public function gethelp()
    {
        return $this->help;
    }

    public function getVotes()
    {
        $votes = 0;
        foreach ($this->supervisedVotationSubs as $sub) {
            $votes += $sub->getWeight();
        }
        return $votes;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     *
     * @return Votation
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set beginTime
     *
     * @param string $beginTime
     *
     * @return Votation
     */
    public function setBeginTime($beginTime)
    {
        $this->beginTime = $beginTime;

        return $this;
    }

    /**
     * Get beginTime
     *
     * @return string
     */
    public function getBeginTime()
    {
        return $this->beginTime;
    }
    /**
     * Set endTime
     *
     * @param string $endTime
     *
     * @return Votation
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return string
     */
    public function getEndTime()
    {
        return $this->endTime;
    } 
}