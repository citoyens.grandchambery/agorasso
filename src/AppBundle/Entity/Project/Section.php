<?php
namespace AppBundle\Entity\Project;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="section")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Project\SectionRepository")
 */
class Section
{
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\Column(type="text")
     *
     * @var string
     * @Assert\NotBlank(message="Proposition")
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordre", type="integer", nullable=true)
     */
    private $weight;


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $closedReason;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Comment", mappedBy="section", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $NComments;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Project", inversedBy="sections")
     */
    protected $project;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Section", inversedBy="subsections")
     */
    private $section;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project\Section", mappedBy="section", cascade={"persist"})
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $subsections;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Proposal", mappedBy="section", cascade={"persist"})
     * @ORM\OrderBy({"number" = "ASC"})
     */
    private $proposals;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setNComments(0);
        $this->subsections = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proposals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        if(strlen($this->getTitle() < 100))
            return $this->getTitle();
        else
            return sprintf('Proposition n°%d', $this->getId());
    }

    public function getId() {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     *
     * @return Section
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Section
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return Section
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }


    /**
     * Set closedReason
     *
     * @param string $closedReason
     *
     * @return Section
     */
    public function setClosedReason($closedReason)
    {
        $this->closedReason = $closedReason;

        return $this;
    }

    /**
     * Get closedReason
     *
     * @return string
     */
    public function getClosedReason()
    {
        return $this->closedReason;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     *
     * @return Section
     */
    public function addComment(\AppBundle\Entity\Content\Comment $comment)
    {
        $this->comments[] = $comment;
        $comment->setSection($this);

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Content\Comment $comment)
    {
        $this->comments->removeElement($comment);
        $comment->setSection(null);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set NComments
     *
     * @param integer $NComments
     *
     * @return Section
     */
    public function setNComments($NComments)
    {
        $this->NComments = $NComments;

        return $this;
    }

    /**
     * Get NComments
     *
     * @return integer
     */
    public function getNComments()
    {
        return $this->NComments;
    }

    /**
     * Increment NComments
     *
     * @param integer $NComments
     *
     * @return Section
     */
    public function incNComments()
    {
        $this->NComments = $this->NComments + 1;

        return $this;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project\Project $project
     *
     * @return Section
     */
    public function setProject(\AppBundle\Entity\Project\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set section
     *
     * @param \AppBundle\Entity\Project\Section $section
     *
     * @return Section
     */
    public function setSection(\AppBundle\Entity\Project\Section $section = null)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return \AppBundle\Entity\Project\Section
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Add subsection
     *
     * @param \AppBundle\Entity\Project\Section $subsection
     *
     * @return Section
     */
    public function addSubsection(\AppBundle\Entity\Project\Section $subsection)
    {
        $this->subsections[] = $subsection;
        $subsection->setSection($this);

        return $this;
    }

    /**
     * Remove subsection
     *
     * @param \AppBundle\Entity\Project\Section $subsection
     */
    public function removeSubsection(\AppBundle\Entity\Project\Section $subsection)
    {
        $this->subsections->removeElement($subsection);
        $subsection->setSection(null);
    }

    /**
     * Get subsections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubsections()
    {
        return $this->subsections;
    }

    /**
     * Add referedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $referedProposal
     *
     * @return Section
     */
    public function addProposal(\AppBundle\Entity\Content\Proposal $referedProposal)
    {
        $this->proposals[] = $referedProposal;

        return $this;
    }

    /**
     * Remove referedProposal
     *
     * @param \AppBundle\Entity\Content\Proposal $referedProposal
     */
    public function removeProposal(\AppBundle\Entity\Content\Proposal $referedProposal)
    {
        $this->proposals->removeElement($referedProposal);
    }

    /**
     * Get proposals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProposals()
    {
        return $this->proposals;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return Section
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }
}
