<?php

namespace AppBundle\Entity\Project;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Document\Image as Image;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Project\Section;

/**
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Project\ProjectRepository")
 */
class Project
{
    const STATUS_WAITING = 'À venir';
    const STATUS_OPEN = 'Ouvert';
    const STATUS_CLOSED = 'Fermé';
    
    public static $statuses = array(
        self::STATUS_WAITING,
        self::STATUS_OPEN,
        self::STATUS_CLOSED
    );
    public static $statusesChoices = array(
        self::STATUS_WAITING => self::STATUS_WAITING,
        self::STATUS_OPEN => self::STATUS_OPEN,
        self::STATUS_CLOSED => self::STATUS_CLOSED
    );

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Date
     *
     * @ORM\Column(name="beginDate", type="date")
     */
    private $beginDate;

    /**
     * @var \Date
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="shortDescription", type="text")
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Content\PublicForm", inversedBy="citizenProject")
     * @Assert\Valid
     */
    private $citizenForm;


    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project\Contribution", inversedBy="supervisedProjects")
     * )
     */
    private $referedContributions;
    

    /**
     * @var string
     *
     * @ORM\Column(name="help", type="text", nullable=true)
     */
    private $help;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project\Section", mappedBy="project"))
     */
    private $sections;
    

    public function __construct()
    {
        $this->referedContributions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->sections = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     *
     * @param string $shortDescription
     *
     * @return Project
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Group
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $date
     *
     * @return Project
     */
    public function setBeginDate($date)
    {
        $this->beginDate = $date;

        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }
    
    /**
     * Set endDate
     *
     * @param \DateTime $date
     *
     * @return Project
     */
    public function setEndDate($date)
    {
        $this->endDate = $date;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    
    /**
     * Add referedContribution
     *
     * @param \AppBundle\Entity\Project\Contribution $referedContribution
     *
     * @return Project
     */
    public function addReferedContribution(\AppBundle\Entity\Project\Contribution $referedContribution)
    {
        $this->referedContributions[] = $referedContribution;

        return $this;
    }

    /**
     * Remove referedContribution
     *
     * @param \AppBundle\Entity\Project\Contribution $referedContribution
     */
    public function removeReferedContribution(\AppBundle\Entity\Project\Contribution $referedContribution)
    {
        $this->referedContributions->removeElement($referedContribution);
    }

    /**
     * Get referedContributions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferedContributions()
    {
        return $this->referedContributions;
    }
    
    public function getStatus()
    {
        $now = new \DateTime();
        $now->setTime(0, 0, 0);
        if($this->getBeginDate() > $now)
            return self::STATUS_WAITING;
        if($this->getEndDate() < $now)
            return self::STATUS_CLOSED;
        return self::STATUS_OPEN;
    }

    /**
     * Set help
     *
     * @param string $help
     *
     * @return Project
     */
    public function sethelp($help)
    {
        $this->help = $help;

        return $this;
    }

    /**
     * Get help
     *
     * @return string
     */
    public function gethelp()
    {
        return $this->help;
    }

    /**
     * Set citizenForm
     *
     * @param \AppBundle\Entity\Content\PublicForm $citizenForm
     *
     * @return Project
     */
    public function setCitizenForm($citizenForm)
    {
        $this->citizenForm = $citizenForm;

        return $this;
    }

    /**
     * Get citizenForm
     *
     * @return \AppBundle\Entity\Content\PublicForm
     */
    public function getCitizenForm()
    {
        return $this->citizenForm;
    }

    /**
     * Add section
     *
     * @param \AppBundle\Entity\Project\Section $section
     *
     * @return Project
     */
    public function addSection(\AppBundle\Entity\Project\Section $section)
    {
        $this->sections[] = $section;
        return $this;
    }

    /**
     * Remove section
     *
     * @param \AppBundle\Entity\Project\Section $section
     */
    public function removeSection(\AppBundle\Entity\Project\Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return \Doctrine\Common\Collections\Collection|\AppBundle\Entity\Project\Section[]
     */
    public function getSections()
    {
        return $this->sections;
    }


}