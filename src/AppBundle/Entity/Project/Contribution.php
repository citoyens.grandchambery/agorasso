<?php
namespace AppBundle\Entity\Project;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Content\Answer;
//use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="contribution")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Project\ContributionRepository")
 */
class Contribution
{
    const STATUS_OPEN = 'Création';
    const STATUS_QUESTIONS = 'Questions';
    const STATUS_SOCIALDATA = 'Données sociales';
    const STATUS_VALIDATION = 'Validation';
    const STATUS_CLASSIFICATION = 'Classification';
    const STATUS_ANALYSIS = 'Analyse';

    public static $statuses = array(
        self::STATUS_OPEN,
        self::STATUS_QUESTIONS,
        self::STATUS_SOCIALDATA,
        self::STATUS_VALIDATION,
        self::STATUS_CLASSIFICATION,
        self::STATUS_ANALYSIS
    );
    public static $statusesChoices = array(
        self::STATUS_OPEN => self::STATUS_OPEN,
        self::STATUS_QUESTIONS => self::STATUS_QUESTIONS,
        self::STATUS_SOCIALDATA => self::STATUS_SOCIALDATA,
        self::STATUS_VALIDATION => self::STATUS_VALIDATION,
        self::STATUS_CLASSIFICATION => self::STATUS_CLASSIFICATION,
        self::STATUS_ANALYSIS => self::STATUS_ANALYSIS,
    );

    const TYPE_CITIZEN = 'Citoyen-ne';
    const TYPE_INHABITANT = 'Habitant-e';
    const TYPE_LANDMARK = 'Repère';
    const TYPE_EVENT = 'Réunion';
    const TYPE_DOCUMENT = 'Document public';

    public static $types = array(
        self::TYPE_CITIZEN,
        self::TYPE_INHABITANT,
        self::TYPE_LANDMARK,
        self::TYPE_EVENT,
        self::TYPE_DOCUMENT,
    );

    public static $typesChoices = array(
        self::TYPE_CITIZEN => self::TYPE_CITIZEN,
        self::TYPE_INHABITANT => self::TYPE_INHABITANT,
        self::TYPE_LANDMARK => self::TYPE_LANDMARK,
        self::TYPE_EVENT => self::TYPE_EVENT,
        self::TYPE_DOCUMENT => self::TYPE_DOCUMENT,
    );

    public static $personsChoices = array(
        self::TYPE_CITIZEN => self::TYPE_CITIZEN,
        self::TYPE_INHABITANT => self::TYPE_INHABITANT,
        self::TYPE_LANDMARK => self::TYPE_LANDMARK,
    );

   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;
    

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project\Project", mappedBy="referedContributions")
     * )
     */
    private $supervisedProjects;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="attachedContributions")
     */
    protected $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=170)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $closedReason;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Comment", mappedBy="contribution", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "ASC"})
     */
    private $comments;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $NComments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Content\Response", mappedBy="contribution")
     * @ORM\OrderBy({"weight" = "ASC"})
     */
    private $responses;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project\Project")
     * @Assert\Valid
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Content\PublicForm")
     * @Assert\Valid
     */
    private $publicForm;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Content\SocialData", inversedBy="contribution")
     * @Assert\Valid
     */
    private $socialData;


    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->supervisedProjects = new \Doctrine\Common\Collections\ArrayCollection();
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setStatus(self::STATUS_OPEN);
        $this->setNComments(0);
        $this->responses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    public function __toString()
    {
        if($this->getTitle()) {
            return sprintf('Contribution n°%d - %s', $this->getId(), $this->getTitle());
        } else {
            return sprintf('Contribution n°%d', $this->getId());
        }
    }
    
    public function getLastUpdate()
    {
        if ($this->updatedAt != null){
            return $this->updatedAt; 
        } else {
            return $this->createdAt; 
        }
    }

    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Contribution
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Contribution
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Contribution
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
  

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Contribution
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param \AppBundle\Entity\User $createdBy
     *
     * @return Contribution
     */
    public function setCreatedBy(\AppBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy
     *
     * @param \AppBundle\Entity\User $updatedBy
     *
     * @return Contribution
     */
    public function setUpdatedBy(\AppBundle\Entity\User $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
    
    /**
     * Set status
     *
     * @param string $status
     *
     * @return Contribution
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set closedReason
     *
     * @param string $closedReason
     *
     * @return Contribution
     */
    public function setClosedReason($closedReason)
    {
        $this->closedReason = $closedReason;

        return $this;
    }

    /**
     * Get closedReason
     *
     * @return string
     */
    public function getClosedReason()
    {
        return $this->closedReason;
    }

    /**
     * Add supervisedProject
     *
     * @param \AppBundle\Entity\Project\Project $supervisedProject
     *
     * @return Project
     */
    public function addSupervisedProject(\AppBundle\Entity\Project\Project $supervisedProject)
    {
        $this->supervisedProjects[] = $supervisedProject;

        return $this;
    }

    /**
     * Remove supervisedProject
     *
     * @param \AppBundle\Entity\Project\Project $supervisedProject
     */
    public function removeSupervisedProject(\AppBundle\Entity\Project\Project $supervisedProject)
    {
        $this->supervisedProjects->removeElement($supervisedProject);
    }

    /**
     * Get supervisedProjects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisedProjects()
    {
        return $this->supervisedProjects;
    }

    /**
     * Add comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     *
     * @return Contribution
     */
    public function addComment(\AppBundle\Entity\Content\Comment $comment)
    {
        $this->comments[] = $comment;
        $comment->setContribution($this);

        return $this;
    }

    /**
     * Remove comment
     *
     * @param \AppBundle\Entity\Content\Comment $comment
     */
    public function removeComment(\AppBundle\Entity\Content\Comment $comment)
    {
        $this->comments->removeElement($comment);
        $comment->setContribution(null);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set NComments
     *
     * @param integer $NComments
     *
     * @return Contribution
     */
    public function setNComments($NComments)
    {
        $this->NComments = $NComments;

        return $this;
    }

    /**
     * Get NComments
     *
     * @return integer
     */
    public function getNComments()
    {
        return $this->NComments;
    }

    /**
     * Increment NComments
     *
     * @param integer $NComments
     *
     * @return Contribution
     */
    public function incNComments()
    {
        $this->NComments = $this->NComments + 1;

        return $this;
    }

    /**
     * Add response
     *
     * @param \AppBundle\Entity\Content\Response $response
     *
     * @return Contribution
     */
    public function addResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses[] = $response;

        return $this;
    }

    /**
     * Remove response
     *
     * @param \AppBundle\Entity\Content\Response $response
     */
    public function removeResponse(\AppBundle\Entity\Content\Response $response)
    {
        $this->responses->removeElement($response);
    }

    /**
     * Get responses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * Set publicForm
     *
     * @param \AppBundle\Entity\Content\PublicForm $publicForm
     *
     * @return Contribution
     */
    public function setPublicForm($publicForm)
    {
        $this->publicForm = $publicForm;

        return $this;
    }

    /**
     * Get publicForm
     *
     * @return \AppBundle\Entity\Content\PublicForm
     */
    public function getPublicForm()
    {
        return $this->publicForm;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project\Project $project
     *
     * @return Contribution
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Get response
     *
     * @return \AppBundle\Entity\Content\Response
     */
    public function getResponse(Answer $answer)
    {
        foreach ($this->responses as $response) {
            if($response->getAnswer()->getId() == $answer->getId()) {
                return $response;
            }
        }
        return null;
    }

    /**
     * Set socialData
     *
     * @param \AppBundle\Entity\Content\SocialData $socialData
     *
     * @return Contribution
     */
    public function setSocialData($socialData)
    {
        $this->socialData = $socialData;

        return $this;
    }

    /**
     * Get socialData
     *
     * @return \AppBundle\Entity\Content\SocialData
     */
    public function getSocialData()
    {
        return $this->socialData;
    }

    public function isCurrent()
    {
        if($this->status == Contribution::STATUS_CLASSIFICATION or
            $this->status == Contribution::STATUS_ANALYSIS) {
            return false;
        }
        return true;
    }
}
